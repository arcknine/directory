<?php
session_start();
include("classes/connect.php");
include("classes/DBConnect.php");
include("classes/Util.php");

$title = "Dr Profile";
$auth_arr = $_SESSION['auth'];
include("include/top.php");
include("include/menu.php");

//===== ADD DOCTOR ===============//

?>
<script type="text/javascript">
	var count = 1;
	tinyMCE.init({
	mode : "textareas",
	forced_root_block : false,
   	force_br_newlines : true,
   	force_p_newlines : false,
   	theme : "advanced",
   	theme_advanced_layout_manager : "SimpleLayout",
   	theme_advanced_buttons1 : "bold,italic,underline,justifyleft,justifyfull,justifyright,bullist,numlist,indent,outdent,undo,redo,forecolor,sub,sup,fontsizeselect,code",
   	theme_advanced_buttons2 : "",
	theme_advanced_buttons3 : "",
	theme_advanced_font_sizes : "4,5,6,7",
	auto_resize : true,
	width: "700"
	});
	
</script>

<div id="profile">

	<p class=pageTitle> &nbsp;Profile</p>
	
	<?php
	$util = new Util();
	$db = new DBConnect();
	$edit_bool = false;
	$userid = $_SESSION['auth']['userid'];
	
	//=== ADD DOCTOR ==============================
	if(isset($_REQUEST['submit']) && $_REQUEST['submit'] ==  "Add")
	{
		if(!isset($_REQUEST['citouse']))
		{
			$_REQUEST['citouse'] = 1;
		}
		$sql = "insert into numbers (`id`,`name`,`license`,`ptr`,`s2`,`clinicinfo1`,`clinicinfo2`,`clinicinfo3`,`userid`,`citouse`) " .
				"values('','".$_REQUEST['name']."','".$_REQUEST['license']."','".$_REQUEST['ptr']."'" .
				",'".$_REQUEST['s2']."','".addslashes($_REQUEST['clinicinfo1'])."','".addslashes($_REQUEST['clinicinfo2'])."','".addslashes($_REQUEST['clinicinfo3'])."','$userid','".addslashes($_REQUEST['citouse'])."')";
		$db->insert($sql);
		echo $db->msg;
	}
	elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "del")
	{
	//=== DELETE DOCTOR ==============================
		$sql = "delete from numbers where id=".$_REQUEST['numid'];
		$db->delete($sql);
	}
	elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "edit")
	{
		$edit_bool = true;
	}
	elseif(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "Update")
	{
	//=== DELETE DOCTOR ==============================
		
		if(!isset($_REQUEST['citouse']))
		{
			$_REQUEST['citouse'] = 1;
		}

		$sql = "update numbers set name='".$_REQUEST['name']."',license='".$_REQUEST['license']."',".
				"ptr='".$_REQUEST['ptr']."',s2='".$_REQUEST['s2']."',clinicinfo1='".addslashes($_REQUEST['clinicinfo1'])."'
				,clinicinfo2='".addslashes($_REQUEST['clinicinfo2'])."',clinicinfo3='".addslashes($_REQUEST['clinicinfo3'])."',citouse='".addslashes($_REQUEST['citouse'])."' where id=".$_REQUEST['numid'];
		$db->update($sql);
		echo $db->msg;
	}
	if($edit_bool)
	{
		$sql = "select * from numbers where id=".$_REQUEST['numid'];
		$dum = $db->getRecord($sql);
		//print_r($dum);
			$ci1 = "";$ci2 = "";$ci3 = "";
			if($dum['citouse'] == 1 )
			{
				$ci1 = " checked ";
			}
			if($dum['citouse'] == 2 )
			{
				$ci2 = " checked ";
			}
			if($dum['citouse'] == 3 )
			{
				$ci3 = " checked ";
			}
		?>
		<form action=numbers.php?p=s method=post style=padding-top:20px>
		<input type=hidden name=numid value="<?php echo $dum['id']?>">
			<table cellpadding=5 cellspacing=5 width=75% class=profile>
				<tr><th colspan=2>Update Doctor Details</th></tr>
				<tr><td colspan=2><br></td></tr>
				<tr><td>Name</td><td><input type=text name=name size=100 value="<?php echo $dum['name']?>"></td></tr>
				<tr><td valign=top><input type=radio name=citouse <?php echo $ci1?> value=1>Clinic Info 1</td><td><textarea cols=50 rows=5 name=clinicinfo1><?php echo $dum['clinicinfo1']?></textarea></td></tr>
				<tr><td valign=top><input type=radio name=citouse <?php echo $ci2?> value=2>Clinic Info 2</td><td><textarea cols=50 rows=5 name=clinicinfo2><?php echo $dum['clinicinfo2']?></textarea></td></tr>
				<tr><td valign=top><input type=radio name=citouse <?php echo $ci3?> value=3>Clinic Info 3</td><td><textarea cols=50 rows=5 name=clinicinfo3><?php echo $dum['clinicinfo3']?></textarea></td></tr>
				<tr><td>License</td><td><input type=text name=license size=40 value="<?php echo $dum['license']?>"></td></tr>
				<tr><td>PTR</td><td><input type=text name=ptr size=40 value="<?php echo $dum['ptr']?>"></td></tr>
				<tr><td>S2</td><td><input type=text name=s2 size=40 value="<?php echo $dum['s2']?>"></td></tr>
				<tr><td colspan=2><br></td></tr>
				<tr><td colspan=2 align=right><input type=submit value=Cancel id=cancel> <input type=submit name=submit value=Update id=update></td></tr>
			</table>
		</form>
		<?php
	}
	else
	{	
		//==== VIEW ==========================================
		$sql = "select * from numbers where userid=$userid order by name";
		$dum = $db->getAllRecord($sql);
		$count = 0;
		$bf = "";
		foreach($dum as $val)
		{
			$ci1 = "";$ci2 = "";$ci3 = "";
			if($val['citouse'] == 1 )
			{
				$ci1 = " style=color:blue;font-weight:bold ";
			}
			if($val['citouse'] == 2 )
			{
				$ci2 = " style=color:blue;font-weight:bold ";
			}
			if($val['citouse'] == 3 )
			{
				$ci3 = " style=color:blue;font-weight:bold  ";
			}
			//echo "$ci1 $ci2 $ci3";
			$bf.="<tr><td class=ctr>".$val['name']."</td><td class=ctr>".$val['license']."</td><td class=ctr>".
				$val['ptr']."</td><td class=ctr>".$val['s2']."</td><td>".$util->setEdel("numbers.php?numid=".$val['id']."&do=edit","")."</td></tr></table>";
			$bf.="<table cellpadding=5 cellspacing=5 width=75% class=profile><tr><th>Clinics</th><th>Address</th></tr></tr>
			<tr><td valign=top $ci1>Clinic 1</td><td align=center>".$val['clinicinfo1']."</td></tr>";
			$bf.="<tr><td valign=top $ci2>Clinic 2</td><td align=center>".$val['clinicinfo2']."</td></tr>";
			$bf.="<tr><td valign=top $ci3>Clinic 3</td><td align=center>".$val['clinicinfo3']."</td></tr>";
				//"numbers.php?numid=".$val['id']."&do=del")."
			$bf.="</td></tr>";
			$count++;
		}
		if($bf)
		{
			echo "<table cellpadding=5 cellspacing=5 width=75% class=profile>";
			echo "<tr><th>Name</th><th>License No</th><th>PTR No</th><th>S2</th></tr>";
			echo $bf."</table>";
		}
		if($count < 1)
		{
		?>
		<form action=numbers.php?p=s method=post style=padding-top:20px>
		<table cellpadding=5 cellspacing=5 width=75% class=profile>
			<tr><th colspan=2>Doctor Details</th></tr>
			<tr><td>Name</td><td><input type=text name=name size=40></td></tr>
			<tr><td><input type=radio name=citouse value=1>Clinic 1</td><td><textarea cols=50 rows=5 name=clinicinfo1></textarea></td></tr>
			<tr><td><input type=radio name=citouse value=2>Clinic 2</td><td><textarea cols=50 rows=5 name=clinicinfo2></textarea></td></tr>
			<tr><td><input type=radio name=citouse value=3>Clinic 3</td><td><textarea cols=50 rows=5 name=clinicinfo3></textarea></td></tr>
			<tr><td>License</td><td><input type=text name=license size=40></td></tr>
			<tr><td>PTR</td><td><input type=text name=ptr size=40></td></tr>
			<tr><td>S2</td><td><input type=text name=s2 size=40></td></tr>
			<tr><td colspan=2><input type=submit name=submit value=Add></td></tr>
		</table>
		</form>
	<?php
		}
	}
	?>
	
<br><br><br>
<p align=center>Note: Clinic hours is printed as centered on the prescription print out.</p>

</div>
<div id='footer_page'>
<?php
include("include/bottom.php");
?>
</div>