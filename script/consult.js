<!-- extra spaces will be passed as single space -->
<!-- adding a + symbol will create errror in the javascript -->
<script language=javascript>
	function getMed(target)
	{
	
		if(target == "sel_brand")
		{
			genericid = document.medadd.sel_generic.selectedIndex;
	 		generic_val = document.medadd.sel_generic.options[genericid].text;
	 		document.getElementById('generic').value = generic_val;
	 		//alert(generic_val);
	 		if( generic_val != "Choose")
	 		{
			
			new Ajax.Request('ajax/consult_ajax.php?generic='+generic_val+'&do=generic', {
				method: 'get',
	  			onSuccess: function(response) {
	    			
	    			//alert("response brand "+response.responseText);
	    			
	    			var strArr = response.responseText.split("_");
	    			var selcount = strArr.length;
	    			document.medadd.sel_brand.length = selcount + 1;
	    			
	    			document.medadd.sel_brand.options[0].text = "Choose";
					document.medadd.sel_brand.options[0].value = "0";
	    			
	    			for(i = 0; i < selcount; i++)
	    			{
	    				y = i + 1;
	    				document.medadd.sel_brand.options[y].value = i;
	    				document.medadd.sel_brand.options[y].text = strArr[i]; 
	    			}
				  },
			 		onFailure: function(t) {
        			alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
    				}
				});
				
			}
		}
		
		if(target == "sel_dose")
		{
			//Choose dosage, based on the brand selected

			brandid = document.medadd.sel_brand.selectedIndex;
	 		brand_val = document.medadd.sel_brand.options[brandid].text;
	 		document.getElementById('brand').value = brand_val;
	 		if( brand_val != "Choose")
	 		{
			
				new Ajax.Request('ajax/consult_ajax.php?brand='+brand_val+'&do=brand', {
				method: 'get',
	  			onSuccess: function(response) {
	    			
	    			//alert("response dose "+response.responseText);
	    			
	    			var strArr = response.responseText.split("_");
	    			var selcount = strArr.length;
	    			document.medadd.sel_dose.length = selcount + 1;
	    			
	    			document.medadd.sel_dose.options[0].text = "Choose";
					document.medadd.sel_dose.options[0].value = "0";
	    			
	    			for(i = 0; i < selcount; i++)
	    			{
	    				y = i + 1;
	    				document.medadd.sel_dose.options[y].value = i;
	    				document.medadd.sel_dose.options[y].text = strArr[i]; 
	    			}
				  },
			 		onFailure: function(t) {
        			alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
    				}
				});
				
			
			}
		}
	
	}


</script>