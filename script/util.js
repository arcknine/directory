
function chooseType(list,field)
{
	var mylist=document.getElementById(list);
	//alert(mylist.selectedIndex);
	if(mylist.selectedIndex == 0)
	{
			document.getElementById(field).value='';
	}
	else
	{
		document.getElementById(field).value=mylist.options[mylist.selectedIndex].text
	}
}

function chooseTypeMore(list,field)
{
	var mylist=document.getElementById(list);
	//alert(mylist.selectedIndex);
	if(mylist.selectedIndex == 0)
	{
			document.getElementById(field).value='';
	}
	else
	{
		if(document.getElementById(field).value != '')
		{
			document.getElementById(field).value= document.getElementById(field).value+"; "+mylist.options[mylist.selectedIndex].text
		}
		else
		{
			document.getElementById(field).value= mylist.options[mylist.selectedIndex].text
		}
	}
}

function confirmDelete(delUrl,name_cat) {
      if (confirm("Are you sure you want to remove this item?")) {
        document.location = delUrl;
      }
    }


function checkEmpty(field)
{
	if(document.getElementById(field).value == "")
	{
		alert("Field "+field+" is empty!.");
		return false;
	}
	return true;
}

function popimage(url, caption, w, h){
	    day = new Date();
	    id = day.getTime();
	    
	    var iwidth = screen.width/2;
	    var iheight = screen.height/2;
	    
	    if(iwidth >= w)
	    {
	    		//newwin.resizeBy(w,h);  
	    		iwidth=w;
	    		iheight=h;
	    		//alert(iwidth+" "+iheight);
	    }
	    else 
	    {
	    		//alert(iwidth+ "  "+iwidth*(h/w));
	    		iheight=iwidth*(h/w);
	    		iwidth = iwidth;
	    		//newwin.resizeBy(iwidth,iwidth*(h/w));
	    		
	    }
	    var nw = parseInt(iwidth) + 100;
	    var nh = parseInt(iheight) + 100;
	    //alert(nw+" "+nh);
	    var newwin = window.open("", "_blank","menubar=0,scrollbars=1,resizable,width="+nw+",height="+nh+"");
	    //var newwin = window.open("", "_blank","menubar=0,scrollbars=1,resizable");
	    
	    
	    
	    newwin.document.open("text/html");
	    newwin.document.writeln("<html><head><title>"+caption+"</title></head><body>");
	    newwin.document.writeln("<p align=center style=font-family:arial;font-weight:bold;font-size:10pt>"+caption);
	    newwin.document.writeln("<p align=center><img src="+url+" width="+iwidth+"></body></html>");
	    newwin.document.close();
	    
  }
  
  
  function showhide(rowID){

        s = 'document.getElementById('+rowID+').style.display';
        sShow = 'document.getElementById('+rowID+').style.display=""';
        sHide = 'document.getElementById('+rowID+').style.display="none"';
        if(eval(s) == 'none'){
            eval(sShow);
        }else{
            eval(sHide);
        }
        return false;
    }
  /*
   * for textarea with tinymce
   */
  function addText(list,target)
  {
  	var mylist=document.getElementById(list);
  	var value = mylist.options[mylist.selectedIndex].value;
	if(tinyMCE.get(target).getContent() != '')
	{
		tinyMCE.get(target).setContent(tinyMCE.get(target).getContent()+"<br>"+value);
	}
	else
	{
		tinyMCE.get(target).setContent(value);
	}
  }