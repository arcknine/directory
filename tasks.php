<?php
$title = "Tasks";
include("classes/connect.php");
include("classes/DBConnect.php");
include("classes/Util.php");

include("include/top.php");
include("include/menu.php");

echo '<div id=task>';
$q = 0;
if(isset($_REQUEST['q']) && $_REQUEST['q'] > 0)
{
	$q = $_REQUEST['q'];
}
if($q == 1)
{
	include("task/alltask.php");
}
else
{
	include("task/index.php");
}
echo '</div>';
?>
<div id=footer_page>
<?php
include("include/bottom.php");
?>
</div>