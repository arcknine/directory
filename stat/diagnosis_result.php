<?php

$sql1 = ""; $sql2 = ""; $sql3 = "";
if(isset($_REQUEST['r']) && $_REQUEST['r'] == 1)
{
	$sql1 = isset($_SESSION['stat']['sql1']) ? $_SESSION['stat']['sql1']:"";
}
if(isset($_REQUEST['r']) && $_REQUEST['r'] == 2)
{
	$sql2 = isset($_SESSION['stat']['sql2']) ? $_SESSION['stat']['sql2']:"";
}
if(isset($_REQUEST['r']) && $_REQUEST['r'] == 3)
{
	$sql3 = isset($_SESSION['stat']['sql3']) ? $_SESSION['stat']['sql3']:"";
}

//echo $sql1."<br>";
//echo $sql2."<br>";
//echo $sql3."<br>";


$start = 0; $rowstoview = 20;
if (isset ($_REQUEST['start']) && $_REQUEST['start'])
{
	$start = $_REQUEST['start'];
}

if (isset ($_REQUEST['rowstoview']) && $_REQUEST['rowstoview'])
{
	$rowstoview = $_REQUEST['rowstoview'];
}


if(isset($_REQUEST['r']) && $_REQUEST['r'] == 1)
{
	echo '<p class=pageTitle> &nbsp;Search Result from Consults</p>';
	$db->getAllRecord($sql1);
	$totalRows = $db->recordCount;
	
	$sql1.=" limit $start, $rowstoview";
	$result = $db->getAllRecord($sql1);


	$count = 1 + $start; $sb = "";
	if(count($result)>0)
	{
		$patient = new Patient();
		foreach($result as $row)
		{
			$data = $patient->getData($row['patientid']);
			$sb.="<tr><td class=tab_hdr>$count.</td>
				<td><a href='patient.php?pf=6&patientid=".$row['patientid']."'>".$data['firstname'].
				" ".$data['lastname']."</a></td><td>".$row['assessment']."</td></tr>";
			$count++;
		}
		if($sb)
		{
			echo "<table style=\"margin-left:auto;margin-right:auto\" class=diag_result cellpadding=5 cellspacing=5 width=25%;>
			<tr><th class=tab_ttl colspan=2>Patient</th><th>Assessment</th></tr>";
			echo "$sb</table>";
			echo  "<table style=\"margin-left:auto;margin-right:auto\" rules=rows cellpadding=2 cellspacing=2><tr><td>".
			$util->navi('stat.php?file=result&r=1',$start,$rowstoview,$totalRows,"image")."</td></tr></table>";
		}
	}
	else
	{
		echo "<p align=center>No result</p>";
	}

}
elseif(isset($_REQUEST['r']) && $_REQUEST['r'] == 2)
{
	echo '<p class=pageTitle> &nbsp;Search Result from Surgical History</p>';
	$db->getAllRecord($sql2);
	$totalRows = $db->recordCount;
	
	$sql1.=" limit $start, $rowstoview";
	$result = $db->getAllRecord($sql2);


	$count = 1 + $start; $sb = "";
	if(count($result)>0)
	{
		$patient = new Patient();
		foreach($result as $row)
		{
			$data = $patient->getData($row['patientid']);
			$sb.="<tr><td class=tab_hdr>$count.</td>
				<td><a href='patient.php?pf=11&patientid=".$row['patientid']."'>".$data['firstname'].
				" ".$data['lastname']."</a></td><td>".$row['shdiag']."</td></tr>";
			$count++;
		}
		if($sb)
		{
			echo "<table style=\"margin-left:auto;margin-right:auto\" class=diag_result cellpadding=5 cellspacing=5>
			<tr><th class=tab_ttl colspan=2>Patient</th><th>Diagnosis</th></tr>";
			echo "$sb</table>";
			echo  "<table style=\"margin-left:auto;margin-right:auto\" rules=rows cellpadding=2 cellspacing=2><tr><td>".
			$util->navi('stat.php?file=result&r=1',$start,$rowstoview,$totalRows,"image")."</td></tr></table>";
		}
	}
	else
	{
		echo "<p align=center>No result</p>";
	}

}
elseif(isset($_REQUEST['r']) && $_REQUEST['r'] == 3)
{
	echo '<p class=pageTitle>Search Result from Hospital Admission Report</p>';
	$db->getAllRecord($sql3);
	$totalRows = $db->recordCount;
	
	$sql1.=" limit $start, $rowstoview";
	$result = $db->getAllRecord($sql3);


	$count = 1 + $start; $sb = "";
	if(count($result)>0)
	{
		$patient = new Patient();
		foreach($result as $row)
		{
			$data = $patient->getData($row['patientid']);
			$sb.="<tr><td class=tab_hdr>$count.</td>
				<td><a href='patient.php?pf=13&patientid=".$row['patientid']."'>".$data['firstname'].
				" ".$data['lastname']."</a></td><td>".$row['diagnosis']."</td></tr>";
			$count++;
		}
		if($sb)
		{
			echo "<table style=\"margin-left:auto;margin-right:auto\" class=diag_result cellpadding=5 cellspacing=5>
			<tr><th class=tab_ttl colspan=2>Patient</th><th>Diagnosis</th></tr>";
			echo "$sb</table>";
			echo  "<table style=\"margin-left:auto;margin-right:auto\" rules=rows cellpadding=2 cellspacing=2><tr><td>".
			$util->navi('stat.php?file=result&r=1',$start,$rowstoview,$totalRows,"image")."</td></tr></table>";
		}
	}
	else
	{
		echo "<p align=center>No result</p>";
	}

}
