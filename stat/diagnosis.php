<p class=pageTitle> &nbsp;Search Assessment</p>

<?php
$search = ""; 
if(isset($_REQUEST['submit']) && $_REQUEST['submit'] != "Search")
{
	$search = $_SESSION['text'];
}
elseif(isset($_REQUEST['text']))
{
	$search = $_REQUEST['text'];
}

?>

<form action="stat.php" method="post">
<table width=600px style="margin-right:auto;margin-left:auto;">
	<tr>
		<td align=center><input type="text" name="text" size="50" value="<?php echo $search;?>"> <input type="submit"
			name="submit" value="Search" id=search> <a href=''
			onclick='showhide(1);return false;'>Search Help</a></td>
	</tr>
	<tr id=1 style="display: none">
		<td><!-- === SEARCH HELP -->
		<fieldset><legend>Search Help</legend>
		<table width=500px cellpadding=2 cellspacing=2 rules=rows align=center>
			<tr>
				<td>apple banana</td>
				<td>Find rows that contain at least one of the two words.</td>
			</tr>
			<tr>
				<td>+apple +juice</td>
				<td>Find rows that contain both words.</td>
			</tr>
			<tr>
				<td>+apple macintosh</td>
				<td>Find rows that contain the word "apple", but rank rows higher if
				they also contain "macintosh".</td>
			</tr>
			<tr>
				<td>+apple -macintosh</td>
				<td>Find rows that contain the word "apple" but not "macintosh".</td>
			</tr>
			<tr>
				<td>+apple ~macintosh</td>
				<td>Find rows that contain the word "apple", but if the row also
				contains the word "macintosh", rate it lower than if row does not.
				This is "softer" than a search for '+apple -macintosh', for which
				the presence of "macintosh" causes the row not to be returned at
				all.</td>
			</tr>
			<tr>
				<td>+apple +(&gt;turnover &lt;strudel)</td>
				<td>Find rows that contain the words "apple" and "turnover", or
				"apple" and "strudel" (in any order), but rank "apple turnover"
				higher than "apple strudel".</td>
			</tr>
			<tr>
				<td>apple*</td>
				<td>Find rows that contain words such as "apple", "apples",
				"applesauce", or "applet".</td>
			</tr>
			<tr>
				<td>"some words"</td>
				<td>Find rows that contain the exact phrase "some words" (for
				example, rows that contain "some words of wisdom" but not "some
				noise words").</td>
			</tr>
		</table>
		</fieldset>
		<!-- === END SEARCH HELP --></td>
	</tr>
</table>
</form>

<?php
$db = new DBConnect();
$util = new Util();
$search = "";


//==== NAVIGATION ====
$start = 0; $rowstoview = 10;
if (isset ($_REQUEST['start']) && $_REQUEST['start'])
{
	$start = $_REQUEST['start'];
}

if (isset ($_REQUEST['rowstoview']) && $_REQUEST['rowstoview'])
{
	$rowstoview = $_REQUEST['rowstoview'];
}
$searchUrl = "";
if(isset($_REQUEST['submit']) && $_REQUEST['submit'] != "Search")
{
	$_REQUEST['submit'] = "Search";
	$_REQUEST['text'] = $_SESSION['text'];
	$searchUrl = "&submit=Search&text=".$_REQUEST['text'];
}

if(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "Search")
{
	$search = $_REQUEST['text'];
	$_SESSION['text'] = $_REQUEST['text'];

	$sql1 = "select patientid,consult.* from consult  where ";
	$sql2 = "select * from sh where ";
	$sql3 = "select * from inpatient where ";
	if(strlen($search) < 4  )
	{
		$sql1.=" assessment like '" . $search . "%'";
		$sql2.=" shdiag like '" . $search . "%'";
		$sql3.=" diagnosis like '" . $search . "%'";
	}
	else
	{
		$sql1.=" match(assessment) against('".$search."' IN BOOLEAN MODE)";
		$sql2.=" match(shdiag) against('".$search."' IN BOOLEAN MODE)";
		$sql3.=" match(diagnosis) against('".$search."' IN BOOLEAN MODE)";

	}
	
	$sql1.="and  patientid in (select id from patient where userid=".$_SESSION['auth']['userid'].")"; 
	$sql2.=" and userid=".$_SESSION['auth']['userid'];
	$sql3.=" and userid=".$_SESSION['auth']['userid'];
	
	//echo $sql1.'<br>';
	
	$_SESSION['stat']['sql1'] = $sql1;
	$_SESSION['stat']['sql2'] = $sql2;
	$_SESSION['stat']['sql3'] = $sql3;
	$_SESSION['stat']['text'] = $_REQUEST['text'];
	
	$db->getAllRecord($sql1);
	echo '<p style="margin-left:auto;margin-right:auto" align=center>There are '.$db->recordCount.' in patients\' Consult. <a href=stat.php?file=result&r=1>Result</a></p>';
	//echo $sql2.'<br>';
	
	$db->getAllRecord($sql2);
	echo '<p style="margin-left:auto;margin-right:auto" align=center>There are '.$db->recordCount.' in patients\' Surgical History. <a href=stat.php?file=result&r=2>Result</a></p>';
	//echo $sql3;
	
	$db->getAllRecord($sql3);
	echo '<p style="margin-left:auto;margin-right:auto" align=center>There are '.$db->recordCount.' in patients\' Hospital Admission Report. <a href=stat.php?file=result&r=3>Result</a></p>';
	
	/*
	$searchUrl = "&submit=Search&text=".$_REQUEST['text'];
	
	$db->getAllRecord($sql1);
	$totalRows = $db->recordCount;
	
	$sql1.=" limit $start, $rowstoview";
	$result = $db->getAllRecord($sql1);

	
	$count = 1 + $start; $sb = "";
	if(count($result)>0)
	{
		$patient = new Patient();
		foreach($result as $row)
		{
			$data = $patient->getData($row['patientid']);
			$sb.="<tr><td class=tab_hdr>$count.</td>
				<td><a href='patient.php?pf=6&patientid=".$row['patientid']."'>".$data['firstname'].
				" ".$data['lastname']."</a></td><td>".$row['assessment']."</td></tr>";
			$count++;
		}
		if($sb)
		{
			echo "<table style=\"margin-left:auto;margin-right:auto\" rules=rows cellpadding=2 cellspacing=3>
			<tr><th class=tab_ttl colspan=2>Patient</th><th>Assessment</th></tr>";
			echo "$sb</table>";
			echo  "<table style=\"margin-left:auto;margin-right:auto\" rules=rows cellpadding=2 cellspacing=2><tr><td>".
			$util->navi("stat.php?$searchUrl",$start,$rowstoview,$totalRows,"image")."</td></tr></table>";
		}
	}
	else
	{
		echo "<p>No result</p>";
	}
	*/
}
else
{
	//echo "<p style=\"text-align:center;color:red\">Nothing to do. </p>";
}

?>