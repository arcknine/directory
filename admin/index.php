<p class=pageTitle4> &nbsp;Users' Profile</p>
<div id=usage>
<?php

	$db = new DBConnect();
	$util = new Util();
	$start = 0;
	if(isset($_REQUEST['start']))
	{
		$start = $_REQUEST['start'];
	}
	$rowstoview = 10;
	if(isset($_REQUEST['rowstoview']))
	{
		$rowstoview = $_REQUEST['rowstoview'];
	}
	
	/*
	 * === PERFORM FORM SUBMISSIONS
	 */
	 
	
	
	//==SQL STATEMENTS
	$searchTag = "";
	$recordCount = 0;
	if(isset($_REQUEST['do']) &&  $_REQUEST['do'] == "Search" && isset($_REQUEST['search']))
	{
		
		if(strlen($_REQUEST['search']) < 4)
		{
			$sql = "select * from users  where name like '".$_REQUEST['search']."%'";
		}
		else
		{
			$sql = "select * from users  where match(name) against('".$_REQUEST['search']."*' IN BOOLEAN MODE) ";
		}
		$sql.= "   and access='doctor' ";
		
		$db->getAllRecord($sql);
		$sql .= " order by name limit $start, $rowstoview";
		$searchTag = "&do=Search&search=".$_REQUEST['search'];
		
	}
	else
	{
		$sql = "select * from users where user!='admin'  and access='doctor' order by name limit $start, $rowstoview";
		$db->getAllRecord("select id from users  where user!='admin' and access='doctor'");
		
	}
	$recordCount = $db->recordCount;
	$result = $db->getAllRecord($sql);
	
	$count = $start + 1; 
	
	echo @$msg;
?>



<?php if($recordCount > 0){?>
	<form>
	<table cellpadding="5" cellspacing="5" class="view" width=100%>
		<tr><th colspan=2>Name</th><th></th><th></th></tr>
		
		<?php
		foreach($result as $arr)
		{
		 ?>
			<tr><td><?php echo $count?>.</td>
				<td align=center style="font-size:14px;">
					<?php echo $arr['name']?>
				</td>
				<td>
					<table>
						<tr><th style="font-size:16px; color:#f78f1e;">User Name / E-mail</th><td><?php echo $arr['user']?></td></tr>
						<tr><th style="font-size:16px; color:#f78f1e;">Specialty</th><td><?php echo $arr['specialty']?></td></tr>
						<tr><th style="font-size:16px; color:#f78f1e;">Date of First Login</th><td><?php echo $util->setDateTime($arr['dateoffirstlogin'],"M j, Y g:i A");?></td></tr>
						<tr><th style="font-size:16px; color:#f78f1e;">Date of Last Login</th><td><?php echo $util->setDateTime($arr['lastlogin'],"M j, Y g:i A");?></td></tr>
						<?php 
						/*
						 * NO of PATIENTS
						 */
						$count_patient = $db->getRecord("select count(id) from patient where userid=".$arr['id']);
						
						?>
						<tr><th style="font-size:16px; color:#f78f1e;">No. of Patients</th><td><?php echo $count_patient[0]; ?></td></tr>
					</table>
				</td>
				<td>
					<?php
					$sql2 = "select * from numbers where userid=".$arr['id'];
					$row = $db->getRecord($sql2);
					if(isset($row))
					{
						echo '<table class=view>';
						if(isset($row['clinicinfo1']) && $row['clinicinfo1'] != '')
						{
							echo '<tr><th valign=top style="font-size:16px; color:#f78f1e;">Clinic</th><td>'.$row['clinicinfo1'].'</td></tr>';
						}	
						if(isset($row['clinicinfo2'])  && $row['clinicinfo2'] != '')
						{
							echo '<tr><th valign=top style="font-size:16px; color:#f78f1e;">Clinic</th><td>'.$row['clinicinfo2'].'</td></tr>';
						}
						if(isset($row['clinicinfo3'])  && $row['clinicinfo3'] != '')
						{
							echo '<tr><th valign=top style="font-size:16px; color:#f78f1e;">Clinic</th><td>'.$row['clinicinfo3'].'</td></tr>';
						}
						if(isset($row['license'])  && $row['license'] != '')
						{
							echo '<tr><th valign=top style="font-size:16px; color:#f78f1e;">License</th><td>'.$row['license'].'</td></tr>';
						}
						if(isset($row['ptr'])  && $row['ptr'] != '')
						{
							echo '<tr><th valign=top style="font-size:16px; color:#f78f1e;">PTR No.</th><td>'.$row['ptr'].'</td></tr>';
						}
						if(isset($row['s2'])  && $row['s2'] != '')
						{
							echo '<tr><th valign=top style="font-size:16px; color:#f78f1e;">S2</th><td>'.$row['s2'].'</td></tr>';
						}
						
						echo '</table>';
					}
	
					?>
				</td>
			
			<td>
				<?php $count++;?>
			</td></tr>
		<?php } ?>
	</table>
	</form>
	<table style="margin-left:auto;margin-right:auto">
		<tr>
		<td colspan=2>
		<?php echo $util->navi("admin.php?".$searchTag,$start,$rowstoview,$recordCount,"image");?>
		</td>
		</tr>
	</table>

<?php } ?>
</div>
