<?php
session_start();
if($_REQUEST['patientid'] > 0)
{
	$patientid = $_REQUEST['patientid'];	
}
else
{
	header("location:list.php");
}

$auth_arr = $_SESSION['auth'];

/*
 * === REQUIRED CLASSES ===
 */
	include("classes/connect.php");
	include("classes/DBConnect.php");
	include("classes/Auth.php");
	include("classes/Util.php");
	include("classes/Patient.php");
	include("classes/Appt.php");
	include("classes/Consult.php");
	include("classes/Bill.php");

$util = new Util();


$patient = new Patient();
$data = $patient->getData($patientid);
$age_arr = $util->age($data['birthday'],"");


	if($auth_arr['access'] == "admin" or $auth_arr['access'] == "doctor")
	{
	$links = array("1"=>"Patient Page","2"=>"Appointments",
				"3"=>"Set Appointment","4"=>"Calendar",
				"5"=>"Consult","6"=>"Chart Summary","13"=>"Hospital Admission Report","11"=>"Surgical History",		
				"7"=>"History","8"=>"Statements","12"=>"Laboratory/Radiology Request Form","9"=>"Medical Certificate",
				"14"=>"Medical Clearance","15"=>"Referral Form");
	
	}
	else
	{
		$links = array("1"=>"Patient Page","2"=>"Appointments",
				"3"=>"Set Appointment","4"=>"Calendar","5"=>"Consult","8"=>"Statements"
				);
		
	}
	$pagetitle = "";
	
	if(isset($_REQUEST['pf']))
	{
		@ $title = $links[$_REQUEST['pf']];
		if(isset($_REQUEST['pf']) && $_REQUEST['pf'] == '10')
		{
			$title = "Report";
		}
	}
	else
	{
		$title = "Patient Page";
	}
	
/*
 * Include the html headers and the menu
 */
	include("include/top.php");	
	include("include/menu.php");
	
?>
<script language=javascript>
function geticd(list,target)
      {
      var mylist=document.getElementById(list);
      var icdid = mylist.options[mylist.selectedIndex].value;
     new Ajax.Request('ajax/icd_ajax.php', 
      {
      parameters:'icdid='+icdid,
      onSuccess: function(t){
      	//alert("response "+t.responseText);
      	if(icdid == 0)
		{
				tinyMCE.get(target).setContent('');
		}
		else
		{
			if(tinyMCE.get(target).getContent() != '')
			{
				tinyMCE.get(target).setContent(tinyMCE.get(target).getContent()+"<br>"+t.responseText);
			}
			else
			{
				tinyMCE.get(target).setContent(t.responseText);
			}
		}
      },
       onFailure: function(t) {
              alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
          }

      }
      );
 }



</script>

<table width="100%">
<tr><td colspan=2 align=center>  
	<table><tr><td>
	<?php 
	if(file_exists($_SESSION['photothumb']."/".$data['photo']) && $data['photo'] != '')
	{
	?>
	<img src="<?php echo $_SESSION['urlthumb'] ?>/<?php echo $data['photo']?>" border=0 align=middle>
	<?php }?>
	</td><td  style="font-size:16px;" valign=middle>
	<span style=font-size:80%;font-variant:small-caps;>Patient:</span> <span style="letter-spacing:.5px;font-size:18px"><?php echo $data['firstname']?> 
		<?php 
					if($data['middlename'])
					{				
						echo substr($data['middlename'],0,1).".";
					}
		?> 
		 <?php echo $data['lastname']?></span>
	
	<?php
	if($auth_arr['access'] == "admin"  || $auth_arr['access'] == "doctor")
	{
		echo $util->setEdel("patient.php?pf=1&submit=edit&patientid=$patientid","list.php?do=delete&patientid=$patientid");
	}
	else
	{
		echo $util->setEdel("patient.php?pf=1&submit=edit&patientid=$patientid","");
	}
	?>
	<br> 
	<span style=font-size:80%;font-variant:small-caps>Age:</span> <span style=font-weight:bold><?php echo $age_arr['year']?> yrs. old</span>
	<span style=font-size:80%;font-variant:small-caps>Birthday:</span> <span style=font-weight:bold><?php echo $util->convertmysqldate($data['birthday'],"F j, Y")?></span>
	<span style=font-size:80%;font-variant:small-caps>Sex:</span> <span style=font-weight:bold><?php echo $data['sex']?></span>
	</td></tr>
	</table>
	
</td></tr>
<tr>
	<td valign="top" width=225px>
 
<div id=naviright> 
<table style="padding-top:0px; line-height: 25px;" cellpadding=2 width=175px rules=rows>


<?php

$selected = "background-color:#ff5a00;"; 
//$not_selected = "background-color:#339933;color:#ffffff;";
//$not_selected = "background-color:white;color:black;";
$page = $_REQUEST['pf'];

	foreach($links as $key=>$value)
	{
		if($page == $key)
		{
			echo "<tr><td style=\"$selected\"> " .
					"<a href=\"patient.php?pf=$key&patientid=$patientid\" style=\"text-decoration:none;font-weight:bold;color:white;\">$value</a></td></tr>";
		}
		else
		{
			echo "<tr><td class=\"notselected\">" .
					"<a href=\"patient.php?pf=$key&patientid=$patientid\"  style=\"text-decoration:none;font-weight:bold;color:#0039a6;\">$value</a></td></tr>";
			
		}
	}
?>
</table>
</div>
    </td>
	<td valign="top" style="padding-left:10px">
  <?php 
		if($_REQUEST['pf'] == 1)
		{	
			include("patient/patientpage.php");
		}
		elseif($_REQUEST['pf'] == 2)
		{	
			include("patient/appointment.php");
		}
		elseif($_REQUEST['pf'] == 3)
		{	
			include("patient/setappt.php");
		}
		elseif($_REQUEST['pf'] == 4)
		{	
			include("patient/calendar.php");
		}
		elseif($_REQUEST['pf'] == 5)
		{	
			include("patient/consult.php");
		}
		elseif($_REQUEST['pf'] == 6)
		{	
			if($auth_arr['access'] == "admin" || $auth_arr['access'] == "doctor")
			{
				include("patient/chart.php");
			}	
			else
			{
				include("patient/patientpage.php");
			}
		}
		elseif($_REQUEST['pf'] == 7)
		{	if($auth_arr['access'] == "admin"  || $auth_arr['access'] == "doctor")
			{
				include("patient/history.php");
			}	
			else
			{
				include("patient/patientpage.php");
			}
		}
		elseif($_REQUEST['pf'] == 8)
		{	
			include("patient/bill.php");
		}
		elseif($_REQUEST['pf'] == 9)
		{	
			include("patient/medcert.php");
		}
		elseif($_REQUEST['pf'] == 10)
		{	
			include("patient/report.php");
		}
		elseif($_REQUEST['pf'] == 11)
		{	
			include("patient/surgicalhistory.php");
		}
		elseif($_REQUEST['pf'] == 12)
		{	
			include("patient/labrequest.php");
		}
		elseif($_REQUEST['pf'] == 13)
		{	
			include("patient/inpatient.php");
		}
		elseif($_REQUEST['pf'] == 14)
		{	
			include("patient/medicalclearance.php");
		}
		elseif($_REQUEST['pf'] == 15)
		{	
			include("patient/referral.php");
		}
		else
		{
			include("patient/".$_REQUEST['pf'].".php");
		}

?>
    </td>
</tr>
</table>

<?php 
include("include/bottom.php");
?>