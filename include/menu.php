<style>
/*NAVIGATION*/
body { 	min-width:1200px;
}
#nav {	background-image: url('image/bluebar.png'); 
		background-repeat:no-repeat;
		background-size:100% 100%;
		background-origin:content-box;
		margin: 0px;
		min-width:1200px;
		padding:auto;
		font-size:12px;
		height:44px;
		
}
#nav a { color:white; text-decoration:none; padding:0px 5px 0px 5px; line-height: 38px;
}
#nav a:hover { color:orange;
}
#nav a.current:hover { 	color:blue;				
}
#nav a.current { 	background-image: url('image/orangebar.png');
					background-size:100% 100%;
					padding:14px 35px 14px 35px;
					text-decoration:none;				
}

#searchwrapper {
					width:310px; /*follow your image's size*/
					height:20px;/*follow your image's size*/
					background-image:url(THE_SEARCH_BOX_IMAGE);
					background-repeat:no-repeat; /*important*/
					padding:0px;
					margin:0px;
					position:relative; /*important*/
}
 
#searchwrapper form { display:inline ; }
 
.searchbox {	border:0px; /*important*/
				background-color:white; /*important*/
				position:absolute; /*important*/
				top:1px;
				left:2px;
				width:220px;
				height:17px;
				border: 1px solid #DDDDDD;
				-webkit-transition: all 0.30s ease-in-out;
				-moz-transition: all 0.30s ease-in-out;
				-ms-transition: all 0.30s ease-in-out;
				-o-transition: all 0.30s ease-in-out;
				outline: none;
				color: #ff5a00;
   				font-size:14px;
   				font-weight:bold;
}

.searchbox:focus {    box-shadow: 0 0 5px rgba(81, 203, 238, 1);
					  border: 2px solid rgba(81, 203, 238, 1);
					  padding-left:5px;
}
 
.searchbox_submit {
						border:0px; /*important*/
						background-image:url('image/search_button.png'); /*important*/
						background-repeat:no-repeat;
						background-size:100% 100%;	
						background-color:transparent;
						position:absolute; /*important*/
						top:-4px;
						left:190px;
						width:70px;
						height:30px;
						padding:0px 5px 0px 5px;
						cursor:pointer;
						color:transparent;
}
/*.searchbox_submit:hover { 	border:0px; /*important*/
							/*background-image:url('image/searchicon2.png'); /*important*/
							/*background-repeat:no-repeat;
							background-size:100% 100%;	
							background-color:transparent;
							position:absolute; /*important*/
							/*top:8px;
							left:190px;
							width:75px;
							height:25px;
							cursor:pointer;
							color:transparent;
}*/



</style>

<div id="nav">

<table width="100%" height="20px">
<tr>
	
	<?php 
	$auth_arr = isset($_SESSION['auth'])?$_SESSION['auth']: array();
	
	if($auth_arr['access'] == 'admin') {?>
	<td align=center>
		<a href=tasks.php  >Tasks</a> |
		<a href=admin.php>Usage</a> |
  <!--  <a href=directory.php>Directory Services</a> | -->
	    <a href=users.php>User</a> |
	    <a href=manage.php>Manage</a>
    </td>
    <!--
    <td align=center>
		<div id="searchwrapper">
			<form action="list.php">
				<input type="text" class="searchbox" value="" name=search id=search />
				<input type="submit" src="image/searchicon.png" class="searchbox_submit" name=do value="Search" />	
			</form>
		</div>
	</td>
	-->
	<?php }elseif($auth_arr['access'] == 'doctor') {?>
	<td align=center>
		<a href=list.php >Patient List</a> |
    	<a href=newpatient.php  >Add New Patient</a> | 
		<a href=tasks.php  >Tasks</a> |
		<a href=calendar.php  >Calendar</a> |
    	<a href=queue.php  >Queue</a> |
    	<a href=bill.php  >Statement</a> |
    	<a href=stat.php  >Stat</a> |
    	<a href=numbers.php  >Profile</a> |
	    <a href=user_staff.php>User</a> |
	    <a href=manage.php>Manage</a>
    </td>
    <!--
    <td align=center>
		<div id="searchwrapper">
			<form action="list.php">
				<input type="text" class="searchbox" value="" name=search id=search />
				<input type="submit" src="image/searchicon.png" class="searchbox_submit" name=do value="Search" />	
			</form>
		</div>
	</td>	
	-->
	<?php }elseif($auth_arr['access'] == 'staff') {?>
	<td align=center>
		<a href=list.php >Patient List</a> |
    	<a href=newpatient.php  >Add New Patient</a> | 
		<a href=calendar.php  >Calendar</a> |
    	<a href=queue.php  >Queue</a> |
    	<a href=bill.php  >Statement</a> |
    	<a href=stat.php  >Stat</a> |
    	<a href=numbers.php  >Profile</a> |
	    <a href=user_staff.php>User</a> |
    </td>
    <!-- <td align=center>
		<div id="searchwrapper">
			<form action="list.php">
				<input type="text" class="searchbox" value="" name=search id=search />
				<input type="submit" src="image/searchicon.png" class="searchbox_submit" name=do value="Search" />	
			</form>
		</div>
	</td>
	 -->	
	<?php }?>

</tr>
</table>
<script language="javascript">setPage();</script>
</div>