<?php

class Bill
{
	var $db;
	var $msg;
	var $userid = 0;
	function bill()
	{
		$this->db = new DBConnect();
	}
	function add($arr)
	{
		foreach($arr as $k=>$v)
		{
			if(!is_array($arr[$k]))
			{
				$arr[$k] = trim(mysql_real_escape_string($v));
			}
		}
		
		$arr['profee'] = str_ireplace(",","",$arr['profee']);
		$discount = isset($arr['discount']) ? $arr['discount']:0;
		$consultid = isset($arr['consultid']) ? $arr['consultid']:0;
		$sql = "INSERT INTO `statement` (`id` ,`consultid` ,`patientid` ,`date` ,`profee` ,".
			"`remark`,`discount`,`userid`,`hmo`,`hmoid`,`approvalcode`,`company`) VALUES ".
			"('', '".$consultid."' , '".$arr['patientid']."','".$arr['date']."', '".$arr['profee']."' ," .
			"'".$arr['remark']."','".$discount."','".$arr['userid']."','".$arr['hmo']."',
			'".$arr['hmoid']."','".$arr['approvalcode']."','".$arr['company']."')";		
		if($this->db->insert($sql) > 0)
		{
			
		}
		else
		{
			$this->msg = $this->db->msg." - ".$sql;	
		}
	}
	function delete($billid)
	{
		$sql = "delete from cashpayment where statementid=".$billid;
		$this->db->delete($sql);
		
		$sql = "delete from statement where id=$billid";
		$this->db->delete($sql);	
		
	}
	function deleteConsultBill($consultid)
	{
		$arr = $this->getConsultBill($consultid);
		$billid = $arr['id'];
		$this->delete($billid);
	}
	function deleteAll($billid)
	{
		$sql = "delete from statement where id=$billid";
		$this->db->delete($sql);	
		
	}
	function update($arr)
	{
		$date = $arr['y4']."-".$arr['m4']."-".$arr['d4'];
		if(isset($arr['remark']))
		{
			
		}
		else
		{
			$arr['remark'] = "";
		}
		$discount = isset($arr['discount']) ? $arr['discount']:0;
		$sql = "update statement set profee='".$arr['profee']."', discount='".$discount."', remark='".$arr['remark']."', date='$date' where id=".$arr['billid'];
		
		$this->db->update($sql);
	}
	
	function getBill($billid)
	{
		$sql = "select * from statement where id=$billid";
		return $this->db->getRecord($sql);
	}
	function getConsultBill($consultid)
	{
		$sql = "select * from statement where consultid=$consultid";
		return $this->db->getRecord($sql);
	}
	
	function getDateBill($sqldate,$patientid)
	{
		$sql = "select * from statement where patientid=$patientid and date='$sqldate'";
		return $this->db->getAllRecord($sql);	
	}
	
	function getAllBill($patientid)
	{
		$sql = "select * from statement where patientid=$patientid order by date desc";
		return $this->db->getAllRecord($sql);
	}
	
	function addCashPayment($arr)
	{
		if(floatval($arr['amount']) > 0)
		{
			$sql = "insert into cashpayment (`id`,`statementid`,`amount`,`date`) " .
					"values(NULL,'".$arr['billid']."','".$arr['amount']."','".$arr['date']."')";
			$this->msg = $sql;
			$lastid = $this->db->insert($sql);
			$this->msg = $this->db->msg." - ".$sql;
			return $lastid;
		}
		else
		{
			return 0;
		}
	}
	
	function addCheckPayment($arr)
	{
		if(floatval($arr['amount']) > 0)
		{
			$sql = "insert into checkpayment (`id`,`statementid`,`date`,`bank`,`checkno`,`amount`) " .
					"values(NULL,'".$arr['billid']."','".$arr['date']."','".$arr['bank']."','".$arr['checkno']."','".$arr['amount']."')";
			$lastid = $this->db->insert($sql);
			$this->msg = $this->db->msg." - ".$sql;
			return $lastid;
		}
		else
		{
			return 0;
		}
	}
	
	function addCreditPayment($arr)
	{
		if(floatval($arr['amount']) > 0)
		{
			$sql = "insert into creditpayment (`id`,`statementid`,`duedate`,`name`,`card`,`cardno`,`amount`) " .
					"values(NULL,'".$arr['billid']."','".$arr['duedate']."','".$arr['name']."','".$arr['card']."','".$arr['cardno']."','".$arr['amount']."')";
			$lastid = $this->db->insert($sql);
			$this->msg = $this->db->msg." - ".$sql;
			return $lastid;
		}
		else
		{
			return 0;
		}
	}
	function getTotalCashPayments($billid)
	{
		 $sql = "select amount from cashpayment where statementid=$billid";
		 
		$amountpaid = 0;
		$result = $this->db->getAllRecord($sql);
		if($result)
		{
			foreach($result as $val)
			{
				$amountpaid+=$val['amount'];
			}
		}
		return $amountpaid;
	}
	
	function getTotalCheckPayments($billid)
	{
		 $sql = "select amount from checkpayment where statementid=$billid";
		  $amountpaid = 0;
		 $result = $this->db->getAllRecord($sql);
		 if($result)
		 {
			 foreach($result as $val)
			 {
			 	$amountpaid+=$val['amount'];
			 }
		 }
		 return $amountpaid;
	}
	
	function getTotalCreditPayments($billid)
	{
		$sql = "select amount from creditpayment where statementid=$billid";
		 $amountpaid = 0;
		 $result = $this->db->getAllRecord($sql);
		 if($result)
		 {
			 foreach($result as $val)
			 {
			 	$amountpaid+=$val['amount'];
			 }
		 }
		 return $amountpaid;
	}
	
	function getTotalPayment($billid)
	{
		//get all payments for a consult
		$cash = $this->getTotalCashPayments($billid);
		$check = $this->getTotalCheckPayments($billid);
		$credit = $this->getTotalCreditPayments($billid);
		 
		return $cash + $check + $credit;
	}
	
	function boolBilled($consultid)
	{
		$sql = "select * from statement where consultid=$consultid";
		return count($this->db->getAllRecord($sql));
	}
	
	function boolBilledDate($date)
	{
		$sql = "select * from statement where date='$date'";
		return count($this->db->getAllRecord($sql));
	}
	function getAllCashPayments($billid)
	{
		$sql = "select * from cashpayment where statementid=$billid order by id";
		return $this->db->getAllRecord($sql);
	}
	function getAllCheckPayments($billid)
	{
		$sql = "select * from checkpayment where statementid=$billid order by id";
		return $this->db->getAllRecord($sql);
	}
	function getAllCreditPayments($billid)
	{
		$sql = "select * from creditpayment where statementid=$billid order by id";
		return $this->db->getAllRecord($sql);
	}
	function delCashPayment($cashid)
	{
		return $this->db->delete("delete from cashpayment where id=$cashid");
	}
	
	function delCheckPayment($checkid)
	{
		return $this->db->delete("delete from checkpayment where id=$checkid");
	}
	function delCreditPayment($creditid)
	{
		return $this->db->delete("delete from creditpayment where id=$creditid");
	}
	
	function getRange($start,$end,$arr,$show)
	{
		 
		$dum_arr = array();
		$sql = "select t1.id as id, t2.id as patientid,t1.date as date, t1.consultid as consultid,t1.profee as profee, `show`,t1.company as company, t1.hmo as hmo, t1.hmoid as hmoid from statement as t1 left join patient as t2 on t1.patientid=t2.id where '$start' <= date and date <= '$end' $show";
		if(isset($arr['hmo']) && $arr['hmo'])
		{
			$dum_arr[] = "t1.hmo='".$arr['hmo']."'";
		}
		if(isset($arr['company']) && $arr['company'])
		{
			$dum_arr[] = "company='".$arr['company']."'";
		}
		if(isset($arr['profee']) && $arr['profee'] == "pf")
		{
			$dum_arr[] = "profee > 0";
		}
		if(isset($arr['profee']) && $arr['profee'] == "cn")
		{
			$dum_arr[] = "profee = 0";
		}
		$dum_str = implode(" and ",$dum_arr);
		if($dum_str)
		{
			$sql.=" and $dum_str";
		}
		
		$sql.= " order by date desc";
	
		return $sql;
		
	}

	function discount($profee,$discount)
	{
		return $profee*($discount/100);
	}
	function beforeDiscount($discounted_amount,$discount)
	{
		//Get the original amount
		if($discount > 0)
		{
			return $discounted_amount*100/(100 - $discount);	
		}
		elseif($discount == 0)
		{
			return $discounted_amount;
		}
	}
	function afterDiscount($profee,$discount)
	{
		if($discount > 0)
		{
			return $profee*(1 - $discount/100);
		}
		elseif($discount == 0)
		{
			return $profee;
		}	
	}
}
?>
