<?php

include('Mail.php');
include('Mail/mime.php');
include_once('DBConnect.php');

class Register extends Auth
{
	var $password;
	var $alpha = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
		"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
		 "1","2","3","4","5","6","7","8","9");
	
	
	var $db = "";
 	
	var $instruction = "";
	
	function __construct()
 	{
 		$this->db = new DBConnect();
 	}
	function createPassword()
	{
		$this->password = "";
		$sizeAlpha = count($this->alpha);
		for($i=0;$i<8;$i++)
		{
			$this->password.=($this->alpha[rand()%$sizeAlpha]);
			
		}
		return $this->password;
	}
	
	function newUser($arr)
	{
	
		$cc = 'norjaliza@medasiaphils.com,joelpira@medasiaphils.com';
		
		$email = $arr['user'].','.$cc;
	
		$headers['From']    = 'medriks@medriks.com';
		$headers['Subject'] = 'Medriks Registration';
		$headers['Bcc'] = $cc;
		
		$arr['password'] = $this->createPassword();
		$arr['access'] = "doctor";
		
		$this->db->table = "users";
		$insertId = $this->db->insertArr($arr); 
		
		if($insertId == 0)
		{
			return "duplicate";
		}
		else
		{	
			$this->db->table = "numbers";
			if(isset($arr['clinic']) && $arr['clinic'] != "")
			{
				$this->db->insertArr(array("clinic"=>$arr['clinic'],"userid"=>$insertId));
			}
		
			$this->db->update("update users set password=password('".$arr['password']."') where id=".$insertId);
			
			$html = '<html><body>
				
				<img src=http://www.medriks.com/image/medriks_logo.jpg alt=http://www.medriks.com>
				<p>Dear '.$arr['name'].',</p>
				<p>Thank you for your registration. Your username is '.$arr['user'].' and your password is '.$arr['password'].'.</p>
				
				<p>Please perform the following:</p>
				<ol>
					<li>Change password and add staff (if needed) in User Menu.</li>
					<li>Fill-in Profile Menu. </li>
					<li>Select ICD 10 in Manage Menu. (Optional)</li>
				</ol>
				
				<p>Log on now to <a href="http://www.medriks.com">http://www.medriks.com</a></p>
				
				For inquiries email <a href=mailto:support@medriks.com>support@medriks.com</a>.
				</body></html>';
			
			$this->instruction = '
				<img src=http://www.medriks.com/image/medriks_logo.jpg alt=http://www.medriks.com>
				<p>Dear '.$arr['name'].',</p>
				<p>Thank you for your registration. Your username is '.$arr['user'].' and your password is '.$arr['password'].'.</p>
				
				<p>Please perform the following:</p>
				<ol>
					<li>Change password and add staff (if needed) in User Menu.</li>
					<li>Fill-in Profile Menu. </li>
					<li>Select ICD 10 in Manage Menu. (Optional)</li>
				</ol>
				
				<p>Log on now to <a href="http://www.medriks.com">http://www.medriks.com</a></p>
				
				For inquiries email <a href=mailto:support@medriks.com>support@medriks.com</a>.
				';
			
			$crlf = "\n";
		
			$mime = new Mail_mime($crlf);
	
			$mime->setHTMLBody($html);
		
	
	
			$body = $mime->get();
			$hdrs = $mime->headers($headers);
	
			
			$params['sendmail_path'] = '/usr/lib/sendmail';
			
			$mail_object =& Mail::factory('sendmail', $params);
		
			$mail_object->send($email, $hdrs, $body);
		
			if (PEAR::isError($mail_object)) 
			{ 
				echo $mail_object->getMessage();
				return "rejected";
			}
		
			return "accepted";
		}
		
	}
	
	function forgot($email)
	{
		
		$sql = "select count(id) as count from users where user='$email'";
		$rst = $this->db->getRecord($sql);
		if($rst['count'] == 1)
		{
			$headers['From']    = 'medriks@medriks.com';
			$headers['Subject'] = 'Medriks Password Reset';
			$password = $this->createPassword();
			
			$this->db->update("update users set password=password('".$password."') where user='".$email."'");

			$html = '<html><body>
				<img src=http://www.medriks.com/image/medriks_logo.jpg alt=http://www.medriks.com>
				
				<p>Your new password is '.$password.'</p>
				<p>For inquiries email <a href=mailto:support@medriks.com>support@medriks.com.</a></p>
				</body></html>';
			
			
			$crlf = "\n";
		
			$mime = new Mail_mime($crlf);
	
			$mime->setHTMLBody($html);
		
	
	
			$body = $mime->get();
			$hdrs = $mime->headers($headers);
	
			
			$params['sendmail_path'] = '/usr/lib/sendmail';
			
			$mail_object =& Mail::factory('sendmail', $params);
		
			$mail_object->send($email, $hdrs, $body);
		
			if (PEAR::isError($mail_object)) 
			{ 
				//echo $mail_object->getMessage();
				echo "<p>Email send failed.</p>";
			}
			else
			{
				echo "<p>New password sent successfully.</p>";
			}
						
		}
		else
		{
			 echo '<p>Invalid email address.</p>';
		}
	}
}
