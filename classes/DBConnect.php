<?php
ini_set("memory_limit","500M");

class DBConnect 
{
	var $msg = "";
	var $recordCount = 0;
	var $error;
	var $sql;
	
	function getAllRecord($sqlStatement)
	{
		$this->recordCount = 0;
		$arr_d2 = array();
		$count = 0;
		$result = mysql_query($sqlStatement);
		$this->sql = $sqlStatement;
		if($result)
		{
			while($r=mysql_fetch_array($result))
			{
				$arr_d2[] = $r;
				$count++;
			}
			$this->recordCount = $count;
			return $arr_d2;
		}
		else
		{
			$this->msg = "getAllRecord [$sqlStatement]".mysql_error();
			$this->error = $this->msg;
			return null;
		}
	}
	function getOneCol($sql)
	{
		$arr = array();
		$result = mysql_query($sql);
		if($result)
		{
			while($r=mysql_fetch_array($result))
			{
				$arr[]=$r[0];
			}	
			return $arr;
		}
		else
		{
			$this->msg = "getAllRecordOneCol [$sql]".mysql_error();
			return null;
		}
	}
	
	function getRecord($sqlStatement)
	{
		$result = mysql_query($sqlStatement);
		$this->sql = $sqlStatement;
		if($result)
		{
			$r = mysql_fetch_array($result);
			return $r;
		}
		else
		{
			$this->msg = "getRecord [$sqlStatement]".mysql_error();
			$this->error = $this->msg;
			return null;
		}
	}
	
	
	
	function delete($sqlStatement)
	{
		if(mysql_query($sqlStatement))
		{
			return 1;	
		}
		else
		{
			$this->msg = "Delete failed ".mysql_error();
			return 0;
		}
	}
	
	function insert($insertStatement)
	{
		if(mysql_query($insertStatement))
		{
			$this->msg = " Insert successful";
		 	return mysql_insert_id();
		}
		else
		{
			$this->msg = " Insert failed ".mysql_error();
			return 0;
		}
	}
	function update($updateStatement)
	{
		if(mysql_query($updateStatement))
		{
			$this->msg = "Update sucessful";
		}
		else
		{
			$this->msg = "Update error ".mysql_error();
		}
		return mysql_affected_rows();
	}
	function htmlSelect($sql,$option,$value)
	{
		$sql = "";
		
		$x = "<option value=\">";
		if(is_array($value))
		{
			$y = explode(" ",$value);
			$x.=$y."\"";
		}
		else
		{
			$x.=$value."\"";
		}
		if(is_array($option))
		{
			$y = explode(" ",$option);
			$x.=$y;
		}
		else
		{
			$x.="$option";
		}
		$x.="</option>";
		return $x;
	}
	
	function getColumns($table)
	{
		$arr_d2 = $this->getAllRecord("show columns from $table");
		$arr_d1 = array();
		foreach($arr_d2 as $value)
		{
			$arr_d1[] = $value[0];
		}
		if(count($arr_d1) > 0)
		{
			$this->msg = "no of cols ".count($arr_d1);
			return $arr_d1;	
		}
		else
		{
			$this->msg = "no of cols none";
			return null;
		}
		
	}
	
	function rowCount($col,$table)
	{
		
		$sql = "select count($col) from $table";
		$result = mysql_query($sql);
		$row = @mysql_fetch_array($result);
		return $row[0];
	}

	var $table;
	function setTable($table)
	{
		$this->table = $table;
	}
	function insertArr($arr,$primarykey = "id")
	{
		if($this->table)
		{
			$cols = $this->getColNames($this->table);
	 		$sql = "insert into ".$this->table."  ";
	 		$x = array();
	 		$sql.=" (";
	 		if($primarykey)
	 		{
	 			$sql.=$primarykey.",";
	 		}
	 		else
	 		{
	 			$sql.="`id`,";
	 		}
	 		foreach($cols as $val)
	 		{
	 			
	 			if(array_key_exists($val,$arr))
	 			{
	 				$x[] = "`$val`";
	 			}
	 		}
	 		
	 		$sql.= implode(",",$x);
	 		$sql.=") values(NULL,";
			unset($x);
	 		foreach($cols as $val)
	 		{
	 			if($val != $primarykey)
	 			{
	 				if(array_key_exists($val,$arr))
	 				{
	 					$x[] = "'".trim(mysql_real_escape_string($arr[$val]))."'";
	 				}
	 			}
	 		}
	 		$sql.= implode(",",$x).")";
	 		$this->sql = $sql;
	 		if(mysql_query($sql))
	 		{
	 			
	 			return mysql_insert_id();
	 		}
	 		else
	 		{
			 	$this->error = "Mysql error: ".mysql_error();
			 	return 0;	
	 		}
	
		}
		else
		{
			$this->error = "No database table";
			return 0;
		}
	}
	function updateArr($arr,$primaryKey = "id",$primaryKeyValue = "0")
	{
		if($this->table)
		{
			$col = $this->getColNames();
			$d = array();
			$sql = "update ".$this->table." set ";
			foreach($col as $val)
			{
				if($val != $primaryKey && array_key_exists($val,$arr))
				{
					if($val == "password")
					{
						$d[] = $val."=".$arr[$val];	
					}
					else
					{
						$d[] = "`".$val."`='".trim(mysql_real_escape_string($arr[$val]))."'";
					} 
				}

			}
			$sql.= implode(",",$d)." where $primaryKey=$primaryKeyValue";
			if(mysql_query($sql))
			{
				$this->sql = $sql;
				return 1;
			}
			else
			{
				$this->sql = $sql;
				$this->error = mysql_error();
				return 0;
			}
		}
		else
		{
			$this->error = "No database table";
		}
	}
	
	function getColNames()
	{
		if($this->table)
		{
			$sql = "show columns from ".$this->table;
			$this->sql = $sql;
			$arr_d2 = $this->getAllRecord($sql);
			$arr_d1 = array();
			foreach($arr_d2 as $value)
			{
				$arr_d1[] = $value[0];
			}
			if(count($arr_d1) > 0)
			{
				$this->msg = "no of cols ".count($arr_d1);
				return $arr_d1;
			}
			else
			{
				$this->msg = "no of cols none";
				return null;
			}
		}
		else
		{
			$this->error = "Missing database table";
		}
	}
}
?>
