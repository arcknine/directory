<?php

class Inpatient
{
	var $db;
	var $dbtable = "inpatient";
	var $recordCount;
	function __construct()
	{
		$this->db = new DBConnect();
	}
	function add($arr)
	{
		$cols = $this->db->getColumns($this->dbtable);
		$sql = "insert into ".$this->dbtable." set ";
		$x = array();
		foreach($cols as $val)
		{
			if(array_key_exists($val,$arr))
			{
				$x[] = "`$val`='".trim(mysql_real_escape_string($arr[$val]))."'";
			}
		}
		$sql.= implode(",",$x);
			
		$lastid = $this->db->insert($sql);
		if($lastid > 0)
		{
			return $lastid;
		}
		else
		{
			echo $this->db->msg;
		}


	}
	function update($arr,$primary = "")
	{
		$x = array();
			
		$cols = $this->db->getColumns($this->dbtable);
		$sql = "update  ".$this->dbtable."  set ";
			
		foreach($cols as $val)
		{
			if($val != $primary)
			{
				if(array_key_exists($val,$arr))
				{
					$x[] = "`$val`='".trim(mysql_real_escape_string($arr[$val]))."'";
				}
			}
		}
		$sql.= implode(",",$x);
			
		$sql .=	" where id=".$primary;

		$affected_rows =  $this->db->update($sql);
		$this->msg = $this->db->msg;
		return $affected_rows;
	}
	
	function fetchAll($patientid,$userid)
	{
		$sql = "select * from ".$this->dbtable." where patientid=$patientid and userid=$userid order by dateadmit desc";
		$x = $this->db->getAllRecord($sql);
		$this->recordCount = $this->db->recordCount;
		if($this->db->recordCount > 0)
		{
			return $x;
		}
		else
		{
			return null;	
		}
	}
	
	function delete($id)
	{
		return $this->db->delete("delete from ".$this->dbtable." where id=$id");	
	}
}