<?php


class Util {

	function mouseOver($string, $width) {
		return "onmouseover=\"this.T_WIDTH=$width;this.T_FONTCOLOR='#003399';return escape('$string')\"";
	}

	function deleteImage($image, $path) {
		@ unlink($path . "/" . $image) or print ("<p>File cannot be deleted");
	}
	function navi($page, $start, $rowstoview, $totalrows, $imagepath) {
		$navi = "";
		if ($start - ($rowstoview) > -1) {
			$prev = $start - ($rowstoview);
			$navi .= "$start <a href='$page&start=" . $prev . "&rowstoview=" . $rowstoview . "'>
						      	<img src=$imagepath/arrowleft.png border=0  align=middle width=25 height=25></a>";
		}
		if ($totalrows > ($start + $rowstoview)) {
			$start = $start + $rowstoview;
			$left = $totalrows - $start;
			$navi .= " <a href='$page&start=" . $start . "&rowstoview=" . $rowstoview . "'>
								<img src=$imagepath/arrowright.png border=0  align=middle width=25 height=25></a> $left";
		}
		if ($totalrows > $rowstoview) {
			$navi .= "<form action=\"$page\" method=\"post\" style=padding-top:5px>" .
			"<b>LIST</b> <select name=\"rowstoview\" id=\"rowstoview\" class='input'>";

			$views = array (
				"10",
				"20",
				"30",
				"50",
				"100"
			);
			foreach ($views as $value) {
				if ($rowstoview == $value) {
					$navi .= "<option selected>" . $rowstoview . "</option>";
				} else {
					$navi .= "<option>" . $value . "</option>";
				}
			}
			$navi .= "</select>
								<input type=submit name=submit value=GO class=go_button>
								</form>";

		}
		return $navi;
	}

	function monthName() {
		return array (
			"1" => "January",
			"2" => "February",
			"3" => "March",
			"4" => "April",
			"5" => "May",
			"6" => "June",
			"7" => "July",
			"8" => "August",
			"9" => "September",
			"10" => "October",
			"11" => "November",
			"12" => "December"
		);
	}

	function selectDate($yearvar, $monthvar, $dayvar) {
		$months = $this->monthName();
		$today['mon'] = $_SESSION['mon'];
		$today['year'] = $_SESSION['year'];
		$today['mday'] = $_SESSION['day'];
		$bf = "<select id='$monthvar' name='$monthvar'><option value=0>Choose</option>";
		foreach ($months as $key => $value) {
			if ($key == $today['mon']) {
				$bf .= "<option value=\"$key\" selected>$value</option>";
			} else {
				$bf .= "<option value=\"$key\">$value</option>";
			}
		}
		$bf .= "</select><select id='$dayvar' name='$dayvar'><option value=0>Choose</option>";

		for ($i = 1; $i < 32; $i++) {
			if ($i == $today['mday']) {
				$bf .= "<option selected>$i</option>";
			} else {
				$bf .= "<option>$i</option>";
			}
		}
		$bf .= "</select><input type=text id='$yearvar' name='$yearvar' size=4 maxlength=4 value=" . $today['year'] . ">";

		return $bf;

	}
	function selectNewDateNoCalendar($year_name,$month_name,$day_name,$minus_year,$add_year,$olddate) //year-month-date mysql format
{

	@list($oyear,$omonth,$oday) = split("-",$olddate);
	$out = "";
	$mon = array ("-", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	$out =  $out."<select name=$month_name id=$month_name>";
	$out = $out."<option>-</option>";
	for ($i = 1; $i < 13;$i++) {

		if ($i == intval($omonth))
		{
			$out = $out. "<option  value=\"$i\" selected=\"selected\">$mon[$i]</option>";

		} else {
			$out = $out.  "<option value=\"$i\">$mon[$i]</option>";
		}
	}
	$out = $out."</select>";
	$out = $out."<select name=$day_name id=$day_name>";
	$out = $out."<option>-</option>";
	for($i=1;$i<32;$i++)
	{
		if($i == $oday)
		{
			$out = $out. "<option value=\"$i\" selected=\"selected\">$i</option>";
		}
		else
		{
			$out = $out.  "<option value=\"$i\">$i</option>";
		}
	}
	$out = $out.  "</select>";
	$out =$out.  "<select name=$year_name id=$year_name>";
	$out = $out."<option>-</option>";
	$year = isset($_SESSION['year'])?$_SESSION['year']: 0;
	for($i=$year - $minus_year; $i<=$year + $add_year;$i++)
	{
		if($i==$oyear)
		{
			$out = $out.  "<option  value=\"$i\" selected=\"selected\">$i</option>";
		}
		else
		{
			$out = $out.  "<option value=\"$i\">$i</option>";
		}
	}
	$out = $out. "</select>";
	return $out;
}

/*
 * USE WITH JAVASCRIPT CALENDAR
 */
	function selectNewDate($month_name,$day_name,$year_name,$olddate) //year-month-date mysql format
	{
	
	@list($oyear,$omonth,$oday) = split("-",$olddate);
	$dum = explode(" ",$oday);
	if(count($dum) > 0)
	{
		$oday = $dum[0];
		
	}
	
	$mon = array ("-", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	$out = "";
	$out =  $out."<select name=$month_name id=$month_name>";
	$out = $out."<option>-</option>";
	for ($i = 1; $i < 13;$i++) {

		if ($i == intval($omonth))
		{
			$out = $out. "<option  value=\"$i\" selected>$mon[$i]</option>";

		} else {
			$out = $out.  "<option value=\"$i\">$mon[$i]</option>";
		}
	}
	$out = $out."</select>";
	$out = $out."<select name=$day_name id=$day_name>";
	$out = $out."<option>-</option>";
	for($i=1;$i<32;$i++)
	{
		if($i == $oday)
		{
			$out = $out. "<option value=\"$i\" selected>$i</option>";
		}
		else
		{
			$out = $out.  "<option value=\"$i\">$i</option>";
		}
	}
	$out = $out.  "</select>";
	$out .= "<input type=text name=$year_name id=$year_name size=4 value=$oyear>";
	return $out;
}	
	function convertmysqldate($date, $phpDateFormat) 
	{
		@list ($y, $m, $d) = split("-", $date);
		if (@ checkdate($m, $d, $y)) {
			return date($phpDateFormat, @mktime(0, 0, 0, $m, $d, $y));
		} else {
			return null;
		}
	}
	
	function setEdel($editlink,$dellink)
	{
		$edit_m = " onmouseover=\"this.T_WIDTH=50;this.T_FONTCOLOR='#003399';return escape('Update')\"";
		$del_m = " onmouseover=\"this.T_WIDTH=50;this.T_FONTCOLOR='#003399';return escape('Delete')\"";
		if($editlink == "" && $dellink != "")
		{
			return "<a href=\"javascript:confirmDelete('$dellink')\" $del_m><img src=image/delete.png  border=0 width='40' height='40' ></a>";
	
		}
		elseif($editlink != "" && $dellink == "")
		{
	
			return "<a href=$editlink $edit_m><img src=image/edit.png  border=0 width='40' height='40' $edit_m></a>";
	
		}
		else if($editlink != "" && $dellink != "")
		{
	
			return  "<a href=\"$editlink\" $edit_m><img src=image/edit.png  border=0 width='40' height='40'></a>".
				"<a href=\"javascript:confirmDelete('$dellink')\" $del_m><img src=image/delete.png  border=0 width='40' height='40'></a>";
		}

	}
	
	function age($birthday,$sqldate_arr)
{
	$birthdate = explode("-",$birthday);
	if($sqldate_arr)
	{
		$today = explode("-",$sqldate_arr);
	}
	else
	{
		$xtoday = getdate();
		$today[0] = $xtoday['year'];
		$today[1] = $xtoday['mon'];
		$today[2] = $xtoday['mday'];
	}
	$y1 = $birthdate[0];
	$y2 = $today[0];
	$y =  $y2 - $y1;
	
	$m1 = intval($birthdate[1]);
	$m2 = $today[1];
	
	$m = $m2 - $m1;
	
	$d1 = intval($birthdate[2]);
	$d2 = $today[2];
	
	$d = $d2 - $d1;
	$age = 0;
	if($d >= 0){
		$d = $d2 - $d1;
	}
	elseif($d < 0){
		if($m2 != 1){ // $m2 is not january
			$m2 = $m2 - 1;
     		$d = mktime(0, 0, 0, $m2, 0, $y2) + $d2 - $d1;
			
		}
		else{
			$y2 = $y2 - 1;
			$m2 = 12;
      		$d = mktime(0, 0, 0, $m2, 0, $y2) + $d2 - $d1;
		}
		
	}
	$m = $m2 - $m1;
	if($m >= 0){
     
	}
	else{
		$y2 = $y2 - 1;
		$m = 12 + $m2 - $m1;
	}
	$y = $y2 - $y1;
	$age = array();
	if($y < 0){
		$age = null;
	}
	else{
		
		$age['year'] = $y;
		$age['month'] = $m;
		$age['day'] = $d;
	}
	
	return $age;
}
	
	function getHtmlSelect($option,$value_arr,$selIdName,$target_field,$function)
	{
	$string = array();
	$number = array();
	$sel = "\n<select name=$selIdName id=$selIdName  $function>";
	$sel = $sel."<option value=0>Choose</option>";
	
	foreach($value_arr as $key=>$val)
	{
		if(is_numeric($val)) //if zero number
		{
			if(!in_array($val,$number))
			{
				$number[intval($option[$key])] = $val;
			}
		}
		else
		{
			if(!in_array($val,$string))
			{
				$string[$option[$key]] = $val;
			}
		}
	
	}
	asort($number,SORT_NUMERIC);
	foreach($number as $key => $value)
	{
		$sel = $sel."\n<option value=\"$key\">$value</option>";
	}
	asort($string);
	foreach($string as $key => $value)
	{
		$sel = $sel. "\n<option value=\"$key\">$value</option>";	
	}
	$sel = $sel."\n</select>";
	return $sel;

}

	function selectJSDate($var,$function_name,$y1,$m1,$d1)
	{
		echo "<script> var $var = new CalendarPopup();";
		echo "$var.setReturnFunction(\"$function_name\");";
		echo "
		function $function_name(y,m,d) 
		{
			//alert(y+\" \"+m+\" \"+d);
		
			var mylist=document.getElementById('$d1');
			mylist.selectedIndex = d;
			
			document.getElementById('$y1').value = y;
			//var year = document.getElementById('$y1');
			//year.selectedIndex = 0;
			//year.options[0].value = y;
			//year.options[0].text = y;
			
			var month = document.getElementById('$m1');
			month.selectedIndex = m;
		    	//alert(month.options[m].text);
		 }</script>";
		
	}
	
	function selectTime($type,$hr,$min,$ampm,$given)
	{
	$date = new DateTime("Asia/Manila");
	
	/*
	 * given: H:i H - in 24hr format
	 */
	$hrx = $date->format("H");
	$minx = $date->format("i");
	
	if($given != "")
	{
		@list($hrx,$minx) = split(":",$given);
	}
	echo "<select name=$hr id=$hr>";
	if($type == "12")
	{
		for($i =1; $i<13;$i++)
		{
			if($i == $hrx%12)
			{
				echo "<option value=\"$i\" selected>$i</option>";
			}
			else
			{
				echo "<option value=\"$i\">$i</option>";
			}
		}

	}
	else
	{
		for($i =1; $i<23;$i++)
		{
			if($i == $hrx)
			{
				echo "<option value=\"$i\" selected>$i</option>";
			}
			else
			{
				echo "<option value=\"$i\">$i</option>";
			}
		}
	}
	echo "</select>:";
	echo "<select name=$min id=$min>";
	for($i=0;$i<61;$i++)
	{
		if($i < 10)
		{
			$i = "0$i";
		}
		if($i == $minx)
		{
			echo "<option value=\"$i\" selected>$i</option>";
		}
		else
		{
			echo "<option value=\"$i\">$i</option>";
		}
		
	}
	echo "</select>";
	if($type == "12")
	{
			if($hrx > 11)
			{
				echo "<select name=$ampm id=$ampm><option value=am>AM</option><option value=pm selected>PM</option></select>";
			}	
			else
			{
				echo "<select name=$ampm id=$ampm><option value=am selected>AM</option><option value=pm>PM</option></select>";
			}
		
	}
}
	function setDateTime($dateTime,$phpFormat)
	{

	
	@list($date,$time) = split(" ",$dateTime);
	@list($y,$m,$d) = split("-",$date);
	$h = 0; $min = 0; $sec =0;
	@list($h,$min,$sec) = split(":",$time);
	if(@checkdate($m,$d,$y))
	{
		return date($phpFormat,mktime($h,$min,$sec,$m,$d,$y));
	}
	else
	{
		return false;
	}
}

	function formatMoney($money)
	{
	return "Php".number_format($money,2,'.',',');
}

	function imageupload($prefix,$oldname,$imagefolder,$thumbfolder,$tmp_file,$thumbnailsize)
	{
		$x = explode(".",$oldname);
		$size = count($x) - 1;
		$ext = $x[$size];
	
		$newname = $prefix.".".$ext;
		$uploadfile = $imagefolder."/".$newname;	
		
		if(move_uploaded_file($tmp_file,$uploadfile)) 
		{
			
			
			if(!$this->thumbnail($imagefolder,$thumbfolder,$newname,$thumbnailsize))
			{
				echo "<p class=err>Thumbnail error"; 
			}
			
			
			@ unlink($imagefolder."/".$oldname);
			@ unlink($thumbfolder."/".$oldname);
			
			return $newname;
		}
		else
		{
			echo "<p class=err>Photo upload failed.";
		}
	}
	
	function thumbnail($image_path,$thumb_path,$image_name,$thumb_width)
{

	$x = explode(".",$image_name);
	$size = count($x) - 1;
	$ext = $x[$size];
	$tag = 0;
	if(strtolower($ext) == "jpeg" || strtolower($ext) == "jpg")
	{
		$src_img = imagecreatefromjpeg("$image_path/$image_name");
		$tag = 1;
	}
	elseif(strtolower($ext) == "png")
	{
		$src_img = imagecreatefrompng("$image_path/$image_name");
		$tag = 2;
	}
	elseif(strtolower($ext) == "gif")
	{
		$src_img = imagecreatefromgif("$image_path/$image_name");
		$tag = 3;
	}
	else
	{
		return false;
	}

	$origw=imagesx($src_img);
	$origh=imagesy($src_img);
	$new_w = $thumb_width;
	$diff=$origw/$new_w;
	$new_h=$origh/$diff;
	$dst_img = imagecreatetruecolor($new_w,intval($new_h));
	imagecopyresized($dst_img,$src_img,0,0,0,0,$new_w,$new_h,imagesx($src_img),imagesy($src_img)); 
	if(file_exists($thumb_path))
	{
		if($tag == 1)
		{
			imagejpeg($dst_img, "$thumb_path/$image_name");
		}
		elseif($tag == 2)
		{
			imagepng($dst_img, "$thumb_path/$image_name");
		}
		elseif($tag == 3)
		{
			imagegif($dst_img, "$thumb_path/$image_name");
		}
		else
		{
			return false;
		}
	}
	else
	{
	
		if(mkdir($thumb_path))
		{
			if($tag == 1)
			{
				imagejpeg($dst_img, "$thumb_path/$image_name");
			}
			elseif($tag == 2)
			{
				imagepng($dst_img, "$thumb_path/$image_name");
			}
			elseif($tag == 3)
			{
				imagegif($dst_img, "$thumb_path/$image_name");
			}
			else
			{
				return false;
			}
		}
	}
	return true;
}

	function getImageWidth($image_path,$image_name)
	{
	if($image_name != "")
	{
		$ext = $this->getImageExtension($image_name);
		if(strtolower($ext) == "jpeg" || strtolower($ext) == "jpg")
		{
			$src_img = @imagecreatefromjpeg("$image_path/$image_name"); 
		}
		elseif(strtolower($ext) == "png")
		{
			$src_img = @imagecreatefrompng("$image_path/$image_name");
		}
		elseif(strtolower($ext) == "gif")
		{
			$src_img = @imagecreatefromgif("$image_path/$image_name");
		}
		else
		{
			return 0;
		}
	
		return @imagesx($src_img);
	}
	else
	{
		
	}
}

	function getImageHeight($image_path,$image_name)
	{

	if($image_name != "")
	{
		$ext =  $this->getImageExtension($image_name);
	
		if(strtolower($ext) == "jpeg" || strtolower($ext) == "jpg")
		{
			$src_img = @imagecreatefromjpeg("$image_path/$image_name"); 
		}
		elseif(strtolower($ext) == "png")
		{
			$src_img = @imagecreatefrompng("$image_path/$image_name");
		}
		elseif(strtolower($ext) == "gif")
		{
			$src_img = @imagecreatefromgif("$image_path/$image_name"); 
		}
		else
		{
			return 0;
		}
	
		return @imagesy($src_img);
	}
	else
	{
		
	}
}
	function getParameters($arr)
	{
	foreach ($arr as $varname => $varvalue) 
	{
   		echo "<br>".$varname." = ".$varvalue;
	}
	}
	function days_in_month($month, $year){

	return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
	}	 

	function getImageExtension($filename)
	{

	if(@ eregi(".jpg",$filename))
	{
		return "jpg";
	}
	else
	if(@ eregi(".jpeg",$filename))
	{
		return "jpeg";
	}
	else
	if(@ eregi(".gif",$filename))
	{
		return "gif";
	}
	else
	if(@ eregi(".png",$filename))
	{
		return "png";
	}
	else
	{
		return null;
	}
}

	function splitsemicolon($arr)
	{
	$str = implode(";",$arr);
	@$arr2 = split(";",$str);
	$arr3 = array();
	foreach($arr2 as $val)
	{
		if(in_array($val,$arr3))
		{
			
		}
		else
		{
			$arr3[] = $val;
		}
	} 
	return $arr3;
}

/*
 * Email Validation
 */
	function check_email_address($email) {
	  // First, we check that there's one @ symbol, 
	  // and that the lengths are right.
	  if (@!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
	    // Email invalid because wrong number of characters 
	    // in one section or wrong number of @ symbols.
	    return false;
	  }
	  // Split it into sections to make life easier
	  $email_array = explode("@", $email);
	  $local_array = explode(".", $email_array[0]);
	  for ($i = 0; $i < sizeof($local_array); $i++) {
	    if
	(!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&
	↪'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$",
	$local_array[$i])) {
	      return false;
	    }
	  }
	  // Check if domain is IP. If not, 
	  // it should be valid domain name
	  if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
	    $domain_array = explode(".", $email_array[1]);
	    if (sizeof($domain_array) < 2) {
	        return false; // Not enough parts to domain
	    }
	    for ($i = 0; $i < sizeof($domain_array); $i++) {
	      if
	(!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|
	↪([A-Za-z0-9]+))$",
	$domain_array[$i])) {
	        return false;
	      }
	    }
	  }
	  return true;
	}

}
?>
