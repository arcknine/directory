<?php
class Patient
 {
 	var $data = array();
 	var $patientid = 0;
 	var $db = "";
 	var $msg = "";
 	
 	function __construct()
 	{
 		$this->db = new DBConnect();
 	}
 	
 	function getData($id)
 	{
 		$this->patientid = $id;
 		$sql = "select * from patient where id=$id";
 		$this->data =$this->db->getRecord($sql); 
 		return $this->data;
 	}
 	function add($arr)
 	{
 		
 		foreach($arr as $key=>$val)
 		{
 			$arr[$key] = trim(mysql_escape_string($val)); 
 		}
 
 		$sql = "insert into patient (`id`,`salutation`,`firstname`,`lastname`,`middlename`,`address`," .
 				"`birthday`,`nickname`,`email`,`father`,`fatherphone`," .
 				"`mother`,`motherphone`,`hmo`,`hmoid`,`company`," .
 				"`philhealth`,`otherinfo`,`sex`,`phone`,`status`,`referredby`,`userid`) 
 				values('NULL','".ucwords($arr['salutation'])."','".ucwords($arr['firstname'])."','".ucwords($arr['lastname'])."'," .
 				"'".$arr['middlename']."','".$arr['address']."','".$arr['birthday']."','".$arr['nickname']."'," .
				"'".$arr['email']."','".$arr['father']."'," .
				"'".$arr['fatherphone']."','".$arr['mother']."','".$arr['motherphone']."'," .
				"'".$arr['hmo']."','".$arr['hmoid']."','".$arr['company']."','".$arr['philhealth']."'," .
				"'".$arr['otherinfo']."','".$arr['sex']."','".$arr['phone']."','".$arr['status']."','".$arr['referredby']."',".
				"'".$arr['userid']."')";
		$lastid = $this->db->insert($sql);
		if($lastid > 0)
		{
			return $lastid;
		}
		else
		{
			echo $this->db->msg." - ".$sql;
		}
		
		
 	}
 	function update($arr)
 	{
 		foreach($arr as $key=>$val)
 		{
 			$arr[$key] = trim(mysql_escape_string($val)); 
 		}
 		if($arr['philhealth'] != 'yes')
 		{
 			$arr['philhealth'] = 'no';
 		}
 		$sql = "update patient set".
 				"`salutation`='".$arr['salutation']."',`firstname`='".$arr['firstname']."',`lastname`='".$arr['lastname']."',`middlename`='".$arr['middlename']."',`address`='".$arr['address']."'," .
 				"`birthday`='".$arr['birthday']."',`nickname`='".$arr['nickname']."',`email`='".$arr['email']."',`father`='".$arr['father']."',`fatherphone`='".$arr['fatherphone']."'," .
 				"`mother`='".$arr['mother']."',`motherphone`='".$arr['motherphone']."',`hmo`='".$arr['hmo']."',`hmoid`='".$arr['hmoid']."',`company`='".$arr['company']."'," .
 				"`philhealth`='".$arr['philhealth']."',`otherinfo`='".$arr['otherinfo']."',`sex`='".$arr['sex']."',`phone`='".$arr['phone']."'
 				,`status`='".$arr['status']."',`referredby`='".$arr['referredby']."' where id=".$arr['patientid'];
		$affected_rows =  $this->db->update($sql);
		$this->msg = $this->db->msg;
		return $affected_rows;
 	}
 	function photoUpdate($photoname,$id)
 	{
 		$sql = "update patient set photo='$photoname' where id=$id";
 		return $this->db->update($sql); 
 		
 	}
 	
 	function delete($patientid,$photo,$thumb)
 	{
 		$this->msg  = "";
 		$data = $this->getData($patientid);
 		if($data['photo'])
 		{
 			@ unlink($photo."/".$data['photo']) or $this->msg.="File cannot be deleted.<br>";
 			@ unlink($thumb."/".$data['photo']) or  $this->msg.="File cannot be deleted.<br>";
 		}
 		$sql = "select id from consult where patientid=$patientid";
 		$r = $this->db->getAllRecord($sql);
 		$consult = new Consult();
 		foreach($r as $val)
 		{
 			$consult->deleteConsult($val['id']);
 		}
 		$tables = array("appointment","consult","medcert","history");
 		foreach($tables as $table)
 		{
 			$sql = "delete from $table where patientid=$patientid";
 			$this->db->delete($sql);
 		}
 		
 		$sql = "delete from patient where id=$patientid";
 		if($this->db->delete($sql) == 1)
 		{
 			$this->msg = "Patient deleted successfully.";
 			return 1;
 		}
 		else
 		{
 			$this->msg = "Patient deletion FAILED.";
 			return 0;
 		}  
 	}
 }
?>
