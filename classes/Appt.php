<?php


class Appt
{
	
	var $db = "";
	var $msg = "";
	var $patientid = 0;
	function __construct()
	{
		$this->db = new DBConnect();
	}
	function setPatient($patientid)
	{	
		$this->patientid = $patientid;
	}
	
	function getAllPatientAppts($patientid)
	{
		$sql = "select * from appointment where patientid = $patientid";
		$arr = $this->db->getAllRecord($sql);
		$this->msg = $this->db->msg;
		return $arr;
	}
	
	function getAllApptsByDate($sqldate,$userid)
	{
		$sql = "select * from appointment where date(start)='$sqldate' and userid=$userid order by start asc";
		
		$arr = $this->db->getAllRecord($sql);
		$this->msg = $this->db->msg;
		return $arr;
	}
	
	
	function getTotal($date)
	{
			
	}
	
	function setAppt($startdate,$enddate,$visittype,$remark,$userid)
	{
		$remark = trim($remark);	
		$start = $startdate;
		$end = $enddate;
		
		$conflict_bool = false;
		if($start != $end)
		{
			$startend12h = "";
			$query = "select date_format(start,'%l:%i'),date_format(end,'%l:%i'),id from appointment 
				where (time(start) <= time('$start')  and time('$end') <= time(end))
				 and date(start)=date('$start') and userid=$userid";
			
			if(count($this->db->getAllRecord($query)))
			{
				$conflict_bool = true;
			} 
			echo $this->db->msg;
			
			$query = "select time(start),time('$start'),time(end),time('$end') from appointment
				where time(start) <= time('$start') and time('$start') < time(end) and time(end) <= time('$end') 
				and date(start)=date('$start')  and userid=$userid";
			if(count($this->db->getAllRecord($query)))
			{
				$conflict_bool = true;
			} 
			echo $this->db->msg;
			
	
			$query = "select time(start),time(end),time('$start'),time('$end') from appointment
				where time('$start') < time(start) and time('$end') <= time(end) and time(start) < time('$end') 
				and date(start)=date('$start')  and userid=$userid";
			
			if(count($this->db->getAllRecord($query)))
			{
				
				$conflict_bool = true;
			} 
			echo $this->db->msg;
			$query = "select time('$start'),time(start),time(end),time('$end') from appointment
				where time('$start') <= time(start)  and time(end) <= time('$end') and date(start)=date('$start')
				 and userid=$userid";
			
			if(count($this->db->getAllRecord($query)))
			{
				
				$conflict_bool = true;
			} 
			
				
		}
		else
		{
			return 0;
		}
		
		
		if($conflict_bool == false)
		{
			$sql = "insert into appointment set start='$startdate', end='$enddate', ".
				"patientid=".$this->patientid.",visittype='$visittype',status='APPT',remark='".$remark."'".
				",userid=$userid";
			
			$insertid = $this->db->insert($sql);
			if($insertid > 0)
			{
				
				$this->msg = $this->db->msg;
				return $insertid;
			}
			else
			{
				
				$this->msg = $this->db->msg;
				return 0;
			}
		}
		
			
	}
	function getAppt($appid)
	{
		
		return $apptid;
	}
	
	function delAppt($apptid)
	{
		$sql = "delete from appointment where id=$apptid";
		if($this->db->update($sql) > 0)
		{
			$this->msg = $this->db->msg;
			return 1;
		}
		else
		{
			return 0;
		}
		
		
	}
	
	function setVisit($appid,$visit)
	{
		$query = "update appointment set visittype='$visit' where id=$appid";
		$this->db->execute($query);
		
	}
	function getVisit($apptid)
	{
		$sql = "select * from appointment where id=$apptid";
		return $this->db->fetchRow($sql);
	}
	function setQstatus($appid,$qstatus)
	{
		$query = "select date(start) as date,arrive,astart,aend from appointment where id=$appid";
		
		
		$r = $this->db->getRecord($query);
		
		$date = $r['date'];
		$arrive = $r['arrive'];
		$astart = $r['astart'];
		$aend = $r['aend'];
		if($qstatus == "IN")
		{
			$query = "select id,aend from appointment where status='IN' and date(start) = '$date'";
			$result = $this->db->getAllRecord($query);
			foreach($result as $r)
			{
				if($r['aend'] == "0000-00-00 00:00:00")
				{
					$query = "update appointment set status='SEEN',aend=now() where status='$qstatus' 
					and date(start) = '$date'";
				}
				else
				{
					$query = "update appointment set status='SEEN' where status='$qstatus' and 
					date(start) = '$date'";
					
				}
				$this->db->update($query);
				
			}
			 
			
			if($astart == "0000-00-00 00:00:00")
			{
				if($arrive == "0000-00-00 00:00:00")
				{
					$query = "update appointment set status='$qstatus', astart=now(),arrive=now() where id=$appid";
				}
				else
				{
					$query = "update appointment set status='$qstatus', astart=now() where id=$appid";
				}
			}
			else
			{
				$query = "update appointment set status='$qstatus' where id=$appid";
			}
			
		}
		else if($qstatus == "SEEN")
		{
			if($aend == "0000-00-00 00:00:00")
			{
				if($arrive == "0000-00-00 00:00:00")
				{
					$query = "update appointment set status='$qstatus', aend=now(), arrive=now(), astart=now() where id=$appid";
				}
				else
				{
					$query = "update appointment set status='$qstatus', aend=now() where id=$appid";
				}
			}
			else
			{
				$query = "update appointment set status='$qstatus' where id=$appid";
			}
			
		}
		else if($qstatus == "WAIT") 
		{
			if($arrive == "0000-00-00 00:00:00")
			{
				$query = "update appointment set status='$qstatus', arrive=now() where id=$appid";
			}
			else
			{
				$query = "update appointment set status='$qstatus' where id=$appid";
			}
			
		}
		else if($qstatus == "NEXT") 
		{
			if($arrive == "0000-00-00 00:00:00")
			{
				$query = "update appointment set status='$qstatus', arrive=now() where id=$appid";
			}
			else
			{
				$query = "update appointment set status='$qstatus' where id=$appid";
			}
			
		}
		else
		{
			$query = "update appointment set status='$qstatus' where id=$appid";	
		}
		
		$this->db->update($query);
	}
	function getQstatus()
	{
		$sql = "select id,status from appointment where date(start)=curdate()";
		$result = $this->db->getAllRecord($sql);
		$arr = array();
		foreach($result as $row)
		{
			$arr[$row['id']] = $row['status'];
		}
		return $arr;
	}
	
	function getNextAppt($patientid)
	{
		$query = "select date_format(start,'%W'),date_format(start,'%M %e, %Y'), date_format(start,'%l:%i'),
		date_format(start,'%p'), date_format(end,'%l:%i'),
		date_format(end,'%p')  from appointment 
		where patientid=$patientid and current_date <= date(start)
		order by 1 desc";
		
		return $this->db->fetchAll($query);
	}
	
	function getStatus()
	{
		return array("IN","NEXT","WAIT","APPT","SEEN","CANC","NO SHOW","CALL");
	}
	function getVisitType()
	{
		return array("-","Consult","Follow up","New Patient","Walk In");
	}
}
?>