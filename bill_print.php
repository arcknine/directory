<?php
session_start();

/** Error reporting */
//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);

date_default_timezone_set('Asia/Manila');

//define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once 'classes/PHPExcel.php';

require_once("classes/connect.php");
require_once("classes/DBConnect.php");
require_once("classes/Auth.php");
require_once("classes/Util.php");
require_once("classes/Patient.php");
require_once("classes/Bill.php");
require_once("classes/Consult.php");

//print_r($_SESSION);

$auth_arr = $_SESSION['auth'];
$patient = new Patient();
$bill = new Bill();
$auth = new Auth();
$consult = new Consult();
$util = new Util();

$objPHPExcel = new PHPExcel();


$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('E1',$auth_arr['name'])
			->setCellValue('E2',strip_tags($auth->getClinicUsed($auth_arr['userid'])));
			
$date_range = "";
if($_SESSION['print_bill_enddate'] == $_SESSION['print_bill_startdate'])
{
	$date_range =  ' for '.$util->convertmysqldate($_SESSION['print_bill_startdate'],"M j, Y");
}
else
{
	$date_range =  ' from '.$util->convertmysqldate($_SESSION['print_bill_startdate'],"M j, Y"). ' to '.$util->convertmysqldate($_SESSION['print_bill_enddate'],"M j, Y");
}

$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('E4','Summary of In-Patient and Out-Patient Statements'.$date_range);
			
			
			
$row_i = 7;



$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('B'.$row_i,'Patient')
			->setCellValue('C'.$row_i,'Date')
			->setCellValue('D'.$row_i,'HMO/Company')
			->setCellValue('E'.$row_i,'Diagnosis')
			->setCellValue('F'.$row_i,'PF');

if($auth_arr['access'] == "admin"  || $auth_arr['access'] == "doctor")
{			
	$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('G'.$row_i,'Discount')
			->setCellValue('H'.$row_i,'Amount Due')
			->setCellValue('I'.$row_i,'Payment')
			->setCellValue('J'.$row_i,'Balance')
			->setCellValue('K'.$row_i,'Remarks');
}
else
{
	$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('G'.$row_i,'Payment')
			->setCellValue('H'.$row_i,'Balance')
			->setCellValue('I'.$row_i,'Remarks');
}
			
$sql = $_SESSION['print_bill_sql'];
$db = new DBConnect();
$arr = $db->getAllRecord($sql);
if($arr)
{
	$i = $row_i + 1; $c = 1;
	foreach($arr as $r)
	{
		$patient->getData($r['patientid']); 
		$patient_name = $patient->data['salutation']." ".$patient->data['firstname']." ".$patient->data['lastname'];
		$d = array();
		if(isset($r['hmo']) && $r['hmo']!='')
		{
			$d[] = 	'HMO: '.$r['hmo'];
		}
		if(isset($r['hmoid']) && $r['hmoid']!='')
		{
			$d[] = 	'HMO ID: '.$r['hmoid'];
		}
		if(isset($r['company']) && $r['company']!='')
		{
			$d[] = 	'Company: '.$r['company'];
		}
		if(isset($r['approvalcode']) && $r['approvalcode']!='')
		{
			$d[] = 	'Approval Code: '.$r['approvalcode'];
		}
		$hmo_str = implode(" ; ",$d);
		
		$soap = $consult->getConsultById($r['consultid']);
		
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i,$c)
			->setCellValue('B'.$i,$patient_name)
			->setCellValue('C'.$i,$r['date'])
			->setCellValue('D'.$i,$hmo_str)
			->setCellValue('E'.$i,strip_tags($soap['assessment']));

			
		if($auth_arr['access'] == "admin"  || $auth_arr['access'] == "doctor")
		{			
			$amount_paid = $bill->getTotalPayment($r['id']);
			if($r['discount'] > 0)
			{
				$amount_due = $r['profee']*(1 - $r['discount']/100);
			}
			else
			{
				$amount_due = $r['profee'];
			}
			
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('F'.$i,$r['profee'])
					->setCellValue('G'.$i,$r['discount'])
					->setCellValue('H'.$i,$amount_due)
					->setCellValue('I'.$i,$amount_paid)
					->setCellValue('J'.$i,$amount_due - $amount_paid)
					->setCellValue('K'.$i,$r['remark']);
		}
		else
		{
			$amount_paid = $bill->getTotalPayment($r['id']);
			if($r['discount'] > 0)
			{
				$amount_due = $r['profee']*(1 - $r['discount']/100);
			}
			else
			{
				$amount_due = $r['profee'];
			}echo $sql;
		
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('F'.$i,$amount_due)
					->setCellValue('G'.$i,$amount_paid)
					->setCellValue('H'.$i,$amount_due - $amount_paid)
					->setCellValue('I'.$i,$r['remark']);
		}
		$i++; $c++;
	}
}           
            
 
$objPHPExcel->getActiveSheet()->setTitle('Statements');
 
$objPHPExcel->setActiveSheetIndex(0);


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$print_filename = 'statement_'.$auth_arr['userid'].'.xls'; 
$objWriter->save(getcwd().'/tmp/'.$print_filename);

header("location:tmp/".$print_filename);
