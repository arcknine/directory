<style>
	select {width:90px}	
</style>
<?php

$util = new Util();
$db = new DBConnect();
$userid = $_SESSION['auth']['userid'];
$patientid = $_REQUEST['patientid'];
$patient = new Patient();
$patient->getData($patientid);
$middlename = "";
if($patient->data['middlename'] != "")
{
	$middlename = substr($patient->data['middlename'],0,1).".";
}
$patient_name =  $patient->data['salutation']." ".$patient->data['firstname']." $middlename ".$patient->data['lastname'];

if($patient->data['birthday'])
{
	$age_arr = $util->age($patient->data['birthday'],$_SESSION['year']."-".$_SESSION['mon']."-".$_SESSION['day']);
	
}


$sql = "select * from numbers  where userid=$userid ";
$doctor = $db->getRecord($sql);
?>
<br><br>
<fieldset><legend>Add New Referral Letter</legend>
	<form action=patient/referralpdf.php method=post>
	<input type=hidden name=patientid id=patientid value="<?php echo $patientid?>">
	<input type=hidden name=userid id=userid value="<?php echo $userid;?>">
	<input type=hidden name=age id=age value="<?php echo $age_arr['year']?>">
	<input type=hidden name=date value="<?php echo $_SESSION['datetoday']?>">
	<input type=hidden name=datesql value="<?php echo $_SESSION['year']."-".$_SESSION['mon']."-".$_SESSION['day'];?>"/>
	<p align=center style=font-size:16px;font-weight:bold>REFERRAL FORM</p>
	
	
	<div style=position:relative;left:10px;>Date: <?php echo $_SESSION['datetoday']?></div>
	<div style=position:relative;left:10px;>To: 
		<?php 
			$table = "referral";
			$col = "towhom";
			$sql = "select  distinct $col from $table where userid=$userid and $col != '' order by $col";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","style='width:90px' onchange=chooseType(\"sel_$col\",\"$col\")")."<br>";
			?>
		<input type=text name=towhom id=towhom class=input value="" size="50"/></div>
	<div style=position:relative;left:10px;padding-top:20px>Referring patient  
		<input type=text name=patient_name class=input value="<?php echo $patient_name;?>" size=50 />,
		<input type=text name=age class=input value="<?php echo $age_arr['year'];?>" size=3 /> y/o, 
		<input type=text name=sex class=input value="<?php echo strtolower($patient->data['sex']);?>" size=10 /> 
	</div>
	<table>
		<tr><td align=right>for</td>
			<td>
				<?php 
				$table = "referral";
				$col = "text1";
				$sql = "select  distinct $col from $table where userid=$userid and $col != '' order by $col";
				$sal_arr = $db->getOneCol($sql);
				if(count($sal_arr) > 0)
					echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","style='width:90px' onchange=chooseType(\"sel_$col\",\"$col\")")."<br>";
				?>
				<input type=text id="text1" name="text1" class=input size="50"/>.
				 
			</td>
		</tr>
		<tr>
			<!-- <td>and management of</td>
			
				<?php
				/*	 
				$table = "referral";
				$col = "text2";
				$sql = "select  distinct $col from $table where userid=$userid and $col != '' order by $col";
				$sal_arr = $db->getOneCol($sql);
				if(count($sal_arr) > 0)
					echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","style='width:90px' onchange=chooseType(\"sel_$col\",\"$col\")")."<br>";
				*/
				?>
				<input type=text id="text2" name="text2" class=input size="50"/>.
			 
			</td> 
			-->
		</tr>
	</table>
	<div style=position:relative;left:10px;>
		<table>
			<tr><td align=right style="vertical-align:top">Impression:</td>
			<td>
			<?php 
			$table = "referral";
			$col = "impression";
			$sql = "select  distinct $col from $table where userid=$userid and $col != '' order by $col";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","style='width:90px' onchange=chooseType(\"sel_$col\",\"$col\")")."<br>";
			?>
			<textarea cols=40 rows=2 name="impression" class=input id="impression"></textarea>
			 
			</td></tr>
			<tr><td><div style=position:relative;left:10px;>Thank you!</div></td></tr>
			<tr><td colspan=2 align=right><input type=submit name=submit value=Submit class=submit_button></td></tr>
		</table>
	</div>
	
</form>
</fieldset>

<?php

//echo $patient_name;

if(isset($_REQUEST['do']) && $_REQUEST['do'] == "del")
{
	$db->delete("delete from referral where id=".$_REQUEST['referralid']);
}

$age_arr = array();

$sql = "select * from referral where patientid=$patientid order by date desc";
$medarr = $db->getAllRecord($sql);
$row_count = $db->recordCount;

$start = 0;
if(isset($_REQUEST['start']))
{
	$start = $_REQUEST['start'];
}
$rowstoview = 10;
if(isset($_REQUEST['rowstoview']))
{
	$rowstoview = $_REQUEST['rowstoview'];
}

$count = $start + 1;
if($row_count > 0)
{
	echo '<br><br><br><a name=list></a>';
	echo "<table cellpadding=4 cellspacing=4 align=center class=view>";
	echo "<tr><th colspan=2>Date Issued</th><th>To</th><th>Reason for Referral</th><th>Impression</th></tr>";
	for($i = $start;$i < $start + $rowstoview;$i++)
	{
		if($i < $row_count)
		{
			$val = $medarr[$i];
			?>
			<form method=post action=patient/referralpdf.php target=_blank>
			<input type=hidden name=referralid value="<?php echo $val['id'];?>"/>
			<input type=hidden name=submit value="Reprint"/>
			<?php
			echo "<tr><td class=b_c>$count.</td><td>".$util->convertmysqldate($val['date'],"F j, Y")."</td>" .
				"<td align=center>".$val['towhom']."</td>" .
				"<td align=center>".$val['text1']."</td>" .
				"<td align=center>".$val['impression']."</td>" .
				"<td><input type=image name=submit value=Reprint src=image/printer.png></td><td>".$util->setEdel("","patient.php?pf=15&patientid=$patientid&do=del&referralid=".$val['id']."#list")."</td></tr>";
			?>
			</form>
			<?php
			$count++;
			
		}
	}
	echo "</table>";
	echo "<div align=center>".$util->navi("patient.php?pf=14&patientid=$patientid", $start, $rowstoview, $row_count, "image")."</div>";
}
?>
