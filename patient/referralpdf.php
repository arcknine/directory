<?php
error_reporting(E_ERROR);
session_start();
$userid = $_SESSION['auth']['userid'];
include ("../classes/connect.php");
include ("../classes/DBConnect.php");
include ("../classes/Patient.php");
include ("../classes/Util.php");
include ("../include/html2fpdf/fpdf.php");
include ("../include/html2fpdf/html2fpdf.php");

class XPDF extends HTML2FPDF {

	function Footer() {

	}
}
?>
<?php
$db = new DBConnect();

//== DR DETAILS ===========
$sql = "select * from numbers  where userid=$userid ";
$doctor = $db->getRecord($sql);
$util = new Util();

$patientid = $_REQUEST['patientid'];
$patient = new Patient();
$data = $patient->getData($patientid);
$age_arr = $util->age($data['birthday'],"");

$datesql = $_REQUEST['datesql'];
$date = $_REQUEST['date'];

$show = true;
if($_REQUEST['submit'] == "Reprint")
{
	$sql = "select * from referral where id=".$_REQUEST['referralid'];
	$_REQUEST = $db->getRecord($sql);
	$data = $patient->getData($_REQUEST['patientid']);
	$date = $_REQUEST['date'];
	$_REQUEST['patient_name'] = $data['firstname'].' '.$data['lastname'];
	$date = $util->convertmysqldate($date,"F j, Y");
	
	$age_arr = $util->age($patient->data['birthday'],$_SESSION['year']."-".$_SESSION['mon']."-".$_SESSION['day']);
	$_REQUEST['age'] = $age_arr['year'];
	$_REQUEST['sex'] = strtolower($data['sex']);
	
}
else
{
	$db->setTable("referral");
	$_POST['date'] = $datesql;
	$_POST['userid'] = $userid;
	$db->insertArr($_POST);
	 
}

if($show) 
{

	$pdf = new XPDF("P", "mm", array (
		140,
		215
	));

	//$pdf->DisplayPreferences();
	$pdf->DisplayPreferences('HideWindowUI');
	$pdf->AddPage();
	$mleft = $pdf->lMargin;
	$mright = $pdf->rMargin;
	$writinglength = 140 - $mleft - $mright;
	

	//============= H E A D E R ==========================================================	

	$pdf->SetFontSize("18px");
	$pdf->Cell($writinglength, "5", $doctor['name'], 0, 0, 'C');
	$pdf->SetY($pdf->GetY());	
	$pdf->SetFontSize("10px");
	$clinic = "";
	if($doctor['citouse'] == 1)
	{
		$clinic = $doctor['clinicinfo1'];
	}
	elseif($doctor['citouse'] == 2)
	{
		$clinic = $doctor['clinicinfo2'];
	}
	elseif($doctor['citouse'] == 3)
	{
		$clinic = $doctor['clinicinfo3'];
	}
	$pdf->WriteHTML("<p align=center>".stripslashes($clinic)."</p>");
	$pdf->Line($mleft, $pdf->GetY(), $writinglength +13, $pdf->GetY());
	$pdf->Line($mleft, $pdf->GetY() + 1, $writinglength +13, $pdf->GetY() + 1);
	
	$pdf->Cell("",5, "", 0, 1);
	$pdf->Cell("","5","REFERRAL FORM", 0, 0, 'C');
	$pdf->SetFontSize("10px");
	
 	$pdf->SetMargins(15,20,15,10);
	$pdf->Cell("",20, "", 0, 1);
	$pdf->Cell("",5,$date,0,1,"R");
	$pdf->Cell("",5, "", 0, 1);
	$pdf->Cell("",5,"To: ".$_REQUEST['towhom'], 0, 1);
	$pdf->Cell("",5, "", 0, 1);
	$pdf->MultiCell("",5,"Referring patient ".$_REQUEST['patient_name'].", ".$_REQUEST['age']." y/o, ".$_REQUEST['sex']." for ".$_REQUEST['text1'].".", 0,"J");
	$pdf->Cell("",8, "", 0, 1);
	
	
	$pdf->MultiCell("",5,"Impression: ".stripslashes(trim($_REQUEST['impression'])),0,1,"J");
	$pdf->Cell("",3, "", 0, 1);
	$pdf->Cell("",8, "Thank you!", 0, 1);
	$indention = 80;
	$max = max($pdf->GetStringWidth("Lic. No. "),$pdf->GetStringWidth("PTR No. "), $pdf->GetStringWidth("S2. No. "));
	$indention = 140 - $mright - $pdf->GetStringWidth(html_entity_decode($doctor['name'])); 
	if($max >  $pdf->GetStringWidth(html_entity_decode($doctor['name'])))
	{
		$indention = 80;
	}

	$pdf->SetXY($indention, 174);
	$pdf->cell("", 5, html_entity_decode($doctor['name']), 0, 1);
	$pdf->SetXY($indention, 179);
	$pdf->cell($max, 5, "Lic. No. ",0, 0,"l");
	$pdf->cell("", 5, $doctor['license'], 0, 1);
	$pdf->SetXY($indention, 184);
	$pdf->cell($max, 5, "PTR No. ", 0, 0,"l");
	$pdf->cell("", 5, $doctor['ptr'], 0, 1);
	$pdf->SetXY($indention, 189);
	if ($_REQUEST['s2'] == "on") {
		$pdf->cell($max, 5, "S2. No. ",0, 0,"l");
		$pdf->cell("", 5, $doctor['s2'], 0, 1);
		$pdf->SetXY($mleft, 215);
	}
	
	$pdf->Output("referral_".rand().".pdf", 'D');
}
?>