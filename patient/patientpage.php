<div id=patientpage>

<?php
	
	
	$db = new DBConnect();
	$util = new Util();
	$patient = new Patient();
	$msg = "";
	$patientid = $_REQUEST['patientid'];
	$edit = false;
	
	//$data = $patient->getData($patientid);
	//print_r($db->getColumns("patient"));

if(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "edit")
{
	$edit = true;
}


?>


<?php echo $msg?>

<?php
/*
 * === EDIT PATIENT INFORMATION ================================================
 */
if($edit)
{
?>

<p class=pageTitle2> &nbsp;Patient Information</p>
<form  enctype="multipart/form-data" method="post" action="patient/patientupdate.php?patientid=<?php echo $patientid?>">
<table>
<tr><td>
<table cellpadding="2" cellspacing="2">
  <tr>
    <th colspan="2" nowrap="nowrap"></th>
  </tr>
  <tr>
    <th nowrap="nowrap"><div align="right">Photo</div></th>
    <td>
      <input type="file" name="photo" id="photo" />    </td>
  </tr>
  <tr>
    <th width="194" nowrap="nowrap">
      <div align="right">First Name </div></th>
    <td width="411">
    <input name="firstname" type="text" id="firstname" size="50" value="<?php echo $data['firstname']?>" class="input">    </td>
  </tr>
    <tr>
    <th>
    	<div align="right">Last Name       </div></th>
      <td><input name="lastname" type="text" id="lastname" size="50"value="<?php echo $data['lastname']?>" class="input"></td>
    </tr>
    <tr><th><div align="right">Middle Name</div></th>
    
      <td><input name="middlename" type="text" id="middlename"  value="<?php echo $data['middlename']?>" class="input" size=50></td>
  </tr>
  <tr>
		<th>
		<div align="right">Nick Name</div>
		</th>
		<td>
			<input type=text name=nickname value="<?php echo $data['nickname']?>" class="input"vvv size=50>
		</td>
	</tr>
    <tr>
      <th><div align="right">Sex</div></th>
      <td>        
      <?php
      $male = ""; $female = "";
      if($data['sex'] == "Male")
      {
      	$male = "checked";
      }
      else
      {
      	$female = "checked";
      }
      
      
      ?>
      	<input type="radio" name="sex" id="sex"  value=Male <?php echo $male?>/>
        Male       <input type="radio" name="sex" id="sex" value=Female <?php echo $female?>/> 
        Female</td>
    </tr>
    <tr>
      <th><div align="right">Date of Birth</div></th>
      <td>        
      <?php 
			$today = getDate();
			//echo $data['birthday']; 
			if($data['birthday'] != "0000-00-00")
			{
				echo $util->selectNewDateNoCalendar("by","bm","bd","100","1",$data['birthday']);
			}
			else
			{
				echo $util->selectNewDateNoCalendar("by","bm","bd","100","1",$today['year']."-".$today['mon']."-".$today['mday']);
			}
      ?>
      </td>
    </tr>
   <tr><th align=right>Address</th><td><input type=text name=address value="<?php echo $data['address']?>" class="input" size=50></td></tr>
   <tr><th align=right>Email</th><td><input type=text name=email value="<?php echo $data['email']?>" class="input" size=50></td></tr>
   <tr><th align=right>Phone</th><td><input type=text name=phone value="<?php echo $data['phone']?>" class="input" size=50></td></tr>
   <tr><th align="right">Status</th><td>
		<?php 
		$table = "patient";
		$select = "sel_stat";
		$input = "status";
		@$sal_arr = $db->getOneCol("select distinct $input from $table where userid=$userid order by $input");
		if(count($sal_arr) > 1)
		{
			echo $util->getHtmlSelect($sal_arr,$sal_arr,"$select","$input","onchange=chooseType(\"$select\",\"$input\")"); 
		}
		?>
		<input type=text name=status id=status size=30 value="<?php echo $data['status']?>" class="input">
		</td>
	</tr>
	<tr><th align="right">Referred By</th><td>
		<?php 
		$table = "patient";
		$select = "sel_ref";
		$input = "referredby";
		@$sal_arr = $db->getOneCol("select distinct $input from $table  where userid=$userid  order by $input");
		if(count($sal_arr) > 1)
		{
			echo $util->getHtmlSelect($sal_arr,$sal_arr,"$select","$input","onchange=chooseType(\"$select\",\"$input\")"); 
		}
		?>
		<input type=text name=referredby id=referredby size=30 value="<?php echo $data['referredby']?>" class="input">
		</td>
	</tr>
	</table>
	
	</td><td>
	
	<table cellpadding="2" cellspacing="2">
	
   <tr><th align=right>PARENTS</th></tr>
   <tr><th align=right>Father</th><td><input type=text name=father value="<?php echo $data['father']?>" class="input" size=50></td></tr>
   <tr><th align=right>Phone</th><td><input type=text name=fatherphone value="<?php echo $data['fatherphone']?>" class="input" size=50></td></tr>
	<tr><th align=right>Mother</th><td><input type=text name=mother value="<?php echo $data['mother']?>" class="input" size=50></td></tr>
	<tr><th align=right>Phone</th><td><input type=text name=motherphone value="<?php echo $data['motherphone']?>" class="input" size=50></td></tr>
	<tr><th align="right">HMO</th><td>
		<?php 
		$table = "patient";
		$select = "sel_hmo";
		$input = "hmo";
		@$sal_arr = $db->getOneCol("select distinct $input from $table  userid=$userid  order by $input");
		if(count($sal_arr) > 1)
		{
			echo $util->getHtmlSelect($sal_arr,$sal_arr,"$select","$input","onchange=chooseType(\"$select\",\"$input\")"); 
		}
		?>
		<input type=text name=hmo id=hmo size=30 value="<?php echo $data['hmo']?>" class="input">
		</td>
	</tr>
	<tr><th align="right">HMO ID</th><td><input type=text name=hmoid id=hmoid size=30 value="<?php echo $data['hmoid']?>" class="input"></td></tr>
	<tr><th align="right">Company</th><td>
		<?php 
		$table = "patient";
		$select = "sel_co";
		$input = "company";
		@$sal_arr = $db->getOneCol("select distinct $input from $table where userid=$userid order by $input");
		if(count($sal_arr) > 1)
		{
			echo $util->getHtmlSelect($sal_arr,$sal_arr,"$select","$input","onchange=chooseType(\"$select\",\"$input\")"); 
		}
		?>
		<input type=text name=company id=company size=30 value="<?php echo $data['company']?>" class="input">
		</td>
	</tr>
	<tr><th align=right>Philhealth</th><td>
		<?php 
		$philhealth_checked = "";
		if($data['philhealth'] == "yes")
		{
			$philhealth_checked = "checked";
		}		
		?>
		<input type=checkbox name=philhealth value="yes" <?php echo $philhealth_checked?>></td></tr>
	<tr align=right><th>Other Info</th><td align=left><textarea cols=50 rows=5 name=otherinfo class="input"><?php echo $data['otherinfo']?></textarea></td></tr>
</table>
</td></tr>
<tr><td colspan=2 align=center>
<input type="submit" name="submit" id="submit" value="Cancel" class="cancel_button"/>
<input type="submit" name="submit" id="submit" value="Update" class="update_button"/>
</td></tr>
</table>

</form>

<?php
} //=== END EDIT ============================================================================
else
{
/*
 * === SHOW PATIENT INFO ====================================================================
 */

 ?>
 <p class=pageTitle2> &nbsp;Patient Information</p>
 <table border="0" cellpadding=2 cellspacing=2 width=100%>
 <tr><td width=50%>
 <table border="0" cellpadding=2 cellspacing=2 class=patient_information>
	<tr>
		<!-- <th colspan="2" align="left" class="tab_hd"></th> -->
	</tr>

	<tr>
		<th align="right">Photograph</th>
		<td align="center" nowrap style="min-width: 150px">
			
			<?php
			if(file_exists($_SESSION['photothumb']."/".$data['photo']))
			{
				
				echo "<img src='".$_SESSION['urlthumb']."/".$data['photo']."'>";
			}
			?>
		</td>
	</tr>
	<tr><th align="right">Salutation</th><td align=center>
			<?php echo $data['salutation']?></td>
	</tr>
	<tr>
		<th width="139" align="right">First Name</th><td align=center>
			<?php echo $data['firstname']?>
		</td>
	</tr>
	<tr>
		<th width="139" align="right">Last Name</th><td align=center>
			<?php echo $data['lastname']?>
		</td>
	</tr>
	<tr>
		<th width="139" align="right">Middle Name</th><td align=center>
			<?php echo $data['middlename']?>
		</td>
	</tr>
	<tr>
		<th>
		<div align="right">Nick Name</div>
		</th>
		<td align=center>
			<?php echo $data['nickname']?>
		</td>
	</tr>
	<tr>
		<th>
		<div align="right">Sex</div>
		</th>
		<td align=center>
			<?php 
				if($data['sex'] == "Male")
				{
					echo "Male";
				}
				else
				{
					echo "Female";	
				}
			?>
		</td>
	</tr>
	<tr>
		<th>
		<div align="right">Birthday</div>
		</th>
		<td align=center>
		<?php echo $util->convertmysqldate($data['birthday'],"F j, Y")?>
		</td>
	</tr>
	<tr>
		<th>
		<div align="right">Address</div>
		</th>
		<td align=center>
			<?php echo $data['address']?></td>
	</tr>
	<tr>
		<th>
		<div align="right">Email</div>
		</th>
		<td align=center>
			<?php echo $data['email']?>		
		</td>
	</tr>
	
	<tr>
		<th align="right">
		Telephone Nos
		</th>
		<td align=center>
			<?php echo $data['phone']?></td>
	</tr>
	<tr>
		<th align="right">
		Status
		</th>
		<td align=center>
			<?php echo $data['status']?></td>
	</tr>
</table>
</td>
<td width=50%>
<!-- HATI -->	
<table border="0" cellpadding=2 cellspacing=2 class=patient_information >
	
	<tr>
		<th align="right">
		Referred By
		</th>
		<td align=center style="min-width: 150px">
			<?php echo $data['referredby']?></td>
	</tr>
	<tr>
		<th>
		<div align="right">PARENTS</div>
		</th>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<th>
		<div align=right>Father</div>
		</th>
		<td align=center>
		<?php echo $data['father']?>
		</td>
	</tr>
	<tr>
		<th align=right>
		Phone Nos
		</th>
		<td align=center>
		<?php echo $data['fatherphone']?>
		</td>
	</tr>
	<tr>
		<th>
		<div align=right>Mother</div>
		</th>
		<td align=center>
		<?php echo $data['mother']?>
		</td>
	</tr>
	<tr>
		<th align=right>
		Phone Nos
		</th>
		<td align=center>
		<?php echo $data['motherphone']?></td>
	</tr>	
	<tr>
		<th align="right">HMO</th>
		<td align=center>
		<?php echo $data['hmo']?>
		</td>
	</tr>
	<tr>
		<th align="right">HMO ID</th>
		<td align=center>
		<?php echo $data['hmoid']?>
		</td>
	</tr>
	<tr>
		<th align="right">Company</th>
		<td align=center>
		<?php echo $data['company']?>
		</td>
	</tr>
	<tr>
		<th align="right">Philhealth</th>
		<td align="center">
		<?php
			if($data['philhealth'] == "yes")
			{
				echo "yes";
			}
			else
			{
				echo "no";
			}
		?>
		</td>
	</tr>
	<tr>
		<th align="right">Other Info</th>
		<td align="center">
		<?php echo $data['otherinfo']?>
		</td>
	</tr>
 </table>
 </td></tr>
 <tr><td colspan=2 align=center>
 <a href="patient.php?pf=1&submit=edit&patientid=<?php echo $patientid?>" <?php echo $util->mouseover("Edit",50)?> class=update_button><!-- <img src=image/edit.png border=0> --> Edit </a>
 </td></tr>
 </table>

<?php
}
?>
 </div>
