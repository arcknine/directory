<?php 
session_start();
header("Content-Type: application/msword");
include("../classes/connect.php");
include("../classes/DBConnect.php");
include("../classes/Util.php");
include("../classes/Consult.php");
include("../classes/Patient.php");

$db = new DBConnect();
$util = new Util();
$consult = new Consult();
$patient  = new Patient();
$userid = $_SESSION['auth']['userid'];

$consultid = 0;
$consult_arr = array();
$consult_date = "";


$patientid = $_REQUEST['patientid'];
$data = $patient->getData($patientid);

?>
<html>
<head>
	<title>SURGICAL HISTORY  REPORT</title>
	<style>
		* {font-family:arial; font-size:12px}
		.tdl {border-bottom: 1px solid black}
		table {empty-cells:show;}
		td.cv {text-align:center;vertical-align:top}
	</style>
</head>
<body>
<p align=center>
<?php 
$doctor = $db->getRecord("select * from numbers where userid=$userid");
?>
<span style=font-size:150%><?php echo $doctor['name'];?></span>
<br><?php
 $clinic = "";
if($doctor['citouse'] == 1)
{
	$clinic = $doctor['clinicinfo1'];
}
elseif($doctor['citouse'] == 2)
{
	$clinic = $doctor['clinicinfo2'];
}
elseif($doctor['citouse'] == 3)
{
	$clinic = $doctor['clinicinfo3'];
}
echo $clinic;

$left = array();
$right = array();

$sql = "select * from sh where id=".$_REQUEST['id'];
$r = $db->getRecord($sql);

if($data['middlename'] != "")
{
	$middlename = substr($data['middlename'],0,1).". ";
}
?>
</p>
<br>
<p align=center style="letter-spacing:2px;font-size:120%;font-weight:bold">Surgical History Report</p>
<p><?php echo date("F j, Y");?><br><br>
Patient name: <?php echo $data['firstname']." $middlename ".$data['lastname'];?></p>

<table rules=rows cellpadding=2 cellspacing=2 style=margin-left:auto;margin-right:auto>
			<tr><td align=right>Diagnosis:</td><td class=cv><?php echo $r['shdiag'];?></td></tr>
			<tr><td align=right>Procedure:</td><td class=cv><?php echo $r['shproc'];?></td></tr>
			<tr><td align=right>Date:</td><td class=cv><?php echo $util->convertmysqldate($r['date'],"F j, Y");?></td></tr>
			<tr><td align=right>Hospital:</td><td class=cv><?php echo $r['hospital'];?></td></tr>
			<tr><td align=right>Anesthesia:</td><td class=cv><?php echo $r['anesthesia']; ?> </td></tr>
			<tr><td align=right>Anesthesiologist:</td><td class=cv><?php echo $r['anesthesiologist'];?> </td></tr>
			<tr><td align=right>Findings:</td><td class=cv><?php echo $r['finding'];?></td></tr>
			<tr><td align=right>Technique:</td><td class=cv><?php echo $r['technique'];?></td></tr>
			<tr><td align=right>Histopath:</td><td class=cv><?php echo $r['result'];?></td></tr>
			<tr><td align=right>Remarks:</td><td class=cv><?php echo $r['remark'];?></td></tr>		
		</table>

<?php 
echo "<br><br><br><br><p style=padding-top:60px>";


echo "<br>".$doctor['name']."<br>";
echo "Lic. No. ".$doctor['license']."<br>";
echo "PTR  No. ".$doctor['ptr']."<br>";
?>	
</body>
</html>