<?php
session_start();
$userid = $_SESSION['auth']['userid'];

include ("../classes/connect.php");
include ("../classes/DBConnect.php");
include ("../classes/Patient.php");
include ("../classes/Util.php");
include ("../include/html2fpdf/fpdf.php");
include ("../include/html2fpdf/html2fpdf.php");


class XPDF extends HTML2FPDF {

	function Footer() {

	}
}
?>
<?php


$db = new DBConnect();

//== DR DETAILS ===========
$sql = "select * from numbers where userid=$userid";
$doctor = $db->getRecord($sql);
$util = new Util();

$patientid = $_REQUEST['patientid'];
$patient = new Patient();
$data = $patient->getData($patientid);
$age_arr = $util->age($data['birthday'],"");
$title = $_REQUEST['title'];

$middlename = "";
if(isset($data['middlename']) && strlen($data['middlename']) > 0)
	$middlename = ucwords(substr($data['middlename'],0,1)).".";

$show = true;

$name = $data['salutation']." ".$data['firstname']." ".$middlename." ".$data['lastname'];

$impression = "";
if(isset($_REQUEST['impression'])) { $impression = mysql_real_escape_string($_REQUEST['impression']);}

$remark = "";
if(isset($_REQUEST['remark'])) { $remark = mysql_real_escape_string($_REQUEST['remark']); }

$to = "";
if(isset($_REQUEST['to'])) { $to = mysql_real_escape_string($_REQUEST['to']);}

$dateseen = "";
if(isset($_REQUEST['dateseen'])) { $dateseen = $_REQUEST['dateseen'];}


$lab = array();

if(isset($_REQUEST['do']) && $_REQUEST['do'] == "reprint")
{
	$x = array();
	$sql = "select * from labrequest where id=".$_REQUEST['labid'];
	$r = $db->getRecord($sql);
	
	$to = $r['to'];
	$impression = $r['impression'];
	$remark = $r['remark'];
	$x = explode(",",$r['labid']);
	$title = $r['title'];	
	if(count($x)>0)
	{
		foreach($x as $val)
		{
			$labname = $db->getRecord("select name from labrequestdb where id=".$val);
			$lab[] = $labname['name'];
		}
	}
	
}
else
{
	$labreq_arr = array();
	if(isset($_REQUEST['labrequest']))
	{
		$labreq_arr = $_REQUEST['labrequest'];
	}
	if($labreq_arr)
	{
		foreach($labreq_arr as $val)
		{
			$labname = $db->getRecord("select name from labrequestdb where id=".$val);
			$lab[] = $labname['name'];	
		}
	
		$sql = "insert into labrequest (`id`,`patientid`,`to`,`labid`,`date`,`impression`,`remark`,`userid`,`title`)" .
				"values(NULL,'$patientid','$to','".implode(",",$labreq_arr)."',now(),'$impression','$remark','$userid','$title')";
		$db->insert($sql);
	}
}
	
	$html="";
	if(count($lab) > 5)
	{
		$html.="<table align=center width=70%>"; $count=0;
		foreach($lab as $val)
		{
			
			if($count%2 == 0)
		  	{
		  		$html.="<tr><td>- $val</td>";
		  	}
		  	else
		  	{
		  		$html.="<td>- $val</td></tr>";
		  	}
		  	$count++;
		}
		if($count%2==1)
		{
			$html.="</tr>";
		}
	  	$html.="</table>";
	}
	else
	{
		
		$html.="<ul>";
		foreach($lab as $val)
		{
			$html.="<li>".$val;
		}
	  	$html.="</ul>";
	}


if ($show) 
{

	$pdf = new XPDF("P", "mm", array (
		140,
		215
	));

	
	$pdf->DisplayPreferences('HideWindowUI');
	$pdf->AddPage();
	$mleft = $pdf->lMargin;
	$mright = $pdf->rMargin;
	$writinglength = 140 - $mleft - $mright;
	
	//============= H E A D E R ==========================================================	
	$pdf->SetFontSize("16px");
	$pdf->Cell($writinglength, "5", $doctor['name'], 0, 0, 'C');
	$pdf->SetY($pdf->GetY());
	$pdf->SetFontSize("10px");
	$clinic = "";
	if($doctor['citouse'] == 1)
	{
		$clinic = $doctor['clinicinfo1'];
	}
	elseif($doctor['citouse'] == 2)
	{
		$clinic = $doctor['clinicinfo2'];
	}
	elseif($doctor['citouse'] == 3)
	{
		$clinic = $doctor['clinicinfo3'];
	}
	$pdf->WriteHTML("<p align=center>".stripslashes($clinic)."</p>");

	$pdf->Line($mleft, $pdf->GetY(), $writinglength +13, $pdf->GetY());
	$pdf->Line($mleft, $pdf->GetY() + 1, $writinglength +13, $pdf->GetY() + 1);
	$pdf->SetFontSize("12px");
	
	$date = $_SESSION['datetoday'];
	$name = "Name: " . $name;
	$age_arr =  $util->age($data['birthday'],$_SESSION['year']."-".$_SESSION['mon']."-".$_SESSION['day']);
	
	$agesex = "Age: " . $age_arr['year'] ." yrs. old";
	$pdf->Cell("", 5, "", 0, 1);
	$pdf->Cell(50,5,$date,0,1,"L");
	$pdf->Cell("", 5, "", 0, 1);
	$pdf->MultiCell("",5,$name,0,"L");
	$pdf->Cell("",5,$agesex,0,0,"L");
				
		
	
	$pdf->Cell("", 10, "", 0, 1);
	$pdf->SetFontSize("16px");
	$pdf->Cell("", 5,strtoupper($title)." REQUEST FORM", 0, 1,"C");
	$pdf->SetFontSize("12px");
	
	//============= LABORATORY REQUEST HERE ====================
	$pdf->Cell("", 5,"", 0, 1);
	if($to != "")
	{
		$pdf->MultiCell("", 5,"To: ".stripslashes($to), 0, 1,"L");
		$pdf->Cell("", 5, "", 0, 1);
	}
	
	$pdf->WriteHTML("<p>Request for:</p>");
	$pdf->WriteHTML(stripslashes($html));
	
	$pdf->Cell("", 5, "", 0, 1);
	if($impression != "")
	{
		$pdf->MultiCell("",5,"Impression: ".stripslashes($impression),0,1,"C");
		$pdf->Cell("", 5, "", 0, 1);
	}
	if($remark != "")
	{
		$pdf->MultiCell("",5,"Remarks: ".stripslashes($remark),0,1,"L");
		$pdf->Cell("", 5, "", 0, 1);
	}


	//============== END MEDICAL CERTIFICATE HERE  =============================
	$max = max($pdf->GetStringWidth("Lic. No. "),$pdf->GetStringWidth("PTR No. "), $pdf->GetStringWidth("S2. No. "));
	$indention = 140 - $mright - $pdf->GetStringWidth($doctor['name']); 
	if($max >  $pdf->GetStringWidth($doctor['name']))
	{
		$indention = 80;
	}
	
	$pdf->SetXY($indention, 174);
	$pdf->cell("", 5, $doctor['name'], 0, 1);
	$pdf->SetXY($indention, 179);
	$pdf->cell($max, 5, "Lic. No. ",0, 0,"l");
	$pdf->cell("", 5, $doctor['license'], 0, 1);
	$pdf->SetXY($indention, 184);
	$pdf->cell($max, 5, "PTR No. ", 0, 0,"l");
	$pdf->cell("", 5, $doctor['ptr'], 0, 1);
	$pdf->SetXY($indention, 189);
	if ($_REQUEST['s2'] == "on") {
		$pdf->cell($max, 5, "S2. No. ",0, 0,"l");
		$pdf->cell("", 5, $doctor['s2'], 0, 1);
		$pdf->SetXY($mleft, 215);
	}
				
	$pdf->Output("labrequest_".rand().".pdf", 'D');
}
?>