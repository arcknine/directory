<?php 
session_start();
header("Content-Type: application/msword");
include("../classes/connect.php");
include("../classes/DBConnect.php");
include("../classes/Util.php");
include("../classes/Consult.php");
include("../classes/Patient.php");

$db = new DBConnect();
$util = new Util();
$consult = new Consult();
$patient  = new Patient();
$userid = $_SESSION['auth']['userid'];

$consultid = 0;
$consult_arr = array();
$consult_date = "";
if(isset($_REQUEST['consultid']) && $_REQUEST['consultid'] > 0)
{
	$consultid = $_REQUEST['consultid'];
	$consult_arr = $db->getRecord("select * from consult where id=$consultid");
	echo $db->msg;
	$consult_date =  $consult_arr['date'];
}

$patientid = $_REQUEST['patientid'];
$data = $patient->getData($patientid);
//print_r($data);
?>
<html>
<head>
	<title>HOSPITAL ADMISSION REPORT</title>
	<style>
		* {font-family:arial; font-size:12px}
		.tdl {border-bottom: 1px solid black}
		table {empty-cells:show;}
		td.cv {text-align:center;vertical-align:top}
	</style>
</head>
<body>
<p align=center>
<?php 
$doctor = $db->getRecord("select * from numbers where userid=$userid");
?>
<span style=font-size:150%><?php echo $doctor['name'];?></span>
<br><?php
 $clinic = "";
if($doctor['citouse'] == 1)
{
	$clinic = $doctor['clinicinfo1'];
}
elseif($doctor['citouse'] == 2)
{
	$clinic = $doctor['clinicinfo2'];
}
elseif($doctor['citouse'] == 3)
{
	$clinic = $doctor['clinicinfo3'];
}
echo $clinic;

$left = array();
$right = array();

$sql = "select * from inpatient where id=".$_REQUEST['id'];
$r = $db->getRecord($sql);

if($data['middlename'] != "")
{
	$middlename = substr($data['middlename'],0,1).". ";
}
?>
</p>
<br>
<p align=center style="letter-spacing:2px;font-size:120%;font-weight:bold">Hospital Admission Report</p>
<p><?php echo date("F j, Y");?><br><br>
Patient name: <?php echo $data['firstname']." $middlename ".$data['lastname'];?></p>

<table rules=rows cellpadding=2 cellspacing=2 style=margin-left:auto;margin-right:auto>
			<tr><td align=right>Attending Physician:</td><td class=cv><?php echo $r['attendphy'];?></td></tr>
			<tr><td align=right>Hospital:</td><td class=cv><?php echo $r['hospital'];?></td></tr>
			<tr><td align=right>Room No.:</td><td class=cv><?php echo $r['room'];?></td></tr>
			<tr><td align=right>ID No.:</td><td class=cv><?php echo $r['pin'];?></td></tr>
			<tr><td align=right>Date Admitted:</td><td class=cv><?php echo $util->convertmysqldate($r['dateadmit'],"F j, Y"); ?> </td></tr>
			<tr><td align=right>Date Discharged:</td><td class=cv><?php echo $util->convertmysqldate($r['datedischarge'],"F j, Y");?> </td></tr>
			<tr><td align=right>Chief Complaint:</td><td class=cv><?php echo $r['reason'];?></td></tr>
			<tr><td align=right>Pertinent P.E. Findings:</td><td class=cv><?php echo $r['finding'];?></td></tr>
			<tr><td align=right>Final Diagnosis:</td><td class=cv><?php echo $r['diagnosis'];?></td></tr>
			<tr><td align=right>Diagnostic Procedures:</td><td class=cv><?php echo $r['procedure'];?></td></tr>
			<tr><td align=right>Surgical Procedures:</td><td class=cv><?php echo $r['surgical'];?></td></tr>
			<tr><td align=right>Treatment:</td><td class=cv><?php echo $r['treatment'];?></td></tr>
			<tr><td align=right>Other Physicians:</td><td class=cv><?php echo $r['otherphy'];?></td></tr>
			<tr><td align=right>Condition on Discharged:</td><td class=cv><?php echo $r['condition'];?></td></tr>
			<tr><td align=right>Discharged Medications:</td><td class=cv><?php echo $r['medication'];?></td></tr>
			<tr><td align=right>Discharged Instructions:</td><td class=cv><?php echo $r['instruction'];?></td></tr>
			<tr><td align=right>Remarks:</td><td class=cv><?php echo $r['remark'];?></td></tr>		
		</table>

<?php 
echo "<br><br><br><br><p style=padding-top:60px>";


echo "<br>".$doctor['name']."<br>";
echo "Lic. No. ".$doctor['license']."<br>";
echo "PTR  No. ".$doctor['ptr']."<br>";
?>	