<?php
session_start();
$userid = $_SESSION['auth']['userid'];
include ("../classes/connect.php");
include ("../classes/DBConnect.php");
include ("../classes/Patient.php");
include ("../classes/Util.php");
include ("../include/html2fpdf/fpdf.php");
include ("../include/html2fpdf/html2fpdf.php");


class XPDF extends HTML2FPDF {

	function Footer() {

	}
}
?>
<?php


$db = new DBConnect();

//== DR DETAILS ===========
$sql = "select * from numbers  where userid=$userid ";
$doctor = $db->getRecord($sql);
$util = new Util();

$patientid = $_REQUEST['patientid'];
$patient = new Patient();
$data = $patient->getData($patientid);
$age_arr = $util->age($data['birthday'],"");

//$show = false;
$show = true;
//echo getParameters($_REQUEST);
//print_r($_REQUEST['prescription']);

$name = $_REQUEST['name'];
$diagnosis = $_REQUEST['diagnosis'];
$consult_reason = $_REQUEST['consult_reason'];
$to1 = $_REQUEST['to1'];
$dateseen = $_REQUEST['dateseen'];



if($_REQUEST['submit'] == "Reprint")
{
}
else
{
	$sql = "insert into medcert (`id`,`patientid`,`consult_reason`,`diagnosis`,`date`,`to1`,`dateseen`,`remark`,`userid`)" .
			"values(NULL,'$patientid','".mysql_real_escape_string($consult_reason)."','".mysql_real_escape_string($diagnosis)."',now(),
			'".mysql_real_escape_string($to1)."','".mysql_real_escape_string($dateseen)."','".mysql_real_escape_string($_REQUEST['remark'])."','$userid')";
	$db->insert($sql);
}

if ($show) 
{

	$pdf = new XPDF("P", "mm", array (
		140,
		215
	));

	//$pdf->DisplayPreferences();
	$pdf->DisplayPreferences('HideWindowUI');
	$pdf->AddPage();
	$mleft = $pdf->lMargin;
	$mright = $pdf->rMargin;
	$writinglength = 140 - $mleft - $mright;
	
	//============= H E A D E R ==========================================================	
	$pdf->SetFontSize("18px");
				$pdf->Cell($writinglength, "5", $doctor['name'], 0, 0, 'C');
				$pdf->SetFontSize("10px");
				$pdf->SetY($pdf->GetY());
				$clinic = "";
				if($doctor['citouse'] == 1)
				{
					$clinic = $doctor['clinicinfo1'];
				}
				elseif($doctor['citouse'] == 2)
				{
					$clinic = $doctor['clinicinfo2'];
				}
				elseif($doctor['citouse'] == 3)
				{
					$clinic = $doctor['clinicinfo3'];
				}
				$pdf->WriteHTML("<p align=center>".stripslashes($clinic)."</p>");
				$pdf->Line($mleft, $pdf->GetY(), $writinglength +13, $pdf->GetY());
				$pdf->Line($mleft, $pdf->GetY() + 1, $writinglength +13, $pdf->GetY() + 1);
				$pdf->SetFontSize("12px");
				
				$date = $_SESSION['datetoday'];
				$name = "Name: " . $name;
				$age = $util->age($patient->data['birthday'],$_SESSION['year']."-".$_SESSION['mon']."-".$_SESSION['day']);
				$agesex = "Age: " . $age_arr['year'] ." yrs. old";
				$pdf->Cell("", 5, "", 0, 1);

				$pdf->Cell(50,5,$date,0,1,"L");
				$pdf->Cell("", 5, "", 0, 1);
				$pdf->MultiCell("",5,$name,0,"L");
				$pdf->Cell("",5,$agesex,0,0,"L");
				
				$pdf->Cell("", 5, "", 0, 1);

	$pdf->Cell("", 5, "", 0, 1);
	
	//============= MEDICAL CERTIFICATE HERE ====================
	$pdf->SetFontSize("18px");
	$pdf->Cell("",5, "", 0, 1);
	$pdf->Cell("","5","MEDICAL CLEARANCE", 0, 0, 'C');
	$pdf->Cell("",10, "", 0, 1);
	$pdf->SetFontSize("10px");
	
	$pdf->Cell("", 5, "$to1", 0, 1,"L");
	
	$pdf->Cell("", 5, "", 0, 1);
	$pdf->MultiCell("",8,"This is to certify that ".$_REQUEST['name'].", ".$age['year']." years old ".$dateseen." because of ".stripslashes($consult_reason)." and was diagnosed to have $diagnosis.",0,1,"J");
	$pdf->Cell("", 5, "", 0, 1);
	if($_REQUEST['remark'])
	{
		$pdf->MultiCell("",8,stripslashes($_REQUEST['remark']),0,1,"J");
		$pdf->Cell("", 5, "", 0, 1);
	}
	$pdf->MultiCell("",8,"This certificate is issued upon the request of the patient for whatever purpose it may serve.",0,1,"J");


	

	//============== END MEDICAL CERTIFICATE HERE  =============================
	$max = max($pdf->GetStringWidth("Lic. No. "),$pdf->GetStringWidth("PTR No. "), $pdf->GetStringWidth("S2. No. "));
	
	$indention = 140 - $mright - $pdf->GetStringWidth($doctor['name']);
	if($max >  $pdf->GetStringWidth($doctor['name']))
	{
		$indention = 80;
	}
	 
	$pdf->SetXY($indention, 174);
	$pdf->cell("", 5, $doctor['name'], 0, 1);
	$pdf->SetXY($indention, 179);
	$pdf->cell($max, 5, "Lic. No. ",0, 0,"l");
	$pdf->cell("", 5, $doctor['license'], 0, 1);
	$pdf->SetXY($indention, 184);
	$pdf->cell($max, 5, "PTR No. ", 0, 0,"l");
	$pdf->cell("", 5, $doctor['ptr'], 0, 1);
	$pdf->SetXY($indention, 189);
	if ($_REQUEST['s2'] == "on") {
		$pdf->cell($max, 5, "S2. No. ",0, 0,"l");
		$pdf->cell("", 5, $doctor['s2'], 0, 1);
		$pdf->SetXY($mleft, 215);
	}
				
	$pdf->Output("medcertificate_".rand().".pdf", 'D');
}
?>