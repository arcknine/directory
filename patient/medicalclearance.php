<style>
	#sel_rea,#sel_dia,#sel_remark {width:90px}	
</style>
<?php

$util = new Util();
$db = new DBConnect();
$userid = $_SESSION['auth']['userid'];
$patientid = $_REQUEST['patientid'];
$patient = new Patient();
$patient->getData($patientid);
$middlename = "";
if($patient->data['middlename'] != "")
{
	$middlename = substr($patient->data['middlename'],0,1).".";
}
$patient_name =  $patient->data['salutation']." ".$patient->data['firstname']." $middlename ".$patient->data['lastname'];

if($patient->data['birthday'])
{
	$age_arr = $util->age($patient->data['birthday'],$_SESSION['year']."-".$_SESSION['mon']."-".$_SESSION['day']);
	
}


$sql = "select * from numbers  where userid=$userid ";
$doctor = $db->getRecord($sql);
?>
<br><br>
<fieldset><legend>Medical Clearance</legend>
	<form action=patient/medicalclearancepdf.php method=post>
	<input type=hidden name=patientid id=patientid value="<?php echo $patientid?>">
	<input type=hidden name=userid id=userid value="<?php echo $userid;?>">
	<input type=hidden name=age id=age value="<?php echo $age_arr['year']?>">
	<input type=hidden name=date value="<?php echo $_SESSION['datetoday']?>">
	<input type=hidden name=datesql value="<?php echo $_SESSION['year']."-".$_SESSION['mon']."-".$_SESSION['day'];?>"/>
	<div style="margin-left:auto;margin-right:auto;width:50%;padding-top:20px;padding-bottom:20px;">
	<?php 
			$table = "clearance";
			$col = "title";
			$sql = "select  distinct $col from $table where userid=$userid and $col != '' order by $col";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
			{
				echo '<div style="position:relative;left:0">';
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","style='width:90px' onchange=chooseType(\"sel_$col\",\"$col\")")."<br>";
				echo '</div>';
			}
		?>
		<input type=text name=title id=title class=input value="Medical Clearance" size="50"/></div>
	
	
	<div style=position:relative;left:10px;>Date: <?php echo $_SESSION['datetoday']?></div>
	<div style=position:relative;left:10px;>To: 
		<?php 
			$table = "clearance";
			$col = "towhom";
			$sql = "select  distinct $col from $table where userid=$userid and $col != '' order by $col";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","style='width:90px' onchange=chooseType(\"sel_$col\",\"$col\")")."<br>";
		?>
		<input type=text name=towhom id=towhom class=input value="" size="50"/></div>
	<div style=position:relative;left:10px;padding-top:20px>Thank you for referring 
		<input type=text name=patient_name class=input value="<?php echo $patient_name;?>" size=50 />,
		<input type=text name=age class=input value="<?php echo $age_arr['year'];?>" size=3 /> y/o, 
		<input type=text name=sex class=input value="<?php echo strtolower($patient->data['sex']);?>" size=10 /> <br/>
	</div>
		
	 
	<div style=position:relative;left:10px;>
		<table>
			<tr><td  align=right style="vertical-align:top">
				for:</td><td>
				<?php 
					$table = "clearance";
					$col = "text";
					$sql = "select  distinct $col from $table where userid=$userid and $col != '' order by $col";
					$sal_arr = $db->getOneCol($sql);
					if(count($sal_arr) > 0)
						echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","style='width:90px' onchange=chooseType(\"sel_$col\",\"$col\")")."<br>";
				?> 
				<textarea cols=40 rows=2 name="text" id="text" class=input></textarea>.
				</td></tr>
			<tr><td align=right style="vertical-align:top">History:</td><td>
				<?php 
					$table = "clearance";
					$col = "history";
					$sql = "select  distinct $col from $table where userid=$userid and $col != '' order by $col";
					$sal_arr = $db->getOneCol($sql);
					if(count($sal_arr) > 0)
						echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","style='width:90px' onchange=chooseType(\"sel_$col\",\"$col\")")."<br>";
				?>
				<textarea cols=40 rows=2 name="history" id="history" class=input></textarea></td></tr>
			<tr><td align=right style="vertical-align:top">PPE:</td><td>
				<?php 
					$table = "clearance";
					$col = "ppe";
					$sql = "select  distinct $col from $table where userid=$userid and $col != '' order by $col";
					$sal_arr = $db->getOneCol($sql);
					if(count($sal_arr) > 0)
						echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","style='width:90px' onchange=chooseType(\"sel_$col\",\"$col\")")."<br>";
				?>
				<textarea cols=40 rows=2 name="ppe" id="ppe" class=input></textarea></td></tr>
			<tr><td align=right style="vertical-align:top">Impression:</td><td>
				<?php 
					$table = "clearance";
					$col = "impression";
					$sql = "select  distinct $col from $table where userid=$userid and $col != '' order by $col";
					$sal_arr = $db->getOneCol($sql);
					if(count($sal_arr) > 0)
						echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","style='width:90px' onchange=chooseType(\"sel_$col\",\"$col\")")."<br>";
				?>
				<textarea cols=40 rows=2 name="impression" id="impression" class=input></textarea></td></tr>
			<tr><td align=right style="vertical-align:top">Treatment Plan:</td><td>
				<?php 
					$table = "clearance";
					$col = "treatment";
					$sql = "select  distinct $col from $table where userid=$userid and $col != '' order by $col";
					$sal_arr = $db->getOneCol($sql);
					if(count($sal_arr) > 0)
						echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","style='width:90px' onchange=chooseType(\"sel_$col\",\"$col\")")."<br>";
				?>
				<textarea cols=40 rows=2 name="treatment" id="treatment" class=input></textarea></td></tr>
			<tr><td colspan=2 align=right><input type=submit name=submit value=Submit class=submit_button></td></tr>		
		</table>
	</div>

</form>
</fieldset>

<?php

//echo $patient_name;

if(isset($_REQUEST['do']) && $_REQUEST['do'] == "del")
{
	$db->delete("delete from clearance where id=".$_REQUEST['clearanceid']);
}

$age_arr = array();

$sql = "select * from clearance where patientid=$patientid order by date desc";
$medarr = $db->getAllRecord($sql);
$row_count = $db->recordCount;

$start = 0;
if(isset($_REQUEST['start']))
{
	$start = $_REQUEST['start'];
}
$rowstoview = 10;
if(isset($_REQUEST['rowstoview']))
{
	$rowstoview = $_REQUEST['rowstoview'];
}

$count = $start + 1;
if($row_count > 0)
{
	echo '<br><br><br><a name=list></a>';
	echo "<table cellpadding=4 cellspacing=4 align=center class=view>";
	echo "<tr><th colspan=2>Date Issued</th><th>Title</th><th>To</th><th>Reason for Referral<th>History</th><th>PPE</th><th>Impression</th><th>Treatment Plan</th></tr>";
	for($i = $start;$i < $start + $rowstoview;$i++)
	{
		if($i < $row_count)
		{
			$val = $medarr[$i];
			?>
			<form method=post action=patient/medicalclearancepdf.php target=_blank>
			<input type=hidden name=clearanceid value="<?php echo $val['id'];?>"/>
			<input type=hidden name=submit value="Reprint"/>
			<?php
			echo "<tr><td class=b_c>$count.</td><td>".$util->convertmysqldate($val['date'],"F j, Y")."</td>" .
				"<td align=center>".$val['title']."</td>" .
				"<td align=center>".$val['towhom']."</td>" .
				"<td align=center>".$val['text']."</td>" .
				"<td align=center>".$val['history']."</td>" .
				"<td align=center>".$val['ppe']."</td>" .
				"<td align=center>".$val['impression']."</td>" .
				"<td align=center>".$val['treatment']."</td>" .
				"<td><input type=image name=submit value=Reprint src=image/printer.png></td><td>".$util->setEdel("","patient.php?pf=14&patientid=$patientid&do=del&clearanceid=".$val['id']."#list")."</td></tr>";
			?>
			</form>
			<?php
			$count++;
			
		}
	}
	echo "</table>";
	echo "<div align=center>".$util->navi("patient.php?pf=14&patientid=$patientid", $start, $rowstoview, $row_count, "image")."</div>";
}


?>
