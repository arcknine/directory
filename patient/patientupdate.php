<?php
session_start();
ini_set( "memory_limit", "200M" );
/*
 * Created on Apr 30, 2009
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include("../classes/connect.php");
 include("../classes/Patient.php");
 include("../classes/DBConnect.php");
 include("../classes/Util.php");
 
 $patient = new Patient();
 $patientid = $_REQUEST['patientid'];
 $util = new Util();
 
 if($_REQUEST['submit'] == "Update")
{	
	$_REQUEST['birthday'] = $_REQUEST['by']."-".$_REQUEST['bm']."-".$_REQUEST['bd'];
	
	
	if($_REQUEST['firstname'] != "")
	{
		$affected_rows = $patient->update($_REQUEST);
		/*
		 * ADD photo
		 */
		 $imagefolder = $_SESSION['photo'];
		 $thumbfolder = $_SESSION['photothumb'];
		 
		if($_FILES['photo']['name'])
	 	{
			$rand_num = rand();
	 		$newname = $util->imageupload("photo_".$rand_num."_".$patientid,$_FILES['photo']['name'],$imagefolder,$thumbfolder,$_FILES['photo']['tmp_name'],100);
			$patient->photoUpdate($newname,$patientid);
	 	}
	 	header("location:../patient.php?pf=1&patientid=$patientid&".rand());
	}
	else
	{
		$msg = "<p class=err>Error: Firstname is required.</p>";
	}
}
elseif($_REQUEST['submit'] == "Cancel")
{
	header("location:../patient.php?pf=1&patientid=$patientid");
}

?>
