<div id=consult>
<?php
include("classes/Medicine.php");
ini_set( "memory_limit", "200M" );

$url = "patient.php?pf=5&patientid=$patientid";

$db = new DBConnect();

if(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "Save")
{
	if(isset($_REQUEST))
	{
		foreach($_REQUEST as $key=>$val)
		{
			$_REQUEST[$key] = mysql_real_escape_string($val);
		}
		$sql = "update consult set plan='".$_REQUEST['plan']."',subjectivecomplaint='".$_REQUEST['subjectivecomplaint']."',".
			"objectivefinding='".$_REQUEST['objectivefinding']."',assessment='".$_REQUEST['assessment']."'," .
			"bp='".$_REQUEST['bp']."',temp='".$_REQUEST['temp']."',pr='".$_REQUEST['pr']."',rr='".$_REQUEST['rr']."',".
			"ht='".$_REQUEST['ht']."',os='".$_REQUEST['os']."',od='".$_REQUEST['od']."'".
			" where id=".$_REQUEST['consultid'];
		
		$db->update($sql);
	}
}

if(isset($_REQUEST['prevmed']) && $_REQUEST['prevmed'] == "Add")
{
	if($_REQUEST['medid'])
		{
			foreach($_REQUEST['medid'] as $row)
			{
				$darr = array();
				$darr['consultid'] = $_REQUEST['consultid'];
				$darr['patientid'] = $_REQUEST['patientid'];
				$darr['generic'] = $_REQUEST['generic'][$row];
				$darr['brand'] = $_REQUEST['brand'][$row];
				$darr['dose'] = $_REQUEST['dose'][$row];
				
				$darr['quantity'] = $_REQUEST['quantity'][$row];
				$darr['userid'] = $userid;
				$darr['sig'] = $_REQUEST['sig'][$row];
				$db->setTable("medicine");
				$db->insertArr($darr);
				//print_r($_REQUEST);
			}
		}
}
?>

<script type="text/javascript">
var count = 1;
	tinyMCE.init({
	mode : "textareas",
	forced_root_block : false,
   	force_br_newlines : true,
   	force_p_newlines : false,
   	theme : "advanced",
   	theme_advanced_layout_manager : "SimpleLayout",
   	theme_advanced_buttons1 : "bold,italic,underline,justifyleft,justifyfull,justifyright,bullist,numlist,indent,outdent,undo,redo,forecolor,sub,sup,fontsizeselect",
   	theme_advanced_buttons2 : "",
	theme_advanced_buttons3 : "",
	theme_advanced_font_sizes : "2,3,4,5,6,7",
	plugins: "autoresize",
	autoresize_max_height: "600",
	autoresize_min_height: "300",
	width: "600"
	});


function autoSave(field,content)
	{
		content = content.replace('&amp;','and');
		new Ajax.Request('ajax/consult_ajax.php', 
		{
			parameters:'content='+content+'&field='+field+'&do=consult',
			onSuccess: function(t){
				//alert("response "+t.responseText);
			},
			 onFailure: function(t) {
        		alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
    		}

		}
		);
	}
	
	
function vtsave(tag,consultid)
 {
 	
 	var val = document.getElementById(tag).value;
	new Ajax.Request('ajax/consult_ajax.php', 
		{
			parameters:'field='+tag+'&value='+val+'&consultid='+consultid+'&do=vital',
			onSuccess: function(t){
				//alert("response "+t.responseText);
			},
			 onFailure: function(t) {
        		alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
    		}

		}
		);
}

function chptype(tag,consultid)
{
	
	var val = document.getElementById(tag).value;
	new Ajax.Request('ajax/consult_ajax.php', 
		{
			parameters:'field='+tag+'&value='+val+'&consultid='+consultid+'&do=ptype',
			onSuccess: function(t){
			},
			 onFailure: function(t) {
       		alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
   		}

		}
		);
}

function getICD(userid)
	{
		
		new Ajax.Request('ajax/surg_ajax.php', 
		{
			parameters:'userid='+userid,
			onSuccess: function(t){
				var icdarr = t.responseText.split("/");
				var select = document.getElementById("icdsel");
				select.length = 1;
				select.options[0] = new Option("Choose","0");
				var l = icdarr.length;
				for(var i = 0; i < l;i++)
				{
					select.options[i + 1] = new Option(icdarr[i],icdarr[i]);
				}
			},
			 onFailure: function(t) {
      		alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
  		}

		}
		);
	}
</script>

<style>
	#sel_generic, #sel_brand, #sel_sig, #sel_qty, #sel_dose, #sel_image_1, #sel_image_2, #sel_rem 
	{ 
		width:80px; 
	}
</style>

<?php
unset($_SESSION['referrer']); 
$userid = $_SESSION['auth']['userid'];

$consult = new Consult();
$consultid = 0;
$consult_arr = array();
$consult_date = "";
if(isset($_REQUEST['consultid']) && $_REQUEST['consultid'] > 0)
{
	$consultid = $_REQUEST['consultid'];
	$consult_arr = $db->getRecord("select * from consult where id=$consultid");
	$consult_date =  $consult_arr['date'];
}
elseif(isset($_REQUEST['consult_date']) && $_REQUEST['consult_date'])
{
	$consult_date = $_REQUEST['consult_date'];
	$consult_arr = $consult->getConsult($consult_date,$patientid);
	$consultid = $consult_arr['id'];
}
else
{
	$today = getdate();
	if($today['mon'] < 10)
	{
		$today['mon']="0".$today['mon'];
	}
	if($today['mday'] < 10)
	{
		$today['mday']="0".$today['mday'];
	}
	
	$consult_date = $today['year']."-".$today['mon']."-".$today['mday'];
	
	if($consult->checkConsult($consult_date,$patientid) == 0)
	{
		$consultid = $consult->create($patientid,$consult_date);	
		$consult_arr = $consult->getConsult($consult_date,$patientid);
	}
	else
	{
		/*
		 * Retrieve current data. 
		 */
		 
		 $consult_arr = $consult->getConsult($consult_date,$patientid);
		 $consultid = $consult_arr['id'];
	}
}

		
?>


<div class=pageTitle2> &nbsp;Consult Date: <?php echo $util->convertmysqldate($consult_date,"F j, Y");?></div>
<a name=top></a>
<form action="<?php echo $url?>" method="post">
<input type=hidden name=consultid value="<?php echo $consult_arr['id'];?>">
<table border=0 cellpadding=2 cellspacing style=padding-top:15px;margin-left:auto;margin-right:auto>
	<tr><td align=left>
		<table><tr><th>Patient Type:</th>
			<td>
			<?php 
			$inp = "";$outp = "";
			if($consult_arr['ptype'] == "out")
			{
				$outp = " checked ";
			}
			else
			{
				$inp = " checked ";
			}
			?>
			<input type=radio name=ptype id=ptypeo value=out <?php echo $outp;?> onclick="chptype('ptypeo',<?php echo $consult_arr['id'];?>)" >Out-Patient 
			<input type=radio name=ptype id=ptypei value=in <?php echo $inp;?> onclick="chptype('ptypei',<?php echo $consult_arr['id'];?>)" >In-Patient  
			</td>
			</tr>
			<tr><td colspan=2>In-Patient Info <input type=text name=inpatientinfo id=inpatientinfo  size=50 class="input" value="<?php echo $consult_arr['inpatientinfo'];?>" onkeyup="vtsave('inpatientinfo','<?php echo $consult_arr['id'];?>')"></td></tr>
		</table>
		</td></tr>
	<tr>
		<td align=left nowrap>
			<table><tr><td>
			<fieldset style="border: 1px dashed lightgray;"><legend>Vital Signs</legend>
				<table>
					<tr><td>HT</td><td><input type=text size=5 name=ht id=ht class="input" value="<?php echo $consult_arr['ht']?>" onkeyup="vtsave('ht','<?php echo $consult_arr['id']?>')"></td>
					<td>WT</td><td><input type=text size=5 name=wt id=wt class="input" value="<?php echo $consult_arr['wt']?>" onkeyup="vtsave('wt','<?php echo $consult_arr['id']?>')"></td>
					<td>BP</td><td><input type=text size=5 name=bp id=bp class="input" value="<?php echo $consult_arr['bp']?>" onkeyup="vtsave('bp','<?php echo $consult_arr['id']?>')"></td></tr>
					<tr><td>Temp</td><td><input type=text size=5 name=temp id=temp class="input" value="<?php echo $consult_arr['temp']?>"  onkeyup="vtsave('temp','<?php echo $consult_arr['id']?>')"></td>
					<td>PR</td><td><input type=text size=5 name=pr id=pr class="input" value="<?php echo $consult_arr['pr']?>"  onkeyup="vtsave('pr','<?php echo $consult_arr['id']?>')"></td>
					<td>RR</td><td><input type=text size=5 name=rr  id=rr class="input" value="<?php echo $consult_arr['rr']?>"  onkeyup="vtsave('rr','<?php echo $consult_arr['id']?>')"></td></tr>
				</table>
			</fieldset>
		</td>
		<td>
			<fieldset style="border: 1px dashed lightgray;"><legend>Visual Acuity</legend>
			<table>
				<tr><td>OS</td><td><input type=text  name=os id=os size=20 class="input" value="<?php echo $consult_arr['os']?>" onkeyup="vtsave('os','<?php echo $consult_arr['id']?>')"></td></tr>
				<tr><td>OD</td><td><input type=text name=od id=od size=20 class="input" value="<?php echo $consult_arr['od']?>" onkeyup="vtsave('od','<?php echo $consult_arr['id']?>')"></td></tr>
			</table>
			</fieldset>
		</td>
	</tr>
	<?php if($auth_arr['access'] == "admin" || $auth_arr['access'] == "doctor") {?>
	<tr>
		<th align=left>Subjective Complaint
		<?php 
		$table = "consult";
		$col = "subjectivecomplaint_".$consult_arr['id'];
		$opt = array(); $val=array();
		$sql = "select  subjectivecomplaint from $table where patientid in (select id from patient where userid=$userid) and subjectivecomplaint!='' order by subjectivecomplaint";
		$rst = $db->getAllRecord($sql);
		if($rst)
		{
			foreach($rst as $row)
			{
				$str = strip_tags($row['subjectivecomplaint'],'<br><br/>');
				if($str > 50)
				{
					$opt[] = substr($str,0,50)."...";
				}
				else
				{
					$opt[] = $str;
				}
				$val[] = $str;
			}
		}
		if(count($val) > 0)
			echo $util->getHtmlSelect($val,$opt, "sel_sub","$col"," style=width:80px onchange=addText(\"sel_sub\",\"$col\")");
		?>
		</th>
	</tr>
	<tr>
		<td><textarea rows=3 cols=100 name=subjectivecomplaint id=subjectivecomplaint_<?php echo $consult_arr['id']?>><?php echo $consult_arr['subjectivecomplaint']?></textarea>
		</td>
	</tr>
	<tr>
		<th align=left>Objective Findings
		<?php 
		$table = "consult";
		$col = "objectivefinding_".$consult_arr['id'];
		$opt = array(); $val=array();
		$sql = "select  objectivefinding from $table  where patientid in (select id from patient where userid=$userid) and objectivefinding!='' order by objectivefinding";
		$rst = $db->getAllRecord($sql);
		if($rst)
		{
			foreach($rst as $row)
			{
				if(strlen($row['objectivefinding']) > 50)
				{
					$opt[] = substr($row['objectivefinding'],0,50)."...";
				}
				else
				{
					$opt[] = $row['objectivefinding'];
				}
				$val[] = $row['objectivefinding'];
			}
		}
		if(count($val) > 0)
			echo $util->getHtmlSelect($val,$opt, "sel_obj","$col"," style=width:80px onchange=addText(\"sel_obj\",\"$col\")");
	?>
		</th>
	</tr>
	<tr>
		<td><textarea rows=3 cols=100 name=objectivefinding id=objectivefinding_<?php echo $consult_arr['id']?>><?php echo $consult_arr['objectivefinding']?></textarea>
		
		</td>
	</tr>
	<tr>
		<th align=left>Assessment
		<?php 
		$table = "consult";
		$col = "assessment_".$consult_arr['id'];
		$opt = array(); $val=array();
		$sql = "select  assessment from $table  where patientid in (select id from patient where userid=$userid) and assessment!='' order by assessment";
		$rst = $db->getAllRecord($sql);
		if($rst)
		{
			foreach($rst as $row)
			{
				if(strlen($row['assessment']) > 50)
				{
					$opt[] = substr($row['assessment'],0,50)."...";
				}
				else
				{
					$opt[] = $row['assessment'];
				}
				$val[] = $row['assessment'];
			}
		}
		if(count($val) > 0)
		{
			echo $util->getHtmlSelect($val,$opt, "sel_ass","$col"," style=width:80px onchange=addText(\"sel_ass\",\"$col\")");
		}
		?>
		&nbsp;&nbsp;&nbsp;ICD 10:
		<?php 
		$table = "icdselect";
		$col = "assessment_".$consult_arr['id'];
		$arr2 = array();
		$sql = "select distinct  icd.id as id,code,disease from $table left join icd on icd.id=icdselect.icdid where code != '' and  disease != ''  and userid=$userid  order by code";
		$rst = $db->getAllRecord($sql);
		if($rst)
		{
			foreach($rst as $row)
			{
				$arr2[] = $row['code'].": ".$row['disease'];
			}
		}
		 
		echo '<select name=icdsel id=icdsel onmousedown=getICD('.$userid.')  style=width:80px onchange=addText("icdsel","'.$col.'")>';
		echo '<option value=0>Choose</option>';
		foreach($arr2 as $v)
		{
			echo '<option>'.$v.'</option>';
		}
		echo '</select>'
		?>
		
		<span style=font-size:10pt;font-weight:normal>&nbsp;&nbsp;<a href="manage.php?page=1" target=_blank alt="Add additional ICD selection">Manage ICD</a></span>
		

		</th>
	</tr>
	<tr>
		<td><textarea rows=3 cols=100 name=assessment id=assessment_<?php echo $consult_arr['id']?>><?php echo $consult_arr['assessment']?></textarea>
		
		</td>
	</tr>
	<tr>
		<th align=left>Plan
		<?php 
		$table = "consult";
		$col = "plan_".$consult_arr['id'];
		$opt = array(); $val=array();
		$sql = "select  plan from $table  where patientid in (select id from patient where userid=$userid) and plan!='' order by plan";
		$rst = $db->getAllRecord($sql);
		if($rst)
		{
			foreach($rst as $row)
			{
				if(strlen($row['plan']) > 50)
				{
					$opt[] = substr($row['plan'],0,50)."...";
				}
				else
				{
					$opt[] = $row['plan'];
				}
				$val[] = $row['plan'];
			}
		}
		if(count($val) > 0)
		echo $util->getHtmlSelect($val,$opt, "sel_plan","$col"," style=width:80px onchange=addText(\"sel_plan\",\"$col\")");
		?>
		</th>
	</tr>
	<tr>
		<td><textarea rows=3 cols=100 name=plan id=plan_<?php echo $consult_arr['id']?>><?php echo $consult_arr['plan']?></textarea>
		</td>
	</tr>
	<?php }?>
	<tr><td colspan=2>
		<input type=submit name=submit value=Save class="save_button">
		</td>
	</tr>
</table>
</form>

<?php if($auth_arr['access'] == "admin" || $auth_arr['access'] == "doctor") 
{?>

	<h3 class=pageTitle2> &nbsp;Medicines <a href=#top class=top_button>TOP</a><a name=medicine></a> </h3> 
	<?php 
	include("script/consult.js");
	$medicine = new Medicine();
	if(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "Add New Medicine")
	{
		$_REQUEST['userid'] = $userid;
		$medicine->add($_REQUEST);
		echo "<p>Medicine added</p>";
	}
	elseif(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "delmed")
	{
		$medicine->del($_REQUEST['medid']);
		echo "<p>Delete successfully.</p>";
	}
	?>
	
	
	
	<form action="patient/prescriptionpdf.php?patientid=<?php echo $patientid?>&consultid=<?php echo $consultid?>#medicine" method=post>
	<table style="margin-left: auto;margin-right: auto;">
		<tr>
			<td>
		
		<?php
		$sql = "select * from medicine where consultid=$consultid";
		$dum = $db->getAllRecord($sql);
		$bf = ""; $count  = 1;
		if(count($dum)>0)
		{
			echo "<fieldset><legend>Prescribed Meds</legend><table rules=rows align=center cellpadding=2 cellspacing=2><tr><td colspan=3 align=center><strong>Generic</strong></td>" .
					"<td align=center><strong>Brand</strong></td><td align=center><strong>Dose</strong></td>" .
					"<td align=center><strong>Quantity</strong></td><td align=center><strong>Sig</strong></td></tr>";
			foreach($dum as $val)
			{
				$bf .= "<tr><td><input type=checkbox name=prescription[] value=".$val['id']." checked=checked></td><td>$count.</td><td align=right>".$val['generic']."</td><td align=right>".$val['brand']."</td>".
					"<td align=right>".$val['dose']."</td><td align=center>".$val['quantity']."</td><td align=right>".$val['sig']."</td>".
					"<td>".$util->setEdel("","patient.php?pf=5&patientid=$patientid&consultid=$consultid&submit=delmed&medid=".$val['id']."#medicine")."</td></tr>";
				$count++;
			}
			echo "$bf";
			echo "<tr><td><input type=checkbox name=s2 value=on></td><td colspan=4>Add S2</td></tr>";
			echo "<tr><td colspan=7><input type=submit name=submit value=Prescribe class=prescribe_button></td></tr></table></fieldset><br>";
		}
		
		?>
	</table>
	</form>
	
	<form action="patient.php?pf=5&patientid=<?php echo $patientid?>&consultid=<?php echo $consult_arr['id']?>#medicine" method=post name=medadd>
		<fieldset><legend>Add New Medicine</legend>
			<table style="margin-left: auto;margin-right: auto;">
				<tr><td>Generic</td><td>
					
					<?php 
						$sql = "select  generic from medicine where userid=$userid order by generic asc";
						$sal_arr = $db->getOneCol($sql);
						if(count($sal_arr) > 0)
							echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_generic","generic","onchange=getMed(\"sel_brand\")"); 
					?>
					<input type=text name=generic id=generic size=20 class="input"></td>
				</tr>
				<tr><td>Brand</td>
				<td>
					<select name=sel_brand id=sel_brand onchange=getMed('sel_dose')>
	
					</select>
					<input type=text name=brand id=brand size=20 class="input">
					<div id="brand_choices" class="autocomplete"></div>
					</td></tr>
				<tr>
					<td>Dose</td>
					<td>
					<?php 
						$sql = "select  distinct dose from medicine  where userid=$userid  order by dose asc";
						$sal_arr = $db->getOneCol($sql);
						if(count($sal_arr) > 0)
							echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_dose","dose","onchange=chooseType(\"sel_dose\",\"dose\")"); 
					?>
					<input type=text name=dose id=dose size=20 class="input"></td></tr>
				<tr><td>Quantity</td>
				<td>
					<?php 
						$sql = "select  distinct quantity from medicine  where userid=$userid  order by quantity";
						$sal_arr = $db->getOneCol($sql);
						if(count($sal_arr) > 0)
							echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_qty","quantity","onchange=chooseType(\"sel_qty\",\"quantity\")"); 
					?>
				<input type=text name=quantity id=quantity size=20 class="input"></td></tr>
				
				<tr><td>Sig</td>
					<td>
					<?php 
						$sql = "select distinct sig from medicine  where userid=$userid  order by sig";
						$sal_arr = $db->getOneCol($sql);
						if(count($sal_arr) > 0)
							echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_sig","sig","onchange=chooseType(\"sel_sig\",\"sig\")"); 
					?>
					<input type=text name=sig id=sig size=20 class="input"></td></tr>
				<tr><td colspan=2><input type=submit name=submit value="Add New Medicine" class="add_button"></td></tr>
			</table></fieldset>
	</form>
	
	<form action="patient.php?pf=5&patientid=<?php echo $patientid?>&consultid=<?php echo $consult_arr['id']?>#medicine" method=post name=prevmed>
			<!--  ================================ P R E V I O U S   M E D S =======================-->
		<?php
			$sb = "";
			$count = 1;
			$sql = "select generic,brand,dose,quantity,sig, medicine.id as id,date_format(date,'%M %e, %Y') as date from medicine left join consult on medicine.consultid=consult.id 
				where (generic != '' or brand != '')  and patientid=$patientid and consultid < ".$consult_arr['id']." group by generic,brand
				order by date desc, generic, brand";
			//echo $sql;
			$result = $db->getAllRecord($sql);
			asort($result);
			foreach($result as $row)
			{		
					$bgcolor = "";
					if($count%2 == 0)
					{
						$bgcolor = " class=rowColor ";
					}
					$sb.="<tr $bgcolor><td><input type=checkbox name=medid[] value=".$count."></td><td>$count.</td>
						<td>".$row['date']."</td>
						<td><input type=text size=20 name=generic[".$count."] value=\"".$row['generic']."\"/></td>
						<td><input type=text size=15 name=brand[".$count."] value=\"".$row['brand']."\"/></td>
						<td><input type=text size=15 name=dose[".$count."] value=\"".$row['dose']."\"/></td>
						<td><input type=text size=5 name=quantity[".$count."] value=\"".$row['quantity']."\"/></td>
						<td><input type=text size=40 name=sig[".$count."] value=\"".$row['sig']."\"/></td></tr>";
				$count++;
			}
			if($sb != "")
			{
				echo "<a href=\"\" onclick='showhide(111);return false;'>Previous Medicines</a>";
				echo "<form action=\"controller.php?pp=consult&pId=$pId&consultid=$consultid#med\" method=post>";
				echo "<table><tr id=111 style=\"display:none;\"><td><table>
					<tr><th class=th_top colspan=3>Date</th><th class=th_top>Generic</th><th class=th_top>Brand</th><th class=th_top>Dose</th>
					<th class=th_top>Quantity</th><th class=th_top>Sig</th></tr>$sb
					<tr><td colspan=8 align=right><input type=submit name=prevmed value=Add></td></tr>	
					</table></td></tr></table>";
				echo "</form>";
			}
		?>
		</form>
		
		<script type="text/javascript" language="javascript">
		new Ajax.Autocompleter("brand", "brand_choices", "ajax/autocomplete.php?col=brand&table=medicine", 
					{afterUpdateElement : getSelectionId});
		function getSelectionId(text,id) {

			new Ajax.Request('ajax/consult_ajax.php?brand='+text.value+'&do=getgeneric', {
				method: 'get',
	  			onSuccess: function(response) {
	    			
	    				//alert("response generic "+response.responseText);
    					document.getElementById("generic").value = response.responseText;
    					
	    			},
			 		onFailure: function(t) {
        			alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
    				}
				});
			
			new Ajax.Request('ajax/consult_ajax.php?brand='+text.value+'&do=brand', {
				method: 'get',
	  			onSuccess: function(response) {
	    			
	    			//alert("response dose "+response.responseText);
	    			
	    			var strArr = response.responseText.split("_");
	    			var selcount = strArr.length;
	    			document.medadd.sel_dose.length = selcount + 1;
	    			
	    			document.medadd.sel_dose.options[0].text = "Choose";
					document.medadd.sel_dose.options[0].value = "0";
	    			
	    			for(i = 0; i < selcount; i++)
	    			{
	    				y = i + 1;
	    				document.medadd.sel_dose.options[y].value = i;
	    				document.medadd.sel_dose.options[y].text = strArr[i]; 
	    			}
				  },
			 		onFailure: function(t) {
        			alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
    				}
				});
		}
	 </script>
	<script type="text/javascript" language="javascript">
		new Ajax.Autocompleter("dose", "dose_choices", "ajax/autocomplete.php?col=dose&table=medicine", 
					{afterUpdateElement : getSelectionId});
		function getSelectionId(text,id) {
		}
	 </script>			
	<script type="text/javascript" language="javascript">
		new Ajax.Autocompleter("quantity", "quantity_choices", "ajax/autocomplete.php?col=quantity&table=medicine", 
					{afterUpdateElement : getSelectionId});
		function getSelectionId(text,id) {
		}
	 </script>
	<script type="text/javascript" language="javascript">
		new Ajax.Autocompleter("sig", "sig_choices", "ajax/autocomplete.php?col=sig&table=medicine", 
					{afterUpdateElement : getSelectionId});
		function getSelectionId(text,id) {
		}
	 </script>
		
	<h3 class=pageTitle2> &nbsp;Images <a name=image></a><a href=#top class=top_button>TOP</a></h3>
	<?php 
	/*
	 * === IMAGES ====================================================
	 */
	
	$show_image = true;
	$show_rad = true;
	$imagefolder = $_SESSION['photo'];
	$thumbfolder = $_SESSION['photothumb'];
	if(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "Upload")
	{
		
		$consultid  = $_REQUEST['consultid'];
		
		$imagefilename_arr = $_FILES['imageimage']['name'];
		$imagetmpfile_arr = $_FILES['imageimage']['tmp_name'];
		$imagedesc_arr = $_REQUEST['imagedesc'];
		$imagetest_arr= $_REQUEST['imagetest'];
		
		$idx = 0;
		if($imagetest_arr )
		{
			foreach($imagetest_arr as $val)
			{
				if($val != "")
				{
					if($idx == 0)
					{
						$img_date = $_REQUEST['y8']."-".$_REQUEST['m8']."-".$_REQUEST['d8'];
					}
					else
					{
						$img_date = $_REQUEST['y9']."-".$_REQUEST['m9']."-".$_REQUEST['d9'];
					}
					$sql = "insert into image (`id`,`consultid`,`name`,`remark`,`date`,`userid`)".
					" values('','$consultid','".$imagetest_arr[$idx]."','".$imagedesc_arr[$idx]."','$img_date','$userid')";
					$insertid = $db->insert($sql);
					if($imagefilename_arr[$idx] != "")
					{
						$newname =  $util->imageupload("image_".$insertid,$imagefilename_arr[$idx],$imagefolder,$thumbfolder,$imagetmpfile_arr[$idx],100);
						$db->update("update image set image='$newname' where id=$insertid");
					}
				}
				
				$idx++;
			}
		}
	}
		if(isset($_REQUEST['do']) &&  $_REQUEST['do'] == "imagedel")
		{
			$sql = "select * from image where id=".$_REQUEST['imageid'];
			$row = $db->getRecord($sql);
			if(isset($row['photo']))
			{
				$util->deleteImage($row['photo'],$imagefolder);
				$util->deleteImage($row['photo'],$thumbfolder);
			}
			$db->delete("delete from image where id=".$_REQUEST['imageid']);
			echo "<p style=color:blue>Image deleted.</p>";	
		}
		elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "imageedit")
		{
			$show_image = false;
		}
		elseif(isset($_REQUEST['imagesubmit']) && $_REQUEST['imagesubmit'] == "Update")
		{
			if($_FILES['photo']['name'] != "")
			{
				$rand_num = rand();
				$newname = $util->imageupload("image_".$rand_num."_".$_REQUEST['imageid'],$_FILES['photo']['name'],$imagefolder,$thumbfolder,$_FILES['photo']['tmp_name'],100);
				$db->update("update image set image='$newname', name='".$_REQUEST['name']."', remark='".$_REQUEST['remark']."' where id=".$_REQUEST['imageid']);
			}
			else
			{
				$db->update("update image set name='".$_REQUEST['name']."', remark='".$_REQUEST['remark']."' where id=".$_REQUEST['imageid']);
			}
			
		}
	
		
	?>
	
	
	
	
	<?php
	//========= VIEW image IMAGES =============================
	if($show_image)
	{
		$sql = "select * from image where consultid=$consultid order by name asc";
		$dum = $db->getAllRecord($sql);
		$bf = "";
		$imagecount = 1;
		
		foreach($dum as $image)
		{
			$bf.="<tr><td><strong>$imagecount.</strong></td><td align=center>".$util->convertmysqldate($image['date'],"M j, Y")."</td><td align=center>";
			if(is_file($thumbfolder."/".$image['image'])) 
			{ 
				$bf .= "<a href=\"javascript:popimage('".$_SESSION['urlphoto']."/".$image['image']."','".$image['name']."','".$util->getImageWidth($imagefolder,$image['image'])."','".$util->getImageHeight($imagefolder,$image['image'])."')\" ".$util->mouseOver("Click To Enlarge","100").">
					<img src=\"".$_SESSION['urlthumb']."/".$image['image']."\" border=0 width=50></a><br>";
			}
			$bf.= $image['name']."</td><td align=center>".$image['remark']."</td><td>";
			$bf.= $util->setEdel("patient.php?pf=5&patientid=$patientid&consultid=$consultid&do=imageedit&imageid=".$image['id']."#image","patient.php?pf=5&patientid=$patientid&consultid=$consultid&do=imagedel&imageid=".$image['id']."#image")."</td></tr>";
			$bf.="</td></tr>";
			$imagecount++;
			
		}
		if($bf)
		{
		echo "<table cellpadding=15 cellspacing=4 align=center class='image_view'>" .
				"<tr><th colspan=2 align=center><strong>Date</strong></td><th align=center><strong>Test</strong></td>" .
			"<th align=center><strong>Description</strong></td><th></td></tr>$bf</table>";
		}
	
	}
	else
	{
		/*
		 * ========================= E D I T  L A B O R A T O R Y ==================================
		 */
		 echo "<fieldset><legend>Update Images</legend>";
		 $sql = "select * from image where id=".$_REQUEST['imageid'];
		 $row = $db->getRecord($sql);
		 ?>
		 <form action="patient.php?pf=5&patientid=<?php echo $patientid?>&consultid=<?php echo $consultid?>#image" method=post enctype="multipart/form-data">
		 <input type=hidden name=imageid value="<?php echo $_REQUEST['imageid']?>">
		 <table style="margin-left: auto;margin-right: auto;">
		 	<tr><td>Image</td><td><img src="<?php echo $_SESSION['urlthumb']."/".$row['image']?>"><br><input type=file name=photo size=41></td></tr>
		 	<tr><td>Name</td><td><input type=text name=name size=40 value="<?php echo $row['name']?>" class="input"></td></tr>
		 	<tr><td>Description</td><td><textarea name=remark cols=50 rows=3><?php echo $row['remark']?></textarea></td></tr>
		 	<tr><td colspan=2><input type=submit name=imagesubmit value=Update class="update_button"></td></tr>
		 </table>
		 
		 </form>
		 
		 <?php
		 echo "</fieldset>";
	}
	 
	?>
	<?php if($show_image) { ?>
		<?php echo $util->selectJSDate("cal8","fcn8","y8","m8","d8")?>
		<?php echo $util->selectJSDate("cal9","fcn9","y9","m9","d9")?>
		<fieldset><legend><a href="#image" onclick='showhide(2);return false;'>Add New Image</a></legend>
		<form action="patient.php?pf=5&patientid=<?php echo $patientid?>&consultid=<?php echo $consultid?>#image" method="post" enctype="multipart/form-data">
		<table style="margin-left: auto;margin-right: auto;display:none" id=2>
			<tr>
				<td>
					<table>	
					<?php for($i=1;$i<3;$i++){ 
						$dx = $i + 7;
					?>
					<tr><td><strong>Date</strong></td><td><strong>Name</strong></td><td><strong>Image</strong></td></tr>
					<tr>
						<td>
						<?php echo $util->selectNewDate("m".$dx,"d".$dx,"y".$dx,$consult_date)?><A HREF="#" onClick="cal<?php echo $dx?>.showCalendar('anchor<?php echo $dx?>'); return false;" TITLE="" NAME="anchor<?php echo $dx?>" ID="anchor<?php echo $dx?>">select date</A>
						</td>
						<td>
							<?php
							$sql = "select distinct name from image where userid=$userid order by name";
							$sal_arr = $db->getOneCol($sql);
							if($sal_arr)
							{
								echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_image_$i","image_$i","onchange=chooseType(\"sel_image_$i\",\"imagetest_$i\")");
							}
							?>
							<input type=text name="imagetest[]" class="input" id="imagetest_<?php echo $i?>"></td>
						<td><input type=file name="imageimage[]" id="imageimage[]"></td></tr>
						<tr><th colspan=3><strong>Description</strong></th></tr>
						<tr><td colspan=3><textarea cols=50 rows=3 name="imagedesc[]" id="imagedesc[]"></textarea></td>
					</tr>
					<?php } ?>
					</table>
				</td>
			</tr>
			<tr><td><input type=submit name=submit value=Upload class="upload_button"></td></tr>
		</table>
		</form>
		</fieldset>
		
	<?php } ?>
	
	
	<?php echo $util->selectJSDate("cal4","fcn4","y4","m4","d4")?>
	<?php echo $util->selectJSDate("cal5","fcn5","y5","m5","d5")?>
	
	<!-- CASH PAYMENT -->
	<?php echo $util->selectJSDate("cal7","fcn7","y7","m7","d7")?>
	<!-- CHECK PAYMENT -->
	<?php echo $util->selectJSDate("cal6","fcn6","y6","m6","d6")?>
	
	<h3 class=pageTitle2> &nbsp;Statement<a name=bill></a> <a href=#top class=top_button>TOP</a></h3>
	<?php
	
	
	/*
	 * ============================== S T A T E M E N T S ===========================
	 */
	$bill = new Bill();
	$do_edit = false;
	$url = "patient.php?pf=5&patientid=$patientid&consultid=$consultid";
	
		
	
	if(isset($_REQUEST['dobill']) && $_REQUEST['dobill'] == "Add")
	{
		$_REQUEST['userid'] = $userid;
		if($bill->boolBilled($_REQUEST['consultid']) == 0)
		{
			$_REQUEST['date'] = $_REQUEST['y4']."-".$_REQUEST['m4']."-".$_REQUEST['d4'];
			$bill->add($_REQUEST);
		}
		else
		{
			echo "<p style=color:red>Billed already</p>";
		}
	}
	if(isset($_REQUEST['do']) && $_REQUEST['do'] == "billdelete")
	{
		if(isset($_REQUEST['billid']))
		{
			$bill->delete($_REQUEST['billid']);
		}
	}
	elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "edit")
	{
		$do_edit = true;
		$billid = $_REQUEST['billid'];
		$bill_arr = $bill->getBill($billid);
		
		?>
		<fieldset><legend>Update Bill</legend>
		<form action="<?php echo $url?>#bill" method=post>
			<input type=hidden name=billid value="<?php echo $billid?>">
		<table cellpadding=2 cellspacing=2 style=margin-left:auto;margin-right:auto>
			<tr><td>Date</td>
				<td><?php echo $util->selectNewDate("m4","d4","y4",$bill_arr['date'])?><A HREF="#" onClick="cal4.showCalendar('anchor4'); return false;" TITLE="" NAME="anchor4" ID="anchor4">select date</A>
				</td>
			</tr>
			<tr><td>HMO</td><td><?php echo $bill_arr['hmo']?></td></tr>
			<tr><td>HMO ID</td><td><?php echo $bill_arr['hmoid']?></td></tr>
			<tr><td>Company</td><td><?php echo $bill_arr['company']?></td></tr>
			<tr><td>Approval Code</td><td><input type=text name=approvalcode size=20 value="<?php echo $bill_arr['approvalcode'];?>"/></td></tr>
			<tr><td>PF</td><td><input type=text name=profee size=20 class="input" value="<?php echo $bill_arr['profee']?>"></td></tr>
			<tr><td>Discount (%)</td>
				<td>
				<?php
				$percentArr = array("5","10","20","25","50");
				$table = "statement";
				$col = "discount";
				$sql = "select  distinct $col from $table where $col != '' order by $col";
				$sal_arr = $db->getOneCol($sql);
				$sal_arr = array_unique(array_merge($percentArr,$sal_arr));
				if(count($sal_arr) > 0)
					echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
				?>
				<input type=text name=discount id=discount size=20 class="input" value="<?php echo $bill_arr['discount']?>">
				</td>
			</tr>
			<tr><td>Remark</td><td>
				<?php 
							$sql = "select  distinct remark from statement order by remark";
							$sal_arr = $db->getOneCol($sql);
							$sal_arr['No PF'] = "No PF";
							echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_rem","remark","onchange=chooseType(\"sel_rem\",\"remark_bill\")"); 
						?>
						<input type=text name=remark id=remark_bill size=50 class="input" value="<?php echo $bill_arr['remark']?>">
				</td>
			</tr>
			<tr><td colspan=2><input type=submit name=dobill value=Update class="update_button"> <input type=submit value=Cancel class="cancel_button"></td></tr>
		</table>
		</form>
		</fieldset>
		
		<?php
	}
	elseif(isset($_REQUEST['dobill']) && $_REQUEST['dobill'] == "Update")
	{
		
		$_REQUEST['date'] = $_REQUEST['y4']."-".$_REQUEST['m4']."-".$_REQUEST['d4'];
		$bill->update($_REQUEST);
	}
	
	
	
	if(isset($_REQUEST['billpayment']) && $_REQUEST['billpayment'] == "Add Cash Payment")
	{
		
		$_REQUEST['date'] = $_REQUEST['y7']."-".$_REQUEST['m7']."-".$_REQUEST['d7'];
		$success = $bill->addCashPayment($_REQUEST);
		if($success == 0)
		{
			echo "<p style=color:red>No amount indicated. Add failed.</p>";
		}
		
	}
	elseif(isset($_REQUEST['billpayment']) && $_REQUEST['billpayment'] == "Add Check Payment")
	{
		$_REQUEST['date'] = $_REQUEST['y8']."-".$_REQUEST['m8']."-".$_REQUEST['d8'];
		$success = $bill->addCheckPayment($_REQUEST);
		if($success == 0)
		{
			echo "<p style=color:red>No amount indicated. Add failed.</p>";
		}
	}
	elseif(isset($_REQUEST['billpayment']) && $_REQUEST['billpayment'] == "Add Credit Payment")
	{
		$_REQUEST['duedate'] = $_REQUEST['y9']."-".$_REQUEST['m9']."-".$_REQUEST['d9'];
		$success = $bill->addCreditPayment($_REQUEST);
		if($success == 0)
		{
			echo "<p style=color:red>No amount indicated. Add failed.</p>";
		}
	}
	
	if(isset($_REQUEST['dobill']) && $_REQUEST['dobill'] == "cashdel")
	{
		 $bill->delCashPayment($_REQUEST['cashid']);
	}
	elseif(isset($_REQUEST['dobill']) && $_REQUEST['dobill'] == "checkdel")
	{
		 $bill->delCheckPayment($_REQUEST['checkid']);
	}
	elseif(isset($_REQUEST['dobill']) && $_REQUEST['dobill'] == "creditdel")
	{
		 $bill->delCreditPayment($_REQUEST['creditid']);
	}
	
	
	// === V I E W  B I L L =============================================
	
	
	
	if($do_edit == false)
	{
	$bf = "";
	$bill_arr = $bill->getConsultBill($consultid);
	if(count($bill_arr) > 1)
	{
		$payment_cash = $bill->getTotalCashPayments($bill_arr['id']);
		$payment_check = $bill->getTotalCheckPayments($bill_arr['id']);
		$payment_credit = $bill->getTotalCreditPayments($bill_arr['id']);
		$discountBool = 0;
		$amount_paid = $payment_cash + $payment_check + $payment_credit;
		$billid = $bill_arr['id'];
		?>
		<table cellpadding=15 cellspacing=4 align=center class='image_view'>
			<tr>
				<th>HMO</th><th>HMO ID</th><th>Company</th><th>Approval Code</th>
				<th>PF</th>
				<?php if($bill_arr['discount'] > 0) {
					$discountBool = 1;
				?>
				<th align=center>Discount</th>
				<th align=center>Amount Due</th>
				<?php } ?>
				<th>Payment</th><th>Remarks</th>
			</tr>
			<tr>
				<?php
				echo '<td align=center>'.$bill_arr['hmo'].'</td>';
				echo '<td align=center>'.$bill_arr['hmoid'].'</td>';
				echo '<td align=center>'.$bill_arr['company'].'</td>';
				echo '<td align=center>'.$bill_arr['approvalcode'].'</td>';
				//PF
				if($bill_arr['profee'] > 0)
				{
					echo "<td align=center>Php".number_format($bill_arr['profee'],2,".",",")."</td>";
				}
				else
				{
					echo "<td align=center>Php".number_format(00.00,2,".",",")."</td>";
				}
				
				if(isset($discountBool) && $discountBool == 1)
				{
					
					echo "<td align=center>".$bill_arr['discount']."%</td>";
					echo "<td align=center>Php".number_format($bill->afterDiscount($bill_arr['profee'],$bill_arr['discount']),2,".",",")."</td>";
				}
				
				$amount_due =  $bill->afterDiscount($bill_arr['profee'],$bill_arr['discount']);
				$color = "";
				$balance = "";
				if($amount_paid > $amount_due)
				{
					$color = "style=color:blue";
				}
				elseif($amount_paid < $amount_due)
				{
					$color = "style=color:red";
					$bal = $amount_due - $amount_paid;
					$balance = "<tr>";
					if($discountBool == 1)
					{
						$balance.="<td></td><td></td>";
					}
					$balance.="<td align=right>Balance</td><td align=right $color><strong>Php".number_format($bal,2,".",",")."</strong></td></tr>";
				}
				echo "<td align=right $color><strong>Php".number_format($amount_paid,2,'.',',')."</strong></td>";
				echo "<td align=center>".$bill_arr['remark']."</td><td>".
				$util->setEdel("$url&do=edit&billid=".$bill_arr['id']."#bill","$url&do=billdelete&billid=".$bill_arr['id']."#bill");
				?>
			</tr>
			<?php
			echo $balance; //prints balance if needed
			if($payment_cash > 0)
			{
				$cash_arr = $bill->getAllCashPayments($billid);
				foreach($cash_arr as $cash_val)
				{
					echo "<tr><td align=right>".$util->convertmysqldate($cash_val['date'],"M j, Y")." " .
						"Cash Payment</td><td>Php".number_format($cash_val['amount'],2,'.',',')."</td>" .
						"<td>".$util->setEdel('',"$url&dobill=cashdel&cashid=".$cash_val['id']."#bill")."</td></tr>";
				}	
			}
	//-- CHECK PAYMENT
				if($payment_check)
				{
					echo "<tr><td colspan=1 align=right></td>";
					echo "<td align=center>Date</td><td align=center>Bank</td><td align=center>Check No</td><td align=right>Amount</td></tr>";
					$check_arr = $bill->getAllCheckPayments($billid);
					foreach($check_arr as $check_row)
					{
						echo "<tr><td colspan=1></td><td align=center>".$util->convertmysqldate($check_row['date'],"F j, Y")."</td>
						<td align=center>".$check_row['bank']."</td><td align=center>".$check_row['checkno']."</td>
						<td align=right>Php".number_format($check_row['amount'],2,".",",")."</td>
						<td>".$util->setEdel("","$url&dobill=checkdel&checkid=".$check_row['id']."#bill")."</td></tr>";
					}
					
				}
				//-- CREDIT PAYMENT
				if($payment_credit)
				{
					$credit_arr = $bill->getAllCreditPayments($billid);
					echo "<tr><td></td>";
					echo "<td align=center>Name</td><td align=center>Card</td>
					<td align=center>Exp Date</td><td align=center>Card No</td><td align=right>Amount</td></tr>";
					foreach($credit_arr as $credit_row)
					{
						echo "<tr><td></td><td align=center>".$credit_row['name']."</td><td align=center>".$credit_row['card']."</td>
						<td align=center>".$util->convertmysqldate($credit_row['duedate'],"F j, Y")."</td>
						<td align=center>".$credit_row['cardno']."</td>
						<td align=right>Php".number_format($credit_row['amount'],2,".",",")."</td>
						<td>".$util->setEdel("","$url&dobill=creditdel&creditid=".$credit_row['id']."#bill")."</td></tr>";
					}
				}
			
			?>
		</table>
		
		<?php if($amount_due > $payment_cash + $payment_check + $payment_credit) { ?>
		<fieldset ><legend>Add Payment</legend>
			<form action="<?php echo $url?>#bill" method=post>
			<input type=hidden name=billid value='<?php echo $bill_arr['id'];?>'>
			<table>
				<tr><td>
					<?php echo $util->selectNewDate("m7","d7","y7",$consult_date);?>
					<A HREF="#" onClick="cal7.showCalendar('anchor7'); return false;" TITLE="Calendar" NAME="anchor7" ID="anchor7">select date</A>
					</td><td class=b_c>Cash</td><td><input type=text name=amount id=amount size=10 value="<?php echo $bal;?>"></td></tr>
				<tr><td colspan=2><input type=submit name=billpayment value='Add Cash Payment'></td></tr>
			</table>
			</form>
			<!-- CHECK PAYMENT -->
						<form action="<?php echo $url?>#bill" method=post>
						<input type=hidden name=billid value='<?php echo $bill_arr['id'];?>'>
						<table>
							<tr><td >Date</td><td >Bank</td><td >Check No</td><td >Amount</td></tr>
							<tr><td>
								<?php echo $util->selectDate("y8","m8","d8");?>
								</td>
								<td>
									<?php 
										$sql = "select  distinct bank from checkpayment order by bank";
										$sal_arr = $db->getOneCol($sql);
										if(count($sal_arr) > 0)
										{
											echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_bank","bank","onchange=chooseType(\"sel_bank\",\"bank\")");
										} 
									?>
									<input type=text name=bank id=bank size=30>
								</td>
								<td><input type=text name=checkno id=checkno size=10></td>
								<td><input type=text name=amount id=amount size=10 value="<?php echo $bal;?>"></td></tr>
							<tr><td colspan=2><input type=submit name=billpayment value='Add Check Payment'></tr>
						</table>
						</form>
						<!-- CREDIT CARD PAYMENT -->
						<form action="<?php echo $url;?>#bill" method=post>
						<input type=hidden name=billid value='<?php echo $bill_arr['id'];?>'>
						<table>
							<tr><td>Name</td><td><input type=text name=name id=name size=50></td></tr>
							<tr>
								<td>Card</td>
								<td>
									<?php 
										$sql = "select  distinct card from creditpayment order by card";
										$sal_arr = $db->getOneCol($sql);
										if(count($sal_arr) > 0)
										{
											echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_card","card","onchange=chooseType(\"sel_card\",\"card\")");
										} 
									?>
									<input type=text name=card id=card size=50>
								</td>
							</tr>
							<tr><td>Card No</td><td><input type=text name=cardno id=cardno size=30></td></tr>
							<tr><td>Due Date</td><td><?php echo $util->selectDate("y9","m9","d9");?></td></tr>
							<tr><td>Amount</td><td><input type=text name=amount id=amount size=10 value="<?php echo $bal;?>"></td></tr>
							<tr><td colspan=2><input type=submit name=billpayment value='Add Credit Payment'></tr>
						</table>
						</form>
		</fieldset>
		<?php
		}//end if due != cash + check
	}
	else
	{
	?>
		<fieldset><legend>Add Bill</legend>
		<form action="<?php echo $url?>#bill" method=post>
			<input type=hidden name=vac_amount value='<?php echo @$vac_amount?>'>
			
			
			
			
		<table cellpadding=2 cellspacing=2 style=margin-left:auto;margin-right:auto>
			<tr><td>Date</td>
				<td><?php echo $util->selectNewDate("m4","d4","y4",$consult_date);?><A HREF="#" onClick="cal4.showCalendar('anchor4'); return false;" TITLE="" NAME="anchor4" ID="anchor4">select date</A>
				</td>
			</tr>
			<tr><td>HMO</td><td><input type=text name=hmo value="<?php echo $patient->data['hmo']?>" size=20 /></td></tr>
			<tr><td>HMO ID</td><td><input type=text name=hmoid value="<?php echo $patient->data['hmoid']?>" size=20 /></td></tr>
			<tr><td>Company</td><td><input type=text name=company value="<?php echo $patient->data['company']?>" size=20 /></td></tr>
			
			
			
			<tr><td>Approval Code</td><td><input type=text name=approvalcode size=20 /></td></tr>
			<tr><td>PF</td><td><input type=text name=profee size=20 class="input"></td></tr>
			<tr><td>Discount (%)</td>
					<td>
					<?php
					$percentArr = array("5","10","20","25","50");
					$table = "statement";
					$col = "discount";
					$sql = "select  distinct $col from $table where $col != '' and patientid in (select id from patient where userid=$userid) order by $col";
					$sal_arr = $db->getOneCol($sql);
					$sal_arr = array_unique(array_merge($percentArr,$sal_arr));
					if(count($sal_arr) > 0)
					{
						echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
					}
					?>
					<input type=text name=discount id=discount size=20 value="" class="input">
					</td>
				</tr>
			<tr><td>Remarks</td><td>
				<?php 
							$sql = "select  distinct remark from statement where patientid in (select id from patient where userid=$userid) order by remark";
							$sal_arr = $db->getOneCol($sql);
							$sal_arr['No PF'] = "No PF";
							echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_rem","remark","onchange=chooseType(\"sel_rem\",\"remark_bill\")"); 
						?>
						<input type=text name=remark id=remark_bill size=50 class="input">
				</td>
			</tr>
			<tr><td colspan=2><input type=submit name=dobill value=Add class="add_button"> <input type=reset class="reset_button"></td></tr>
		</table>
		</form>
		</fieldset>
		
	<?php }//end add bill 
	} ///END VIEW 
} // LIMITS ACCESS OF THE STAFF TO VITAL SIGN ONLY 
?>
</div>