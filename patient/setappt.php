<div id=setappt>
<?php
$userid = $_SESSION['auth']['userid'];

$appt = new Appt();
$appt->setPatient($patientid);
$msg = "";
$db = new DBConnect();

$today = getDate();


$year = $_SESSION['year'];
$mon = $_SESSION['mon'];
$day = $_SESSION['day'];

$appt_date = $year."-".$mon."-".$day;

if(isset($_REQUEST['y']) && $_REQUEST['m'] && $_REQUEST['d'])
{
	$appt_date =  $_REQUEST['y']."-".$_REQUEST['m']."-".$_REQUEST['d'];
	$mon=$_REQUEST['m'];
	$year=$_REQUEST['y'];
	$day=$_REQUEST['d'];
}

if(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "Add")
{
	
	
	if($_REQUEST['ampm1'] == "PM" && $_REQUEST['pref_hour1'] < 12)
	{
		$_REQUEST['pref_hour1'] = $_REQUEST['pref_hour1'] + 12;
	}
	
	$startdate = $appt_date." ".$_REQUEST['pref_hour1'].":".$_REQUEST['pref_min1'].":00";
	
	if($_REQUEST['ampm2'] == "PM" && $_REQUEST['pref_hour2'] < 12)
	{
		$_REQUEST['pref_hour2'] = $_REQUEST['pref_hour2'] + 12;
	}
	
	$enddate = $appt_date." ".$_REQUEST['pref_hour2'].":".$_REQUEST['pref_min2'].":00";
	
	
	$dum = $appt->setAppt($startdate,$enddate,$_REQUEST['visittype'],$_REQUEST['remark'],$userid);
	if($dum > 0)
	{
		$msg = "Insert successful.";
	}
	else
	{
		$msg = "<p style=color:red>Appointment failed to set.</p>";
			
	}
	
}
elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "del")
{
	$appt->delAppt($_REQUEST['apptid']);

}
?>

<div class=pageTitle2> &nbsp;Set Appointment for <?php echo $util->convertmysqldate($appt_date,"M j, Y");?></div>
<?php
if($msg)
{
	echo "<br>".$msg;
}

//==== VIEW APPOINTMENT FOR THE SELECTED DATE ==================================================================

$count = 1;

$appt_all = $appt->getAllApptsBydate($appt_date,$userid);


$total_rows = count($appt_all);

$last_end_time = 0;
$lasttime = 0; //24hr time

/*
 * ============================== L I S T  A L L  A P P O I N T M E N T S ==============================
 */
if($total_rows > 0)
{
	echo "<table cellpadding=2 cellspacing=2 border=0><tr><th colspan=3>Start</th>" .
			"<th>End</th><th>Visit Type</th><th>Remarks</th></tr>";
	//foreach($appt_all as $row)
	for($i = 0;$i<$total_rows;$i++)
	{
		$row = $appt_all[$i];
		if($i < $total_rows)
		{
			if($row['patientid'] != $patientid)
			{
				$otherPatientAppt = new Patient();
				$p_data = $otherPatientAppt->getData($row['patientid']);
				echo "<tr><td>$count.</td><td><a href=\"patient.php?pf=2&patientid=".$row['patientid']."\">".$otherPatientAppt->data['firstname']." ".$otherPatientAppt->data['lastname']."</a></td>" .
						"<td>".$util->setDateTime($row['start'],"g:i A")."</td><td>".$util->setDateTime($row['end'],"g:i A")."</td>" .
					"<td>".$row['remark']."</td></tr>";
			}
			else
			{ 
				echo "<tr><td>$count.</td><td>".$data['firstname']." ".$data['lastname']."</td><td>".$util->setDateTime($row['start'],"g:i A")."</td><td>".$util->setDateTime($row['end'],"g:i A")."</td>" .
				"<td align=center>".$row['visittype']."</td>".
				"<td>".$row['remark']."</td><td>".$util->setEdel("","patient.php?pf=3&do=del&apptid=".$row['id']."&patientid=$patientid&y=".$year."&m=".$mon."&d=".$day)."</td></tr>";
				
			}
			$count++;
			//$last_end_time = setDateTime($row['end'],"g-i-A");
			$lasttime = $util->setDateTime($row['end'],"H-i");
		}
	}
	echo "</table>";
}
else
{
	
}

/*
 * ================================= S E T  N E W  A P P O I N T M E N T ====================================
 */

?>
<br><br><br><br>
<fieldset><legend>New Appointment</legend>
		<form action="patient.php?pf=3&patientid=<?php echo $patientid?>" method=post>
		<table><tr><td colspan=6>
<?php
if(isset($_REQUEST['y']) && $_REQUEST['m'] && $_REQUEST['d'])
{
	echo "<input type=hidden name=m value=\"".$_REQUEST['m']."\">";
	echo "<input type=hidden name=d value=\"".$_REQUEST['d']."\">";
	echo "<input type=hidden name=y value=\"".$_REQUEST['y']."\">";
}
else
{
	echo "<input type=hidden name=m value=\"".$mon."\">";
	echo "<input type=hidden name=d value=\"".$day."\">";
	echo "<input type=hidden name=y value=\"".$year."\">";
}

$end_h = 0; $end_m = 0; $end_ampm = 0; 
if($lasttime)
{
	@list($end_h,$end_m,$end_ampm) = split("-",$lasttime);
}
?>
		<tr><th>Type of Visit</th>
		<td nowrap>
			<select name=visittype id=visittype class="input">
			<?php 
			
			$sal_arr = $appt->getVisitType();
			foreach($sal_arr as $val)
			{
				echo "<option>$val</option>";
			}
			?>
			</select>
		</td>
		</tr>
		<tr><th>Start</th><td><select name="pref_hour1" class="input">
		      <?php
	            	$pref_hour = array("8","9","10","11","12","1","2","3","4","5","6","7");
					foreach($pref_hour as $value)
			        {
			        	if($end_h > 12)
			        	{
							if($end_h%12 == $value)
							{
				        		print "<option selected>$value</option>";
							}
							else
							{
								print "<option>$value</option>";
							}
			        	}
			        	else
			        	{
			        		if($end_h == $value)
							{
				        		print "<option selected>$value</option>";
							}
							else
							{
								print "<option>$value</option>";
							}
			        	}
					}
			   ?>
	            </select>
			      hr
			      <select name="pref_min1" class="input">
			      <?php
	            	$pref_min = array("00","15","30","45");
					foreach($pref_min as $value)
			        {
			        	if($end_m == $value)
						{
			        		print "<option selected>$value</option>";
						}
						else
						{
							print "<option>$value</option>";
						}
			        }
			        
	           ?>
			    </select>
			    min
		      	<select name="ampm1" class="input">
		      	<?php
		      		if($end_h > 11)
		      		{
		      			print "<option >AM</option><option selected>PM</option>";
					}
					else
					{
						print "<option selected>AM</option><option>PM</option>";
					}
		      	?>
		      	
	          	</select> 
	          	
	     </td><td></td></tr>
	     <?php
	     	$startTime = $end_h + $end_m/60;
	     	//echo $startTime;
	     	$endTime = $startTime + .5; // .5 = 30/60
	     	//echo $endTime;
	     	$new_endHour = intval($endTime);
	     	$new_endMinute = 60*($endTime - $new_endHour);
	     	//echo   $new_endHour." - ".$new_endMinute;
	     ?>
	     <tr><th>End</th>
	     <td> 
		      <select name="pref_hour2" class="input">
		      <?php
	            	$pref_hour = array("8","9","10","11","12","1","2","3","4","5","6","7");
					foreach($pref_hour as $value)
			        {
			        	if($new_endHour)
			        	{
							if($new_endHour > 12)
							{
								if($value == $new_endHour%12)
								{
					        		print "<option selected>$value</option>";
								}
								else
								{
									print "<option>$value</option>";
								}
							}
							else
							{
								if($value == $new_endHour)
								{
					        		print "<option selected>$value</option>";
								}
								else
								{
									print "<option>$value</option>";
								}	
							}
			        	}
			        	else
			        	{
							if($value == "8")
							{
				        		print "<option selected>$value</option>";
							}
							else
							{
								print "<option>$value</option>";
							}
			        	}
					}
			   ?>
	            </select>
			      hr 
			      <select name="pref_min2"  class="input">
			      <?php
	            	$pref_min = array("00","15","30","45");
					foreach($pref_min as $value)
			        {
			        	if(intval($new_endMinute) == intval($value))
						{
			        		print "<option selected>$value</option>";
						}
						else
						{
							print "<option>$value</option>";
						}
					}
			        
	           ?>
			    </select>
			    min
		      	<select name="ampm2" class="input">
		      	<?php
		      		if($new_endHour > 11)
		      		{
		      			print "<option >AM</option><option selected>PM</option>";
					}
					else
					{
						print "<option selected>AM</option><option>PM</option>";
					}
		      	?>
		      	
	          	</select>	
			</td></tr>
			<tr><th>Remarks</th><td><input type=text name=remark size=50 class="input"></td></tr>
			<tr><td colspan=2 align=right><input type=submit name=submit value=Add class="add_button"></td></tr>
		</table></form></fieldset>

<p style=padding-top:20pt>
<a href="patient.php?pf=4&patientid=<?php echo $patientid?>" class="calendar_button"><!-- <img src="image/calendar.png" border=0>  --> Calendar </a>
</p>
</div>