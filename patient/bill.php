<!-- CASH PAYMENT -->
<?php echo $util->selectJSDate("cal4","fcn4","y4","m4","d4")?>
<?php echo $util->selectJSDate("cal7","fcn7","y7","m7","d7")?>

<script type="text/javascript">
function showbill(id)
{
		new Ajax.Request('ajax/bill_ajax.php', 
		{
			parameters:'billid='+id+'&do=show',
			onSuccess: function(t){
				//document.getElementById('response').innerHTML = t.responseText;
				document.getElementById('archive'+id).innerHTML = t.responseText;
			},
			 onFailure: function(t) {
        		alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
    		}
		}
		);
}

function hidebill(id)
{	
		new Ajax.Request('ajax/bill_ajax.php', 
		{
			parameters:'billid='+id+'&do=hide',
			onSuccess: function(t){
				//document.getElementById('response').innerHTML = t.responseText;
				document.getElementById('archive'+id).innerHTML = t.responseText;
			},
			 onFailure: function(t) {
        		alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
    		}
		}
		);
	
}
</script>

<p class="pageTitle2"> &nbsp;Statements</p>


<?php
//=== INITIALIZATIONS ====================================================
$util = new Util();
$bill = new Bill();
$db = new DBConnect();
$patientid = $_REQUEST['patientid'];
$view_on = true;
$url = "patient.php?pf=8&patientid=$patientid";
$userid = isset($_SESSION['auth']['userid'])?$_SESSION['auth']['userid']:0;
$auth_arr =  $_SESSION['auth']; 


if(isset($_REQUEST['dobill']) && $_REQUEST['dobill'] == "Add")
{
	$_REQUEST['date'] = $_REQUEST['y4']."-".$_REQUEST['m4']."-".$_REQUEST['d4'];
	$_REQUEST['vac_amount'] = 0;
	$_REQUEST['userid'] = $userid;
	if($bill->boolBilled($_REQUEST['date']) == 0)
	{
		$bill->add($_REQUEST);
		echo $bill->msg;
	}
	else
	{
		echo "<p style=color:red>Billed already</p>";
	}
}
if(isset($_REQUEST['do']) && $_REQUEST['do'] == "billdelete")
{
	if(isset($_REQUEST['billid']) && $_REQUEST['billid'])
	{
		$bill->delete($_REQUEST['billid']);
	}
}
elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "billedit")
{
	$view_on = false;
	$billid = $_REQUEST['billid'];
	$bill_arr = $bill->getBill($billid);
	

	?>
		<fieldset><legend>Update Bill</legend>
	<form action="<?php echo $url?>&do=billupdate#bill" method=post>
		<input type=hidden name=billid value="<?php echo $billid?>">
	<table cellpadding=2 cellspacing=2 align=center>
		<tr><td>Date</td>
			<td>
			<?php
			if($auth_arr['access'] == "admin" || $auth_arr['access'] == "doctor")
			{
				echo $util->selectNewDate("m4","d4","y4",$bill_arr['date']);
				echo "<A HREF=\"#\" onClick=\"cal4.showCalendar('anchor4'); return false;\" TITLE=\"\" NAME=\"anchor4\" ID=\"anchor4\">select date</A>";
			}
			else
			{
				echo $util->convertmysqldate($bill_arr['date'],"M j, Y");
				list($y,$m,$d) = split("-",$bill_arr['date']);
				?>
				<input type=hidden name=m4 value="<?php echo $m?>">
				<input type=hidden name=d4 value="<?php echo $d?>">
				<input type=hidden name=y4 value="<?php echo $y?>">
				<?php	
			}
			?>
			
			</td>
		</tr>
		<tr><td>HMO</td><td><?php echo $patient->data['hmo']?></td></tr>
		
		<tr><td>PF</td><td>
		<?php
		if($auth_arr['access'] == "admin" || $auth_arr['access'] == "doctor")
		{
		?>
			<input type=text name=profee size=20 class=input value="<?php echo $bill_arr['profee']?>">
		<?php
		}
		else
		{
			echo $bill_arr['profee']; 
		}
		?>
		
		</td></tr>
		<tr><td>Discount (%)</td>
					<td>
					<?php
					$percentArr = array("5","10","20","25","50");
					$table = "statement";
					$col = "discount";
					$sql = "select  distinct $col from $table where $col != '' and userid=$userid  order by $col";
					$sal_arr = $db->getOneCol($sql);
					$sal_arr = array_unique(array_merge($percentArr,$sal_arr));
					if(count($sal_arr) > 0)
					{
						echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
					}
					?>
					<input type=text name=discount id=discount size=20 class=input value="<?php echo $bill_arr['discount'];?>">
					</td>
				</tr>		
		<tr><td>Remark</td><td>
			<?php 
						$sql = "select  distinct remark from statement order by remark";
						$sal_arr = $db->getOneCol($sql);
						//echo $db->msg;
						$sal_arr['No PF'] = "No PF";
						echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_rem","remark"," style=width:80px onchange=chooseType(\"sel_rem\",\"remark_bill\")"); 
					?>
					<input type=text name=remark id=remark_bill size=50 class=input value="<?php echo $bill_arr['remark']?>">
			</td>
		</tr>
		<tr><td colspan=2 align=right><input type=submit value=Cancel class=cancel_button> <input type=submit name=dobill value=Update class=update_button> </td></tr>
	</table>
	</form>
	</fieldset>
	
		
	
	<?php
	
}
elseif(isset($_REQUEST['do']) &&  $_REQUEST['do'] == "billupdate")
{
	//print_r($_REQUEST);
	$_REQUEST['date'] = $_REQUEST['y4']."-".$_REQUEST['m4']."-".$_REQUEST['d4'];
	$bill->update($_REQUEST);
}

if(isset($_REQUEST['billpayment']) && $_REQUEST['billpayment'] == "Add Cash Payment")
{
	$_REQUEST['date'] = $_REQUEST['y7']."-".$_REQUEST['m7']."-".$_REQUEST['d7'];
	$success = $bill->addCashPayment($_REQUEST);
	if($success == 0)
	{
		echo "<p style=color:red>No amount indicated. Add failed.</p>";
	}
}
elseif(isset($_REQUEST['billpayment']) && $_REQUEST['billpayment'] == "Add Check Payment")
{
	$_REQUEST['date'] = $_REQUEST['y8']."-".$_REQUEST['m8']."-".$_REQUEST['d8'];
	$success = $bill->addCheckPayment($_REQUEST);
	if($success == 0)
	{
		echo "<p style=color:red>No amount indicated. Add failed.</p>";
	}
}
elseif(isset($_REQUEST['billpayment']) && $_REQUEST['billpayment'] == "Add Credit Payment")
{
	$_REQUEST['duedate'] = $_REQUEST['y9']."-".$_REQUEST['m9']."-".$_REQUEST['d9'];
	$success = $bill->addCreditPayment($_REQUEST);
	if($success == 0)
	{
		echo "<p style=color:red>No amount indicated. Add failed.</p>";
	}
}

if(isset($_REQUEST['dobill']) && $_REQUEST['dobill'] == "cashdel")
{
	 $bill->delCashPayment($_REQUEST['cashid']);
}
elseif(isset($_REQUEST['dobill']) && $_REQUEST['dobill'] == "checkdel")
{
	 $bill->delCheckPayment($_REQUEST['checkid']);
}
elseif(isset($_REQUEST['dobill']) && $_REQUEST['dobill'] == "creditdel")
{
	 $bill->delCreditPayment($_REQUEST['creditid']);
}

/*
 * ======== VIEW STATEMENTS ============================================
 */


if($view_on)
{

	$bf = "";
	

	if($auth_arr['access'] == "admin" || $auth_arr['access'] == "doctor")
	{
		$arr = $bill->getAllBill($patientid);
	}
	else
	{
		$sql = "select * from statement where patientid=$patientid and `show`='1' order by date desc";
		$arr = $db->getAllRecord($sql);
	}
	$start = 0;
	if(isset($_REQUEST['start']))
	{
		$start = $_REQUEST['start'];
	}
	$rowstoview = 10;
	if(isset($_REQUEST['rowstoview']))
	{
		$rowstoview = $_REQUEST['rowstoview'];
	}
		
	$row_count = count($arr);
	$count = 1 + $start;
	
	
	
	if($row_count > 0)
	{
		?>
		<table align=center class=view cellspacing=4 cellpadding=4 >
			<tr><th colspan=2>Date</th><th>PF</th>
			<?php if($auth_arr['access'] == "admin"  || $auth_arr['access'] == "doctor")
				{
					echo "<th>Discount</th><th>Amount Due</th>";
				}
				?>
			<th>Payment</th><th>Remarks</th>
			</tr>
		
		<?php
		$totalpaid = 0; $totaldue = 0;
		for($i = $start; $i < $start + $rowstoview; $i++)
		{
			
			if($i < $row_count)
			{
			$row_color = " style=background-color:darkgray;color:blue;text-align:center";
			if($i%2 == 0)
			{
				$row_color = " style=background-color:lightgray;text-align:center";
			}
			$bill_arr = $arr[$i]; //print_r($row);echo "<br>";
	
		
			$payment_cash = $bill->getTotalCashPayments($bill_arr['id']);
			$payment_check = $bill->getTotalCheckPayments($bill_arr['id']);
			$payment_credit = $bill->getTotalCreditPayments($bill_arr['id']);
			
			$amount_paid = $payment_cash + $payment_check + $payment_credit;
			$billid = $bill_arr['id'];
			echo "<tr><td  $row_color><strong>$count.</strong><a name=$count></a></td>";
			if($bill_arr['consultid'] > 0)
			{
				echo "<td><a href=\"patient.php?pf=5&consultid=".$bill_arr['consultid']."&patientid=$patientid\">".
					$util->convertmysqldate($bill_arr['date'],"M j, Y")."</a></td>";
			}
			else
			{
				echo "<td>".$util->convertmysqldate($bill_arr['date'],"M j, Y")."</td>";
			}
					
					if($bill_arr['profee'] > 0)
					{
						echo "<td align=center>Php".number_format($bill_arr['profee'],2,".",",")."</td>";
					}
					else
					{
						echo "<td align=center>Php".number_format(00.00,2,".",",")."</td>";
					}
					
					$discount = 0;
					if(isset($bill_arr['discount']))
					{
						$discount = $bill_arr['discount'];
					}
					if($auth_arr['access'] == "admin" || $auth_arr['access'] == "doctor")
					{
						
						if($discount)
						{
							echo "<td align=center>".$bill_arr['discount']."%</td>";
							
						}
						else
						{
							echo "<td align=center>0%</td>";
						}
						
						echo "<td align=center>Php".number_format($bill->afterDiscount($bill_arr['profee'],$discount),2,".",",")."</td>";	
					}
					
					$amount_due = $bill->afterDiscount($bill_arr['profee'],$discount);
					$color = "";
					$balance = "";
					if($amount_paid > $amount_due)
					{
						$color = "style=color:blue";
					}
					elseif($amount_paid < $amount_due)
					{
						$color = "style=color:red";
						$bal = $amount_due - $amount_paid;
						$balance = "<tr><td align=right colspan=3>Balance</td><td align=right $color><strong>Php".number_format($bal,2,".",",")."</strong></td></tr>";
					}
					echo "<td align=right $color><strong>Php".number_format($amount_paid,2,'.',',')."</strong></td>";
					
					
					$del_aname = $count - 1;
					echo "<td align=center>".$bill_arr['remark']."</td>";
					if($auth_arr['access'] == "admin"  || $auth_arr['access'] == "doctor")
					{
						echo "<td>"; 
						if($bill_arr['show'] == 0)
						{
							echo "<div id=archive".$bill_arr['id']."><image src=image/lock.png onclick='showbill(".$billid.")'></div>";
						}
						else
						{
							echo "<div id=archive".$bill_arr['id']."><image src=image/unlock.png onclick='hidebill(".$billid.")'></div>";
						}
						
						echo "</td>";
					}		
					
					echo "<td>";
					if($auth_arr['access'] == "admin"  || $auth_arr['access'] == "doctor")
					{
						echo $util->setEdel("$url&do=billedit&billid=".$bill_arr['id']."&start=$start&rowstoview=$rowstoview","$url&do=billdelete&billid=".$bill_arr['id']."&start=$start&rowstoview=$rowstoview#$del_aname");
					}
					else
					{
						echo $util->setEdel("$url&do=billedit&billid=".$bill_arr['id']."&start=$start&rowstoview=$rowstoview","");
					}
					echo "</td></tr>";
					?>
				
				<?php
				echo $balance; //prints balance if needed
				if($payment_cash > 0)
				{
					$cash_arr = $bill->getAllCashPayments($billid);
					foreach($cash_arr as $cash_val)
					{
						echo "<tr><td colspan=2 align=right>".$util->convertmysqldate($cash_val['date'],"M j, Y")."</td>" .
							"<td align=right>Cash Payment</td><td align=right>Php".number_format($cash_val['amount'],2,'.',',')."</td>" .
							"<td>".$util->setEdel('',"$url&dobill=cashdel&cashid=".$cash_val['id']."#bill")."</td></tr>";
					}	
				}
			//-- CHECK PAYMENT
				if($payment_check)
				{
					echo "<tr>";
					echo "<td align=center>Date</td><td align=center>Bank</td><td align=center>Check No</td><td align=right>Amount</td></tr>";
					$check_arr = $bill->getAllCheckPayments($billid);
					foreach($check_arr as $check_row)
					{
						echo "<tr><td align=center>".$util->convertmysqldate($check_row['date'],"F j, Y")."</td>
						<td align=center>".$check_row['bank']."</td><td align=center>".$check_row['checkno']."</td>
						<td align=right>Php".number_format($check_row['amount'],2,".",",")."</td>
						<td>".$util->setEdel("","$url&dobill=checkdel&checkid=".$check_row['id']."#bill")."</td></tr>";
					}
					
				}
				//-- CREDIT PAYMENT
				if($payment_credit)
				{
					$credit_arr = $bill->getAllCreditPayments($billid);
					echo "<tr>";
					echo "<td align=center>Name</td><td align=center>Card</td>
					<td align=center>Exp Date</td><td align=center>Card No</td><td align=right>Amount</td></tr>";
					foreach($credit_arr as $credit_row)
					{
						echo "<tr><td align=center>".$credit_row['name']."</td><td align=center>".$credit_row['card']."</td>
						<td align=center>".$util->convertmysqldate($credit_row['duedate'],"F j, Y")."</td>
						<td align=center>".$credit_row['cardno']."</td>
						<td align=right>Php".number_format($credit_row['amount'],2,".",",")."</td>
						<td>".$util->setEdel("","$url&dobill=creditdel&creditid=".$credit_row['id']."#bill")."</td></tr>";
					}
				}
				
				if($amount_due > $payment_cash + $payment_check + $payment_credit) 
				{
					?>		
					<tr><td colspan=9>	
					<fieldset><legend>Add Payment</legend>
					<form action="<?php echo $url?>#<?php echo $count?>" method=post>
					<input type=hidden name=billid value='<?php echo $bill_arr['id']?>'>
					<table>
						
						<tr><td>
							<?php echo $util->selectDate("y7","m7","d7")?>
							</td><td class=b_c>Cash</td><td><input type=text name=amount id=amount size=10 value="<?php echo $bal?>"></td></tr>
						<tr><td colspan=2><input type=submit name=billpayment value='Add Cash Payment'></td></tr>
					</table>
					</form>
					<!-- CHECK PAYMENT -->
					<form action="<?php echo $url?>#<?php echo $count?>" method=post>
					<input type=hidden name=billid value='<?php echo $bill_arr['id']?>'>
					<table>
						<tr><td >Date</td><td >Bank</td><td >Check No</td><td >Amount</td></tr>
						<tr><td>
							<?php echo $util->selectDate("y8","m8","d8")?>
							</td>
							<td>
								<?php 
									$sql = "select  distinct bank from checkpayment where bank!='' order by bank";
									$sal_arr = $db->getOneCol($sql);
									if(count($sal_arr) > 0)
										echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_bank","bank","onchange=chooseType(\"sel_bank\",\"bank\")"); 
								?>
								<input type=text name=bank id=bank size=30>
							</td>
							<td><input type=text name=checkno id=checkno size=10></td>
							<td><input type=text name=amount id=amount size=10 value="<?php echo $bal?>"></td></tr>
						<tr><td colspan=2><input type=submit name=billpayment value='Add Check Payment'></tr>
					</table>
					</form>
					<!-- CREDIT CARD PAYMENT -->
					<form action="<?php echo $url?>#<?php echo $count?>" method=post>
					<input type=hidden name=billid value='<?php echo $bill_arr['id']?>'>
					<table>
						<tr><td>Name</td><td><input type=text name=name id=name size=50></td></tr>
						<tr>
							<td>Card</td>
							<td>
								<?php 
									$sql = "select  distinct card from creditpayment where card!='' order by card";
									$sal_arr = $db->getOneCol($sql);
									if(count($sal_arr) > 0)
										echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_card","card","onchange=chooseType(\"sel_card\",\"card\")"); 
								?>
								<input type=text name=card id=card size=50>
							</td>
						</tr>
						<tr><td>Card No</td><td><input type=text name=cardno id=cardno size=30></td></tr>
						<tr><td>Due Date</td><td><?php echo $util->selectDate("y9","m9","d9")?></td></tr>
						<tr><td>Amount</td><td><input type=text name=amount id=amount size=10 value="<?php echo $bal?>"></td></tr>
						<tr><td colspan=2><input type=submit name=billpayment value='Add Credit Payment'></tr>
					</table>
					</form>
					
					</fieldset>
					</td></tr>
					<?php
					}//end if due != cash + check
				$totalpaid += $amount_paid; $totaldue += $amount_due;
				$count++;
			}//end if
		}//end for
		echo "<tr><td align=right colspan=4><strong>Total</strong></td><td align=right><strong>Php".number_format($totaldue,2,".",",")."</strong></td>";
		echo "<td align=right><strong>Php".number_format($totalpaid,2,".",",")."</strong></td></tr>";
		?>
		</table>
		<?php 
		echo $util->navi("$url", $start, $rowstoview, $row_count, "image");
	
	}//end if
	
	?>
	<br><br><br>
	<fieldset><legend>Add Bill</legend>
		<form action="<?php echo $url?>#bill" method=post>
			<input type=hidden name=vac_amount value='<?php echo @$vac_amount?>'>
		<table cellpadding=2 cellspacing=2 align=center>
			<tr><td>Date</td>
				<td><?php echo $util->selectDate("y4","m4","d4")?><A HREF="#" onClick="cal4.showCalendar('anchor4'); return false;" TITLE="" NAME="anchor4" ID="anchor4">select date</A>
				</td>
			</tr>
			<tr><td>HMO</td><td><?php echo $patient->data['hmo']?></td></tr>
			<tr><td>PF</td><td><input type=text name=profee size=20 class=input></td></tr>
			<tr><td>Discount (%)</td>
					<td>
					<?php
					$percentArr = array("5","10","20","25","50");
					$table = "statement";
					$col = "discount";
					$sql = "select  distinct $col from $table where $col != '' and userid=$userid  order by $col";
					$sal_arr = $db->getOneCol($sql);
					$sal_arr = array_unique(array_merge($percentArr,$sal_arr));
					if(count($sal_arr) > 0)
					{
						echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
					}
					?>
					<input type=text name=discount id=discount size=20 class=input value="">
					</td>
				</tr>
			<tr><td>Remark</td><td>
				<?php 
							$sql = "select  distinct remark from statement where userid=$userid order by remark";
							$sal_arr = $db->getOneCol($sql);
							//echo $db->msg;
							$sal_arr['No PF'] = "No PF";
							echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_rem","remark"," style=width:90px onchange=chooseType(\"sel_rem\",\"remark_bill\")"); 
						?>
						<input type=text name=remark id=remark_bill size=50 class=input>
				</td>
			</tr>
			<tr><td colspan=2 align=right><input type=reset class=reset_button> <input type=submit name=dobill value=Add class=add_button> </td></tr>
		</table>
		</form>
		</fieldset>
		
	
<?php 
} //end view 
?>

