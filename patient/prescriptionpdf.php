<?php
session_start();

include ("../classes/connect.php");
include ("../classes/DBConnect.php");
include ("../classes/Patient.php");
include ("../classes/Util.php");
include ("../include/html2fpdf/fpdf.php");
include ("../include/html2fpdf/html2fpdf.php");

class XPDF extends HTML2FPDF {

	function Footer() {

	}
}
?>
<?php

$util = new Util();
$db = new DBConnect();
$userid = $_SESSION['auth']['userid'];

//== DR DETAILS ===========
$sql = "select * from numbers where userid=$userid";
$doctor = $db->getRecord($sql);
$show = true;

$patientid = $_REQUEST['patientid'];
$consultid = $_REQUEST['consultid'];

$patient = new Patient();
$patient->getData($patientid);

$name = $patient->data['salutation'] . " " . $patient->data['firstname'] . " " . $patient->data['lastname'];

if ($show) {
	$pdf = new XPDF("P", "mm", array (
		140,
		215
	));

	
	$pdf->DisplayPreferences('HideWindowUI');
	$pdf->AddPage();
	$mleft = $pdf->lMargin;
	$mright = $pdf->rMargin;
	$writinglength = 140 - $mleft - $mright;
	$boolPrintDoctor = false;
	
	if (count($_REQUEST['prescription'])) {
		$count = 0;

		foreach ($_REQUEST['prescription'] as $val) {
			$boolPrintDoctor = false;
			if ($count % 3 == 0) {
				//============= H E A D E R ==========================================================	
				$pdf->SetFontSize("18px");
				$pdf->Cell($writinglength, "5", $doctor['name'], 0, 0, 'C');
				$pdf->SetY($pdf->GetY());
				$pdf->SetFontSize("10px");
				$clinic = "";
				if($doctor['citouse'] == 1)
				{
					$clinic = $doctor['clinicinfo1'];
				}
				elseif($doctor['citouse'] == 2)
				{
					$clinic = $doctor['clinicinfo2'];
				}
				elseif($doctor['citouse'] == 3)
				{
					$clinic = $doctor['clinicinfo3'];
				}
				$pdf->WriteHTML("<p align=center>".stripslashes($clinic)."</p>");

				$pdf->Line($mleft, $pdf->GetY(), $writinglength +13, $pdf->GetY());
				$pdf->Line($mleft, $pdf->GetY() + 1, $writinglength +13, $pdf->GetY() + 1);
				$pdf->SetFontSize("12px");
				$name = "Name: " . $name;
				$namelen = $pdf->GetStringWidth($name);
				$age = $util->age($patient->data['birthday'],$_SESSION['year']."-".$_SESSION['mon']."-".$_SESSION['day']);
				$agesex = "Age: " . $age['year'] ." yrs. old";
				$agesexlen = $pdf->GetStringWidth($agesex);
				$date = "Date: " . $_SESSION['datetoday'];
				$datelen = $pdf->GetStringWidth($date);
				$pdf->Cell("", 5, "", 0, 1);

				$pdf->Cell("",5,$date,0,1,"L");
				$pdf->Cell("", 5, "", 0, 1);
				$pdf->MultiCell("",5,$name,0,"L");
				$pdf->Cell(70,5,$agesex,0,0,"L");
				
				$pdf->Cell("",8, "", 0, 1);
				
			
				//============= PLACE PRESCRIPTION HERE ====================
				$pdf->Image("../image/rx.jpg",$pdf->GetX(),$pdf->GetY(),10,10,'jpg');
			}//print header
				$sql = "select * from medicine where consultid=" . $consultid . " and id=$val order by generic, brand";
				$dum = $db->getRecord($sql);
				$pdf->SetFont("", "", 0);
				$item = $count + 1;
				$pdf->Cell(15, 5, $item . ".", 0, 0, "R");
				$pdf->SetFont("", "", 0);
				$pdf->MultiCell("", 5, $dum['generic']." (" . $dum['brand'] . ")         ".$dum['dose']."\n          #".$dum['quantity']."\n Sig: " . $dum['sig']."\n",0,"L");
				$pdf->Cell("", 2, "", 0, 1);

				//============== END PRESCRIPTION =============================
			
			if ($count % 3 == 2) 
			{
				$max = max($pdf->GetStringWidth("Lic. No. "),$pdf->GetStringWidth("PTR No. "), $pdf->GetStringWidth("S2. No. "));
				$indention = 140 - $mright - $pdf->GetStringWidth($doctor['name']); 
				if($max >  $pdf->GetStringWidth($doctor['name']))
				{
					$indention = 80;
				}
				$pdf->SetXY($indention, 174);
				$pdf->cell("", 5, $doctor['name'], 0, 1);
				$pdf->SetXY($indention, 179);
				$pdf->cell($max, 5, "Lic. No. ",0, 0,"l");
				$pdf->cell("", 5, $doctor['license'], 0, 1);
				$pdf->SetXY($indention, 184);
				$pdf->cell($max, 5, "PTR No. ", 0, 0,"l");
				$pdf->cell("", 5, $doctor['ptr'], 0, 1);
				$pdf->SetXY($indention, 189);
				if ($_REQUEST['s2'] == "on") {
					$pdf->cell($max, 5, "S2. No. ",0, 0,"l");
					$pdf->cell("", 5, $doctor['s2'], 0, 1);
					$pdf->SetXY($mleft, 215);
				}
				
			
				if(count($_REQUEST['prescription']) > $count + 1)
				{
					$pdf->AddPage();
				}
				$boolPrintDoctor = true;
			}
			$count++;
		}
		if($boolPrintDoctor == false)
		{
			$max = max($pdf->GetStringWidth("Lic. No. "),$pdf->GetStringWidth("PTR No. "), $pdf->GetStringWidth("S2. No. "));
			$indention = 140 - $mright - $pdf->GetStringWidth($doctor['name']); 
			if($max <  $pdf->GetStringWidth($doctor['name']))
			{
				$indention = 80;
			}
			$pdf->SetXY($indention, 174);
			$pdf->cell("", 5, $doctor['name'], 0, 1);
			$pdf->SetXY($indention, 179);
			$pdf->cell($max, 5, "Lic. No. ",0, 0,"l");
			$pdf->cell("", 5, $doctor['license'], 0, 1);
			$pdf->SetXY($indention, 184);
			$pdf->cell($max, 5, "PTR No. ", 0, 0,"l");
			$pdf->cell("", 5, $doctor['ptr'], 0, 1);
			$pdf->SetXY($indention, 189);
			if ($_REQUEST['s2'] == "on") {
				$pdf->cell($max, 5, "S2. No. ",0, 0,"l");
				$pdf->cell("", 5, $doctor['s2'], 0, 1);
				$pdf->SetXY($mleft, 215);
			}
		}
		
	}
	$pdf->Output("prescription_" . $_REQUEST['consultid'].rand(1,100) . ".pdf", 'D');
}
?>