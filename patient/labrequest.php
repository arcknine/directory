<?php
$util = new Util();
$db = new DBConnect();
$userid = $_SESSION['auth']['userid'];
?>


<style>
#sel_remark,#sel_impression,#sel_to{
	width: 90px }
</style>	

<?php 

$url = "patient.php?pf=12&patientid=$patientid";

if(isset($_REQUEST['do']) && $_REQUEST['do'] == "Add New")
{
	$sql = "insert into labrequestdb (`id`,`name`,`userid`) values(NULL,'".$_REQUEST['name']."','$userid')";
	$db->insert($sql);
}
elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "del")
{
	$sql = "delete from labrequest where id='".$_REQUEST['labid']."'";
	$db->delete($sql);
	echo $db->msg;
}



$str = "<form action=\"patient/labrequestpdf.php\" method=post style=padding-bottom:50px target=_blank>
	<input type=hidden name=patientid value=".$_REQUEST['patientid']."><table>";


$str .= "<tr><td align=right valign=top>Title</td><td valign=top><select name=title><option>Laboratory</option>";
$str .= "<option>Radiology</option></select></td></tr>";
$str .= "<tr><td align=right valign=top>To:</td><td valign=top>";

$table = "labrequest";
$col = "to";
$sql = "select  distinct `$col` from $table where userid=$userid order by `$col`";
$sal_arr = $db->getOneCol($sql);
if(count($sal_arr) > 0)
	$str .= $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")")."<br>";
	
$str.="<input type=input name=to id=to size=73></td></tr>";
$str .= "<tr><td colspan=2>Request for:</td></tr>";
$str .="<tr><td></td><td><table>";
/*
 * LAB REQUEST
 */
$count = 0;
$rst = $db->getAllRecord("select * from labrequestdb where userid=$userid order by name");
if(count($rst)>0)
{
	foreach($rst as $row)
	  {
	  	if($count%3 == 0)
	  	{
	  		$str.="<tr><td><input type=checkbox name=labrequest[] value=".$row['id']."></td><td>".$row['name']."</td>";
	  	}
	  	elseif($count%3 == 1)
	  	{
	  		$str.="<td><input type=checkbox name=labrequest[] value=".$row['id']."></td><td>".$row['name']."</td>";
	  	}
	  	else
	  	{
	  			$str.="<td><input type=checkbox name=labrequest[] value=".$row['id']."></td><td>".$row['name']."</td></tr>";
	  	}
	  	$count++;
	}
	if($count%3 > 0)
	{
		$str.="</tr>";	
	}
}
else
{
	$str.= '<tr><td><span style=color:red>Add test first</span></td><tr>';
}
$str .= "</table></td></tr>";
$str.="<tr><td valign=top  align=right>Impression:</td><td valign=top>";

$table = "labrequest";
$col = "impression";
$sql = "select  distinct $col from $table where userid=$userid and impression != '' order by $col";
$sal_arr = $db->getOneCol($sql);
if(count($sal_arr) > 0)
	$str .= $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")")."<br>";

$str.="<textarea name=impression id=impression cols=70 rows=2></textarea>";
					
$str.="</td></tr>";

$str.="<tr><td valign=top align=right>Remarks:</td><td valign=top>";

$table = "labrequest";
$col = "remark";
$sql = "select  distinct $col from $table where userid=$userid and remark!='' order by $col";
$sal_arr = $db->getOneCol($sql);
if(count($sal_arr) > 0)
	$str .= $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")")."<br>";

$str.="<textarea name=remark id=remark cols=70 rows=2></textarea>";
					
$str.="</td></tr>";
$str.="<tr><td><br></td></tr>";
$str.="<tr><td colspan=2 align=right><input type=submit name=do value=Print class=print_button>&nbsp;&nbsp;&nbsp;</td></tr></table></form>";


//==NAVIGATION
	$start = 0;
	if(isset($_REQUEST['start']))
	{
		$start = $_REQUEST['start'];
	}
	$rowstoview = 10;
	if(isset($_REQUEST['rowstoview']))
	{
		$rowstoview = $_REQUEST['rowstoview'];
	}
	
	$index = $start + 1;
	
/*
 * VIEW PREVIOUS LAB REQUESTS
 */
$sql = "select * from labrequest where patientid=$patientid order by date desc";

$rst = $db->getAllRecord($sql);
$recordCount = $db->recordCount;

$count = 1;
$prev_request = "";
if(count($rst)>0 )
{
	$prev_request = "<table cellpadding=4 cellspacing=4 align=center class=view>
	<tr><th colspan=2 align=center>Type</th><th align=center>Date</th><th align=center>To</th>
	<th align=center>Test</th><th align=center>Impression</th><th align=center>Remarks</th></tr>";
	for($i = $start; $i < $start+$rowstoview; $i++)
	{
		if($i < $recordCount)
		{
			
			$r = $rst[$i];
			$x = explode(",",$r['labid']);
			
			if(count($x)>0)
			{
				foreach($x as $val)
				{
					$labname = $db->getRecord("select name from labrequestdb where id=".$val);
					$lab[] = $labname['name'];
				}
			}
			
			$prev_request.="<tr><td>$count.</td><td>".$r['title']."</td><td>".$util->convertmysqldate($r['date'],"M j, Y")."</td><td>".$r['to']."</td><td>";
			$prev_request.= implode(", ",$lab);
			$prev_request.="</td><td align=center>".$r['impression']."</td><td>".$r['remark']."</td><td>".
				"<form action='patient/labrequestpdf.php?do=reprint&patientid=".$patientid."&labid=".$r['id']."' method=post target=_blank style=display:inline><input type=image name=do value=reprint src=image/printer.png></form>".
				$util->setEdel("","$url&do=del&labid=".$r['id'])."</td></tr>";
			unset($lab);
		}
		$count++;
	}
	$prev_request.="</table>";
}

/*
 * START OF HTML RENDERING
 */
?>
<p class="pageTitle2"> &nbsp;Laboratory and Radiology Request Form</p>

<?php echo $str?>
<h3>Laboratory and Radiology Requested</h3>
<?php echo $prev_request?>

<fieldset style=margin-top:20px><legend>Add New Test</legend>
<form action="" method=post >
<input type=hidden name=id value="<?php echo @$_REQUEST['id']?>">
<table>
<tr><td>Name: <input type=text name=name size=50></td></tr> 
<tr><td align=right><input type=submit name=do value="Add New" class=add_button></td></tr>
</table>
</form>

</fieldset>

<a href='manage.php?page=2&patientid=<?php echo $patientid;?>'>Manage</a>

