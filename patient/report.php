<?php
header("Content-Type: application/msword");
session_start();
include("../classes/connect.php");
include("../classes/DBConnect.php");
include("../classes/Util.php");
include("../classes/Consult.php");
include("../classes/Patient.php");

$db = new DBConnect();
$util = new Util();
$consult = new Consult();
$patient  = new Patient();
$userid = $_SESSION['auth']['userid'];
$imagefolder = $_SESSION['photo'];
$thumbfolder = $_SESSION['photothumb'];

$consultid = 0;
$consult_arr = array();
$consult_date = "";
if($_REQUEST['consultid'] > 0)
{
	$consultid = $_REQUEST['consultid'];
	$consult_arr = $db->getRecord("select * from consult where id=$consultid");
	echo $db->msg;
	$consult_date =  $consult_arr['date'];
}

$patientid = $_REQUEST['patientid'];
$data = $patient->getData($patientid);

?>
<html>
<head>
<title>Consultation Report</title>
<style>
* {
	font-family: arial;
	font-size: 12px
}

.tdl {
	border-bottom: 1px solid black
}

table {
	empty-cells: show;
}
</style>
</head>
<body>
<p align=center><?php 
$doctor = $db->getRecord("select * from numbers where userid=$userid");
?> <span style="font-size: 140%"><?php echo $doctor['name'];?></span> <br>
<?php
$clinic = "";
if($doctor['citouse'] == 1)
{
	$clinic = $doctor['clinicinfo1'];
}
elseif($doctor['citouse'] == 2)
{
	$clinic = $doctor['clinicinfo2'];
}
elseif($doctor['citouse'] == 3)
{
	$clinic = $doctor['clinicinfo3'];
}
echo $clinic;



$sql = "select * from consult where id=".$consultid;
$r = $db->getRecord($sql);


$today = getdate(); 
if($data['middlename'] != "")
{
	$middlename = substr($data['middlename'],0,1).". ";
}

?></p>
<br>
<p align=center
	style="letter-spacing: 2px; font-size: 120%; font-weight: bold">CONSULTATION REPORT</p>
<p><?php echo date("F j, Y");?><br><br>
Patient name: <?php echo $data['firstname']." $middlename ".$data['lastname'];?></p>
<?php
//print_r($consult_arr);

	

	$dum = array();
	if($r['ht']){ $dum[] = "HT: ".$r['ht'];}
	if($r['wt']){ $dum[] = "WT: ".$r['wt'];}
	if($r['bp']){ $dum[] = "BP: ".$r['bp'];}
	if($r['temp']){ $dum[] = "Temp: ".$r['temp'];}
	if($r['pr']){ $dum[] = "PR: ".$r['pr'];}
	if($r['rr']){ $dum[] = "RR: ".$r['rr'];}
	if($r['os']){ $dum[] = "VA(OS): ".$r['os'];}
	if($r['od']){ $dum[] = "VA(OD): ".$r['od'];}
	if(count($dum)>0)
	{
		echo "<p>Vital Signs: ".implode(", ",$dum)."</p>";
	}
	
	echo "SOAP:<table style=margin-left:20px>";
	if($r['subjectivecomplaint'])
	{
		echo "<tr><td valign=top align=right>S:</td><td>".$r['subjectivecomplaint']."</td></tr>";
	}
	unset($dum);
	if($r['objectivefinding'])
	{
		echo "<tr><td valign=top>O:</td><td>".$r['objectivefinding']."</td></tr>";
	}
	if($r['assessment'])
	{
		echo "<tr><td valign=top>A:</td><td>".$r['assessment']."</td></tr>";
	}

	if($r['plan'])
	{
		echo "<tr><td valign=top>P:</td><td>".$r['plan']."</td></tr>";
	}
	echo "</table>";
	/*
	 * IMAGE
	 */
	$sql = "select * from image where consultid=".$r['id']." order by name asc";
	$dum = $db->getAllRecord($sql);
	$bf = "";
	$labcount = 1;

	if(count($dum)>0)
	{
		foreach($dum as $lab)
		{
			$bf.="<tr><td valign=top>$labcount.</td><td align=center valign=top>".$util->convertmysqldate($lab[date],"M j, Y")."</td><td align=center>";
			
			$bf.= $lab['name']."</td><td align=center valign=top>".$lab['remark']."</td></tr>";
			$labcount++;
				
		}
	
		if($bf)
		{
			echo "<p>Images:</p>";
			echo "<table rules=rows cellpadding=2 cellspacing=2 style=margin-left:20px>" .
							"<tr><td colspan=2 align=center>Date</td><td align=center>Image</td>" .
						"<td align=center>Description</td></tr>$bf</table>";
		}
	}
	/*
	 * MEDICINE
	 */

	$sql = "select * from medicine where consultid=".$r['id']." order by generic asc";
	$dum = $db->getAllRecord($sql);
	$bf = "";
	$medcount = 1;
	if(count($dum) > 0)
	{
		foreach($dum as $med)
		{
			$bf.="<tr><td>$medcount.</td><td>";
			$bf.= $med['generic']."</td><td align=center>".$med['brand']."</td><td align=center>".$med['dose']."</td>
						<td align=center>".$med['quantity']."</td><td align=center>".$med['sig']."</td></tr>";
			$medcount++;
				
		}
		if($bf)
		{
	
			echo "<tr><td><br>";
			echo "<p>Medicines:</p>";
			echo "<table rules=rows cellpadding=2 cellspacing=2 style=margin-left:20px>" .
				"<tr><td colspan=2 align=center>Generic</td><td align=center>Brand</td>" .
				"<td align=center>Dose</td><td>Quantity</td><td align=center>Sig</td></tr>$bf</table>";
		}
	}
echo "<br><br><br><br><p style=padding-top:60px;>";

echo "<br>".$doctor['name']."<br>";
echo "Lic. No. ".$doctor['license']."<br>";
echo "PTR. No." . $doctor['ptr']."</p>";

?>
</body>
</html>
<?php
function empline($value,$width)
{
	if(strlen($value) == 0)
	{
		for($i = 0;$i<$width;$i++)
		{
			echo "&nbsp;";
		}
	}
	else
	{
		echo $value;
	}
}
?>