<style>
	#sel_attendphy, #sel_reason, #sel_finding, #sel_diagnosis, #sel_procedure, #sel_treatment, #sel_otherphy, #sel_condition,
	#sel_medication, #sel_instruction, #sel_hospital, #sel_remark {width:90px}
</style>

<div id=inpatient>

<p class=pageTitle2> &nbsp;Hospital Admission Report</p>


<?php
include("classes/Inpatient.php"); 
$util = new Util();
$db = new DBConnect();
$userid = $_SESSION['auth']['userid'];

$patientid = $_REQUEST['patientid'];
$url = "patient.php?pf=13&patientid=$patientid";

echo $util->selectJSDate("cal1","fcn1","y1","m1","d1");
echo $util->selectJSDate("cal2","fcn2","y2","m2","d2");

$inpatient = new Inpatient();
$edit = "false";


if(isset($_REQUEST['do']) && $_REQUEST['do'] == "Add")
{
	$_REQUEST['dateadmit'] = $_REQUEST['y1']."-".$_REQUEST['m1']."-".$_REQUEST['d1'];
	$_REQUEST['datedischarge'] = $_REQUEST['y2']."-".$_REQUEST['m2']."-".$_REQUEST['d2'];
	$_REQUEST['userid'] = $userid;
	
	if($inpatient->add($_REQUEST) > 0)
	{
		echo "<p>Add successful</p>";
	}
	else
	{
		echo "<p class=error>Add failed.</p>";
	}
}
elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "delete")
{
	$result = $inpatient->delete($_REQUEST['id']); 
	if($result == 1)
	{
		echo "<p>Delete successful.</p>";
	}
	else
	{
		echo "<p>Delete failed.</p>";
	}
}
elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "edit")
{
	$edit = "true";
}
elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "Update")
{
	$_REQUEST['dateadmit'] = $_REQUEST['y1']."-".$_REQUEST['m1']."-".$_REQUEST['d1'];
	$_REQUEST['datedischarge'] = $_REQUEST['y2']."-".$_REQUEST['m2']."-".$_REQUEST['d2'];
	if($inpatient->update($_REQUEST,$_REQUEST["id"]) > 0)
	{
		echo "<p>Update successful.</p>";
	}
	else
	{
		echo "<p class=error>Update failed.</p>";
	}
}


/*
 * ================= VIEW ===========================================================
 */
if($edit == "false")
{
//==NAVIGATION
$start = 0;
if(isset($_REQUEST['start']))
{
	$start = $_REQUEST['start'];
}
$rowstoview = 10;
if(isset($_REQUEST['rowstoview']))
{
	$rowstoview = $_REQUEST['rowstoview'];
}

$index = $start + 1;
	
$records = $inpatient->fetchAll($patientid,$userid);

$recordCount = $inpatient->recordCount;

if($recordCount > 0)
{
	echo "<table cellpadding=4 cellspacing=4 class=view>";
	echo "<tr><th colspan=2  style='vertical-align:text-top'>Attending Physician</th><th style='vertical-align:text-top'>Hospital<th style='vertical-align:text-top'>Room No.</th><th style='vertical-align:text-top'>ID No.</th><th style='vertical-align:text-top'>Date Admitted</th><th  style='vertical-align:text-top'>Date Discharged</th>";
	echo "<th style='vertical-align:text-top'>Chief Complaint</th><th style='vertical-align:text-top'>Pertinent P.E. Findings</th><th style='vertical-align:text-top'>Final Diagnosis</th><th style='vertical-align:text-top'>Diagnostic Procedure</th><th style='vertical-align:text-top'>Surgical Procedure</th><th style='vertical-align:text-top'>Treatment</th>";
	echo "<th style='vertical-align:text-top'>Other Physicians</th><th style='vertical-align:text-top'>Condition on Discharged</th><th style='vertical-align:text-top'>Discharged Medications</th><th style='vertical-align:text-top'>Discharged Instructions</th><th style='vertical-align:text-top'>Remarks</th></tr>";
	for($i=$start;$i<$start+$rowstoview;$i++)
	{
		if($recordCount > $i)
		{
			$r = $records[$i];
			echo "<tr><td class=cv><strong>$index.</strong></td><td  class=cv>".$r['attendphy']."</td><td  class=cv>".$r['hospital']."</td><td  class=cv>".$r['room']."</td>";
			echo "<td class=cv>".$r['pin']."</td><td class=cv>".$util->convertmysqldate($r['dateadmit'],"M j, Y")."</td><td  class=cv>".$util->convertmysqldate($r['datedischarge'],"M j, Y")."</td>";
			echo "<td class=cv>".$r['reason']."</td><td  class=cv>".$r['finding']."</td><td  class=cv>".$r['diagnosis']."</td>";
			echo "<td class=cv>".$r['procedure']."</td><td class=cv>".$r['surgical']."</td><td  class=cv>".$r['treatment']."</td><td  class=cv>".$r['otherphy']."</td>";
			echo "<td class=cv>".$r['condition']."</td><td  class=cv>".$r['medication']."</td><td  class=cv>".$r['instruction']."</td>";
			echo "<td class=cv>".$r['remark']."</td>";
			echo "<td><a href=\"patient/inpatientreport.php?patientid=$patientid&id=".$r['id']."\" ".$util->mouseOver("Print Report",30)." target=_blank><img src=image/printer.png border=0></a>";
			echo $util->setEdel("$url&do=edit&id=".$r['id'],"$url&do=delete&id=".$r['id'])."</td></tr>";		
			$index++;
		}
	}
	echo "</table>";
	echo $util->navi("$url",$start,$rowstoview,$recordCount,"image");
}
?>
<br><br><br>
<fieldset ><legend>Add New Entry</legend>
	<form action="<?php echo $url;?>" method="post">
	<table>
		<tr><th align=right>Attending Physician</th><td>
			<?php 
			$table = "inpatient";
			$col = "attendphy";
			$sql = "select  distinct $col from $table where $col != '' and userid=$userid order by $col";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
			?>
			</td>
			<td><input type=text name=attendphy id=attendphy size=70 ></td></tr>
		<tr><th align=right>Hospital</th><td>
			<?php 
			$table = "inpatient";
			$col = "hospital";
			$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
			?>
			</td>
			<td><input type=text name=hospital id=hospital size=70 ></td></tr>
			
		<tr><th align=right>Room No.</th><td></td><td><input type=text name=room id=room size=70 ></td></tr>
		<tr><th align=right>ID No.</th><td></td><td><input type=text name=pin id=pin size=70 ></td></tr>
		<tr><th align=right>Date Admitted</th><td></td><td>
			<?php echo $util->selectDate("y1","m1","d1"); 
				?> 
				<A HREF="#" onClick="cal1.showCalendar('anchor1'); return false;" TITLE="" NAME="anchor1" ID="anchor1">select date</A>
		</td></tr>
		<tr><th align=right>Date Discharged</th><td></td><td>
			<?php echo $util->selectDate("y2","m2","d2"); 
				?> 
				<A HREF="#" onClick="cal2.showCalendar('anchor1'); return false;" TITLE="" NAME="anchor1" ID="anchor1">select date</A>
		</td></tr>
		<tr><th align=right>Chief Complaint</th><td style=vertical-align:top> 
			<?php 
			$table = "inpatient";
			$col = "reason";
			$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
			?>
			</td><td><input type=text name=reason id=reason size=70 >
			</td></tr>
		<tr><th align=right valign=top>Pertinent P.E. Findings</th><td style=vertical-align:top>
			<?php 
			$table = "inpatient";
			$col = "finding";
			$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
			?>
			</td><td>
				<textarea name=finding id=finding cols=68 rows=2></textarea>
			</td></tr>
		<tr><th align=right>Final Diagnosis</th><td>
			<?php 
			$table = "inpatient";
			$col = "diagnosis";
			$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
			?>
			</td><td><input type=text name=diagnosis id=diagnosis size=70 ></td></tr>
			<tr><th align=right><span style=color:red>*</span>Diagnostic Procedures</th><td>
			<?php 
			$table = "inpatient";
			$col = "procedure";
			$sql = "select  distinct `$col` from $table where `$col` != ''  and userid=$userid order by `$col`";
		
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
			{
				$sal_arr = $util->splitsemicolon($sal_arr);
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseTypeMore(\"sel_$col\",\"$col\")");
			}
			?>
			</td><td><input type=text name=procedure id=procedure size=70 ></td></tr>
		<tr><th align=right><span style=color:red>*</span>Surgical Procedures</th><td>
			<?php 
			$table = "inpatient";
			$col = "surgical";
			$sql = "select  distinct `$col` from $table where `$col` != ''  and userid=$userid order by `$col`";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
			{
				$sal_arr = $util->splitsemicolon($sal_arr);
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col"," style=width:90px onchange=chooseTypeMore(\"sel_$col\",\"$col\")");
			}
			?>
			</td><td><input type=text name=surgical id=surgical size=70 ></td></tr>
		<tr><th align=right valign=top>Treatment</th><td style=vertical-align:top>
			<?php 
			$table = "inpatient";
			$col = "treatment";
			$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
			?>
			</td><td>
			<textarea name=treatment id=treatment cols=68 rows=2></textarea></td></tr>
		<tr><th align=right><span style=color:red>*</span>Other Physicians</th><td>
			<?php 
			$table = "inpatient";
			$col = "otherphy";
			$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
			$sal_arr = $db->getOneCol($sql);
			
			if(count($sal_arr) > 0)
			{
				$sal_arr = $util->splitsemicolon($sal_arr);
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseTypeMore(\"sel_$col\",\"$col\")");
			}
			?>
			</td><td><input type=text name=otherphy id=otherphy size=70 ></td></tr>
		<tr><th align=right>Condition on Discharged</th><td>
			<?php 
			$table = "inpatient";
			$col = "condition";
			$sql = "select  distinct `$col` from $table where `$col` != ''  and userid=$userid order by `$col`";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
			?>
			</td><td><input type=text name=condition id=condition size=70 ></td></tr>
		<tr><th align=right valign=top><span style=color:red>*</span>Discharged Medications</th><td style=vertical-align:top>
			<?php 
			$table = "inpatient";
			$col = "medication";
			$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
			{
				$sal_arr = $util->splitsemicolon($sal_arr);
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseTypeMore(\"sel_$col\",\"$col\")");
			}
			?>
			</td><td><textarea name=medication id=medication cols=68 rows=2></textarea></td></tr>
		<tr><th align=right style=vertical-align:top>Discharged Instructions</th><td style=vertical-align:top>
			<?php 
			$table = "inpatient";
			$col = "instruction";
			$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
			?>
			</td><td>
			<textarea name=instruction id=instruction cols=68 rows=2></textarea>
			</td></tr>	
		<tr><th align=right  style=vertical-align:top>Remarks</th><td style=vertical-align:top>
			<?php 
			$table = "inpatient";
			$col = "remark";
			$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
			?>
			</td>
			<td style=vertical-align:top>
			<textarea name=remark id=remark cols=68 rows=2></textarea>
			</td></tr>	
		<tr><td colspan=3 align=right><input type=reset class=reset_button> <input type=submit name=do value=Add class=add_button> </td></tr>
		<tr><td colspan=3><span style=color:red>*</span> Use semi-colon (;) for multiple entries</td></tr>
	</table>
	</form>
</fieldset>
<?php 
}
else
{
	/*
	 * ============================= EDIT ===========================================================
	 */	
	$r = $db->getRecord("select * from inpatient where id=".$_REQUEST['id']);
	?>
		
	<fieldset><legend>Update</legend>
		<form action="<?php echo $url;?>" method="post">
		<input type=hidden name=id value="<?php echo $r['id'];?>">
		<table>
			<tr><th align=right>Attending Physician</th><td>
				<?php 
				$table = "inpatient";
				$col = "attendphy";
				$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
				$sal_arr = $db->getOneCol($sql);
				if(count($sal_arr) > 0)
					echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
				?>
				</td>
				<td><input type=text name=attendphy id=attendphy size=70  value="<?php echo $r['attendphy'];?>"></td>
			</tr>
			<tr><th align=right>Hospital</th><td>
			<?php 
			$table = "inpatient";
			$col = "hospital";
			$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
			?>
			</td>
			<td><input type=text name=hospital id=hospital size=70  value="<?php echo $r['hospital'];?>"></td>
			</tr>
			<tr><th align=right>Room No.</th><td></td><td><input type=text name=room id=room size=70   value="<?php echo $r['room'];?>"></td></tr>
			<tr><th align=right>ID No.</th><td></td><td><input type=text name=pin id=pin size=70   value="<?php echo $r['pin'];?>"></td></tr>
			<tr><th align=right>Date Admitted</th><td></td><td>
				<?php echo $util->selectNewDate("m1","d1","y1",$r['dateadmit']); 
					?> 
					<A HREF="#" onClick="cal2.showCalendar('anchor1'); return false;" TITLE="" NAME="anchor1" ID="anchor1">select date</A>
			</td></tr>
			<tr><th align=right>Date Discharged</th><td></td><td>
				<?php echo $util->selectNewDate("m2","d2","y2",$r['datedischarge']); 
					?> 
					<A HREF="#" onClick="cal2.showCalendar('anchor1'); return false;" TITLE="" NAME="anchor1" ID="anchor1">select date</A>
			</td></tr>
			<tr><th align=right>Chief Complaint</th><td>
				<?php 
				$table = "inpatient";
				$col = "reason";
				$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
				$sal_arr = $db->getOneCol($sql);
				if(count($sal_arr) > 0)
					echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
				?>
				</td><td><input type=text name=reason id=reason size=70   value="<?php echo $r['reason'];?>"></td></tr>
			<tr><th align=right  style=vertical-align:top>Pertinent P.E. Findings</th><td style=vertical-align:top>
				<?php 
				$table = "inpatient";
				$col = "finding";
				$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
				$sal_arr = $db->getOneCol($sql);
				if(count($sal_arr) > 0)
					echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
				?>
				</td><td>
					<textarea name=finding id=finding cols=68 rows=2><?php echo $r['finding'];?></textarea>
				</td></tr>
			<tr><th align=right>Final Diagnosis</th><td>
				<?php 
				$table = "inpatient";
				$col = "diagnosis";
				$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
				$sal_arr = $db->getOneCol($sql);
				if(count($sal_arr) > 0)
					echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
				?>
				</td><td><input type=text name=diagnosis id=diagnosis size=70  value="<?php echo $r['diagnosis'];?>"></td></tr>
			<tr><th align=right><span style=color:red>*</span>Diagnostic Procedures</th><td>
				<?php 
				$table = "inpatient";
				$col = "procedure";
				$sql = "select  distinct `$col` from $table where `$col` != ''  and userid=$userid order by `$col`";
				$sal_arr = $db->getOneCol($sql);
				$sal_arr = $util->splitsemicolon($sal_arr);
				if(count($sal_arr) > 0)
					echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseTypeMore(\"sel_$col\",\"$col\")");
				?>
				</td><td><input type=text name=procedure id=procedure size=70  value="<?php echo $r['procedure'];?>"></td></tr>
			<tr><th align=right><span style=color:red>*</span>Surgical Procedures</th><td>
			<?php 
			$table = "inpatient";
			$col = "surgical";
			$sql = "select  distinct `$col` from $table where `$col` != ''  and userid=$userid order by `$col`";
			$sal_arr = $db->getOneCol($sql);
			$sal_arr = $util->splitsemicolon($sal_arr);
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col"," style=width:90px onchange=chooseTypeMore(\"sel_$col\",\"$col\")");
			?>
			</td><td><input type=text name=surgical id=surgical size=70  value="<?php echo $r['surgical'];?>"></td></tr>
			<tr><th align=right style=vertical-align:top>Treatment</th><td style=vertical-align:top>
				<?php 
				$table = "inpatient";
				$col = "treatment";
				$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
				$sal_arr = $db->getOneCol($sql);
				if(count($sal_arr) > 0)
					echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
				?>
				</td><td>
					<textarea name=treatment id=treatment cols=68 rows=2><?php echo $r['treatment'];?></textarea>
				</td></tr>
			<tr><th align=right><span style=color:red>*</span>Other Physicians</th><td>
				<?php 
				$table = "inpatient";
				$col = "otherphy";
				$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
				$sal_arr = $db->getOneCol($sql);
				$sal_arr = $util->splitsemicolon($sal_arr);
				if(count($sal_arr) > 0)
					echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseTypeMore(\"sel_$col\",\"$col\")");
				?>
				</td><td><input type=text name=otherphy id=otherphy size=70  value="<?php echo $r['otherphy'];?>"></td></tr>
			<tr><th align=right>Condition on Discharged</th><td>
				<?php 
				$table = "inpatient";
				$col = "condition";
				$sql = "select  distinct `$col` from $table where `$col` != ''  and userid=$userid order by `$col`";
				$sal_arr = $db->getOneCol($sql);
				if(count($sal_arr) > 0)
					echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
				?>
				</td><td><input type=text name=condition id=condition size=70  value="<?php echo $r['condition'];?>"></td></tr>
			<tr><th align=right  style=vertical-align:top><span style=color:red>*</span>Discharged Medications</th><td  style=vertical-align:top>
				<?php 
				$table = "inpatient";
				$col = "medication";
				$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
				$sal_arr = $db->getOneCol($sql);
				$sal_arr = $util->splitsemicolon($sal_arr);
				if(count($sal_arr) > 0)
					echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseTypeMore(\"sel_$col\",\"$col\")");
				?>
				</td><td>
					<textarea name=medication id=medication cols=68 rows=2><?php echo $r['medication'];?></textarea>
				</td></tr>
			<tr><th align=right style=vertical-align:top>Discharged Instructions</th><td  style=vertical-align:top>
				<?php 
				$table = "inpatient";
				$col = "instruction";
				$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
				$sal_arr = $db->getOneCol($sql);
				if(count($sal_arr) > 0)
					echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
				?>
				</td><td>
					<textarea name=instruction id=instruction cols=68 rows=2><?php echo $r['instruction'];?></textarea>
				</td></tr>
						
			<tr><th align=right  style=vertical-align:top>Remarks</th><td style=vertical-align:top>
			<?php 
			$table = "inpatient";
			$col = "remark";
			$sql = "select  distinct $col from $table where $col != ''  and userid=$userid order by $col";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
			?>
			</td>
			<td style=vertical-align:top>
			<textarea name=remark id=remark cols=68 rows=2><?php echo $r['remark'];?></textarea>
			</td></tr>
			<tr><td colspan=3 align=right> <input type=submit name=do value=Cancel class="cancel_button"> <input type=submit name=do value=Update class="update_button"></td></tr>
			<tr><td colspan=3><span style=color:red>*</span> Use semi-colon (;) for multiple entries</td></tr>
		</table>
		</form>
	</fieldset>
	
	<?php 
}
?>

</div>