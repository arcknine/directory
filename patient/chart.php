<div id=chart>
<?php
$db = new DBConnect();

$imagefolder = $_SESSION['photo'];
$thumbfolder = $_SESSION['photothumb'];

if(isset($_REQUEST['do']) &&  $_REQUEST['do'] == "del")
{
	$consult = new Consult();
	$consult->deleteConsult($_REQUEST['consultid']);	
	
}
$start = 0;
if(isset($_REQUEST['start']))
{
	$start = $_REQUEST['start'];
}
$rowstoview = 10;
if(isset($_REQUEST['rowstoview']))
{
	$rowstoview = $_REQUEST['rowstoview'];
}
$navitag = "start=$start&rowstoview=$rowstoview";
$count = 1 + $start;

$where = " ";
$ptype_link = ""; $out_sel = ""; $in_sel = "";
if(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "Submit")
{
	if(isset($_REQUEST['ptype']) && $_REQUEST['ptype'] == "out")
	{
		$where = " and ptype='out' ";
		$out_sel = "selected";
	}
	if(isset($_REQUEST['ptype']) && $_REQUEST['ptype'] == "in")
	{
		$where = " and ptype='in' ";
		$in_sel = "selected";
	}
	$ptype_link = "&submit=Submit&ptype=".$_REQUEST['ptype'];
}
$sql = "select * from consult where patientid=$patientid $where order by date desc";
$rows = $db->getAllRecord($sql);
$rowcount = $db->recordCount;


?>
<div class=pageTitle2> &nbsp;Chart Summary</div>
<br>
<form action="" method=post>
	<input type=hidden name=start value=0>
	Search Patient Type: <select name=ptype id=ptype>
		<option value=out <?php echo $in_sel?>>Out-Patient</option>
		<option value=in <?php echo $out_sel?>>In-Patient</option><option value=both>Both</option></select>
	<input type=submit name=submit value=Submit class="submit_button">
</form>
	<table class="view">
		<tr><th colspan=2>Patient Type</th><th>Date</th><th>Vital Signs</th>
			<th>SOAP</th>
			<th>Medicines</th>
			<th>Images</th>
		</tr>
		<?php
		for($i=$start;$i<$start+$rowstoview;$i++)
		{
			if($rowcount > $i)
			{
				$count = $i + 1;
				echo "<tr><td valign=top>$count.</td><td valign=top>";
					if($rows[$i]['ptype'] == "out")
					{
						echo "Out-Patient";
					}
					else
					{
						echo "In-Patient";
						$dum = $rows[$i]['inpatientinfo'];
						if($dum != "")
						{
							echo "<br>".$dum;
						}
					}
				echo"</td><td nowrap  valign=top><a href=patient.php?pf=5&consultid=".$rows[$i]['id']."&patientid=$patientid>".$util->convertmysqldate($rows[$i]['date'],"F j, Y")."</a></td>";
				$vs = array();
				echo "<td valign=top nowrap>";
				if($rows[$i]['ht'])
				{
					$vs[] = "HT: ".$rows[$i]['ht'];
				}
				if($rows[$i]['wt'])
				{
					$vs[] = "WT: ".$rows[$i]['wt'];
				}
				if($rows[$i]['bp'])
				{
					$vs[] = "BP: ".$rows[$i]['bp']; 
				}
				if($rows[$i]['temp'])
				{
					$vs[] = "Temp: ".$rows[$i]['temp'];	
				}
				if($rows[$i]['pr'])
				{
					$vs[] = "PR: ".$rows[$i]['pr'];	
				}
				if($rows[$i]['rr'])
				{
					$vs[] = "RR: ".$rows[$i]['rr'];
				}
				if($rows[$i]['os'])
				{
					$vs[] = "VA(OS): ".$rows[$i]['os']; 
				}
				if($rows[$i]['od'])
				{
					$vs[] = "VA(OD): ".$rows[$i]['od']; 	
				}
				echo implode("<br>",$vs)."</td>";
				
				echo "<td valign=top>";
				$soap = '';
				
				if($rows[$i]['subjectivecomplaint'])
				{
					$soap .= "<tr><td valign=top>S:</td><td>".$rows[$i]['subjectivecomplaint']."</td></tr>";
				}
				if($rows[$i]['objectivefinding'])
				{
					$soap .= "<tr><td valign=top>O:</td><td>".$rows[$i]['objectivefinding']."</td>";
				}
				if($rows[$i]['assessment'])
				{
					$soap .= "<tr><td valign=top>A:</td><td>".$rows[$i]['assessment']."</td></tr>";
				}
				if($rows[$i]['plan'])
				{
					$soap .= "<tr><td valign=top>P:</td><td>".$rows[$i]['plan']."</td></tr>";
				}
				if($soap!='')
				{
					echo "<table rules=none>";
					echo $soap;
					echo  "</table>";	
				}
				
				echo "</td>";
				
				/*
				 * MEDICINE
				 */
				echo "<td valign=top>";
				$sql = "select * from medicine where consultid=".$rows[$i]['id']." order by generic asc";
				$dum = $db->getAllRecord($sql);
				$bf = "";
				$medcount = 1;
				
				foreach($dum as $med)
				{
					$bf.="<tr><td><strong>$medcount.</strong></td><td>";
					$bf.= $med['generic']."</td><td align=center>".$med['brand']."</td><td align=center>".$med['dose']."</td>
					<td align=center>".$med['quantity']."</td><td align=center>".$med['sig']."</td></tr>";
					$medcount++;
					
				}	
				if($bf)
				{
					echo "<table align=center class=view2>" .
						"<tr><th colspan=2>Generic</th><th>Brand</th>" .
					"<th>Dose</td><th>Quantity</th><th>Sig</th></tr>$bf</table>";
				}
				
				echo "</td>";
				/*
				 * IMAGE
				 */
				$sql = "select * from image where consultid=".$rows[$i]['id']." order by name asc";
				$dum = $db->getAllRecord($sql);
				$bf = "";
				$labcount = 1;
				
				foreach($dum as $lab)
				{
					$bf.="<tr><td><strong>$labcount.</strong></td><td align=center>".$util->convertmysqldate($lab['date'],"M j, Y")."</td><td align=center>";
					if(file_exists($thumbfolder."/".$lab['image'])) 
					{ 
						$bf .= "<a href=\"javascript:popimage('".$_SESSION['urlphoto']."/".$lab['image']."','".$lab['name']."','".$util->getImageWidth($imagefolder,$lab['image'])."','".$util->getImageHeight($imagefolder,$lab['image'])."')\" ".$util->mouseOver("Click To Enlarge","100").">
							<img src=\"".$_SESSION['urlthumb']."/".$lab['image']."\" border=0 width=50></a><br>";
					}
					$bf.= $lab['name']."</td><td align=center>".$lab['remark']."</td></tr>";
					$labcount++;
					
				}
				echo "<td valign=top>";
				if($bf)
				{
					echo "<table align=center class=view2>" .
						"<tr><th colspan=2>Date</th><th>Image</th>" .
					"<th>Description</th></tr>$bf</table>";
				}
				echo "</td><td valign=top><a href=\"patient/report.php?pf=10&patientid=$patientid&consultid=".$rows[$i]['id']."\" ".$util->mouseOver("Report",30)." target=_blank><img src=image/printer.png border=0></a></td>";
				echo "<td  valign=top>".$util->setEdel("","patient.php?pf=6&do=del&patientid=$patientid&consultid=".$rows[$i]['id'])."</td>";
				echo "</tr>";
			}
		}
		
		?>
	</table>
	<?php
	echo $util->navi("patient.php?pf=6&patientid=$patientid".$ptype_link,$start,$rowstoview,$rowcount,"image")
	?>

	</div>