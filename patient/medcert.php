<style>
	#sel_rea,#sel_dia,#sel_remark {width:90px}	
</style>
<?php

$util = new Util();
$db = new DBConnect();
$userid = $_SESSION['auth']['userid'];
$patientid = $_REQUEST['patientid'];
$patient = new Patient();
$patient->getData($patientid);
$middlename = "";
if($patient->data['middlename'] != "")
{
	$middlename = substr($patient->data['middlename'],0,1).".";
}
$patient_name =  $patient->data['salutation']." ".$patient->data['firstname']." $middlename ".$patient->data['lastname'];


?>
<br><br>
<fieldset><legend>Add New Medical Certificate</legend>
<form action=patient/medcertpdf.php method=post>
<table width=95%>
	<tr><td><strong>Name: <input type=text name=name class=input value="<?php echo $patient_name;?>" size=<?php echo strlen($patient_name)+10?>></strong></td></tr>
	<tr><td><strong>Age: 
		<?php 
			
			if($patient->data['birthday'])
			{
				$age_arr = $util->age($patient->data['birthday'],$_SESSION['year']."-".$_SESSION['mon']."-".$_SESSION['day']);
				echo $age_arr['year']." yrs. old";
			}
		?>
			</strong></td>
	<td><strong>Date: <?php echo $_SESSION['datetoday']?></strong></td></tr>
</table>

<p align=center><STRONG>MEDICAL CERTIFICATE</STRONG></P>


<input type=hidden name=patientid id=patientid value="<?php echo $patientid?>">
<input type=hidden name=age id=age value="<?php echo $age_arr['year']?>">
<input type=hidden name=date value="<?php echo $_SESSION['datetoday']?>">
<table cellpadding=2 cellspacing=2>
	<tr><td></td>
		<td>
			<table><tr><td><input type=text name=to1 id=to1 size=50 value="To: " class=input></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan=2 style=line-height:2em>
		This is to certify that <?php echo $patient_name;?>, <?php echo $age_arr['year']?> years old <input type=text name=dateseen class=input size=50 value="of <?php echo $patient->data['address'];?> was seen on <?php echo $_SESSION['datetoday']?>"> (Date seen if any)<br> 
		because of
			<?php 
			$sql = "select distinct consult_reason from medcert where userid=$userid  and consult_reason!='' order by consult_reason";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr)>0)
			{
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_rea","consult_reason","onchange=chooseType(\"sel_rea\",\"consult_reason\")");
			}?>
			<input type=text name=consult_reason id=consult_reason size=50 class=input><br>
			and was diagnosed to have 
			<?php 
			$sql = "select distinct diagnosis from medcert  where userid=$userid  and diagnosis!='' order by diagnosis";
			$sal_arr = $db->getOneCol($sql);
			if(count($sal_arr)>0)
			{
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_dia","diagnosis","onchange=chooseType(\"sel_dia\",\"diagnosis\")");
			}
			?>
			<input type=text name=diagnosis id=diagnosis size=50 class=input>.<br>
			
			<?php 
				$table = "medcert";
				$col = "remark";
				$opt = array(); $val=array();
				$sql = "select  $col from $table  where userid=$userid and remark!='' order by $col";
				$rst = $db->getAllRecord($sql);
				if($rst)
				{
					foreach($rst as $row)
					{
						if(strlen($row[$col]) > 50)
						{
							$opt[] = substr($row[$col],0,50)."...";
						}
						else
						{
							$opt[] = $row[$col];
						}
						$val[] = $row[$col];
					}
				}
				if(count($val) > 0)
				{
					echo $util->getHtmlSelect($opt,$val, "sel_$col","$col"," style=width:90px onchange=chooseType(\"sel_$col\",\"$col\")");
				}
			?>
			<textarea rows="3" cols="50" name=remark id=remark class=input></textarea> (Remarks)
			<br>
			This certificate is issued upon the request of the patient for whatever purpose it may serve.
			<br>
		</td>
	</tr>
	<tr><td colspan=2 align=right><input type=submit name=submit value=Submit class=submit_button></td></tr>
</table>

</form>
</fieldset>


<?php




//echo $patient_name;


if(isset($_REQUEST['do']) && $_REQUEST['do'] == "del")
{
	$db->delete("delete from medcert where id=".$_REQUEST['medcertid']);
}

$age_arr = array();

$sql = "select * from medcert where patientid=$patientid order by date desc";
$medarr = $db->getAllRecord($sql);
$row_count = $db->recordCount;
//print_r($medarr);

$start = 0;
if(isset($_REQUEST['start']))
{
	$start = $_REQUEST['start'];
}
$rowstoview = 10;
if(isset($_REQUEST['rowstoview']))
{
	$rowstoview = $_REQUEST['rowstoview'];
}

$count = $start + 1;
if($row_count > 0)
{
	echo '<br><br>';
	echo "<table cellpadding=4 cellspacing=4 align=center class=view>";
	echo "<tr><th colspan=2>Date issued</th><th>Recipient</th><th>Complaint</th><th>Diagnosis</th><th>Remarks</th></tr>";
	for($i = $start;$i < $start + $rowstoview;$i++)
	{
		if($i < $row_count)
		{
			$val = $medarr[$i];
			?>
			<form method=post action=patient/medcertpdf.php target=_blank>
			<input type=hidden name=patientid value='<?php echo $patientid?>'>
			<input type=hidden name=name value='<?php echo $patient_name?>'>
			<input type=hidden name=diagnosis value='<?php echo $val['diagnosis']?>'>
			<input type=hidden name=dateseen value='<?php echo $val['dateseen']?>'>
			<input type=hidden name=consult_reason value='<?php echo $val['consult_reason']?>'>
			<input type=hidden name=to1 value='<?php echo $val['to1']?>'>
			<input type=hidden name=remark value='<?php echo $val['remark']?>'>
			<input type=hidden name=dateseen value='<?php echo $val['dateseen']?>'>
			<?php
			echo "<tr><td class=b_c>$count.</td><td>".
				$util->convertmysqldate($val['date'],"M j, Y")."</a></td>" .
				"<td align=center>";
				$count = 0;
				//str_ireplace('To','',substr($val['to1'],0,2),$count);
				
				if($count > 0)
				{
					echo substr($val['to1'],2,strlen($val['to1']));
				}
				else
				{
					echo $val['to1'];
				}
			echo "</td>" .
				"<td align=center>".$val['consult_reason']."</td>".
				"<td align=center>".$val['diagnosis']."</td>" .
				"<td align=center>".$val['remark']."</td>" .
				"<td><input type=image name=submit value=Reprint src=image/printer.png></td><td>".$util->setEdel("","patient.php?pf=9&patientid=$patientid&do=del&medcertid=".$val['id'])."</td></tr>";
			?>
			</form>
			<?php
			$count++;
			
		}
	}
	echo "</table>";
	echo "<div align=center>".$util->navi("patient.php?pf=9&patientid=$patientid", $start, $rowstoview, $row_count, "image")."</div>";
}


?>

