<?php
	session_start();

	$auth_arr = $_SESSION['auth'];
	$title = "Statements";	
	$userid = $auth_arr['userid'];
	include("classes/connect.php");
	include("classes/DBConnect.php");
	include("classes/Auth.php");
	include("classes/Util.php");
	include("classes/Patient.php");
	include("classes/Bill.php");
	include("classes/Consult.php");
	
	include("include/top.php");
	include("include/menu.php");
	
	$util = new Util();
?>

<style>
	#sel_discount, #sel_hmo, #sel_company,#sel_rem,#sel_bank,#sel_card {width:90px}
</style>
<script type="text/javascript">
function showbill(id)
{
	
		
		new Ajax.Request('ajax/bill_ajax.php', 
		{
			parameters:'billid='+id+'&do=show',
			onSuccess: function(t){
				//document.getElementById('response').innerHTML = t.responseText;
				document.getElementById('archive'+id).innerHTML = t.responseText;
				//alert(t.responseText);
			},
			 onFailure: function(t) {
        		alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
    		}

		}
		);
	
}

function hidebill(id)
{
	
		
		new Ajax.Request('ajax/bill_ajax.php', 
		{
			parameters:'billid='+id+'&do=hide',
			onSuccess: function(t){
				//document.getElementById('response').innerHTML = t.responseText;
				document.getElementById('archive'+id).innerHTML = t.responseText;
				//alert(t.responseText);
			},
			 onFailure: function(t) {
        		alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
    		}

		}
		);
	
}

</script>

<div id="statement">

<p class=pageTitle> &nbsp;Statements</p>

<div id=response></div>

<?php
/*
 * INITIALIZE ===========================================================
 */
$util = new Util();
$bill = new Bill();
$db = new DBConnect();
$patient = new Patient();
$consult = new Consult();
$dateminmax_arr = $db->getRecord("select min(date) as min, max(date) as max from statement where userid=$userid");
$view_on = true;


$start = 0;
if(isset($_REQUEST['start']))
{
	$start = $_REQUEST['start'];
}
$rowstoview = 10;
if(isset($_REQUEST['rowstoview']))
{
	$rowstoview = $_REQUEST['rowstoview'];
}

$url = "bill.php?";
/*
 * ACTIONS =============================================================
 */
if(isset($_REQUEST['dobill']) && $_REQUEST['dobill'] == "Add")
{
	//print_r($_REQUEST);
	$_REQUEST['date'] = $_REQUEST['y4']."-".$_REQUEST['m4']."-".$_REQUEST['d4'];
	if($bill->boolBilled($_REQUEST['date']) == 0)
	{
		$bill->add($_REQUEST);
		echo $bill->msg;
	}
	else
	{
		echo "<p style=color:red>Billed already</p>";
	}
}
if(isset($_REQUEST['do']) && $_REQUEST['do'] == "billdelete")
{
	if(isset($_REQUEST['billid']))
	{
		$bill->delete($_REQUEST['billid']);
	}
}
elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "billedit")
{
	$patient = new Patient();
	$id = isset($_REQUEST['id'])? $_REQUEST['id']: 0;
	$patient->getData($id);
	
	echo "<p><h3>PATIENT:</h3> ".$patient->data['firstname']." ".$patient->data['lastname'];
	
	$view_on = false;
	
	$billid = $_REQUEST['billid'];
	$bill_arr = $bill->getBill($billid);

	
if($auth_arr['access'] == "admin"  || $auth_arr['access'] == "doctor")
	{
	?>
	
		<fieldset class=update_fieldset><legend>Update Bill</legend>
	<form action="<?php echo $url?>&do=billupdate#bill" method=post>
		<input type=hidden name=billid value="<?php echo $billid?>">
	<table cellpadding=2 cellspacing=2 style=margin-left:auto;margin-right:auto>
		<tr><td>Date</td>
			<td><?php echo $util->selectNewDate("m4","d4","y4",$bill_arr['date'])?>
			</td>
		</tr>
		 <tr><td>HMO</td><td><?php echo $bill_arr['hmo']?></td></tr>
			<tr><td>HMO ID</td><td><?php echo $bill_arr['hmoid']?></td></tr>
			<tr><td>Company</td><td><?php echo $bill_arr['company']?></td></tr>
			<tr><td>Approval Code</td><td><?php echo $bill_arr['approvalcode']?></td></tr>
		<tr><td>PF</td><td><input type=text name=profee size=20 value="<?php echo $bill_arr['profee'];?>"></td></tr>
		<tr><td>Discount (%)</td>
			<td>
			<?php
			$percentArr = array("5","10","20","25","50");
			$table = "statement";
			$col = "discount";
			$sql = "select  distinct $col from $table where $col != '' order by $col";
			$sal_arr = $db->getOneCol($sql);
			$sal_arr = array_unique(array_merge($percentArr,$sal_arr));
			if(count($sal_arr) > 0)
				echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
			?>
			<input type=text name=discount id=discount size=20 value="<?php echo $bill_arr['discount']?>">
			</td>
		</tr>
		<tr><td>Remarks</td><td>
			<?php 
						$sql = "select  distinct remark from statement where remark != '' order by remark";
						$sal_arr = $db->getOneCol($sql);
						//echo $db->msg;
						$sal_arr['No PF'] = "No PF";
						echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_rem","remark","onchange=chooseType(\"sel_rem\",\"remark_bill\")"); 
					?>
					<input type=text name=remark id=remark_bill size=50 value="<?php echo $bill_arr['remark']?>">
			</td>
		</tr>
		<tr><td colspan=2 align=right><input type=submit value=Cancel id=cancel> <input type=submit name=dobill value=Update id=update> </td></tr>
	</table>
	</form>
	</fieldset>
	
		
	
	<?php
	} //end admin access
	else
	{
		?>
		
		<fieldset class=update_fieldset><legend>Update Bill</legend>
		<form action="<?php echo $url?>&do=billupdate#bill" method=post>
			<input type=hidden name=billid value="<?php echo $billid?>">
		<table cellpadding=2 cellspacing=2 style=margin-left:auto;margin-right:auto>
			<tr><td>Date</td>
				<td><?php echo $util->convertmysqldate($bill_arr['date'],"F j, Y")?>
				</td>
			</tr>
			<tr><td>HMO</td><td><?php echo $bill_arr['hmo']?></td></tr>
			<tr><td>HMO ID</td><td><?php echo $bill_arr['hmoid']?></td></tr>
			<tr><td>Company</td><td><?php echo $bill_arr['company']?></td></tr>
			<tr><td>Approval Code</td><td><?php echo $bill_arr['approvalcode']?></td></tr>
			<tr><td>PF</td><td><?php echo $util->formatMoney($bill_arr['profee'])?></td></tr>
			<tr><td>Discount (%)</td>
					<td>
					<?php
					$percentArr = array("5","10","20","25","50");
					$table = "statement";
					$col = "discount";
					$sql = "select  distinct $col from $table where $col != '' and userid=$userid  order by $col";
					$sal_arr = $db->getOneCol($sql);
					$sal_arr = array_unique(array_merge($percentArr,$sal_arr));
					if(count($sal_arr) > 0)
					{
						echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","onchange=chooseType(\"sel_$col\",\"$col\")");
					}
					?>
					<input type=text name=discount id=discount size=20 value="<?php echo $bill_arr['discount'];?>">
					</td>
				</tr>		
			<tr><td>Remarks</td><td>
				<?php 
							$sql = "select  distinct remark from statement where userid=$userid and remark != '' order by remark";
							$sal_arr = $db->getOneCol($sql);
							//echo $db->msg;
							$sal_arr['No PF'] = "No PF";
							echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_rem","remark","onchange=chooseType(\"sel_rem\",\"remark_bill\")"); 
						?>
						<input type=text name=remark id=remark_bill size=50 value="<?php echo $bill_arr['remark']?>">
				</td>
			</tr>
			<tr><td colspan=2 align=right><input type=submit name=dobill value=Update id=update> <input type=submit value=Cancel id=cancel></td></tr>
		</table>
		</form>
		</fieldset>
		
		<?php
	}
}
elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "billupdate")
{

	if($auth_arr['access'] == "admin"  || $auth_arr['access'] == "doctor")
	{
		$_REQUEST['date'] = $_REQUEST['y4']."-".$_REQUEST['m4']."-".$_REQUEST['d4'];
		$bill->update($_REQUEST);
	}
	else
	{
		$sql = "update `statement` set remark='".$_REQUEST['remark']."'  where id=".$_REQUEST['billid'];		
		$db->update($sql);
	}
}

if(isset($_REQUEST['billpayment']) &&  $_REQUEST['billpayment'] == "Add Cash Payment")
{
	$_REQUEST['date'] = $_REQUEST['y7']."-".$_REQUEST['m7']."-".$_REQUEST['d7'];
	$success = $bill->addCashPayment($_REQUEST);
	if($success == 0)
	{
		echo "<p style=color:red>No amount indicated. Add failed.</p>";
	}
}
elseif(isset($_REQUEST['billpayment']) && $_REQUEST['billpayment'] == "Add Check Payment")
{
	$_REQUEST['date'] = $_REQUEST['y8']."-".$_REQUEST['m8']."-".$_REQUEST['d8'];
	$success = $bill->addCheckPayment($_REQUEST);
	if($success == 0)
	{
		echo "<p style=color:red>No amount indicated. Add failed.</p>";
	}
}
elseif(isset($_REQUEST['billpayment']) && $_REQUEST['billpayment'] == "Add Credit Payment")
{
	$_REQUEST['duedate'] = $_REQUEST['y9']."-".$_REQUEST['m9']."-".$_REQUEST['d9'];
	$success = $bill->addCreditPayment($_REQUEST);
	if($success == 0)
	{
		echo "<p style=color:red>No amount indicated. Add failed.</p>";
	}
}


if(isset($_REQUEST['dobill']) && $_REQUEST['dobill'] == "cashdel")
{
	 $bill->delCashPayment($_REQUEST['cashid']);
}
elseif(isset($_REQUEST['dobill']) && $_REQUEST['dobill'] == "checkdel")
{
	 $bill->delCheckPayment($_REQUEST['checkid']);
}
elseif(isset($_REQUEST['dobill']) && $_REQUEST['dobill'] == "creditdel")
{
	 $bill->delCreditPayment($_REQUEST['creditid']);
}
/*
 * View bill ===============================================================================
 */


if($view_on)
{

	$bf = "";
	
	/*
	 * $start and $rowstoview - at initialization
	 */
	 
	$sql = "select * from statement where `show`='1' and userid=$userid order by date desc";
	if($auth_arr['access'] == "admin"  || $auth_arr['access'] == "doctor")
	{
		$sql = "select * from statement  where userid=$userid order by date desc";
		$_SESSION['print_bill_startdate'] = $dateminmax_arr['min'];
		$_SESSION['print_bill_enddate'] = $dateminmax_arr['max'];
	}
	 
	if(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "Search")
	{
		$profee = 0;
		if(isset($_REQUEST['profee']))
		{
			$profee = $_REQUEST['profee'];
		}
		
		$url.="&submit=Search&sm=".$_REQUEST['sm']."&em=".$_REQUEST['em']."&sd=".$_REQUEST['sd']."&ed=".$_REQUEST['ed'].
		"&sy=".$_REQUEST['sy']."&ey=".$_REQUEST['ey']."&profee=".$profee."&company=".$_REQUEST['company']."&hmo=".$_REQUEST['hmo'];
		
		
		if(isset($_REQUEST['sm']) && $_REQUEST['sm'] < 10)
		{
			$_REQUEST['sm'] = "0".$_REQUEST['sm'];
		}
		if(isset($_REQUEST['em']) && $_REQUEST['em'] < 10)
		{
			$_REQUEST['em'] = "0".$_REQUEST['em'];
		}
		if(isset($_REQUEST['sd']) && $_REQUEST['sd'] < 10)
		{
			$_REQUEST['sd'] = "0".$_REQUEST['sd'];
		}
		if(isset($_REQUEST['ed']) && $_REQUEST['ed'] < 10)
		{
			$_REQUEST['ed'] = "0".$_REQUEST['ed'];
		}
	 	
		
		
		$startdate = $_REQUEST['sy']."-".$_REQUEST['sm']."-".$_REQUEST['sd'];
		$enddate = $_REQUEST['ey']."-".$_REQUEST['em']."-".$_REQUEST['ed'];
		$_SESSION['print_bill_startdate'] = $startdate;
		$_SESSION['print_bill_enddate'] = $enddate;
		if($auth_arr['access'] == "admin"  || $auth_arr['access'] == "doctor") 
		{
			$sql = $bill->getRange($startdate,$enddate,$_REQUEST," and t2.userid=$userid ");
					
		}
		else
		{
			$sql = $bill->getRange($startdate,$enddate,$_REQUEST," and `show`='1' and t2.userid=$userid ");
		}
		unset($_REQUEST);
	}
	if(isset($_REQUEST['search']) &&  $_REQUEST['search'] == "today")
	{
		$sql = "select * from statement where date='".$_SESSION['year']."-".$_SESSION['mon']."-".$_SESSION['day']."' and `show`='1' and userid=$userid order by date";		
		if($auth_arr['access'] == "admin"  || $auth_arr['access'] == "doctor")
		{
			$sql = "select * from statement where date='".$_SESSION['year']."-".$_SESSION['mon']."-".$_SESSION['day']."' and userid=$userid order by date";
				
		}
		$_SESSION['print_bill_startdate'] = $_SESSION['year']."-".$_SESSION['mon']."-".$_SESSION['day'];
		$_SESSION['print_bill_enddate'] = $_SESSION['year']."-".$_SESSION['mon']."-".$_SESSION['day'];
	}
	
	$_SESSION['print_bill_sql'] = $sql;
	
	$arr = $db->getAllRecord($sql);
	
	$row_count = count($arr);
	$count = 1 + $start;
	
	if($row_count > 0)
	{
		?>
		<table style="margin-left:auto;margin-right:auto;padding-top:10px" rules=rows cellspacing=2>
			<tr><th colspan=2>Patient</th><th>Date</th><th>HMO/Company</th><th>Diagnosis</th>
				<th>PF</th>
				<?php if($auth_arr['access'] == "admin"  || $auth_arr['access'] == "doctor")
				{
					echo "<th>Discount</th><th>Amount Due</th>";
				}
				?>
				<th>Payment</th>
				<th>Remarks</th>
			</tr>
		
		<?php
		//echo $row_count." ".$sql;
		$totalpaid = 0; $totaldue = 0;
		for($i = $start; $i < $start + $rowstoview; $i++)
		{
			
			if($i < $row_count)
			{
			$row_color = " style=background-color:darkgray;color:blue;text-align:center";
			if($i%2 == 0)
			{
				$row_color = " style=background-color:lightgray;text-align:center";
			}
			$bill_arr = $arr[$i]; 
			$payment_cash = $bill->getTotalCashPayments($bill_arr['id']);
			$payment_check = $bill->getTotalCheckPayments($bill_arr['id']);
			$payment_credit = $bill->getTotalCreditPayments($bill_arr['id']);
			
			$amount_paid = $payment_cash + $payment_check + $payment_credit;
			$billid = $bill_arr[0];
			
			echo "<tr><td  $row_color><strong>$count.</strong><a name=$count></a></td>";
			$patient->getData($bill_arr['patientid']); 
			
			echo "<td align=center>";
			echo "<a href=patient.php?pf=6&patientid=".$bill_arr['patientid'].">";
			
			echo $patient->data['salutation']." ".$patient->data['firstname']." ".$patient->data['lastname']."</a></td>";
			
			if($bill_arr['consultid'] > 0)
			{
				
				echo "<td>";
				if($auth_arr['access'] == "admin"  || $auth_arr['access'] == "doctor")
				{
					echo "<a href=\"patient.php?pf=5&consultid=".$bill_arr['consultid']."&patientid=".$bill_arr['patientid']."\">";
					echo $util->convertmysqldate($bill_arr['date'],"M j, Y")."</a>"; 
				}
				else
				{
					echo $util->convertmysqldate($bill_arr['date'],"M j, Y");
				}
				echo "</td>";
			}
			else
			{
				echo "<td>".$util->convertmysqldate($bill_arr['date'],"M j, Y")."</td>";
			}
			
			$d = array();
			if(isset($bill_arr['hmo']) && $bill_arr['hmo'] != '')
			{
				$d[] = 	'HMO: '.$bill_arr['hmo'];
			}
			if(isset($bill_arr['hmoid']) && $bill_arr['hmoid'] != '')
			{
				$d[] = 	'HMO ID: '.$bill_arr['hmoid'];
			}
			if(isset($bill_arr['company']) && $bill_arr['company'] != '')
			{
				$d[] = 	'Company: '.$bill_arr['company'];
			}
			if(isset($bill_arr['approvalcode']) && $bill_arr['approvalcode'] != '')
			{
				$d[] = 	'Approval Code: '.$bill_arr['approvalcode'];
			}
			$hmo_str = implode("<br/>",$d);
			echo '<td align=center>'.$hmo_str.'</td>';
			
			$c = $consult->getConsultById($bill_arr['consultid']);
			
			
			echo '<td align=center>'.$c['assessment'].'</td>';
					
					if($bill_arr['profee'] > 0)
					{
						echo "<td align=right>Php".number_format($bill_arr['profee'],2,".",",")."</td>";
					}
					else
					{
						echo "<td align=right>Php".number_format(00.00,2,".",",")."</td>";
					}
					
					$discount = 0;
					if(isset($bill_arr['discount']))
					{
						$discount = $bill_arr['discount'];
					}
					if($auth_arr['access'] == "admin" || $auth_arr['access'] == "doctor")
					{
						
						if($discount)
						{
							echo "<td align=center>".$bill_arr['discount']."%</td>";
							
						}
						else
						{
							echo "<td align=center>0%</td>";
						}
						
						echo "<td align=right>Php".number_format($bill->afterDiscount($bill_arr['profee'],$discount),2,".",",")."</td>";	
					}
					
					$amount_due = $bill->afterDiscount($bill_arr['profee'],$discount);
					
					$color = "";
					$balance = "";
					$bal_col_size = 5;
					if($auth_arr['access'] == "admin" || $auth_arr['access'] == "doctor")
					{
						$bal_col_size = 6;
					}
					if($amount_paid > $amount_due)
					{
						$bal = $amount_paid - $amount_due;
						$color = "style=color:blue";
						$balance = "<tr><td align=right colspan=$bal_col_size>Overpaid</td><td align=right $color><strong>Php".number_format($bal,2,".",",")."</strong></td></tr>";
					}
					elseif($amount_paid < $amount_due)
					{
						$color = "style=color:red";
						$bal = $amount_due - $amount_paid;
						$balance = "<tr><td align=right colspan=$bal_col_size>Balance</td><td align=right $color><strong>Php".number_format($bal,2,".",",")."</strong></td></tr>";
					}
					
					$del_aname = $count - 1;
					$remark = isset($bill_arr['remark']) ? $bill_arr['remark']: "";
		
						
					echo "<td align=right><strong>Php".number_format($amount_paid,2,".",",")."</strong></td><td align=center>".$remark."</td>";
					if($auth_arr['access'] == "admin"   || $auth_arr['access'] == "doctor")
					{
						echo "<td align=center	>"; 
						if($bill_arr['show'] == 0)
						{
							echo "<div id=archive".$bill_arr['id']."><image src=image/lock.png width='40px' height='40px' onclick='showbill(".$billid.")'></div>";
						}
						else
						{
							echo "<div id=archive".$bill_arr['id']."><image src=image/unlock.png width='40px' height='40px' onclick='hidebill(".$billid.")'></div>";
						}
						
						echo "</td>";
					}		
					echo "<td>";
					if($auth_arr['access'] == "admin" || $auth_arr['access'] == "doctor")
					{
						echo $util->setEdel("$url&do=billedit&billid=".$bill_arr['id']."&start=$start&rowstoview=$rowstoview&patientid=".$bill_arr['patientid'],"$url&do=billdelete&billid=".$bill_arr['id']."&start=$start&rowstoview=$rowstoview#$del_aname");
					}
					else
					{
						echo $util->setEdel("$url&do=billedit&billid=".$bill_arr['id']."&start=$start&rowstoview=$rowstoview&patientid=".$bill_arr['patientid'],"");
					}
					
					?>
				</tr>
				<?php
				echo $balance; //prints balance if needed
				//-- CASH PAYMENT
				if($payment_cash > 0)
				{
					$cash_arr = $bill->getAllCashPayments($billid);
					foreach($cash_arr as $cash_val)
					{
						echo "<tr><td colspan=4 align=right>".$util->convertmysqldate($cash_val['date'],"M j, Y")."</td>" .
							"<td align=right>Cash Payment</td><td align=right>Php".number_format($cash_val['amount'],2,'.',',')."</td>" .
							"<td>".$util->setEdel('',"$url&dobill=cashdel&cashid=".$cash_val['id']."#bill")."</td></tr>";
					}	
				}
				//-- CHECK PAYMENT
				if($payment_check)
				{
					echo "<tr><td colspan=2 align=right></td>";
					echo "<td align=center>Date</td><td align=center>Bank</td><td align=center>Check No</td><td align=right>Amount</td></tr>";
					$check_arr = $bill->getAllCheckPayments($billid);
					foreach($check_arr as $check_row)
					{
						echo "<tr><td colspan=2></td><td align=center>".$util->convertmysqldate($check_row['date'],"F j, Y")."</td>
						<td align=center>".$check_row['bank']."</td><td align=center>".$check_row['checkno']."</td>
						<td align=right>Php".number_format($check_row['amount'],2,".",",")."</td>
						<td>".$util->setEdel("","$url&dobill=checkdel&checkid=".$check_row['id']."#bill")."</td></tr>";
					}
					
				}
				//-- CREDIT PAYMENT
				if($payment_credit)
				{
					$credit_arr = $bill->getAllCreditPayments($billid);
					echo "<tr><td></td>";
					echo "<td align=center>Name</td><td align=center>Card</td>
					<td align=center>Exp Date</td><td align=center>Card No</td><td align=right>Amount</td></tr>";
					foreach($credit_arr as $credit_row)
					{
						echo "<tr><td></td><td align=center>".$credit_row['name']."</td><td align=center>".$credit_row['card']."</td>
						<td align=center>".$util->convertmysqldate($credit_row['duedate'],"F j, Y")."</td>
						<td align=center>".$credit_row['cardno']."</td>
						<td align=right>Php".number_format($credit_row['amount'],2,".",",")."</td>
						<td>".$util->setEdel("","$url&dobill=creditdel&creditid=".$credit_row['id']."#bill")."</td></tr>";
					}
				}
				
				if($amount_due > $payment_cash + $payment_check + $payment_credit) 
				{
					?>		
					<tr><td colspan=11>	
					<fieldset><legend>Add Payment</legend>
					<form action="<?php echo $url?>#<?php echo $count?>" method=post>
					<input type=hidden name=billid value='<?php echo $bill_arr['id']?>'>
					<table>
						
						<tr><td>
							<?php echo $util->selectDate("y7","m7","d7")?>
							</td><td>Cash</td><td><input type=text name=amount id=amount size=10 value="<?php echo $bal?>"></td></tr>
						<tr><td colspan=2 align=right><input type=submit name=billpayment value='Add Cash Payment' id=bill></td></tr>
					</table>
					</form>
					<!-- CHECK PAYMENT -->
					<form action="<?php echo $url?>#<?php echo $count?>" method=post>
					<input type=hidden name=billid value='<?php echo $bill_arr['id']?>'>
					<table>
						<tr><td >Date</td><td >Bank</td><td >Check No</td><td >Amount</td></tr>
						<tr><td>
							<?php echo $util->selectDate("y8","m8","d8")?>
							</td>
							<td>
								<?php 
									$sql = "select  distinct bank from checkpayment order by bank";
									$sal_arr = $db->getOneCol($sql);
									if(count($sal_arr) > 0)
										echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_bank","bank","onchange=chooseType(\"sel_bank\",\"bank\")"); 
								?>
								<input type=text name=bank id=bank size=30>
							</td>
							<td><input type=text name=checkno id=checkno size=10></td>
							<td><input type=text name=amount id=amount size=10 value="<?php echo $bal?>"></td></tr>
						<tr><td colspan=2 align=right><input type=submit name=billpayment value='Add Check Payment' id=bill></tr>
					</table>
					</form>
					<!-- CREDIT CARD PAYMENT -->
					<form action="<?php echo $url?>#<?php echo $count?>" method=post>
					<input type=hidden name=billid value='<?php echo $bill_arr['id']?>'>
					<table>
						<tr><td>Name</td><td><input type=text name=name id=name size=50></td></tr>
						<tr>
							<td>Card</td>
							<td>
								<?php 
									$sql = "select  distinct card from creditpayment order by card";
									$sal_arr = $db->getOneCol($sql);
									if(count($sal_arr) > 0)
										echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_card","card","onchange=chooseType(\"sel_card\",\"card\")"); 
								?>
								<input type=text name=card id=card size=50>
							</td>
						</tr>
						<tr><td>Card No</td><td><input type=text name=cardno id=cardno size=30></td></tr>
						<tr><td>Due Date</td><td><?php echo $util->selectDate("y9","m9","d9")?></td></tr>
						<tr><td>Amount</td><td><input type=text name=amount id=amount size=10 value="<?php echo $bal?>"></td></tr>
						<tr><td colspan=2 align=right><input type=submit name=billpayment value='Add Credit Payment' id=bill></tr>
					</table>
					</form>
					
					</fieldset>
					</td></tr>
					<?php
					}//end if due != cash + check
				$totalpaid += $amount_paid; $totaldue += $amount_due;
				$count++;
			}//end if
		}//end for
		echo "<tr><td align=right colspan=7><strong>Total</strong></td><td align=right><strong>Php".number_format($totaldue,2,".",",")."</strong></td>";
		echo "<td align=right><strong>Php".number_format($totalpaid,2,".",",")."</strong></td></tr>";
		echo "</table>";
		echo "<br><br>";
		echo "<div style=margin-left:50%;>".$util->navi("$url", $start, $rowstoview, $row_count, "image")."</div>";
		echo "<br><br>";
	}//end if
}//end $view_on

$util->selectJSDate("searchCal1","functionCal1","sy","sm","sd");
$util->selectJSDate("searchCal2","functionCal2","ey","em","ed");



echo "<blockquote><a href=bill.php?search=today style=margin-left:25%; class=today_statements>Today's Statements</a> <a href=bill_print.php target=_blank style=margin-left:25%; class=today_statements>Print</a>";
?>

<h2>SELECTION</h2>

<form action=<?php echo $url?> method=post>
<table align=center>
	<tr><th colspan=2></th></tr>
	<tr>
		<td>
			HMO
		</td><td>
			<?php 
				$sql = "select  distinct hmo from statement where userid=$userid order by hmo";
				$sal_arr = $db->getOneCol($sql);
				if(count($sal_arr) > 0)
					echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_hmo","hmo"," style='width:90px' onchange=chooseType(\"sel_hmo\",\"hmo\")"); 
			?>
			<input type=text name=hmo id=hmo size=30>
	</tr>
	<tr>
		<td>
			Company
		</td><td>
			<?php 
				$sql = "select  distinct company from statement where userid=$userid order by company";
				$sal_arr = $db->getOneCol($sql);
				if(count($sal_arr) > 0)
					echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_company","company"," style='width:90px' onchange=chooseType(\"sel_company\",\"company\")"); 
			?>
			<input type=text name=company id=company size=30>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			
						<input type=radio name=profee value="cn">No Charge<br>
						<input type=radio name=profee value="pf">with PF
		</td>
	</tr>
	<tr>
		<td></td>
		
	</tr>
	<tr><td>Start Date</td><td><?php echo $util->selectNewDate("sm","sd","sy",$dateminmax_arr['min'])?>
	<A HREF="#" onClick="searchCal1.showCalendar('anchor4'); return false;" TITLE="" NAME="anchor4" ID="anchor4">select date</A>
	</td></tr>
	<tr><td>End Date</td><td><?php echo $util->selectNewDate("em","ed","ey",$dateminmax_arr['max'])?>
	<A HREF="#" onClick="searchCal2.showCalendar('anchor4'); return false;" TITLE="" NAME="anchor4" ID="anchor4">select date</A>
	</td></tr>
	<tr>
		<td>Records</td>
		<td>
			<select id=rowstoview name=rowstoview>
				<option>10</option>
				<option>30</option>
				<option>50</option>
				<option>100</option>
				<option>200</option>
				<option value=1000000>All</option>
			</select>
		</td>
	</tr>
	<tr><td colspan=2 align=right><input type=submit name=submit value="Search" id=search></td></tr>
</table>
</form>












</table>
</div>

<div id=footer_page>
<?php
include("include/bottom.php");
?>
</div>