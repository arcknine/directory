<?php
	session_start();
	$title = "Calendar";
	include("classes/connect.php");
	include("classes/DBConnect.php");
	include("classes/Util.php");
	include("include/top.php");
	include("include/menu.php");
	
	$util = new Util();
	$userid = $_SESSION['auth']['userid'];
?>

<div id="calendar">
<p class=pageTitle> &nbsp;Calendar</p>

<?php
$date = new DateTime('Asia/Manila');
$year = $_SESSION['year'];
$mon = $_SESSION['mon'];
$day = $_SESSION['day'];


if (isset ($_REQUEST['year']) && isset ($_REQUEST['mon']))
{
	$year = $_REQUEST['year'];
	$mon = $_REQUEST['mon'];
	
}

if($mon < 10)
{
	$mon = "0".intval($mon);
}



//number of dates in a month

$date_count = new  DateTime('Asia/Manila');
$date_count->setDate($year,$mon,1);


$n = $date_count->format("t");
//get the day of the first day of the month
$d = $date_count->format("w");

//print month
$fullmonth = $date_count->format("F");

//week of the year
$w = $date_count->format("W");

print "<p>&nbsp;<br></p><p>";

$_REQUEST['v'] = "m";
if($_REQUEST['v'] == "m")
{
	print "<div style='padding-left:5%;' align=center><table border=0 bgcolor=#FDEEF4 cellpadding=15 cellspacing=4>\n";
	print "<tr><td colspan=7 align=center class=calendar><span class=month_year>$fullmonth&nbsp;$year</span></td></tr>\n";
	print "<tr bgcolor=><td class=th_g>SUN</td><td class=th_g>MON</td><td class=th_g>TUE</td><td class=th_g>WED</td><td class=th_g>THU</td><td class=th_g>FRI</td><td class=th_g>SAT</td></tr>\n";
	$count = 0;
	for ($i = (1 - $d); $i < $n +1; $i++)
	{

		if ($count % 7 == 0)
		{
			print "<tr bgcolor=#ffffff>";
		}
		if ($i > 0)
		{
			//check if there is an appointment
			$i < 10 ? $oi = "0".intval($i):	$oi = $i;
			$db = new DBConnect();
			$appt_no = $db->rowCount("id"," appointment where date(start)='$year-$mon-$i' and userid=$userid");
			$seen_no =  $db->rowCount("id"," appointment where date(start)='$year-$mon-$i' and status='SEEN'  and userid=$userid");
			$tag = $seen_no."/".$appt_no;
			
			if ($day == $i && $year == $date->format("Y") && $mon == $date->format("m"))
			{
				if ($appt_no > 0)
				{
					print "<td align=right><span style=font-weight:bold;font-size:14px;>$i</span><sup>$tag</sup>" .
					" [<span style=font-size:10px><a href=queue.php?year=$year&mon=$mon&day=$i ".$util->mouseOver("Patient Queue","100").">Q</a></span>]</td>";
				} else
				{
					//print the date without link
					print "<td align=right style='background-color:#f78f1e; border:1px solid #E3E4FA;'><span style=font-weight:bold;font-size:125%>$i</span></td>";
				}
			} else
			{
				if ($appt_no> 0)
				{
					print "<td align=right>$i<sup>$tag</sup>" .
					" [<span style=font-size:10px><a href=queue.php?year=$year&mon=$mon&day=$i ".$util->mouseOver("Patient Queue","100").">Q</a></span>]</td>";
				} else
				{
					print "<td align=right style='background-color:white; border:1px solid #E3E4FA;'>$i</td>";
				}
			}
		} else
		{
			print "<td></td>";
		}
		if ($count % 7 == 6)
		{
			print "</tr>\n";
		}
		if ($i == $n && $count % 7 < 6)
		{

			while ($count % 7 != 6)
			{
				print "<td></td>";
				$count++;
			}
			print "</tr>";

		}
		$count++;
	}

	$ny = $year +1;
	$ly = $year -1;
	$nm = $mon +1;
	$y2d = $year;
	if ($nm == 13)
	{
		$nm = 1;
		$year = $ny;
	}
	$lm = $mon -1;
	if ($lm == 0)
	{
		$lm = 12;
		//$year = $ly;
		$ly = $y2d -1;
	}
	if ($mon == 1)
	{
		$y2d = $y2d -1;
	}
	$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;  
	print "\n<tr bgcolor=><td colspan=7 align=center>
		<a href=calendar.php?id=" . $id . "&year=$ly&mon=$mon&v=m title='Last Year'><img src=image/prevyear_calendar.png border=0 width=40 height=40></a> 
		<a href=calendar.php?id=" . $id . "&mon=$lm&year=$y2d&v=m title='Last Month'><img src=image/prev_calendar.png border=0 width=40 height=40></a> 
		<a href=calendar.php?id=" . $id . "&v=m title='This Month'><img src=image/now_calendar.png border=0 width=40 height=40></a> 
		<a href=calendar.php?id=" . $id . "&mon=$nm&year=$year&v=m title='Next Month'><img src=image/next_calendar.png border=0 width=40 height=40></a> 
		<a href=calendar.php?id=" . $id. "&year=$ny&mon=$mon&v=m title='Next Year'><img src=image/nextyear_calendar.png border=0 width=40 height=40></a></td></tr>\n";
	print "\n</table></div>";
	//print "<a href=calendar.php?mon=$mon&year=$y2d&v=w><strong>Week</strong></a>";
}

//============================================== END 

?>
</div>
		
<div id=footer_page>
<?php
include("include/bottom.php");
?>
</div>