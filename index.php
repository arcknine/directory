<?php 
$title = "www.medriks.com";
$start_page = true;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script type="text/javascript">

function noBack() { window.history.forward() }

noBack();

window.onload = noBack;

window.onpageshow = function(evt) { if (evt.persisted) noBack() }

window.onunload = function() { void (0) }

</script>
<style>
/* page */
* { margin:auto;
	
}

body { 	margin:auto;
		width:auto;
		height:auto;
		min-width:1300px;
		font-family:Verdana;
		/*background-image: url("image/HeaderBG.jpg");
		background-size:100% 100%;*/
}

/*HEADER*/
#header { 	background-image: url("image/HeaderBG.jpg"); 
			/*background-repeat:no-repeat;*/
			background-size:100% 100%;
}

/*CONTENT*/
/*---LOGIN FORM---*/
#container { 	background-image: url("image/login_background.jpg"); 
				background-repeat:no-repeat;
				background-position:center;
				/*background-size:100% 100%;*/	
				height:800px;
				width:auto;
}
#container .bluebar { 	display: block;
   				 		margin:auto;
}
#container .loginform { 	color:grey;
							font-size:21px;
							margin:13% 0px 0px 65%;
}
#container a {  	text-decoration:none; font-size:18px; 
}
#container label { 	font-size:18px;
}
#container p { 	font-size:21px;
}
#container .username { 	background:url('image/LoginUser.png') no-repeat left;
						background-size:20px 20px;
						padding-left:25px;
						height: 28px;
						width: 225px;
						border:none;
						border-top:4px solid grey;
						border-left:1px solid grey;
						border-right:1px solid grey;
						border-bottom:1px solid grey;
						border-radius:5px;
						font-size:18px;					
}
#container .password { 	background:url('image/LogInPW.png') no-repeat left;
						background-size:20px 20px;
						padding-left:25px;
						height: 28px;
						width: 225px;
						border-top:4px solid grey;
						border-left:1px solid grey;
						border-right:1px solid grey;
						border-bottom:1px solid grey;
						border-radius:5px;
						font-size:18px;
}
#container .login_submit { 	color:grey;
							background:url('image/LoginLog.png') no-repeat left;
							background-size:25px 20px;
							padding:0px 5px 5px 25px;
							height: 28px;
							font-size:18px;
							border-top:4px solid grey;
							border-left:1px solid grey;
							border-right:1px solid grey;
							border-bottom:1px solid grey;
							border-radius:5px;
							cursor:pointer;	
}
#container .login_button { 	font-size:18px;
							color:white; 
							background-color:#0039a6; 
							width:250px; 
							height:50px; 
							cursor:pointer;
							border-radius:5px;
}
#container .login_button:active{ 	color:#ff5a00;
									box-shadow: 0 0 5px rgba(81, 203, 238, 1);
									border: 2px solid rgba(81, 203, 238, 1);
									border-radius:10px;
}
#container .username, #container .password, 
#container .login_submit, #container .login_submit, 
#container a, #container .login_button{ 	
											-webkit-transition: all 0.30s ease-in-out;
											-moz-transition: all 0.30s ease-in-out;
											-ms-transition: all 0.30s ease-in-out;
											-o-transition: all 0.30s ease-in-out;
											outline: none;

}
#container .username:focus, #container .login_submit:active, 
#container .password:focus
{ 	box-shadow: 0 0 2px rgba(81, 203, 238, 1);
	border: 1px solid rgba(81, 203, 238, 1);
}
 
#container .login_submit:active { 	 
	/*-webkit-box-shadow: 0px 0px 20px #0039a6;
    -moz-box-shadow: 0px 0px 20px #0039a6; 
    box-shadow: 0px 0px 20px #0039a6;*/
    box-shadow: 0 0 5px rgba(81, 203, 238, 1);
	border: 2px solid rgba(81, 203, 238, 1);
	
}  
#container a:hover { 	color:#ff5a00;
}

/*FOOTER*/
#footer { 	padding:10px 0px 10px 0px;
text-align:center; 
}

</style>

<div id="header">

	<?php //include("include/top.php");?>
	<table width='100%' border='0'>		
		<tr><td><img src='image/medriks_logo.png' height='120px' border='0' style=''></img></td>
	 		<td width='30%'>
	 			<a href='index.php' title='Home' style='padding-left: 120px;' ><img src='image/home.png' height='25px' width='35px' style='border:none;'></a>
	 			<a href='help.php' title='Help' style='text-decoration:none;'>							 Help</a>  
	 			<br />
	 			<span style='font-size:16px; color:grey; width:100%;'> Medical Records Filing System  | <a href="directory.php" style='font-size:16px; color:grey; width:100%;'>Directory</a></span>
	 		</td>
	 	</tr>
		</table>
</div> 

<div id="container">

	<img src="image/MedriksWebsiteBlueBar.png" class="bluebar"/>
	<br>
	<table border=0 width=920px; style=""><tr><td>
		<form action="login.php" method="post"> 
			<table align="right" border=0 class="loginform" align=center style="">
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td><?php if(isset($_REQUEST['msg']) && $_REQUEST['msg'] == 1){ echo "<p style='color:red;' align='center'>Login error</p>"; }?> </td></tr>
			<tr><td><label>Username:</label></td></tr>
			<tr><td> <input type="text" id="username" name="username" class="username"/></td></tr>
			<tr><td></td></tr>
			<tr><td><label>Password:</label></td></tr>
			<tr><td></td></tr>
			<tr><td><input type="password" id="password" name="password" class="password"/></td></tr>
			<tr><td></td></tr>
			<tr><td><input type="submit" value="Login" name="submit" class="login_submit"/></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td></td></tr>
			<tr><td align=center> <span><a href="forgot.php" >Forgot Password?</a> | <a href="help.php" >Help</a> </span> </td></tr>
	
			<tr><td><hr></td></tr>
	
			<tr><td align=center><p>No account yet?</p></td></tr>
			<tr><td><br></td></tr>
			<tr><td><input type="button" value="Create My Account" class="login_button" onClick="window.location.href='register.php'" style=""></td></tr>
			
			</table>
		</form>
</td></tr></table>
</div>

<div id="footer">
<br/>
<?php include ("include/bottom.php") ?>
<!--   <a href="directory.php">Directory</a> -->
</div>
