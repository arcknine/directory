<?php
session_start();
$title = "Help";
@$auth_arr = $_SESSION['auth'];

include("classes/connect.php");
include("classes/DBConnect.php");
include("classes/Auth.php");
include("classes/Util.php");

if($auth_arr['user'] != "" && $auth_arr['access'] != ""){
	include("include/top.php");
	include("include/menu.php");
}else echo "
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

<style>
body { background-image: url(image/HeaderBG.jpg);
	   background-repeat:repeat-x;
	   background-size: 100% 20%;
	   font-family:verdana;
}
#header { /*background-image: url(image/HeaderBG.jpg); background-repeat:no-repeat; background-size:100% 100%;*/ }
.pageTitle { border-bottom:1px solid darkgray;
			 color:white;
			 font-size:140%;			 
			 font-variant:small-caps;
			 font-weight:bold;
			 letter-spacing: .1em;
			 background-image: url('image/MedriksWebsitePageTitleBlueBar.png');
			 background-origin:content-box;
			 background-repeat:no-repeat;
			 background-size:85% 125%; 
}
</style>
<div id='header'>
	<table width='100%' border='0'>		
		<tr><td><img src='image/medriks_logo.png' height='120px' border='0' style=''></img></td>
	 		<td width='30%'>
	 			<a href='index.php' title='Home' style='padding-left: 120px;' ><img src='image/home.png' height='25px' width='35px' style='border:none;'></a>
	 			<a href='help.php' title='Help' style='text-decoration:none;'>							 Help</a>  
	 			<br />
	 			<span style='font-size:16px; color:grey; width:100%;'> Medical Records Filing System </span>
	 		</td>
	 	</tr>
		</table>
</div>";

$util = new Util();
?>

<div id="help">

<p class=pageTitle> &nbsp;Help</p>

<?php
$start = 0;
if(isset($_REQUEST['start']))
{
	$start = $_REQUEST['start'];
}
$rowstoview = 1;
if(isset($_REQUEST['rowstoview']))
{
	$rowstoview = $_REQUEST['rowstoview'];
}

$imagepath = "image";
$page = "help.php?";
$totalrows = 43;

$img_bf ="";
for($i=$start;$i<$start+1;$i++)
{
	$img_bf =  "<br><img src=\"help/index$i.png\"><br>";
}

$navi = "";
if ($start - ($rowstoview) > -1) {
	$prev = $start - ($rowstoview);
	$navi .= "$start <a href=$page&start=" . $prev . "&rowstoview=" . $rowstoview . ">
						      	<img src=$imagepath/arrowleft.png border=0  align=middle width=25 height=25></a>";
}
if ($totalrows > ($start + $rowstoview)) {
	$start = $start + $rowstoview;
	$left = $totalrows - $start;
	$navi .= " <a href=$page&start=" . $start . "&rowstoview=" . $rowstoview . ">
								<img src=$imagepath/arrowright.png border=0  align=middle width=25 height=25></a> $left";
}

?>


<table style=margin-left:100px;margin-right:auto><tr><td valign=top>
Bookmarks
<ul>
<li><a href=help.php?start=0&rowstoview=1>Change Profile</a>
<li><a href=help.php?start=3&rowstoview=1>Tasks</a>
<li><a href=help.php?start=4&rowstoview=1>Patient Listing</a>
<li><a href=help.php?start=5&rowstoview=1>Add New Patient</a>
<li><a href=help.php?start=10&rowstoview=1>Set Appointment</a>
<li><a href=help.php?start=13&rowstoview=1>Patient Queue</a>
<li><a href=help.php?start=18&rowstoview=1>Chart Summary</a>
<li><a href=help.php?start=19&rowstoview=1>Consultation</a>
<li><a href=help.php?start=21&rowstoview=1>ICD 10</a>
<li><a href=help.php?start=29&rowstoview=1>Hospital Admission Report</a>
<li><a href=help.php?start=30&rowstoview=1>Surgical History</a>
<li><a href=help.php?start=31&rowstoview=1>History</a>
<li><a href=help.php?start=32&rowstoview=1>Statements</a>
<li><a href=help.php?start=33&rowstoview=1>Lab and Rad Form</a>
<li><a href=help.php?start=37&rowstoview=1>Medical Certificate</a>
<li><a href=help.php?start=39&rowstoview=1>Calendar</a>
<li><a href=help.php?start=40&rowstoview=1>Statements</a>
<li><a href=help.php?start=41&rowstoview=1>User Account</a>
<li><a href=help.php?start=42&rowstoview=1>Manage</a>
</ul>
</td><td align=center>
<?php 

echo $navi;
echo $img_bf;
echo $navi;

?>
</td></tr></table>

</div>

<div id=footer_page>
<?php
include("include/bottom.php");
?>
</div>