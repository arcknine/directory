<?php
	session_start();
	if(!count($_SESSION['auth']) > 0)
	{
		header("location:index.php");
	}
	$auth_arr = $_SESSION['auth'];
	$title = "Search Assessment";	

	include("classes/connect.php");
	include("classes/DBConnect.php");
	include("classes/Auth.php");
	include("classes/Util.php");
	include("classes/Patient.php");
	include("classes/Consult.php");
	include("classes/Bill.php");
	
	include("include/top.php");
	include("include/menu.php");
	
	$db = new DBConnect();
	$util = new Util();
	$patient = new Patient();
	$msg = "";
	// title,file
	$leftNavi = array(""=>"birthday",""=>"referral");

?>
<div id="stat">

<table style=margin-left:auto;margin-right:auto;width=100%>
<tr>
<td width=200px valign=top>
<!--  ========================== NAVIGATION ===================================== -->
	<table style=padding-top:50px>
	<?php
	
	foreach($leftNavi as $key=>$value)
	{
		echo "<tr><td><a href=stat.php?file=$value>$key</a></td></tr>";
	}
	?>
	</table>
</td>
<td width=100% valign=top>
<!--  ========================== CONTENT ========================================= -->
	<?php 
	if(isset($_REQUEST['file']) == 'result')
	{
		include("stat/diagnosis_result.php");
	}
	else
	{
		include("stat/diagnosis.php");
	}
	?>
</td>
</tr>
</table>



</div>
<!-- ============================================================================================= -->
<div id=footer_page>
<?php
	include("include/bottom.php");
?>
</div>
