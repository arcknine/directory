<?php
	session_start();
	$title = "Directory";
	include("directory/connect.php");
	include("classes/DBConnect.php");
	include("classes/Util.php");
	include("include/script.inc");
	//include("include/top_public.php");
	//include("include/menu.php");
	$db = new DBConnect();
	$util = new Util();
?>
<head>
<style type="text/css" media="screen">
.hiddenDiv {
	display: none;
	}
.visibleDiv {
		display: block;
		width:auto; height:auto;
		border:none;
		-webkit-appearance:none;
		padding:6px 5px 5px 10px;
		font-size: 14px;
		-webkit-transition: all 0.30s ease-in-out;
		-moz-transition: all 0.30s ease-in-out;
		-ms-transition: all 0.30s ease-in-out;
		-o-transition: all 0.30s ease-in-out;
	}
.visibleDiv:focus {  	box-shadow: 0 0 5px rgba(81, 203, 238, 1);
				border: 2px solid rgba(81, 203, 238, 1);
}
.drctryFrm {
		margin-top:-200px;
}
.dropdown {
	  -webkit-transition: all 0.30s ease-in-out;
	  -moz-transition: all 0.30s ease-in-out;
	  -ms-transition: all 0.30s ease-in-out;
	  -o-transition: all 0.30s ease-in-out;
	  outline: none;
	  padding: 5px 3px 5px 3px;
	  margin: 5px 1px 3px 0px;
	  border: 1px solid #DDDDDD;
}
.dropdown:focus {
	  box-shadow: 0 0 5px rgba(81, 203, 238, 1);
	  padding: 5px 3px 5px 3px;
	  margin: 5px 1px 3px 0px;
	  border: 1px solid rgba(81, 203, 238, 1);
	}

</style>
<script type="text/javascript" language="javascript">
function Update(ID,Target)
{
 	var DayX=document.getElementById(ID+"Day").selectedIndex;
	var DayY=document.getElementById(ID+"Day").options;
	var MonX=document.getElementById(ID+"Mon").selectedIndex;
	var MonY=document.getElementById(ID+"Mon").options;
	document.getElementById(Target).value=document.getElementById(ID+"Yr").value+"-"+MonY[MonX].value+"-"+DayY[DayX].text;
}
function Click(ID)
{
	Show('SFrame');
	document.getElementById(ID).click();
	document.forms["searchForm"].submit();
}
function Show(ID)
{
	document.getElementById(ID).style.visibility="visible";
}
function showhide(id)
{
	if(document.getElementId(type)=="doctors"){
	document.getElementId().style.visibility="visible";
	}
}
function Ajax(ID,Aim,TB,Fld)
{   document.getElementById(ID).innerHTML="";
	document.getElementById(ID).style.visibility="visible";
	var Val=document.getElementById(Aim).value.toUpperCase();
	if (window.XMLHttpRequest)  xmlhttp=new XMLHttpRequest();
	else					    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	xmlhttp.onreadystatechange=function()
	{ if (xmlhttp.readyState==4 && xmlhttp.status==200) document.getElementById(ID).innerHTML=xmlhttp.responseText;   }
	xmlhttp.open("GET","ajax/AJAX.php?Handler=AJAX&Aim="+Aim+"&TB="+TB+"&Fld="+Fld+"&Val="+Val+"&Div="+ID+"",true);
	xmlhttp.send();
}
function Pick(ID,Aim,Div)
{
	document.getElementById(Aim).value=document.getElementById(ID).innerHTML.replace("&amp;","&");
	document.getElementById(Div).style.visibility="hidden";
}

function showDiv(divName) {

	if(divName == 'div_dentists'){
		document.getElementById('div_dentists').style.display=""
		document.getElementById('div_hospital').style.display="none"
		document.getElementById('div_clinic').style.display="none"
	}
	if(divName == 'div_hospital'){
		document.getElementById('div_hospital').style.display=""
		document.getElementById('div_dentists').style.display="none"
		document.getElementById('div_clinic').style.display="none"
	}
	if(divName == 'div_clinic'){
		document.getElementById('div_clinic').style.display=""
		document.getElementById('div_hospital').style.display="none"
		document.getElementById('div_dentists').style.display="none"
	}
}

function setSize(element) {
    var height = 0;
    var body = window.document.body;
    if (window.innerHeight) {
        height = window.innerHeight;
    } else if (body.parentElement.clientHeight) {
        height = body.parentElement.clientHeight;
    } else if (body && body.clientHeight) {
        height = body.clientHeight;
    }
  //px is arbitrary
    element.style.height = ((height - element.offsetTop) + "px");
  //line below is negligible
  // document.getElementById("right").style.height = ((height - element.offsetTop) + "px");
}

function getAdd(target)
{
	if(target == "city_region")
	{
		cityid = document.searchForm.city_hospital.selectedIndex;
 		city_val = document.searchForm.city_hospital.options[cityid].text;
 		//alert(city_val);
 		if( city_val != "")
 		{
 		document.getElementById('province').style.display="block";
 		document.getElementById('province_hospital').style.display="block";
		new Ajax.Request('ajax/directory_ajax.php?city='+city_val+'&do=city_province', {
			method: 'get',
  			onSuccess: function(response) {

    			//alert("response brand "+response.responseText);
    			var strArr = response.responseText.split("_");
    			var selcount = strArr.length;
    			document.searchForm.province_hospital.length = selcount + 1;

				document.searchForm.province_hospital.options[0].text = "Choose";
				document.searchForm.province_hospital.options[0].value = "";

				for(i = 0; i < selcount; i++)
    			{
    				y = i + 1;
    				document.searchForm.province_hospital.options[y].value = strArr[i];
    				document.searchForm.province_hospital.options[y].text = strArr[i];
    			}
    			},
		 		onFailure: function(t) {
    			alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
				}
			});
		}
	}
	if(target == "province_region")
	{
		provinceid = document.searchForm.province_hospitals.selectedIndex;
		province_val = document.searchForm.province_hospitals.options[provinceid].text;
 		//alert(province_val);
 		if( province_val != "")
 		{
 		document.getElementById('city2').style.display="block";
 	 	document.getElementById('city_hospitals').style.display="block";
		new Ajax.Request('ajax/directory_ajax.php?province='+province_val+'&do=province_city', {
			method: 'get',
  			onSuccess: function(response) {

    			//alert("response brand "+response.responseText);
    			var strArr = response.responseText.split("_");
    			var selcount = strArr.length;
    			document.searchForm.city_hospitals.length = selcount + 1;

    			document.searchForm.city_hospitals.options[0].text = "Choose";
				document.searchForm.city_hospitals.options[0].value = "";

    			for(i = 0; i < selcount; i++)
    			{
    				y = i + 1;
    				document.searchForm.city_hospitals.options[y].value = strArr[i];
    				document.searchForm.city_hospitals.options[y].text = strArr[i];
    			}
			  },
		 		onFailure: function(t) {
    			alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
				}
			});
		}
	}
	if(target == "region_cp")
	{
		regionid = document.searchForm.region_hospital.selectedIndex;
		region_val = document.searchForm.region_hospital.options[regionid].text;
 		//alert(region_val);
 		if(region_val != "")
 		{
 		document.getElementById('city3').style.display="block";
 	 	document.getElementById('city_hospitalss').style.display="block";
 	 	document.getElementById('province3').style.display="block";
 	 	document.getElementById('province_hospitalss').style.display="block";
		new Ajax.Request('ajax/directory_ajax.php?region='+region_val+'&do=region_city', {
			method: 'get',
  			onSuccess: function(response) {

    			//alert("response brand "+response.responseText);
    			var strArr = response.responseText.split("_");
    			var selcount = strArr.length;
    			document.searchForm.city_hospitalss.length = selcount + 1;

    			document.searchForm.city_hospitalss.options[0].text = "Choose";
				document.searchForm.city_hospitalss.options[0].value = "";

    			for(i = 0; i < selcount; i++)
    			{
    				y = i + 1;
    				document.searchForm.city_hospitalss.options[y].value = strArr[i];
    				document.searchForm.city_hospitalss.options[y].text = strArr[i];
    			}
			  },
		 		onFailure: function(t) {
    			alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
				}
			});

		new Ajax.Request('ajax/directory_ajax.php?region='+region_val+'&do=region_province', {
			method: 'get',
  			onSuccess: function(response) {

    			//alert("response brand "+response.responseText);
    			var strArr = response.responseText.split("_");
    			var selcount = strArr.length;
    			document.searchForm.province_hospitalss.length = selcount + 1;

    			document.searchForm.province_hospitalss.options[0].text = "Choose";
				document.searchForm.province_hospitalss.options[0].value = "";

    			for(i = 0; i < selcount; i++)
    			{
    				y = i + 1;
    				document.searchForm.province_hospitalss.options[y].value = strArr[i];
    				document.searchForm.province_hospitalss.options[y].text = strArr[i];
    			}
			  },
		 		onFailure: function(t) {
    			alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
				}
			});
		}
	}
}

function getAdd2(target)
{
	if(target == "city_region")
	{
		cityid = document.searchForm.city_hospital2.selectedIndex;
 		city_val = document.searchForm.city_hospital2.options[cityid].text;
 		//alert(city_val);
 		if( city_val != "")
 		{
 		document.getElementById('province2').style.display="block";
 		document.getElementById('province_hospital2').style.display="block";
		new Ajax.Request('ajax/directory_ajax.php?city='+city_val+'&do=city_province', {
			method: 'get',
  			onSuccess: function(response) {

    			//alert("response brand "+response.responseText);
    			var strArr = response.responseText.split("_");
    			var selcount = strArr.length;
    			document.searchForm.province_hospital2.length = selcount + 1;

				document.searchForm.province_hospital2.options[0].text = "Choose";
				document.searchForm.province_hospital2.options[0].value = "";

				for(i = 0; i < selcount; i++)
    			{
    				y = i + 1;
    				document.searchForm.province_hospital2.options[y].value = strArr[i];
    				document.searchForm.province_hospital2.options[y].text = strArr[i];
    			}
    			},
		 		onFailure: function(t) {
    			alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
				}
			});
		}
	}
	if(target == "province_region")
	{
		provinceid = document.searchForm.province_hospitals2.selectedIndex;
		province_val = document.searchForm.province_hospitals2.options[provinceid].text;
 		//alert(province_val);
 		if( province_val != "")
 		{
 		document.getElementById('city_span2').style.display="block";
 	 	document.getElementById('city_hospitals2').style.display="block";
		new Ajax.Request('ajax/directory_ajax.php?province='+province_val+'&do=province_city', {
			method: 'get',
  			onSuccess: function(response) {

    			//alert("response brand "+response.responseText);
    			var strArr = response.responseText.split("_");
    			var selcount = strArr.length;
    			document.searchForm.city_hospitals2.length = selcount + 1;

    			document.searchForm.city_hospitals2.options[0].text = "Choose";
				document.searchForm.city_hospitals2.options[0].value = "";

    			for(i = 0; i < selcount; i++)
    			{
    				y = i + 1;
    				document.searchForm.city_hospitals2.options[y].value = strArr[i];
    				document.searchForm.city_hospitals2.options[y].text = strArr[i];
    			}
			  },
		 		onFailure: function(t) {
    			alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
				}
			});
		}
	}
	if(target == "region_cp")
	{
		regionid = document.searchForm.region_hospital2.selectedIndex;
		region_val = document.searchForm.region_hospital2.options[regionid].text;
 		//alert(region_val);
 		if(region_val != "")
 		{
 		document.getElementById('city_span').style.display="block";
 	 	document.getElementById('city_hospitalss2').style.display="block";
 	 	document.getElementById('province_span').style.display="block";
 	 	document.getElementById('province_hospitalss2').style.display="block";
		new Ajax.Request('ajax/directory_ajax.php?region='+region_val+'&do=region_city', {
			method: 'get',
  			onSuccess: function(response) {

    			//alert("response brand "+response.responseText);
    			var strArr = response.responseText.split("_");
    			var selcount = strArr.length;
    			document.searchForm.city_hospitalss2.length = selcount + 1;

    			document.searchForm.city_hospitalss2.options[0].text = "Choose";
				document.searchForm.city_hospitalss2.options[0].value = "";

    			for(i = 0; i < selcount; i++)
    			{
    				y = i + 1;
    				document.searchForm.city_hospitalss2.options[y].value = strArr[i];
    				document.searchForm.city_hospitalss2.options[y].text = strArr[i];
    			}
			  },
		 		onFailure: function(t) {
    			alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
				}
			});

		new Ajax.Request('ajax/directory_ajax.php?region='+region_val+'&do=region_province', {
			method: 'get',
  			onSuccess: function(response) {

    			//alert("response brand "+response.responseText);
    			var strArr = response.responseText.split("_");
    			var selcount = strArr.length;
    			document.searchForm.province_hospitalss2.length = selcount + 1;

    			document.searchForm.province_hospitalss2.options[0].text = "Choose";
				document.searchForm.province_hospitalss2.options[0].value = "";

    			for(i = 0; i < selcount; i++)
    			{
    				y = i + 1;
    				document.searchForm.province_hospitalss2.options[y].value = strArr[i];
    				document.searchForm.province_hospitalss2.options[y].text = strArr[i];
    			}
			  },
		 		onFailure: function(t) {
    			alert('AutoSave Error ' + t.status + ' -- ' + t.statusText);
				}
			});
		}
	}
}

function showsearch(searchname)
{
	if(searchname == '')
	{
		document.getElementById('div_city').style.display="none";
		document.getElementById('div_province').style.display="none";
		document.getElementById('div_region').style.display="none";
	}
	if(searchname == 'city')
	{
		document.getElementById('div_city').style.display="block";
		document.getElementById('div_province').style.display="none";
		document.getElementById('div_region').style.display="none";
	}
	if(searchname == 'province')
	{
		document.getElementById('div_province').style.display="block";
		document.getElementById('div_city').style.display="none";
		document.getElementById('div_region').style.display="none";
	}
	if(searchname == 'region')
	{
		document.getElementById('div_city').style.display="none";
		document.getElementById('div_province').style.display="none";
		document.getElementById('div_region').style.display="block";
	}
}

function showsearch2(searchname)
{
	if(searchname == '')
	{
		document.getElementById('div_city2').style.display="none";
		document.getElementById('div_province2').style.display="none";
		document.getElementById('div_region2').style.display="none";
	}
	if(searchname == 'city2')
	{
		document.getElementById('div_city2').style.display="block";
		document.getElementById('div_province2').style.display="none";
		document.getElementById('div_region2').style.display="none";
	}
	if(searchname == 'province2')
	{
		document.getElementById('div_province2').style.display="block";
		document.getElementById('div_city2').style.display="none";
		document.getElementById('div_region2').style.display="none";
	}
	if(searchname == 'region2')
	{
		document.getElementById('div_city2').style.display="none";
		document.getElementById('div_province2').style.display="none";
		document.getElementById('div_region2').style.display="block";
	}
}

</script>
</head>

<table width='100%' border='0'>
		<tr><td><img src='image/medriks_logo.png' height='120px' border='0' style=''></img></td>
	 	</tr>
		</table>

<p class=pageTitle2> &nbsp;Directory</p>
<body onload="setSize(td1);setSize(td2);setSize(SFrame);">
<table border=0>
<tr><td id=td1>

<form id='searchForm' name='searchForm' action="directory/" target="List" method="post" >

<table border="0" class="drctryFrm" style="height:100%;margin-top:-250px;">

<tr><td id=div_search>
<input type="text" name="search" style="width:190px;" class="dropdown" onkeyup="Click('Search')" />
<input type="submit" id="Search" class="search_button" value="Search" onclick="Show('SFrame')" />
</td></tr>

<tr>
	<td  style="color:grey;"> Select </td>
</tr>
<tr>
	<td>
		<select name="type" id=type class="dropdown" style="width:300px;" onchange="Click('Search');showDiv(this.value);">
			<option value="div_hospital">Hospital</option>
			<option value="div_clinic">Clinic</option>
			<option value="div_dentists">Dentist</option>
		</select>
	</td>
</tr>

<tr>
	<td>
<!-- HOSPITAL -->
 			<div id=div_hospital style="">

			<span style="color:grey;" id="dentists">Search By</span>
			<br>
				<select name="searchby" id="searchby" class="dropdown" style="width:300px;" onchange="showsearch(this.value);">
					<option value=""></option>
					<option value="city"> City </option>
					<option value="region"> Region </option>
				 	<option value="province"> Province </option>
				</select>
			<br>

				<div id="div_city" style="display:none;">
				<span style="color:grey;" id="city">Search City / Town</span> <br>
				<select name="city_hospital" id="city_hospital" class="dropdown" style="width:300px;" onchange="Click('Search');getAdd('city_region');">
				<?php
					$sql = "select distinct city from hospital order by city";
					$rst = $db->getAllRecord($sql);
					if($rst)
					{
						if(isset($rst['city']))
						{
							echo '<option value="'.$rst['city'].'" selected>'.$rst['city'].'</option>';
						}
							echo '<option value=""></option>';
							foreach($rst as $r)
							{
								echo '<option value="'.$r['city'].'">'.$r['city'].'</option>';
							}
					}
				?>
				</select>
				<span style="color:grey;display:none;" id="province">Search Province</span>
				<select name="province_hospital" id="province_hospital" class="dropdown" style="width:300px;display:none;" onchange="Click('Search');">
				<?php
					$sql = "select distinct province from hospital order by province";
					$rst = $db->getAllRecord($sql);
					if($rst)
					{
						if(isset($rst['province']))
						{
							echo '<option value="'.$rst['province'].'" selected>'.$rst['province'].'</option>';
						}
							echo '<option value=""></option>';
							foreach($rst as $r)
							{
								echo '<option value="'.$r['province'].'">'.$r['province'].'</option>';
							}
					}
				?>
				</select>
				</div>

				<div id="div_province" style="display:none;">
				<span style="color:grey;" id="province">Search Province</span><br>
				<select name="province_hospitals" id="province_hospitals" class="dropdown" style="width:300px;" onchange="Click('Search');getAdd('province_region');">
				<?php
					$sql = "select distinct province from hospital order by province";
					$rst = $db->getAllRecord($sql);
					if($rst)
					{
						if(isset($rst['province']))
						{
							echo '<option value="'.$rst['province'].'" selected>'.$rst['province'].'</option>';
						}
							echo '<option value=""></option>';
							foreach($rst as $r)
							{
								echo '<option value="'.$r['province'].'">'.$r['province'].'</option>';
							}
					}
				?>
				</select>
				<br><span style="color:grey;display:none;" id="city2">Search City / Town</span> <br>
				<select name="city_hospitals" id="city_hospitals" class="dropdown" style="width:300px;display:none;" onchange="Click('Search');">
				<?php
					$sql = "select distinct city from hospital order by city";
					$rst = $db->getAllRecord($sql);
					if($rst)
					{
						if(isset($rst['city']))
						{
							echo '<option value="'.$rst['city'].'" selected>'.$rst['city'].'</option>';
						}
							echo '<option value=""></option>';
							foreach($rst as $r)
							{
								echo '<option value="'.$r['city'].'">'.$r['city'].'</option>';
							}
					}
				?>
				</select>
				</div>

				<div id="div_region" style="display:none;">
				<span style="color:grey;" id="region">Search Region</span> <br>
				<select name="region_hospital" id="region_hospital" class="dropdown" style="width:300px;" onchange="Click('Search');getAdd('region_cp');">
				<?php
					$sql = "select distinct region from hospital order by region";
					$rst = $db->getAllRecord($sql);
					if($rst)
					{
						if(isset($rst['region']))
						{
							echo '<option value="'.$rst['region'].'" selected>'.$rst['region'].'</option>';
						}
							echo '<option value=""></option>';
							foreach($rst as $r)
							{
								echo '<option value="'.$r['region'].'">'.$r['region'].'</option>';
							}
					}
				?>
				</select>
				<span style="color:grey;display:none;" id="city3">Search City / Town</span>
				<select name="city_hospitalss" id="city_hospitalss" class="dropdown" style="width:300px;display:none;" onchange="Click('Search');">
				<?php
					$sql = "select distinct city from hospital order by city";
					$rst = $db->getAllRecord($sql);
					if($rst)
					{
						if(isset($rst['city']))
						{
							echo '<option value="'.$rst['city'].'" selected>'.$rst['city'].'</option>';
						}
							echo '<option value=""></option>';
							foreach($rst as $r)
							{
								echo '<option value="'.$r['city'].'">'.$r['city'].'</option>';
							}
					}
				?>
				</select>
				<span style="color:grey;display:none;" id="province3">Search Province</span>
				<select name="province_hospitalss" id="province_hospitalss" class="dropdown" style="width:300px;display:none;" onchange="Click('Search');">
				<?php
					$sql = "select distinct province from hospital order by province";
					$rst = $db->getAllRecord($sql);
					if($rst)
					{
						if(isset($rst['province']))
						{
							echo '<option value="'.$rst['province'].'" selected>'.$rst['province'].'</option>';
						}
							echo '<option value=""></option>';
							foreach($rst as $r)
							{
								echo '<option value="'.$r['province'].'">'.$r['province'].'</option>';
							}
					}
				?>
				</select>

				</div>
				</div>
<!-- CLINIC -->
<div id=div_clinic style="display:none;">

			<span style="color:grey;" id="dentists">Search By</span>
			<br>
				<select name="searchby2" id="searchby2" class="dropdown" style="width:300px;" onchange="showsearch2(this.value);">
					<option value=""></option>
					<option value="city2"> City </option>
					<option value="region2"> Region </option>
				 	<option value="province2"> Province </option>
				</select>
			<br>

				<div id="div_city2" style="display:none;">
				<span style="color:grey;" id="city2">Search City / Town</span> <br>
				<select name="city_hospital2" id="city_hospital2" class="dropdown" style="width:300px;" onchange="Click('Search');getAdd2('city_region');">
				<?php
					$sql = "select distinct city from hospital order by city";
					$rst = $db->getAllRecord($sql);
					if($rst)
					{
						if(isset($rst['city']))
						{
							echo '<option value="'.$rst['city'].'" selected>'.$rst['city'].'</option>';
						}
							echo '<option value=""></option>';
							foreach($rst as $r)
							{
								echo '<option value="'.$r['city'].'">'.$r['city'].'</option>';
							}
					}
				?>
				</select>
				<span style="color:grey;display:none;" id="province2">Search Province</span>
				<select name="province_hospital2" id="province_hospital2" class="dropdown" style="width:300px;display:none;" onchange="Click('Search');">
				<?php
					$sql = "select distinct province from hospital order by province";
					$rst = $db->getAllRecord($sql);
					if($rst)
					{
						if(isset($rst['province']))
						{
							echo '<option value="'.$rst['province'].'" selected>'.$rst['province'].'</option>';
						}
							echo '<option value=""></option>';
							foreach($rst as $r)
							{
								echo '<option value="'.$r['province'].'">'.$r['province'].'</option>';
							}
					}
				?>
				</select>
				</div>

				<div id="div_province2" style="display:none;">
				<span style="color:grey;" id="province">Search Province</span><br>
				<select name="province_hospitals2" id="province_hospitals2" class="dropdown" style="width:300px;" onchange="Click('Search');getAdd2('province_region');">
				<?php
					$sql = "select distinct province from hospital order by province";
					$rst = $db->getAllRecord($sql);
					if($rst)
					{
						if(isset($rst['province']))
						{
							echo '<option value="'.$rst['province'].'" selected>'.$rst['province'].'</option>';
						}
							echo '<option value=""></option>';
							foreach($rst as $r)
							{
								echo '<option value="'.$r['province'].'">'.$r['province'].'</option>';
							}
					}
				?>
				</select>
				<br><span style="color:grey;display:none;" id="city_span2">Search City / Town</span> <br>
				<select name="city_hospitals2" id="city_hospitals2" class="dropdown" style="width:300px;display:none;" onchange="Click('Search');">
				<?php
					$sql = "select distinct city from hospital order by city";
					$rst = $db->getAllRecord($sql);
					if($rst)
					{
						if(isset($rst['city']))
						{
							echo '<option value="'.$rst['city'].'" selected>'.$rst['city'].'</option>';
						}
							echo '<option value=""></option>';
							foreach($rst as $r)
							{
								echo '<option value="'.$r['city'].'">'.$r['city'].'</option>';
							}
					}
				?>
				</select>
				</div>

				<div id="div_region2" style="display:none;">
				<span style="color:grey;" id="region">Search Region</span> <br>
				<select name="region_hospital2" id="region_hospital2" class="dropdown" style="width:300px;" onchange="Click('Search');getAdd2('region_cp');">
				<?php
					$sql = "select distinct region from hospital order by region";
					$rst = $db->getAllRecord($sql);
					if($rst)
					{
						if(isset($rst['region']))
						{
							echo '<option value="'.$rst['region'].'" selected>'.$rst['region'].'</option>';
						}
							echo '<option value=""></option>';
							foreach($rst as $r)
							{
								echo '<option value="'.$r['region'].'">'.$r['region'].'</option>';
							}
					}
				?>
				</select>
				<span style="color:grey;display:none;" id="city_span">Search City / Town</span>
				<select name="city_hospitalss2" id="city_hospitalss2" class="dropdown" style="width:300px;display:none;" onchange="Click('Search');">
				<?php
					$sql = "select distinct city from hospital order by city";
					$rst = $db->getAllRecord($sql);
					if($rst)
					{
						if(isset($rst['city']))
						{
							echo '<option value="'.$rst['city'].'" selected>'.$rst['city'].'</option>';
						}
							echo '<option value=""></option>';
							foreach($rst as $r)
							{
								echo '<option value="'.$r['city'].'">'.$r['city'].'</option>';
							}
					}
				?>
				</select>
				<span style="color:grey;display:none;" id="province_span">Search Province</span>
				<select name="province_hospitalss2" id="province_hospitalss2" class="dropdown" style="width:300px;display:none;" onchange="Click('Search');">
				<?php
					$sql = "select distinct province from hospital order by province";
					$rst = $db->getAllRecord($sql);
					if($rst)
					{
						if(isset($rst['province']))
						{
							echo '<option value="'.$rst['province'].'" selected>'.$rst['province'].'</option>';
						}
							echo '<option value=""></option>';
							foreach($rst as $r)
							{
								echo '<option value="'.$r['province'].'">'.$r['province'].'</option>';
							}
					}
				?>
				</select>

				</div>
				</div>

<!-- DENTISTS -->
				<div id=div_dentists style="display:none;">
				<span style="color:grey;">Search Specialization</span> <br>
				<select name=specialization id="specialization" style="width:300px;" class="dropdown" onchange="Click('Search')">
					<?php
						$sql = "select distinct specialization from dentistsanddoctors where type='DMD' order by specialization";
						$rst = $db->getAllRecord($sql);
						if($rst)
						{
							if(isset($rst['specialization']))
							{
								echo '<option value="'.$rst['specialization'].'" selected>'.$rst['specialization'].'</option>';
							}
								echo '<option value=""></option>';
								foreach($rst as $r)
								{
									echo '<option value="'.$r['specialization'].'">'.$r['specialization'].'</option>';
								}
						}
					?>
				</select> <br>
				<span style="color:grey;">Search City / Town</span> <br>
				<input type="text" name="dentists_address" id="dentists_address" class="input" style="width:300px;" onkeyup="Click('Search')">
				</div>
	</td>
</tr>
<tr>
	<td colspan=2>
		<input type="hidden" value="View" name="Handler" />
	</td>
</tr>
<tr><td><input type=button value="Reset" onclick="location.reload();" class="reset_button"></td></tr>
</table>

</form>

</td>
<td width=800px id=td2>
<br>
<iframe src="directory/list.php" frameborder="0" id="SFrame" name="List" style="height:100%; width:780px; visibility:hidden;"></iframe>
</td></tr></table>
</body>
<script language="JavaScript" type="text/javascript" src="script/prototype.js"></script>