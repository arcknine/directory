<?php
	session_start();
	$boolqueue = 1;
	$auth_arr = $_SESSION['auth'];
	$title = "Queue";	
		
	include("classes/connect.php");
	include("classes/DBConnect.php");
	include("classes/Auth.php");
	include("classes/Util.php");
	include("classes/Patient.php");
	include("classes/Appt.php");
	
	include("include/top.php");
	include("include/menu.php");
?>




<?php

$db = new DBConnect();
$appt = new Appt();
$util = new Util();

$_SESSION['queueState'] = $appt->getQstatus();

$year = $_SESSION['year'];
$mon = $_SESSION['mon'];
$day = $_SESSION['day'];


if(isset($_REQUEST['day']) && isset($_REQUEST['mon']) && isset($_REQUEST['year']))
{
	$day = $_REQUEST['day'];
	$mon = $_REQUEST['mon'];
	$year = $_REQUEST['year'];
}


//tomorrow
$tom  = getdate(mktime(0,0,0,$mon,$day+1,$year));

//yesterday
$yday = getdate(mktime(0,0,0,$mon,$day-1,$year));

if($day < 10)
$day = "0".intval($day);

$tom['mday'] < 10 ? $td = "0".intval($tom['mday']) : $td = $tom['mday'];
$tom['mon'] < 10 ? $tm = "0".intval($tom['mon']) : $tm = $tom['mon'];
$tom = "<a href=queue.php?day=".$td."&mon=".$tm."&year=".$tom['year']
." onmouseover=\"this.T_WIDTH=100;this.T_FONTCOLOR='#003399';return escape('Next Day')\"><img src=image/arrowright.png border=0 width=25px height=25px></a>";
$yday['mday'] < 10 ? $yd = "0".intval($yday['mday']) : $yd = $yday['mday'];
$yday['mon'] < 10 ? $ym = "0".intval($yday['mon']) : $ym = $yday['mon'];
$yday ="<a href=queue.php?day=".$yd."&mon=".$ym."&year=".$yday['year']
." onmouseover=\"this.T_WIDTH=100;this.T_FONTCOLOR='#003399';return escape('Previous Day')\"><img src=image/arrowleft.png border=0 width=25px height=25px></a>";

$mon_arr = array ("-", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

if(isset($_REQUEST['do']) &&  $_REQUEST['do'] == "del")
{
	$appt->delAppt($_REQUEST['apptid']);
}

//========================= PAGE TITLE =====================
echo "<div id='queue'>";
echo "<p class=pageTitle> &nbsp;Queue for ".date("l",mktime(0,0,0,$mon,$day,$year))." ".$mon_arr[intval($mon)]." ".intval($day).", $year</p>";

//==NAVIGATION
echo "<table style=margin-left:auto;margin-right:auto;padding-top:15px><tr>
<td valign=bottom align=right>PREV DAY $yday $tom NEXT DAY</td></tr></table>";

$color_arr = array("IN"=>"#99CCFF","NEXT"=>"#E9E6B7","WAIT"=>"#E08180",
		"APPT"=>"#FFCCFF","SEEN"=>"#EBECE2","CANC"=>"#BEDCB3","NO SHOW"=>"#000000");
echo "<div id=response></div>";

//==VIEW QUEUE
$sql = "select id,start,end,patientid, date_format(arrive,'%l:%i') as arrive6, date_format(astart,'%l:%i') as astart7,
		date_format(aend,'%l:%i') as aend8,arrive,astart,aend,status,visittype, timediff(aend,astart) as con_time,
		timediff(astart,arrive) as wait_time,remark 
		from appointment where date(start)='$year-$mon-$day' and userid=".$auth_arr['userid']." order by 
		status='NO SHOW',status='CANC',status='SEEN',status='APPT',status='WAIT',status='NEXT',status='IN',start asc";

$appt_all = $db->getAllRecord($sql);

$total_rows = count($appt_all);
$count = 1;


if($total_rows > 0)
{
	$patient = new Patient();
?>
	<table  cellpadding=2 cellspacing=2 rules=rows style=margin-left:auto;margin-right:auto>
		<tr>
			<th colspan=2>Time</th><th>Patient</th><th>Visit Type</th><th>Status</th><th>Arrival</th>
			<th>In</th><th>Out</th><th>Remarks</th>
		</tr>
		
		<?php
		$data = array();
		foreach($appt_all as $row)
		{
			$data = $patient->getData($row['patientid']);
		?>
		
		<tr>
			<td style="background-color:<?php echo $color_arr[$row['status']]?>;font-weight:bold"><?php echo $count?>.</td>
			<td><?php echo $util->setDateTime($row['start'],"g:i A")?> - <?php echo $util->setDateTime($row['end'],"g:i A")?></td>
			<td align=center >
			
				<a href="patient.php?pf=6&patientid=<?php echo $row['patientid']?>&pp=3" <?php echo $util->mouseOver("New Consult","100")?>>
				<?php if(is_file($_SESSION['photothumb'].'/'.$data['photo'])) {?>
				<img src="<?php echo $_SESSION['urlthumb']?>/<?php echo $data['photo']?>" border=0>
				<br>
				<?php } ?>
				<strong><?php echo $data['salutation']?> <?php echo $data['firstname']?> <?php echo $patient->data['lastname']?></strong></a>
				<?php
				if($data['phone'])
				{
					echo "<br>Phone: ".$data['phone'];
				}	
				?>
			</td>
			<td>
				<?php echo getVisit($row['patientid'],$row['visittype'],"changeRequest('setvisit',this.value,'".$row['id']."')")?>
			</td>
			<td>
				<?php echo getStatus($row['patientid'],$row['status'],"changeRequest('setstatus',this.value,'".$row['id']."')")?>
				
			</td>
			<td>
				<?php
				if($row['arrive'] != "0000-00-00 00:00:00")
				{
					echo $row['arrive6'];
				}
				?>
			</td>
			<td>
				<?php
				if($row['astart'] != "0000-00-00 00:00:00")
				{
					echo $row['astart7'];
				}
				?>
			</td>
			<td>
				<?php
				if($row['aend'] != "0000-00-00 00:00:00")
				{
					echo $row['aend8'];
				}
				?>
			</td>
			<td><?php echo $row['remark']?></td>
			<td>
				<?php echo $util->setEdel("","queue.php?do=del&apptid=".$row['id'])?>
			</td>
		</tr>
	<?php
		$count++;
	}
	?>		
	</table>
<?php
}
else
{
	echo "<p align=center>Sorry no available appointment on queue.</p>";
}
?>

<?php
function getStatus($id,$default,$onchange)
{
	$appt = new Appt();
	$status_arr = $appt->getStatus();
	$x = "<select name=status".$id." id=status".$id." onchange=\"$onchange\">";
	foreach($status_arr as $value)
	{
		if($default == $value)
		{
			$x.="<option selected  value=\"$value\">$value</option>";
		}
		elseif($value == "APPT" && $default == "")
		{
			$x.="<option selected value=\"$value\">$value</option>";
		}
		else
		{
			$x.="<option  value=\"$value\">$value</option>";
		}
	}
	return "$x.</select>";
}

function getVisit($id,$default,$onchange)
{
		$appt = new Appt();
	$status_arr = $appt->getVisitType();
	$x = "<select name=visit".$id." id=visit".$id." onchange=\"$onchange\">";
	foreach($status_arr as $value)
	{
		if($default == $value)
		{
			$x.="<option selected  value=\"$value\">$value</option>";
		}
		elseif($value == "-" && $default == "")
		{
			$x.="<option selected value=\"$value\">$value</option>";
		}
		else
		{
			$x.="<option  value=\"$value\">$value</option>";
		}
	}
	return "$x.</select>";
}

echo "</div>";
?>

<div id=footer_page>
<?php
include("include/bottom.php");
?>
</div>
