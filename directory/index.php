<?php
include("../directory/connect.php");
include("../classes/DBConnect.php");
include("../classes/Util.php");
$db = new DBConnect();
$util = new Util();
//echo "DEBUGGING";
//var_dump($_POST);

if(@$_POST['type']=='div_dentists'){
	$specialization_name = trim($_POST['specialization']);
	if($_POST['search']=='') $name = '';
	else $name = " and (CONCAT_WS(' ', `firstname`, `lastname` ) like '%".$_POST['search']."%' or 
					   	firstname like '%".$_POST['search']."%' or lastname like '%".$_POST['search']."%')";
	if($_POST['dentists_address']=='') $clinic = ''; 
	else $clinic = "and clinic1 like '%".$_POST['dentists_address']."%' or clinic2 like '%".$_POST['dentists_address']."%' or
						clinic3 like '%".$_POST['dentists_address']."%' or clinic4 like '%".$_POST['dentists_address']."%' or
						clinic5 like '%".$_POST['dentists_address']."%'";
	if($_POST['specialization']=='') $specialization=''; 
	else $specialization = "and specialization ='$specialization_name'";
	$AllFil = $name.$specialization.$clinic;
	$sql = "Select * from dentistsanddoctors where type='DMD' and status='ACCREDITED' ".$AllFil." order by lastname";
	$rst = $db->getAllRecord($sql);
	$count = count($rst);

	if($count=='0'){
		echo '<p align=center style="color:red;font-size:24px;font-weight:bold;">No Result</p>';
	} else {
		echo '<p align=center style="font-weight:bold;"><span style="color:#ff5a00;font-size:24px;">Number of Results: </span><span style="color:#0039a6;font-size:24px;">'.$count.'</span></p>';
	}
	foreach($rst as $r){
			
			echo '<table width=100%>';
				echo '<tr><td colspan=3>';
					echo '<span style="color:#ff5a00;font-size:21px;">DR. '.$r['firstname'].'&nbsp;'.$r['lastname'].'</span>';
				echo '</td></tr>';
				if($r['specialization']=='-'){} else {
				echo '<tr><td style="width:160px;">';
					echo '<span style="color:#0039a6;font-size:21px;">Specialization:</span></td><td>'.$r['specialization'].'</td>';
				echo '</td></tr>';
				}if($r['clinic1']=='-'){} else {
				echo '<tr><td style="width:160px;">';
					echo '<span style="color:#0039a6;font-size:21px;">Clinic 1:</span></td><td>'.$r['clinic1'].'</td>';
				echo '</td></tr>';
				}if($r['clinic2']=='-'){} else {
				echo '<tr><td style="width:160px;">';
					echo '<span style="color:#0039a6;font-size:21px;">Clinic 2:</span></td><td>'.$r['clinic2'].'</td>';
				echo '</td></tr>';
				}if($r['clinic3']=='-'){} else {
				echo '<tr><td style="width:160px;">';
					echo '<span style="color:#0039a6;font-size:21px;">Clinic 3:</span></td><td>'.$r['clinic3'].'</td>';
				echo '</tr></td>';
				}if($r['clinic4']=='-'){} else {
				echo '<tr><td style="width:160px;">';
					echo '<span style="color:#0039a6;font-size:21px;">Clinic 4:</span></td><td>'.$r['clinic4'].'</td>';
				echo '</tr></td>';
				}if($r['clinic5']=='-'){} else {
				echo '<tr><td style="width:160px;">';
					echo '<span style="color:#0039a6;font-size:21px;">Clinic 5:</span></td><td>'.$r['clinic5'].'</td>';
				echo '</tr></td>';
				}if($r['mobile_number']=='-'){} else {
				echo '<tr><td style="width:160px;">';
					echo '<span style="color:#0039a6;font-size:21px;">Mobile Number:</span></td><td>'.$r['mobile_number'].'</td>';
				echo '</tr></td>';
				}if($r['contact_number']=='-'){} else {
				echo '<tr><td style="width:160px;">';
					echo '<span style="color:#0039a6;font-size:21px;">Contact Number:</span></td><td>'.$r['contact_number'].'</td>';
				echo '</tr></td>';
				}if($r['fax_number']=='-'){} else {
				echo '<tr><td style="width:160px;">';
					echo '<span style="color:#0039a6;font-size:21px;">Fax Number:</span></td><td>'.$r['fax_number'].'</td>';
				echo '</tr></td>';
				}if($r['email']=='-'){} else {
				echo '<tr><td style="width:160px;">';
					echo '<span style="color:#0039a6;font-size:21px;">Email:</span></td><td>'.$r['email'].'</td>';
				echo '</tr></td>';
				}
				
			echo '</table>';
			echo '<br/>';	
	}
	echo '</td>';
	echo '</tr>';
	echo '</table>';

}elseif(@$_POST['type']=='div_hospital'){
	
	if($_POST['searchby'] == 'city')
	{
		if($_POST['city_hospital']=='') $city = '';
		else $city = ' and city like "%'.$_POST['city_hospital'].'%"';
		if($_POST['region_hospital']=='') $region = '';
		else $region = ' and region like "%'.$_POST['region_hospital'].'%"';
		if($_POST['province_hospital']=='') $province = '';
		else $province = 'and province like "%'.$_POST['province_hospital'].'%"';
	}
	elseif($_POST['searchby'] == 'province')
	{
		if($_POST['city_hospitals']=='') $city = '';
		else $city = ' and city like "%'.$_POST['city_hospitals'].'%"';
		if($_POST['region_hospital']=='') $region = '';
		else $region = ' and region like "%'.$_POST['region_hospital'].'%"';
		if($_POST['province_hospitals']=='') $province = '';
		else $province = 'and province like "%'.$_POST['province_hospitals'].'%"';
	}
	else
	{
		if($_POST['city_hospitalss']=='') $city = '';
		else $city = ' and city like "%'.$_POST['city_hospitalss'].'%"';
		if($_POST['region_hospital']=='') $region = '';
		else $region = ' and region like "%'.$_POST['region_hospital'].'%"';
		if($_POST['province_hospitalss']=='') $province = '';
		else $province = 'and province like "%'.$_POST['province_hospitalss'].'%"';
	}
	$AllFil = $city.$region.$province;
	$namearray = array("MT ","ST ");
	$namearrayresult = array("MT. ","ST. ");
	$name = strtoupper($_POST['search']);
	$namestr = str_replace($namearray,$namearrayresult,$name);
	$sql = "select * from hospital where status='ACCREDITED' and classification='HOSPITAL' and `name` like '%".$namestr."%' ".$AllFil." order by name;";

	$rst = $db->getAllRecord($sql);
	$count = count($rst);
	$cols = $db->getColumns('hospital');
	
	if($count=='0'){
		echo '<p align=center style="color:red;font-size:24px;font-weight:bold;">No Result</p>';
	}else {
		echo '<p align=center style="font-weight:bold;"><span style="color:#ff5a00;font-size:24px;">Number of Results: </span><span style="color:#0039a6;font-size:24px;">'.$count.'</span></p>';
	}
	
	foreach($rst as $r){
		echo '<table width=100%>';
		for($c=0;$c<count($cols);$c++){
			if($cols[$c]=='id' || $cols[$c]=='mobile_number' || $cols[$c]=='fax_number' || $cols[$c]=='date_accredited' 
			|| $cols[$c]=='status' || $cols[$c]=='remarks' || $cols[$c]=='date_encoded' || $cols[$c]=='user' 
			|| $cols[$c]=='dentistsanddoctors_id'){}//do nothing
			else{
				if($r[$c]!=''){
					echo '<tr>';
						if($cols[$c]=='name'){
							echo '<td colspan=2><span style="color:#ff5a00;font-size:21px;">'.$r[$c].'</span></td>';
					}else{
						if($cols[$c]=='city'){
							echo '<td style=width:160px;><span style="color:#0039a6;font-size:21px;">Address: </td><td></span>';
							if($r['street_address'] == ''){}else{ echo $r['street_address'].',&nbsp;'; }
							if($r['subdivision_village'] == ''){}else{ echo $r['subdivision_village'].',&nbsp;'; }
							if($r['barangay'] == ''){}else{ echo $r['barangay'].',&nbsp;'; }
							echo $r['city'];
							echo'</td>';
					}else{
						if($cols[$c]=='type'){
							echo '<td style=width:160px;><span style="color:#0039a6;font-size:21px;">Type:</span></td><td>'.$r['type'].'</td>';
					}else{
						if($cols[$c]=='category'){
							echo '<td style=width:160px;><span style="color:#0039a6;font-size:21px;">Category:</span></td><td>'.$r['category'].'</td>';
					}else{
						if($cols[$c]=='contact_person'){
							echo '<td style=width:160px;><span style="color:#0039a6;font-size:21px;">Contact Person:</span></td><td>'.$r['contact_person'].'</td>';
					}else{
						if($cols[$c]=='contact_number'){
							echo '<td style=width:160px;><span style="color:#0039a6;font-size:21px;">Contact Number:</span></td><td>'.$r['contact_number'].'</td>';
					}
				}
			}
		}
	}echo '</tr>';
}
}
}
}	echo '</td>';
	echo '</tr>';
	echo '</table>';
	echo '<br/>';
}
}elseif(@$_POST['type']=='div_clinic'){

	if($_POST['searchby2'] == 'city2')
	{
		if($_POST['city_hospital2']=='') $city = '';
		else $city = ' and city like "%'.$_POST['city_hospital2'].'%"';
		if($_POST['region_hospital2']=='') $region = '';
		else $region = ' and region like "%'.$_POST['region_hospital2'].'%"';
		if($_POST['province_hospital2']=='') $province = '';
		else $province = 'and province like "%'.$_POST['province_hospital2'].'%"';
	}
	elseif($_POST['searchby2'] == 'province2')
	{
		if($_POST['city_hospitals2']=='') $city = '';
		else $city = ' and city like "%'.$_POST['city_hospitals2'].'%"';
		if($_POST['region_hospital2']=='') $region = '';
		else $region = ' and region like "%'.$_POST['region_hospital2'].'%"';
		if($_POST['province_hospitals2']=='') $province = '';
		else $province = 'and province like "%'.$_POST['province_hospitals2'].'%"';
	}
	else
	{
		if($_POST['city_hospitalss2']=='') $city = '';
		else $city = ' and city like "%'.$_POST['city_hospitalss2'].'%"';
		if($_POST['region_hospital2']=='') $region = '';
		else $region = ' and region like "%'.$_POST['region_hospital2'].'%"';
		if($_POST['province_hospitalss2']=='') $province = '';
		else $province = 'and province like "%'.$_POST['province_hospitalss2'].'%"';
	}
	$AllFil = $city.$region.$province;
	$namearray = array("MT ","ST ");
	$namearrayresult = array("MT. ","ST. ");
	$name = strtoupper($_POST['search']);
	$namestr = str_replace($namearray,$namearrayresult,$name);
	$sql = "select * from hospital where status='ACCREDITED' and classification='CLINIC' and `name` like '%".$namestr."%' ".$AllFil." order by name;";
	$rst = $db->getAllRecord($sql);
	$cols = $db->getColumns('hospital');
	$count = count($rst);

	if($count=='0'){
		echo '<p align=center style="color:red;font-size:24px;font-weight:bold;">No Result</p>';
	}else {
		echo '<p align=center style="font-weight:bold;"><span style="color:#ff5a00;font-size:24px;">Number of Results: </span><span style="color:#0039a6;font-size:24px;">'.$count.'</span></p>';
	}
	foreach($rst as $r){
		echo '<table width=100%>';	
		for($c=0;$c<count($cols);$c++){
			if($cols[$c]=='id' || $cols[$c]=='mobile_number' || $cols[$c]=='fax_number' || $cols[$c]=='date_accredited' 
			|| $cols[$c]=='status' || $cols[$c]=='remarks' || $cols[$c]=='date_encoded' || $cols[$c]=='user' 
			|| $cols[$c]=='dentistsanddoctors_id'){}//do nothing
			else{
				if($r[$c]!=''){
					echo '<tr>';
						if($cols[$c]=='name'){
							echo '<td colspan=2><span style="color:#ff5a00;font-size:21px;">'.$r[$c].'</span></td>';
					}else{
						if($cols[$c]=='city'){
							echo '<td style=width:160px;><span style="color:#0039a6;font-size:21px;">Address:</span></td><td>';
							if($r['street_address'] == ''){}else{ echo $r['street_address'].',&nbsp;'; }
							if($r['subdivision_village'] == ''){}else{ echo $r['subdivision_village'].',&nbsp;'; }
							if($r['barangay'] == ''){}else{ echo $r['barangay'].',&nbsp;'; }
							echo $r['city'];
							echo'</td>';
					}else{
						if($cols[$c]=='type'){
							echo '<td style=width:160px;><span style="color:#0039a6;font-size:21px;">Type:</span></td><td>'.$r['type'].'</td>';
					}else{
						if($cols[$c]=='category'){
							echo '<td style=width:160px;><span style="color:#0039a6;font-size:21px;">Category:</span></td><td>'.$r['category'].'</td>';
					}else{
						if($cols[$c]=='contact_person'){
							echo '<td style=width:160px;><span style="color:#0039a6;font-size:21px;">Contact Person:</span></td><td>'.$r['contact_person'].'</td>';
					}else{
						if($cols[$c]=='contact_number'){
							echo '<td style=width:160px;><span style="color:#0039a6;font-size:21px;">Contact Number:</span></td><td>'.$r['contact_number'].'</td>';
					}
				}
			}
		}
	}echo '</tr>';
}
}
}
}	echo '</td>';
	echo '</tr>';
	echo '</table>';
	echo '<br/>';
}
}
?>