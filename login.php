<?php
session_start();

include("classes/connect.php");
include("classes/DBConnect.php");
include("classes/Auth.php");
	
$date = new DateTime('Asia/Manila');
$today = $date->format("Y-m-d");
$_SESSION['year'] = $date->format("Y");
$_SESSION['mon'] = $date->format("m");
$_SESSION['day'] = $date->format("d");
$_SESSION['datetoday'] = $date->format("F j, Y");


	$auth = new Auth();
	$auth_arr = $auth->login($_REQUEST['username'],$_REQUEST['password']);
	
	$_SESSION['path'] = dirname($_SERVER['SCRIPT_FILENAME']);
	
	
	$_SESSION['photo'] = dirname($_SERVER['SCRIPT_FILENAME'])."/photo";
	$_SESSION['photothumb'] = dirname($_SERVER['SCRIPT_FILENAME'])."/photo/thumb";
	
	
	$dir = dirname($_SERVER['SCRIPT_NAME']);
	
	
	if($dir != "/")
	{
		$_SESSION['urlphoto'] = $dir."/photo";
		$_SESSION['urlthumb'] = $dir."/photo/thumb";
	}
	else
	{
		$_SESSION['urlphoto'] = "photo";
		$_SESSION['urlthumb'] = "photo/thumb";

	}
	
	if($auth_arr['user'] != "" && $auth_arr['access'] != "")
	{
			
		$auth_arr['userid'] = 0;
		if($auth_arr['access'] == "doctor" || $auth_arr['access'] == "admin")
		{
			$auth_arr['userid'] = $auth_arr['id'];
		}
		elseif($auth_arr['access'] == "staff")
		{
			$auth_arr['userid'] = $auth_arr['doctorid'];
		}
		
		$_SESSION['auth'] = $auth_arr;
		
		if($auth_arr['access'] == "doctor")
		{
			if($auth->isFirstLogin($auth_arr['id'],$today))
			{
				header("location:numbers.php");
			}
			else
			{
				header("location:tasks.php");
			}
		}
		elseif($auth_arr['access'] == "staff")
		{
			header("location:queue.php");
		}
		else
		{
			header("location:tasks.php");
		}
		
	}
	else
	{
		header("location:index.php?msg=1");
	}


?>
