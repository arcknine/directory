<?php
session_start();
$title = "Manage";
include("classes/connect.php");
include("classes/DBConnect.php");
include("classes/Util.php");
include("classes/Patient.php");

include("include/top.php");
include("include/menu.php");

/*
* patient data, used this to update
*/

$util = new Util();
$db = new DBConnect();


$page = 0;

if(isset($_REQUEST['page'])){$page = $_REQUEST['page'];}

if($page == 1)
{
	include("manage/icd10.php");
}
elseif($page == 2)
{
	include("manage/labrequest.php");
}
elseif($page == 3)
{
	include("manage/procedure.php");
}
else
{	echo "<div id='manage'>";
	echo "<p class=pageTitle> &nbsp;Manage</p>";
	echo "<ul style=margin-left:150px;>";
	echo "<li><a href='manage.php?page=1'>ICD 10</a>";
	
	if($auth_arr['access'] != 'admin')
	{
		echo "<li><a href='manage.php?page=2'>Laboratory or Radiology</a>";
		echo "<li><a href='manage.php?page=3'>Procedure</a>";
	}
	echo "</ul>";
	echo "</div>";
}
?>



<div id=footer_page>
<?php
include("include/bottom.php");
?>
</div>