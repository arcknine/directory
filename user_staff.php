<?php
session_start();
$title = "Data";
$auth_arr = $_SESSION['auth'];

include("classes/connect.php");
include("classes/DBConnect.php");
include("classes/Auth.php");
include("classes/Util.php");
include("include/top.php");
include("include/menu.php");

$util = new Util();
?>

<div id="user">

	<p class=pageTitle> &nbsp;User Management</p>
	
	<?php
	$db = new DBConnect();
	$edit_bool = false;
	$auth = new Auth();
	$auth_arr = isset($_SESSION['auth'])?$_SESSION['auth']:"";

	//=== ADD DOCTOR ==============================
	if(isset($_REQUEST['submit']) && $_REQUEST['submit'] ==  "Add")
	{
		//print_r($_REQUEST);
		if($_REQUEST['password'] != "")
		{
		
			if(isset($_REQUEST['password']) &&  isset($_REQUEST['password2']) &&  $_REQUEST['password'] == $_REQUEST['password2'])
			{ 
				$_REQUEST['doctorid'] = $auth_arr['id'];
				$return = $auth->addStaff($_REQUEST);
				if($return == 0)
				{
					echo '<p style=color:red>User name may be used already.</p>';
				}
			}
			else
			{
				echo "<p style=color:red>Passwords do not match. Add failed.";
			}
		}
		else
		{
			echo "<p style=color:red>Empty password not allowed.";
		}
	}
	elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "delete")
	{
		//echo $_REQUEST;
	//=== DELETE DOCTOR ==============================
		$sql = "delete from users where id=".$_REQUEST['userid'];
		$db->delete($sql);
	}
	elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "edit")
	{
		$edit_bool = true;
	}
	elseif(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "Update")
	{
	//=== DELETE DOCTOR ==============================
		
		if(isset($_REQUEST['password']) && $_REQUEST['password'] != "")
		{
			if(isset($_REQUEST['password'])  &&  isset($_REQUEST['password2']) && $_REQUEST['password'] == $_REQUEST['password2'])
			{
				$auth->update($_REQUEST);
				//echo $db->msg;
			}
			else
			{
				echo "<p style=color:red>Passwords do not match. Update failed.";
			}
		}
		else
		{
			$auth->updateNoPassword($_REQUEST);
		}
		
		if(isset($_REQUEST['access']) && $_REQUEST['access'] == "doctor")
		{
			$_SESSION['auth'] = $auth->getUser($_REQUEST['userid']);
	
			$auth_arr = $_SESSION['auth'];
			$auth_arr['userid'] = $auth_arr['id'];
			$_SESSION['auth']['userid'] = $auth_arr['id'];
			
		}
	}
	
	if($edit_bool)
	{
		$sql = "select * from users where id=".$_REQUEST['userid'];
		$dum = $db->getRecord($sql);
		?>
		<form action=user_staff.php method=post>
		<input type=hidden name=userid value="<?=$dum['id']?>">
		<input type=hidden name=access value="<?=$dum['access']?>">
		<input type=hidden name=doctorid value="<?=$dum['doctorid']?>">
		<input type=hidden name=user value="<?=$dum['user']?>">
		<h3 class=titleupdate>Update User</h3>
		<table cellpadding=2 cellspacing=2 align=center>		
			<tr><td>Name</td><td><input type=text name=name size=40 value="<?=$dum['name']?>"></td></tr>	
			<tr><td>User Name</td><td><?=$dum['user']?></td></tr>
			<tr><td>New Password</td><td><input type=password name=password size=40 value=""></td></tr>
			<tr><td>Verify Password</td><td><input type=password name=password2 size=40 value=""></td></tr>
			<tr><td colspan=2 align=right><input type=submit value=Cancel id=cancel> <input type=submit name=submit value=Update id=update></td></tr>
		</table>
		</form>
		<?php
	}
	else
	{

		//==== VIEW ==========================================
		$sql = "";
		if($auth_arr['access'] == 'doctor')
		{
			$sql = "select * from users where id = '".$auth_arr['id']."' or doctorid='".$auth_arr['id']."'";
		}
		elseif($auth_arr['access'] == 'staff')
		{
			$sql = "select * from users where id ='".$auth_arr['id']."'";
		}
		//echo $sql;
		$dum = $db->getAllRecord($sql);
		$count = 1;
		$bf = "";
		foreach($dum as $val)
		{
			$bf.="<tr><td><strong>$count.</strong></td><td class=ctr>".$val['name']."</td><td class=ctr>".$val['user']."</td><td class=ctr>".$val['access']."</td>";
			
			if($auth_arr['access'] == $val['access'])
			{	
				$bf.="<td>".$util->setEdel("user_staff.php?userid=".$val['id']."&do=edit","");
			}
			elseif($val['access'] == 'staff')
			{
				$bf.="<td>".$util->setEdel("user_staff.php?userid=".$val['id']."&do=edit","user_staff.php?userid=".$val['id']."&do=delete");
			}
			$bf.="</td></tr>";
			$count++;
		}
		if($bf)
		{
			echo "<table cellpadding=7 cellspacing=5 width=50% class=user align=center> ";
			echo "<tr><th colspan=2>Name</th><th>User</th><th>Access</th></tr>";
			echo $bf."</table>";
		}
		
		if($auth_arr['access'] == 'doctor')
		{
		?>
		 <form action=user_staff.php method=post style=padding-top:20px>
		 	<input type=hidden name=access value="staff">
			<h3 class=pageTitle> &nbsp;Add Staff</h3>
			<table cellpadding=2 cellspacing=2 align=center>	
				<tr><td>Name</td><td><input type=text name=name size=40></td></tr>
				<tr><td>User Name</td><td><input type=text name=user size=40></td></tr>
				<tr><td>Password</td><td><input type=password name=password size=40></td></tr>
				<tr><td>Verify Password</td><td><input type=password name=password2 size=40></td></tr>
				<tr><td colspan=2 align=right><input type=submit name=submit value=Add id=add></td></tr>
			</table>
		</form>
		<?php 
		} 
		?>
		
		
		
		
	<?php
	}
	?>
	
</div>

<div id=footer_page>
<?php
include("include/bottom.php");
?>
</div>