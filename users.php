<?php
session_start();
$title = "Data";
$auth_arr = $_SESSION['auth'];

include("classes/connect.php");
include("classes/DBConnect.php");
include("classes/Auth.php");
include("classes/Util.php");
include("include/top.php");
include("include/menu.php");

$util = new Util();
?>


<!--<p class=pageTitle>User Management</p>-->

<table class=pageTitle3 width=85% align=center>
<tr>
	<td>&nbsp;User Management</td>
	<td align=right>
			<div id="searchwrapper">
				<form action=users.php method=post>
					<input type=text name=searchtext size=20 class="searchbox" placeholder="Search"/>
					<input type=submit name=submit value=Search class="searchbox_submit"/>
				</form>
			</div>
	</td>
	<td>&nbsp;
	</td>	
</tr>
</table>
<div id=users>
	
	<?php
	$db = new DBConnect();
	$edit_bool = false;
	$auth = new Auth();
	$auth_arr = $_SESSION['auth'];
	
	//=== ADD DOCTOR ==============================
	if(isset($_REQUEST['submit']) && $_REQUEST['submit'] ==  "Add")
	{
		if(isset($_REQUEST['password']) && $_REQUEST['password'] != "")
		{
		if(isset($_REQUEST['password2']) &&  isset($_REQUEST['password']) && $_REQUEST['password'] == $_REQUEST['password2'])
		{ 
			if(isset($_REQUEST['access']) && $_REQUEST['access'] == "doctor")
			{
				$_REQUEST['doctorid'] = 0;
			}
			$auth->add($_REQUEST);
		}
		
		else
		{
			echo "<p style=color:red>Passwords do not match. Add failed.";
		}
		}
		else
		{
			echo "<p style=color:red>Empty password not allowed.";
		}
	}
	elseif(isset($_REQUEST['do']) &&  $_REQUEST['do'] == "delete")
	{
		//=== DELETE DOCTOR ==============================
		$sql = "delete from users where id=".$_REQUEST['userid'];
		$db->delete($sql);
	}
	elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "edit")
	{
		$edit_bool = true;
	}
	elseif(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "Update")
	{
		
		if(isset($_REQUEST['password']) && $_REQUEST['password'] != "")
		{
			if(isset($_REQUEST['password2']) && $_REQUEST['password'] == $_REQUEST['password2'])
			{
				$auth->update($_REQUEST);
			}
			else
			{
				echo "<p style=color:red>Passwords do not match. Update failed.";
			}
		}
		else
		{
			$auth->updateNoPassword($_REQUEST);
		}
	
		if($auth_arr['access'] == 'admin')
		{
			
		}
		else
		{
			$_SESSION['auth'] = $auth->getUser($_REQUEST['userid']);
			$auth_arr = $_SESSION['auth'];
		}	
	}
	
	
	if($edit_bool)
	{
		$sql = "select * from users where id=".$_REQUEST['userid'];
		$dum = $db->getRecord($sql);
		if(!isset($dum['name'])){ $dum['name']="";}
		?>
		<form action=users.php method=post style=padding-top:20px>
		<input type=hidden name=userid value="<?=$dum['id']?>">
		<table cellpadding=4 cellspacing=4 class=view>
			<tr><th colspan=2>Update User</th></tr>
			<tr><td colspan=2></td></tr>
			<tr><td>Name</td><td><input type=text name=name size=40 class=input value="<?=$dum['name']?>"></td></tr>
			<tr><td>User Name</td><td><input type=text name=user size=40 class=input value="<?=$dum['user']?>"></td></tr>
			<tr><td>New Password</td><td><input type=password name=password class=input size=40 value=""></td></tr>
			<tr><td>Verify Password</td><td><input type=password name=password2 class=input size=40 value=""></td></tr>
			<tr><td>Access</td><td><select name=access class=input><option selected><?=$dum['access']?></option><option>staff</option><option>doctor</option><option>admin</option></select></td></tr>
			<?php if($dum['access'] == "staff") { ?>
			<tr><td>Staff of </td><td>
			<?php 

			$table = "users";
			$select = "doctorid";
			$input = "name";
			$sal_arr = $db->getAllRecord("select id,$input from $table where access='staff' and  user!='admin' order by $input");
			//print_r($sal_arr);
			if(count($sal_arr) > 0)
			{
				echo "<select name=doctorid>";
				foreach($sal_arr as $r)
				{
					if($dum['doctorid'] == $r['id'])
					{
						echo "<option value=".$r['id']." selected>".$r['name']."</option>";
					}
					else
					{
						echo "<option value=".$r['id'].">".$r['name']."</option>";
					}
				}
				echo "</select>";
			}
			?>
			</td></tr>
			<?php } ?>
			<tr><td colspan=2 align=right> <input type=submit value=Cancel class=cancel_button> <input type=submit name=submit value=Update class=update_button></td></tr>
		</table>
		</form>
		<?php
	}
	else
	{
	
		//==== VIEW ==========================================
		$start = 0;
		if(isset($_REQUEST['start']))
		{
			$start = $_REQUEST['start'];
		}
		$rowstoview =  10;
		if(isset($_REQUEST['rowstoview']))
		{
			$rowstoview = $_REQUEST['rowstoview'];
		}
		
		$sql = "";
		if($auth_arr['access'] == 'admin')
		{
			
			if(isset($_REQUEST['submit']) && isset($_REQUEST['searchtext']) 
				&& $_REQUEST['searchtext'] != '' && $_REQUEST['submit'] == "Search")
			{
				$sql = "select * from users where match(user,name) against('".$_REQUEST['searchtext']."*' IN BOOLEAN MODE) order by user";
				$dum = $db->getAllRecord($sql." limit $start, $rowstoview");
				$sql = "select count(*) from users where match(user,name) against('".$_REQUEST['searchtext']."*' IN BOOLEAN MODE) order by user";
				$xdum = $db->getRecord($sql);
				//print_r($xdum);
				$rowCount = $xdum[0];	
			}
			else
			{
				$sql = "select * from users order by user";
				$dum = $db->getAllRecord($sql." limit $start, $rowstoview");
				$sql = "select count(*) from users order by user";
				$xdum = $db->getRecord($sql);
				//print_r($xdum);
				$rowCount = $xdum[0];
			}
			//echo $sql;
		}
		else
		{
			$sql = "select * from users where user ='".$auth_arr['user']."'";
			$dum = $db->getAllRecord($sql);
		}
		//echo $sql;
		
		$count = 1;
		$bf = "";
		foreach($dum as $val)
		{	
			if(!isset($val['name'])) { $val['name'] = ""; }
			if(!isset($val['doctorid'])) { $val['doctorid'] = ""; }

			$bf.="<tr><td><strong>$count.</strong></td><td class=ctr>".$val['name']."</td><td class=ctr>".$val['user']."</td><td class=ctr>".$val['access']."</td>";
			if($val['access'] == "staff")
			{
				$bf.="<td>Doctor: ".$auth->getDoctor($val['doctorid'])."</td>";

			}
			elseif($val['access'] == "doctor")
			{
				if(count($auth->getStaffs($val['id'])) == 0)
				{
					$bf.="<td></td>";
				}
				else
				{
					$bf.="<td>Staff: ".implode(",",$auth->getStaffs($val['id']))."</td>";
				}

			}
			else
			{
				$bf.="<td></td>";
			}
			$bf.="<td>".$util->setEdel("users.php?userid=".$val['id']."&do=edit","users.php?userid=".$val['id']."&do=delete");
			$bf.="</td></tr>";
			$count++;
		}
		?>
		<!-- 
		<form action=users.php method=post>
		<input type=text name=searchtext size=20><input type=submit name=submit value=Search>
		</form>
		 -->
		<br><br>
		<?php 
		if($bf)
		{
			echo "<table cellpadding=4 cellspacing=4 class=view >";
			echo "<tr><th colspan=2>Name</th><th>User</th><th>Access</th><th>Doctor/Staff</th><th></th></tr>";
			echo $bf."</table>";
			if($auth_arr['access'] == 'admin')
			{
				echo $util->navi("users.php?",$start,$rowstoview,$rowCount,"image");
			}
		}
		
		if($auth_arr['access'] == 'admin')
		{
		?>
		 <form action=users.php method=post style=padding-top:20px>
		 <input type=hidden name=rowstoview value="<?php echo $rowstoview;?>" >
		 <input type=hidden name=start value="<?php echo $start;?>" >
		 
		<table cellpadding=4 cellspacing=4 class=view >
			<tr><th colspan=2>Add User</th></tr>
			<tr><td colspan=2></td></tr>
			<tr><td>Name</td><td><input type=text name=name size=40 class=input></td></tr>
			<tr><td>User Name</td><td><input type=text name=user size=40 class=input></td></tr>
			<tr><td>Password</td><td><input type=text name=password size=40 class=input></td></tr>
			<tr><td>Verify Password</td><td><input type=text name=password2 size=40 class=input></td></tr>
			<tr><td>Access</td><td><select name=access class=input><option>doctor</option><option>staff</option><option>admin</option></select></td></tr>
			<tr><td>Staff of </td><td>
			<?php 
			$table = "users";
			$select = "doctorid";
			$input = "name";
						
			$x = $db->getAllRecord("select id,$input from $table where doctorid 	= 0 and user!='admin' order by $input");
			if($x)
			{
				echo '<select name=doctorid id=doctorid class=input>';
				echo '<option value=0>Choose</option>';
				foreach($x as $d)
				{
					echo '<option value="'.$d['id'].'">'.$d['name'].'</option>';	
				}
				echo '</select>';
			}
			?>
			
			</td></tr>
			<tr><td colspan=2 align=right><input type=submit name=submit value=Add class=add_button></td></tr>
		</table>
		</form>
		<?php } ?>
		
		
		
		
	<?php
	}
	?>
	

<?php
include("include/bottom.php");
?>
</div>