<?php
session_start();
$title = "New Patient";
include("classes/connect.php");
include("classes/DBConnect.php");
include("classes/Patient.php");
include("classes/Util.php");

include("include/top.php");
include("include/menu.php");


$util = new Util();
$db = new DBConnect();
$patient = new Patient();
$pageTitle = "Add New Patient";
$userid = isset($_SESSION['auth']['userid'])?$_SESSION['auth']['userid']:0;


?>
<div id="newpatient">
<p class=pageTitle><?php echo '&nbsp;'.$pageTitle.'';?></p>

<?php 

if(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "Submit")
{
	if($_REQUEST['firstname'])
	{
		$_REQUEST['birthday'] = $_REQUEST['by']."-".$_REQUEST['bm']."-".$_REQUEST['bd'];
		$_REQUEST['userid'] = $userid;
		if(!isset($_REQUEST['philhealth'])){ $_REQUEST['philhealth'] = "no";} 
		$patientid = $patient->add($_REQUEST);
		if($patientid > 0)
		{
			$newname = $util->imageupload("photo_".$patientid,$_FILES['photo']['name'],$_SESSION['photo'],$_SESSION['photothumb'],$_FILES['photo']['tmp_name'],100);
			$patient->photoUpdate($newname,$patientid);
			$patient_name = $_REQUEST['salutation']." ".$_REQUEST['firstname']." ".$_REQUEST['lastname'];
			echo "<p align=center>Patient <a href=patient.php?patientid=$patientid&pf=1>$patient_name</a> added successfully";
		}
		else
		{
			echo "<p class=err align=center>Patient addition failed.</p>";
		}
	}
	else
	{
		echo "<p class=err align=center>First name required.</p>";
	}
}

?>

<form action=newpatient.php method=post enctype="multipart/form-data" id=addpatient >
<table border=0 cellspacing="10" align=center>
<tr><th align="left" id=title>PERSONAL INFORMATION</th><th align="left" id=title> PARENTS </th></tr>
<tr><td>

<table border="0" cellspacing="10">

	<tr>
		<th align="right">Photograph</th>
		<td align="left" nowrap>
			<label><input type=file name=photo></label>
		</td>
	</tr>
	<tr><th align="right">Salutation</th><td>
		<?php 
		$table = "patient";
		$select = "sel_sal";
		$input = "salutation";
		$sal_arr = $db->getOneCol("select distinct $input from $table where user=$userid order by $input");
		if(count($sal_arr) > 1)
		{
			echo $util->getHtmlSelect($sal_arr,$sal_arr,"$select","$input","onchange=chooseType(\"$select\",\"$input\")"); 
		}
		?>
		<input type=text name=salutation id=salutation size=30>
		</td>
	</tr>
	<tr>
		<th width="139" align="right">First Name</th><td>
			<input type=text name=firstname size=30>
		</td>
	</tr>
	<tr>
		<th width="139" align="right">Last Name</th><td>
			<input type=text name=lastname size=30>
		</td>
	</tr>
	<tr>
		<th width="139" align="right">Middle Name</th><td>
			<input type=text name=middlename size=30>
		</td>
	</tr>
	<tr>
		<th>
		<div align="right">Nick Name</div>
		</th>
		<td><input type=text name=nickname size=30>
		</td>
	</tr>
	<tr>
		<th>
		<div align="right">Sex</div>
		</th>
		<td>
			<input type=radio name=sex value=Male checked>Male
			<input type=radio name=sex value=Female>Female
		</td>
	</tr>
	<tr>
		<th>
		<div align="right">Birthday</div>
		</th>
		<td>
		<?php //echo $util->selectDate("by","bm","bd")?>
		<?php 
			$year = isset($_SESSION['year'])?$_SESSION['year']:"0000";
			$mon = isset($_SESSION['mon'])?$_SESSION['mon']:"0000";
			$day = isset($_SESSION['day'])?$_SESSION['day']:"0000";
			echo $util->selectNewDateNoCalendar("by","bm","bd","100","1",$year."-".$mon."-".$day);?>
		</td>
	</tr>
	<tr>
		<th>
		<div align="right">Address</div>
		</th>
		<td><input type=text name=address size=50>		</td>
	</tr>
	<tr>
		<th>
		<div align="right">Email</div>
		</th>
		<td>
		<input type=text name=email size=50>		
		</td>
	</tr>
	
	<tr>
		<th>
		<div align="right">Telephone Nos</div>
		</th>
		<td><input type=text name=phone size=50></td>
	</tr>
	<tr><th align="right">Status</th><td>
		<?php 
		$table = "patient";
		$select = "sel_stat";
		$input = "status";
		$sal_arr = $db->getOneCol("select distinct $input from $table where user=$userid order by $input");
		if(count($sal_arr) > 1)
		{
			echo $util->getHtmlSelect($sal_arr,$sal_arr,"$select","$input"," style=width:90px onchange=chooseType(\"$select\",\"$input\")"); 
		}
		?>
		<input type=text name=status id=status size=30>
		</td>
	</tr>
	<tr><th align="right">Referred By</th><td>
		<?php 
		$table = "patient";
		$select = "sel_ref";
		$input = "referredby";
		$sal_arr = $db->getOneCol("select distinct $input from $table where user=$userid  order by $input");
		if(count($sal_arr) > 1)
		{
			echo $util->getHtmlSelect($sal_arr,$sal_arr,"$select","$input"," style=width:90px onchange=chooseType(\"$select\",\"$input\")"); 
		}
		?>
		<input type=text name=referredby id=referredby size=30>
		</td>
	</tr>
	</table>


</td><td>

	
	<table border="0" cellspacing="10" align=center>
	
	<tr>
		
	</tr>
	
	<tr>
		<th>
		<div width="139" align="right">Father</div>
		</th>
		<td>
		<input type=text name=father size=50>
		</td>
	</tr>
	<tr>
		<th width="139" align="right">
		Phone Nos
		</th>
		<td>
		<input type=text name=fatherphone size=50>
		</td>
	</tr>
	<tr>
		<th>
		<div width="139" align="right">Mother</div>
		</th>
		<td>
		<input type=text name=mother size=50>
		</td>
	</tr>
	<tr>
		<th align=right>
		Phone Nos
		</th>
		<td>
		<input type=text name=motherphone size=50></td>
	</tr>	
	<tr>
		<th align="right">HMO</th>
		<td>
		<?php 
		$table = "patient";
		$select = "sel_hmo";
		$input = "hmo";
		$sal_arr = $db->getOneCol("select distinct $input from $table where user=$userid  order by $input");
		if(count($sal_arr) > 1)
		{
			echo $util->getHtmlSelect($sal_arr,$sal_arr,"$select","$input","  style=width:90px onchange=chooseType(\"$select\",\"$input\")"); 
		}
		?>
		<input type=text name=hmo id=hmo size=50>
		</td>
	</tr>
	<tr>
		<th align="right">HMO ID</th>
		<td><input type="text" name="hmoid" id="hmoid" size="50"/></td>"
	</tr>
	<tr>
		<th align="right">Company</th>
		<td>
		<?php 
		$table = "patient";
		$select = "sel_co";
		$input = "company";
		$sal_arr = $db->getOneCol("select distinct $input from $table where user=$userid  order by $input");
		if(count($sal_arr) > 1)
		{
			echo $util->getHtmlSelect($sal_arr,$sal_arr,"$select","$input"," style=width:90px onchange=chooseType(\"$select\",\"$input\")"); 
		}
		?>
		<input type=text name=company id=company size=50>
		</td>
	</tr>
	<tr>
		<th align="right">Philhealth</th>
		<td align="left">
		<input type=checkbox name=philhealth value=yes>Yes
		</td>
	</tr>
	<tr>
		<th align="right">Other Info</th>
		<td align="center">
		<textarea name=otherinfo rows=5 cols=50></textarea>
		</td>
	</tr>
	
</table>
</td>
</tr>
<tr><td colspan=2 align=right><input type=submit name=submit value=Submit></td></tr>
</table>
</form>
</div>

<div id=footer_page>
<?php
include("include/bottom.php");
?>
</div>
