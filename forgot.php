<?php 
session_start();

include("classes/connect.php");
include("classes/DBConnect.php");
include("classes/Util.php");
include("classes/Auth.php");
include_once("classes/Register.php");

include("include/top_public.php");

$util = new Util();
$db = new DBConnect();
$reg = new Register();

?>
<!--  
<div id=menu>
<table width="100%" height="20px" style="background-color:#4775ED; border-bottom:thin solid #000000; padding-bottom:3px; padding-top:3px">
<tr>
	<td align=center class=menu colspan="2"><a href=index.php>Home</a>&nbsp;</td>
</tr>
</table>
</div>
-->

<div id=forgot>
<p class=pageTitle> &nbsp;Forgot Password</p>

<?php 
if(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "Submit")
{
	$email = isset($_REQUEST['email']) ? $_REQUEST['email'] : ""; 
	if($util->check_email_address($email) && isset($email))
	{
		$reg->forgot($email);
	}
	else
	{
		echo '<p align=center class=error>Invalid email address.</p>
				<form method=post action=forgot.php>
					<table style=margin-left:auto;margin-right:auto>
						<tr><td>Email</td><td><input type=text size=50 name=email><input type=submit name=submit value=Submit></td></tr>
					</table>
				</form>';
	}
}
else
{
?>
<form method=post action=forgot.php>
	<table style=margin-left:auto;margin-right:auto>
		<tr><th>Email</th><td><input type=text size=50 name=email> <input type=submit name=submit value=Submit></td></tr>
		<!-- <tr><td colspan=2 align=right><input type=submit name=submit value=Submit></td></tr> -->
	</table>
</form>

<?php }

?>
</div>
<div id="footer" style="padding-top:25%;">

<?php include ("include/bottom.php") ?>
</div>
</body>
</html>
