<?php
$url = 'tasks.php';
$db = new DBConnect();
$db->setTable("tasks");
$util = new Util();
$date_time = new DateTime('Asia/Manila');

$userid = $_SESSION['auth']['id'];

$do = isset($_REQUEST['do'])? $_REQUEST['do']: "";

if($do == 'Add')
{
	
	$date_time->setDate($_REQUEST['year'],$_REQUEST['mon'],$_REQUEST['day']);
	if(isset($_REQUEST['ampm']) && $_REQUEST['ampm'] == 'pm')
	{
		if($_REQUEST['h'] != 12)
		{
			$_REQUEST['h'] = $_REQUEST['h'] + 12;
		}
	}
	
	$date_time->setTime($_REQUEST['h'],$_REQUEST['m'],0);
	$_REQUEST['datetime'] =  $date_time->format("Y-m-d H:i:00");
	$_REQUEST['userid'] = $userid;
	
	$db->insertArr($_REQUEST);
	
	
}
else
{
	if(isset($_REQUEST['year']) && isset($_REQUEST['day']) && isset($_REQUEST['mon']))
	{
		$date_time->setDate($_REQUEST['year'],$_REQUEST['mon'],$_REQUEST['day']);
	}
	elseif(isset($_REQUEST['year']) && isset($_REQUEST['mon']))
	{
		$date_time->setDate($_REQUEST['year'],$_REQUEST['mon'],1);
	}
}

if($do == 'del')
{
	
	$db->delete("delete from tasks where id=".$_REQUEST['id']);
}
elseif($do == 'Update')
{
	foreach($_REQUEST['id'] as $x)
	{
		$db->update("update tasks set status='undone' where id=$x");
	}
	if(@$_REQUEST['taskid'])
	{
		foreach($_REQUEST['taskid'] as $r)
		{
			$db->update("update tasks set status='done' where id=$r");
		}
		
	}
	
}

$date_display = $date_time->format("F j, Y");
$date = $date_time->format("Y-m-d");
$d = $date_time->format("w");

$year = $date_time->format("Y");
$mon = $date_time->format("n");
$day = $date_time->format("d");


/*
 * for calendaring
 */
$date_count = new  DateTime('Asia/Manila');
$date_count->setDate($year,$mon,1);


$result = $db->getAllRecord("select * from tasks where date_format(datetime,'%Y-%m-%d')='$date' and userid=$userid order by status='undone',status='done',datetime asc");

$url = 'tasks.php';
?>
<p class=pageTitle> &nbsp;Tasks</p>
<div style="margin-left:150px;">
<!-- <p><b>Date:</b> <?php echo $date_display;?></p>  -->
<table border=0 width=100%;>

<tr>
	<td colspan=3 align=left>
		<!-- <h1>Calendar</h1>  -->
	</td>
</tr>
<tr>
	<td></td>
	<td align=center>

<?php 
#number of dates in a month
$n = $date_count->format("t");
$d = $date_count->format("w");

$fullmonth = $date_time->format("F");

//week of the year
$w = $date_time->format("W");

echo "<p>&nbsp;<br></p><p>";
	echo "<div style=margin-left:; align=center><table border=0 bgcolor=#FDEEF4 cellpadding=15 cellspacing=0>\n";
	echo "<tr><th colspan=7 align=center class=calendar><span class=month_year>$fullmonth&nbsp;$year</span></th></tr>\n";
	echo "<tr bgcolor=><th class=th_g>SUN</th><th class=th_g>MON</th><th class=th_g>TUE</th><th class=th_g>WED</th><th class=th_g>THU</th><th class=th_g>FRI</th><th class=th_g>SAT</th></tr>\n";
	$count = 0;
	for ($i = (1 - $d); $i < $n +1; $i++)
	{

		if ($count % 7 == 0)
		{
			echo "<tr bgcolor=white>";
		}
		if ($i > 0)
		{
			$i < 10 ? $oi = "0".intval($i):	$oi = $i;
			$mon < 10 ? $moni = "0".intval($mon):	$moni = $mon;
			
			$rx = $db->getRecord("select count(id) as count from tasks where userid=$userid and status!='done' and date_format(datetime,'%Y-%m-%d')='$year-$moni-$oi'");	
			
			
			
			$count_not_done = "";
			if($rx['count'] > 0)
			{
				$count_not_done = "[".$rx['count']."]";
			}
			
			if($i == $day)
			{
				echo "<td align=right style='background-color:#f78f1e; border:1px solid #E3E4FA;'><a href='$url?year=$year&mon=$mon&day=$i'><strong>$i</strong></a> <span style=color:red;>".$count_not_done."</span>";
			}
			else
			{
				echo "<td align=right bgcolor=white style='border:1px solid #E3E4FA;'><a href='$url?year=$year&mon=$mon&day=$i' class='number_calendar'>$i</a> <span style=color:red;>".$count_not_done."</span>";
			}
		} 
		else
		{
			echo "<td></td>";
		}
		if ($count % 7 == 6)
		{
			echo "</tr>\n";
		}
		if ($i == $n && $count % 7 < 6)
		{

			while ($count % 7 != 6)
			{
				echo "<td></td>";
				$count++;
			}
			echo "</tr>";

		}
		$count++;
	}

	$ny = $year +1;
	$ly = $year -1;
	$nm = $mon +1;
	$y2d = $year;
	$cyear = $year;
	if ($nm == 13)
	{
		$nm = 1;
		$cyear = $ny;
	}
	$lm = $mon -1;
	if ($lm == 0)
	{
		$lm = 12;
		//$year = $ly;
		$ly = $y2d -1;
	}
	if ($mon == 1)
	{
		$y2d = $y2d -1;
	}

	echo "\n<tr bgcolor=><td colspan=7 align=center>
		<a href=$url?year=$ly&mon=$mon title='Last Year'><img src=image/prevyear_calendar.png border=0 width=40 height=40></a> 
		<a href=$url?mon=$lm&year=$y2d title='Last Month'><img src=image/prev_calendar.png border=0 width=40 height=40></a> 
		<a href=$url title='This Month'><img src=image/now_calendar.png border=0 width=40 height=40></a> 
		<a href=$url?mon=$nm&year=$cyear title='Next Month'><img src=image/next_calendar.png border=0 width=40 height=40></a> 
		<a href=$url?year=$ny&mon=$mon title='Next Year'><img src=image/nextyear_calendar.png border=0 width=40 height=40></a></td></tr>\n";
	echo "\n</table></div>";
	
echo '<a name=add></a>';
if($result)
{
	echo '<form method=post action="'.$url.'?year='.$year.'&mon='.$mon.'&day='.$day.'#add" style=margin-top:30px>';
	echo '<table border=0 style=margin-left:25%;><tr><th colspan=2>Status</th><th>Date</th><th>Remarks</th></tr>';
	$count = 1;
	foreach($result as $r)
	{
		echo '<input type=hidden name=id[] value="'.$r['id'].'">';
		echo '<tr><td>'.$count.'.</td>';
		if($r['status'] == 'done')
		{
			echo '<td class=tdcl><input type=checkbox name=taskid[] value='.$r['id'].' checked></td>';
		}
		else
		{
			echo '<td class=tdcl><input type=checkbox name=taskid[] value='.$r['id'].'></td>';
		} 
		echo '<td class=tdl>'.$util->setDateTime($r['datetime'],"M j, Y h:i A").'</td>';
			echo '<td  class=tdl>'.$r['remarks'].'</td>';
		echo '<td>'.$util->setEdel('',$url.'?do=del&id='.$r['id'].'&year='.$year.'&mon='.$mon.'&day='.$day).'</td></tr>';
		$count++;
	}
	echo '<tr><td colspan=5 align=right><br><input type=submit name=do value=Update id=update></td>';
	echo '</table>';
	echo '</form>';
	
}
//echo '<a href=tasks.php?q=1 align=left>All Tasks</a>';
?>

</td>
<td></td>
</tr>
<tr>
	<td>
		<br>
	</td>
</tr>
<tr>
	<td colspan=3>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href=tasks.php?q=1>All Tasks</a>
	</td>
</tr>
<tr>
	<td>
		<br><br><br>
	</td>
</tr>
<tr>
	<td colspan=3>
<p class=pageTitle2> &nbsp;Add New Task</p>
<form  action='<?php echo $url;?>#add' method='post'>
<table align=center>
	<tr>
		<th>Date</th>
		<td><?php echo $util->selectNewDate("mon","day","year",$date)?></td>
	</tr>
	<tr>
		<th>Time</th>
		<td><?php echo $util->selectTime("12","h","m","ampm","")?></td>
	</tr>
	<tr>
		<th>Remarks</th>
		<td>
		<?php 
		$table = "tasks";
		$col = "remarks";
		$sql = "select  distinct `$col` from $table where userid=$userid and remarks!='' order by `$col`";
		$sal_arr = $db->getOneCol($sql);
		if(count($sal_arr) > 0)
			echo $util->getHtmlSelect($sal_arr,$sal_arr, "sel_$col","$col","style=width:90px onchange=chooseType(\"sel_$col\",\"$col\")")."<br>";
		?>
		<textarea name=remarks id=remarks cols=50 rows=2></textarea></td>
	</tr>
	<tr><td colspan=2 align="right"><br><input type=reset>&nbsp;<input type=submit name=do value=Add id=add></td></tr>
</table>
</form>
</td>
</tr>
</table>
</div>		