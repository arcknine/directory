<?php
session_start();
require_once("../classes/connect.php");
require_once("../classes/Util.php");
require_once("../classes/DBConnect.php");

$userid = $_SESSION['auth']['userid'];
$db = new DBConnect();

$sql = "select distinct icd.id as id,code,disease from icdselect left join icd on icd.id=icdselect.icdid where code != '' and  disease != '' and icdselect.userid=$userid order by code";
$rst = $db->getAllRecord($sql);

$text = "";

if($rst)
{
	$arr = array();
	foreach($rst as $row)
	{
		$arr[] = $row['code'].": ".$row['disease']; 
	}
	$text = implode("/",$arr);
}
echo $text;

?>