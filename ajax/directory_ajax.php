<?php
session_start();
require_once("../directory/connect.php");
require_once("../classes/Util.php");
require_once("../classes/DBConnect.php");

$db = new DBConnect();

if(isset($_REQUEST['do']) && $_REQUEST['do'] == "city_region")
{
	$city = trim($_REQUEST['city']);
	$sql = "SELECT distinct region FROM `hospital` where city='$city'";
	$rst = $db->getAllRecord($sql);
	
	$x = array();
	foreach($rst as $row)
	{
		$x[] = $row['region'];
	}
	sort($x);
	echo implode("_",$x);
}
if(isset($_REQUEST['do']) && $_REQUEST['do'] == "city_province")
{
	$city = trim($_REQUEST['city']);
	$sql = "SELECT distinct province FROM `hospital` where city='$city'";
	$rst = $db->getAllRecord($sql);
	
	$x = array();
	foreach($rst as $row)
	{
		$x[] = $row['province'];
	}
	sort($x);
	echo implode("_",$x);
}
elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "province_region")
{
	$province = trim($_REQUEST['province']);
	$sql = "SELECT distinct region FROM `hospital` where province='$province'";
	$rst = $db->getAllRecord($sql);
	
	$x = array();
	foreach($rst as $row)
	{
		$x[] = $row['region'];
	}
	sort($x);
	echo implode("_",$x);
}
elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "province_city")
{
	$province = trim($_REQUEST['province']);
	$sql = "SELECT distinct city FROM `hospital` where province='$province'";
	$rst = $db->getAllRecord($sql);
	
	$x = array();
	foreach($rst as $row)
	{
		$x[] = $row['city'];
	}
	sort($x);
	echo implode("_",$x);
}
elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "region_city")
{
	$region = trim($_REQUEST['region']);
	$sql = "SELECT distinct city FROM `hospital` where region='".$_REQUEST['region']."'";
	$rst = $db->getAllRecord($sql);
	
	$x = array();
	foreach($rst as $row)
	{
		$x[] = $row['city'];
	}
	sort($x);
	echo implode("_",$x);
}
elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == "region_province")
{
	$region = trim($_REQUEST['region']);
	$sql = "SELECT distinct province FROM `hospital` where region='".$_REQUEST['region']."'";
	$rst = $db->getAllRecord($sql);
	
	$x = array();
	foreach($rst as $row)
	{
		$x[] = $row['province'];
	}
	sort($x);
	echo implode("_",$x);
}

?>