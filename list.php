<?php
	session_start();
	
	$auth_arr = $_SESSION['auth'];
	$userid = $auth_arr['userid']; 
	
	
	$title = "Patient Listing";	

	include("classes/connect.php");
	include("classes/DBConnect.php");
	include("classes/Auth.php");
	include("classes/Util.php");
	include("classes/Patient.php");
	include("classes/Consult.php");
	include("classes/Bill.php");
	
	include("include/top.php");
	include("include/menu.php");
	
	$db = new DBConnect();
	$util = new Util();
	$patient = new Patient();
	$msg = "";
?>

<div id=list align=center>

<table class=pageTitle3 width=85% align=center>
<tr>
<td>
&nbsp;Patient Listing
</td>
<td align=right>
		<div id="searchwrapper">
			<form action="list.php">
				<input type="text" class="searchbox" value="" name=search id=search placeholder="Search" />
				<input type="submit" src="image/searchicon.png" class="searchbox_submit" name=do value="Search" />	
			</form>
		</div>
</td>
<td>&nbsp;
</td>	
	</tr>
</table>
<!-- SEARCH -->
<!-- 
<table><tr><td>
<form action=list.php method=post style=padding-top:15px;padding-below:15px;><input type=text name=search id=search size=30/>
<input type=submit name=do value=Search></form>
</td></tr>
</table>
-->

<br><br>
<?php
//==NAVIGATION
	$start = 0;
	if(isset($_REQUEST['start']))
	{
		$start = $_REQUEST['start'];
	}
	$rowstoview = 10;
	if(isset($_REQUEST['rowstoview']))
	{
		$rowstoview = $_REQUEST['rowstoview'];
	}
	
	/*
	 * === PERFORM FORM SUBMISSIONS
	 */
	 
	if($auth_arr['access'] == "admin" || $auth_arr['access'] == "doctor")
	{
	 if(isset($_REQUEST['do']) && $_REQUEST['do'] == "delete")
	 {
	 	
	 	if($patient->delete($_REQUEST['patientid'],$_SESSION['photo'],$_SESSION['photothumb']) == 1) //successful
	 	{
	 		$msg = "<p>".$patient->msg;
	 	}
	 	else
	 	{
	 		$msg = "<p class=err>".$patient->msg;
	 	}
	 }
	}
	
	//==SQL STATEMENTS
	$searchTag = "";
	$recordCount = 0;
	if(isset($_REQUEST['do']) &&  $_REQUEST['do'] == "Search" && isset($_REQUEST['search']))
	{
		
		if(strlen($_REQUEST['search']) < 4)
		{
			$sql = "select * from patient  where (firstname like '".$_REQUEST['search']."%'" .
				"or lastname like '".$_REQUEST['search']."%' or middlename like '".$_REQUEST['search']."%')";
		}
		else
		{
			$sql = "select * from patient  where match(firstname,lastname,middlename) against('".$_REQUEST['search']."*' IN BOOLEAN MODE) ";
		}
		$sql.= "  and userid= ".$userid." ";
		
		$db->getAllRecord($sql);
		$sql .= " order by lastname limit $start, $rowstoview";
		$searchTag = "&do=Search&search=".$_REQUEST['search'];
		
	}
	else
	{
		$sql = "select * from patient where userid= ".$userid." order by lastname limit $start, $rowstoview";
		$db->getAllRecord("select id from patient  where userid= ".$userid."  order by lastname");
		
	}
	$recordCount = $db->recordCount;
	$result = $db->getAllRecord($sql);
	
	$count = $start + 1; 
	
	echo "$msg";
?>



<?php if($recordCount > 0){?>
	<form>
	<table cellpadding="7" cellspacing="5" class="patient_list" width=50% align=center>
		<tr><th colspan=2 align=center>Name</th><th>Contact Info</th><th></th></tr>
		
		<?php
		foreach($result as $arr)
		{
		?>
		
		<tr><td align=right><?php echo $count?>.</td><td align=Left><a href="patient.php?pf=6&patientid=<?php echo $arr['id']?>">
			<?php 
			if(file_exists($_SESSION['photothumb']."/".$arr['photo']) && $arr['photo'] != '')
			{
			?>
				<img src="<?php echo $_SESSION['urlthumb'] ?>/<?php echo $arr['photo']?>" border=0><br>
			<?php }?>
			<?php echo $arr['firstname']?>
				<?php 
					if($arr['middlename'])
					{				
						echo substr($arr['middlename'],0,1).".";
					}
				?> 
			<?php echo $arr['lastname']?></a></td><td>
			<?php
			$dum = array();
			if(isset($_REQUEST['workphone']) && $arr['workphone'] != "")
			{
				$dum[] = "Work Phone: ".$arr['workphone'];
			}
			if(count($dum)>0)
			{
				print implode("<br>",$dum);
			}
			?>
			</td>
		<td>
		<a href="patient.php?pf=3&patientid=<?php echo $arr['id']?>" <?php echo $util->mouseOver("Set Appointment","100")?>><img src="image/appt.png" border=0 width='40' height='40'></a>
		<?php 
			if($auth_arr['access'] == "admin" || $auth_arr['access'] == "doctor")
			{
				echo $util->setEdel("patient.php?pf=1&patientid=".$arr['id'].$searchTag,"list.php?do=delete&patientid=".$arr['id'].$searchTag);
				
			}
			else
			{
				echo $util->setEdel("patient.php?pf=1&patientid=".$arr['id'].$searchTag,"");
			} 
			$count++;
		}?>
		</td></tr>
	</table>
	</form>
	<table style="margin-left:auto;margin-right:auto">
		<tr>
		<td colspan=2>
		<?php echo $util->navi("list.php?".$searchTag,$start,$rowstoview,$recordCount,"image");?>
		</td>
		</tr>
	</table>
<?php 
	
}
else
{
	echo "<p align=center>No records found.</p>";	
} 

?>
</div>

<div id=footer_page>
<?php
include("include/bottom.php");
?>
</div>
