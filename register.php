<?php 
session_start();

include("classes/connect.php");
include("classes/DBConnect.php");
include("classes/Util.php");
include("classes/Auth.php");
include_once("classes/Register.php");

include("include/top_public.php");

$util = new Util();
$db = new DBConnect();
$reg = new Register();

?>

<!-- 
<div id=menu>
<table width="100%" height="20px" style="background-color:#4775ED; border-bottom:thin solid #000000; padding-bottom:3px; padding-top:3px">
<tr>
	<td align=center class=menu colspan="2"><a href=index.php>Home</a>&nbsp;</td>
</tr>
</table>
</div>
-->
<div id="register">
<p class=pageTitle> &nbsp;Registration Page</p>


<?php 
if(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "Next")
{
	$_SESSION['reg_values'] = $_REQUEST;
	
	?>
	<form action=register.php method=post>
	<div style="width:600px;height:300px; overflow:auto; margin-right:auto; margin-left:auto">
		<table style=margin-left:auto;margin-right:auto>
			<tr><td>
			<h2>Privacy Policy</h2>
			<h3>About Medriks and the Information We Collect</h3>
			
			<p><strong>Profile Information submitted by Members to Medriks.</strong></p>
			<p>Medriks is a  medical records filing system. When you sign up for Medriks you provide us the following: 
				<ul>
					<li>Name of users (doctors, staff or secretary)</li>
					<li>Patients’ info (such as names, gender, address, and other personal information)</li>
					<li>Your specialization as a doctor</li>
					<li>Location, contact numbers (e.g., city and clinic address), and</li>
					<li>Photos of Patients’, laboratory results, uploaded by the Member to his or her patients’ profile, to the extent that it includes information.</li>					
				</ul>


 
 

<p>Medriks also collects Member-submitted account information such as name and email address to identify Members, send notifications related to the use of the Medriks service and conduct market research and marketing for Medriks internal use only. 
We also collect some information from Members, as well as from other visitors to the Medriks website, that is not personally identifiable, such as browser type and IP address. This information is gathered from all Members and visitors to the website.
We always appreciate your feedback or other suggestions about Medriks, but you understand that we may use them without any obligation to compensate you for them (just as you have no obligation to offer them).
</p>

<p><strong>Changes in Our Privacy Policy</strong></p>

<p>If we change our privacy policy, we will post those changes on our web site so Members and other users of the Medriks are always aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it. If we are going to use Members' or other users' personally identifiable information in a manner different from that stated at the time of collection, we will notify those Members and users via email or by placing a prominent notice on our website.
</p>
 
<p><strong>Security</strong></p>
<p>
The Medriks account of every Member is password-protected. Medriks takes every precaution to protect the information of the Members, as well as information collected from other users of the Medriks system. We use industry standard measures to protect all information that is stored on our servers and within our database. We limit the access to this information to those employees who need access to perform their job function such as our customer service personnel. If you have any questions about the security at our website, please contact us.
</p>

<p><strong>Contacting Medriks</strong></p>
<p>
If you have any questions about this privacy policy, Medriks’ privacy practices, or your dealings with Medriks, please contact as at support[at]medriks[dot]com.
</p>			
			</td></tr>
			
		</table>
		
	
	
	</div>
	<table style=margin-left:auto;margin-right:auto>
	<tr><td><input type=submit name=submit value=Agree> <input type=submit name=submit value=Disagree></td></tr>
		</table>
	</form>	
	<?php 
}
elseif(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "Agree")
{
	
	
	$email = isset($_SESSION['reg_values']['user']) ? $_SESSION['reg_values']['user'] : ""; 

	if($util->check_email_address($email) && isset($email))
	{
		
		if($reg->newUser($_SESSION['reg_values']) == "accepted")
		{

			echo $reg->instruction;
		}
		elseif($reg->newUser($_SESSION['reg_values']) == "duplicate")
		{
			echo '<p>User is already registered.</p>';
		}  
		elseif($reg->newUser($_SESSION['reg_values']) == "rejected")
		{
			echo '<p>Registration failed.</p>';
		}
		else
		{
			echo '<p>Registration failed.</p>';
		}
	
	}
	else
	{
		echo '<p>Invalid email address for user name. Please use a valid email address.</p>';
	}
	
	echo '<p>Back to the <a href=register.php>start.</a></p>';
}
elseif(isset($_REQUEST['submit']) && $_REQUEST['submit'] == "Disagree")
{
	echo '<p>Back to the <a href=register.php>start.</a></p>';
}
else
{
	if(isset($_SESSION['reg_values']))
	{
		unset($_SESSION['reg_values']);
	}
?>
	<script language=javascript>
	
		function ValidateForm()
		{
			checkSum = 0;
			msg = "";
			if(document.getElementById("user").value == "")
			{
				checkSum += 1;
				msg = msg + "User name is missing.\n";
			}
			if(document.getElementById("name").value == "")
			{
				checkSum += 1;
				msg = msg + "Name is missing.\n";
			}
			if(document.getElementById("specialty").value == "")
			{
				checkSum += 1;
				msg = msg + "Specialty is missing.\n";
			}
				
			if(checkSum > 0)
			{
				alert(msg);
				return false;
			}
			else
			{
				return true;
			}
			
		}
	</script>
	<form action=register.php method=post onsubmit="javascript:return ValidateForm(this)" >
		<table style=margin-left:auto;margin-right:auto>
			<tr><th>User Name</th><td><input type=text name=user id=user value="" style=width:300px> (E-mail address)</td></tr>
			<tr><th>Name</th><td><input type=text name=name id=name value="" style=width:300px></td></tr>
			<tr><th>Country</th><td>
				<select name="country" style=width:305px> 
					<option value="Afghanistan">Afghanistan</option> 
					<option value="Albania">Albania</option> 
					<option value="Algeria">Algeria</option> 
					<option value="American Samoa">American Samoa</option> 
					<option value="Andorra">Andorra</option> 
					<option value="Angola">Angola</option> 
					<option value="Anguilla">Anguilla</option> 
					<option value="Antarctica">Antarctica</option> 
					<option value="Antigua and Barbuda">Antigua and Barbuda</option> 
					<option value="Argentina">Argentina</option> 
					<option value="Armenia">Armenia</option> 
					<option value="Aruba">Aruba</option> 
					<option value="Australia">Australia</option> 
					<option value="Austria">Austria</option> 
					<option value="Azerbaijan">Azerbaijan</option> 
					<option value="Bahamas">Bahamas</option> 
					<option value="Bahrain">Bahrain</option> 
					<option value="Bangladesh">Bangladesh</option> 
					<option value="Barbados">Barbados</option> 
					<option value="Belarus">Belarus</option> 
					<option value="Belgium">Belgium</option> 
					<option value="Belize">Belize</option> 
					<option value="Benin">Benin</option> 
					<option value="Bermuda">Bermuda</option> 
					<option value="Bhutan">Bhutan</option> 
					<option value="Bolivia">Bolivia</option> 
					<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option> 
					<option value="Botswana">Botswana</option> 
					<option value="Bouvet Island">Bouvet Island</option> 
					<option value="Brazil">Brazil</option> 
					<option value="British Indian Ocean Territory">British Indian Ocean Territory</option> 
					<option value="Brunei Darussalam">Brunei Darussalam</option> 
					<option value="Bulgaria">Bulgaria</option> 
					<option value="Burkina Faso">Burkina Faso</option> 
					<option value="Burundi">Burundi</option> 
					<option value="Cambodia">Cambodia</option> 
					<option value="Cameroon">Cameroon</option> 
					<option value="Canada">Canada</option> 
					<option value="Cape Verde">Cape Verde</option> 
					<option value="Cayman Islands">Cayman Islands</option> 
					<option value="Central African Republic">Central African Republic</option> 
					<option value="Chad">Chad</option> 
					<option value="Chile">Chile</option> 
					<option value="China">China</option> 
					<option value="Christmas Island">Christmas Island</option> 
					<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option> 
					<option value="Colombia">Colombia</option> 
					<option value="Comoros">Comoros</option> 
					<option value="Congo">Congo</option> 
					<option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option> 
					<option value="Cook Islands">Cook Islands</option> 
					<option value="Costa Rica">Costa Rica</option> 
					<option value="Cote D'ivoire">Cote D'ivoire</option> 
					<option value="Croatia">Croatia</option> 
					<option value="Cuba">Cuba</option> 
					<option value="Cyprus">Cyprus</option> 
					<option value="Czech Republic">Czech Republic</option> 
					<option value="Denmark">Denmark</option> 
					<option value="Djibouti">Djibouti</option> 
					<option value="Dominica">Dominica</option> 
					<option value="Dominican Republic">Dominican Republic</option> 
					<option value="Ecuador">Ecuador</option> 
					<option value="Egypt">Egypt</option> 
					<option value="El Salvador">El Salvador</option> 
					<option value="Equatorial Guinea">Equatorial Guinea</option> 
					<option value="Eritrea">Eritrea</option> 
					<option value="Estonia">Estonia</option> 
					<option value="Ethiopia">Ethiopia</option> 
					<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option> 
					<option value="Faroe Islands">Faroe Islands</option> 
					<option value="Fiji">Fiji</option> 
					<option value="Finland">Finland</option> 
					<option value="France">France</option> 
					<option value="French Guiana">French Guiana</option> 
					<option value="French Polynesia">French Polynesia</option> 
					<option value="French Southern Territories">French Southern Territories</option> 
					<option value="Gabon">Gabon</option> 
					<option value="Gambia">Gambia</option> 
					<option value="Georgia">Georgia</option> 
					<option value="Germany">Germany</option> 
					<option value="Ghana">Ghana</option> 
					<option value="Gibraltar">Gibraltar</option> 
					<option value="Greece">Greece</option> 
					<option value="Greenland">Greenland</option> 
					<option value="Grenada">Grenada</option> 
					<option value="Guadeloupe">Guadeloupe</option> 
					<option value="Guam">Guam</option> 
					<option value="Guatemala">Guatemala</option> 
					<option value="Guinea">Guinea</option> 
					<option value="Guinea-bissau">Guinea-bissau</option> 
					<option value="Guyana">Guyana</option> 
					<option value="Haiti">Haiti</option> 
					<option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option> 
					<option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option> 
					<option value="Honduras">Honduras</option> 
					<option value="Hong Kong">Hong Kong</option> 
					<option value="Hungary">Hungary</option> 
					<option value="Iceland">Iceland</option> 
					<option value="India">India</option> 
					<option value="Indonesia">Indonesia</option> 
					<option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option> 
					<option value="Iraq">Iraq</option> 
					<option value="Ireland">Ireland</option> 
					<option value="Israel">Israel</option> 
					<option value="Italy">Italy</option> 
					<option value="Jamaica">Jamaica</option> 
					<option value="Japan">Japan</option> 
					<option value="Jordan">Jordan</option> 
					<option value="Kazakhstan">Kazakhstan</option> 
					<option value="Kenya">Kenya</option> 
					<option value="Kiribati">Kiribati</option> 
					<option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option> 
					<option value="Korea, Republic of">Korea, Republic of</option> 
					<option value="Kuwait">Kuwait</option> 
					<option value="Kyrgyzstan">Kyrgyzstan</option> 
					<option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option> 
					<option value="Latvia">Latvia</option> 
					<option value="Lebanon">Lebanon</option> 
					<option value="Lesotho">Lesotho</option> 
					<option value="Liberia">Liberia</option> 
					<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option> 
					<option value="Liechtenstein">Liechtenstein</option> 
					<option value="Lithuania">Lithuania</option> 
					<option value="Luxembourg">Luxembourg</option> 
					<option value="Macao">Macao</option> 
					<option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option> 
					<option value="Madagascar">Madagascar</option> 
					<option value="Malawi">Malawi</option> 
					<option value="Malaysia">Malaysia</option> 
					<option value="Maldives">Maldives</option> 
					<option value="Mali">Mali</option> 
					<option value="Malta">Malta</option> 
					<option value="Marshall Islands">Marshall Islands</option> 
					<option value="Martinique">Martinique</option> 
					<option value="Mauritania">Mauritania</option> 
					<option value="Mauritius">Mauritius</option> 
					<option value="Mayotte">Mayotte</option> 
					<option value="Mexico">Mexico</option> 
					<option value="Micronesia, Federated States of">Micronesia, Federated States of</option> 
					<option value="Moldova, Republic of">Moldova, Republic of</option> 
					<option value="Monaco">Monaco</option> 
					<option value="Mongolia">Mongolia</option> 
					<option value="Montserrat">Montserrat</option> 
					<option value="Morocco">Morocco</option> 
					<option value="Mozambique">Mozambique</option> 
					<option value="Myanmar">Myanmar</option> 
					<option value="Namibia">Namibia</option> 
					<option value="Nauru">Nauru</option> 
					<option value="Nepal">Nepal</option> 
					<option value="Netherlands">Netherlands</option> 
					<option value="Netherlands Antilles">Netherlands Antilles</option> 
					<option value="New Caledonia">New Caledonia</option> 
					<option value="New Zealand">New Zealand</option> 
					<option value="Nicaragua">Nicaragua</option> 
					<option value="Niger">Niger</option> 
					<option value="Nigeria">Nigeria</option> 
					<option value="Niue">Niue</option> 
					<option value="Norfolk Island">Norfolk Island</option> 
					<option value="Northern Mariana Islands">Northern Mariana Islands</option> 
					<option value="Norway">Norway</option> 
					<option value="Oman">Oman</option> 
					<option value="Pakistan">Pakistan</option> 
					<option value="Palau">Palau</option> 
					<option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option> 
					<option value="Panama">Panama</option> 
					<option value="Papua New Guinea">Papua New Guinea</option> 
					<option value="Paraguay">Paraguay</option> 
					<option value="Peru">Peru</option> 
					<option value="Philippines" selected>Philippines</option> 
					<option value="Pitcairn">Pitcairn</option> 
					<option value="Poland">Poland</option> 
					<option value="Portugal">Portugal</option> 
					<option value="Puerto Rico">Puerto Rico</option> 
					<option value="Qatar">Qatar</option> 
					<option value="Reunion">Reunion</option> 
					<option value="Romania">Romania</option> 
					<option value="Russian Federation">Russian Federation</option> 
					<option value="Rwanda">Rwanda</option> 
					<option value="Saint Helena">Saint Helena</option> 
					<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
					<option value="Saint Lucia">Saint Lucia</option> 
					<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option> 
					<option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option> 
					<option value="Samoa">Samoa</option> 
					<option value="San Marino">San Marino</option> 
					<option value="Sao Tome and Principe">Sao Tome and Principe</option> 
					<option value="Saudi Arabia">Saudi Arabia</option> 
					<option value="Senegal">Senegal</option> 
					<option value="Serbia and Montenegro">Serbia and Montenegro</option> 
					<option value="Seychelles">Seychelles</option> 
					<option value="Sierra Leone">Sierra Leone</option> 
					<option value="Singapore">Singapore</option> 
					<option value="Slovakia">Slovakia</option> 
					<option value="Slovenia">Slovenia</option> 
					<option value="Solomon Islands">Solomon Islands</option> 
					<option value="Somalia">Somalia</option> 
					<option value="South Africa">South Africa</option> 
					<option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option> 
					<option value="Spain">Spain</option> 
					<option value="Sri Lanka">Sri Lanka</option> 
					<option value="Sudan">Sudan</option> 
					<option value="Suriname">Suriname</option> 
					<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option> 
					<option value="Swaziland">Swaziland</option> 
					<option value="Sweden">Sweden</option> 
					<option value="Switzerland">Switzerland</option> 
					<option value="Syrian Arab Republic">Syrian Arab Republic</option> 
					<option value="Taiwan, Province of China">Taiwan, Province of China</option> 
					<option value="Tajikistan">Tajikistan</option> 
					<option value="Tanzania, United Republic of">Tanzania, United Republic of</option> 
					<option value="Thailand">Thailand</option> 
					<option value="Timor-leste">Timor-leste</option> 
					<option value="Togo">Togo</option> 
					<option value="Tokelau">Tokelau</option> 
					<option value="Tonga">Tonga</option> 
					<option value="Trinidad and Tobago">Trinidad and Tobago</option> 
					<option value="Tunisia">Tunisia</option> 
					<option value="Turkey">Turkey</option> 
					<option value="Turkmenistan">Turkmenistan</option> 
					<option value="Turks and Caicos Islands">Turks and Caicos Islands</option> 
					<option value="Tuvalu">Tuvalu</option> 
					<option value="Uganda">Uganda</option> 
					<option value="Ukraine">Ukraine</option> 
					<option value="United Arab Emirates">United Arab Emirates</option> 
					<option value="United Kingdom">United Kingdom</option> 
					<option value="United States">United States</option> 
					<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option> 
					<option value="Uruguay">Uruguay</option> 
					<option value="Uzbekistan">Uzbekistan</option> 
					<option value="Vanuatu">Vanuatu</option> 
					<option value="Venezuela">Venezuela</option> 
					<option value="Viet Nam">Viet Nam</option> 
					<option value="Virgin Islands, British">Virgin Islands, British</option> 
					<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option> 
					<option value="Wallis and Futuna">Wallis and Futuna</option> 
					<option value="Western Sahara">Western Sahara</option> 
					<option value="Yemen">Yemen</option> 
					<option value="Zambia">Zambia</option> 
					<option value="Zimbabwe">Zimbabwe</option>
					</select>
				
			</td></tr>
			<tr><th style=vertical-align:top>Specialty</th><td>
				<?php 
				$table = "users";
				$select = "sel_specialty";
				$input = "specialty";
				$sal_arr = $db->getOneCol("select distinct $input from $table where $input!='' order by $input");
				if(count($sal_arr) > 1)
				{
					$dum = array();
					foreach($sal_arr as $r)
					{
						$dum[] = ucwords($r);	
					}
					$sal_arr = $dum;
					echo $util->getHtmlSelect($sal_arr,$sal_arr,"$select","$input","onchange=chooseType(\"$select\",\"$input\")");
					echo '<br>'; 
				}
				?>
				<input type=text name=specialty id=specialty value="" style=width:300px></td>
				</tr>
			<tr><th>Contact Nos</th><td><input type=text name=contactnos value="" style=width:300px>
			
			</td></tr>
			<tr><th style=vertical-align:top>Clinic Address</th>
				<td><textarea cols=70 rows=4 name=clinic id=clinic></textarea></td></tr>
			<tr><td colspan=2 align=right><input type=reset class=reset> <input type=submit name=submit value=Next class=next></td></tr>
		</table>
	</form>

<?php 
}
?>

<p align=center><br>Note: Having a hard time to register? Email support@medriks.com for assistance. Thank you</p>
</div>

<div id="footer" style="padding-top:25%;">

<?php include ("include/bottom.php") ?>
</div>
</body>
</html>
